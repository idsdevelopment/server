﻿namespace TransferRecords;

public static partial class Extensions
{
}

public record AccountTransferRecord( string Program,
                                     string CarrierId,
                                     string CompanyCode,
                                     string LoginCode,
                                     string UserName,
                                     string Password,
                                     CompanyTransferRecord Company,
                                     CompanyTransferRecord BillingCompany,
                                     CompanyTransferRecord ShippingCompany )
{
	public AccountTransferRecord( string carrierId, AddUpdatePrimaryCompany p ) : this( p.ProgramName,
	                                                                                    carrierId,
	                                                                                    p.CustomerCode,
	                                                                                    p.SuggestedLoginCode,
	                                                                                    p.UserName,
	                                                                                    p.Password,
	                                                                                    p.Company.ToTransferRecord(),
	                                                                                    p.BillingCompany.ToTransferRecord(),
	                                                                                    p.ShippingCompany.ToTransferRecord() )
	{
	}
}