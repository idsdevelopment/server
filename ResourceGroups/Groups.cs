﻿namespace ResourceGroups;

public static class Groups
{
	public const string DATABASE_RESOURCE_GROUP = "IdsRoute",
	                    BACKUP_RESOURCE_GROUP   = "Backups";
}