﻿using System;
using Azure_Web_Service;
using Interfaces.Interfaces;
using Utils;

namespace ResourceGroups;

public static partial class Resource
{
	public static bool CreateResourceGroup( IRequestContext context, string groupName )
	{
		try
		{
			var Sub = Subscription.AzureAuthenticate( context );

			Sub.ResourceGroups
			   .Define( groupName )
			   .WithRegion( Subscription.DEFAULT_REGION )
			   .Create();

			return true;
		}
		catch( Exception E )
		{
			context.SystemLogException( E );
		}

		return false;
	}

	public static ( string GroupName, bool Ok ) CreateResourceGroup( IRequestContext context )
	{
		var Cid = context.CarrierId.ToIdent();

		return ( GroupName: Cid, Ok: CreateResourceGroup( context, Cid ) );
	}
}