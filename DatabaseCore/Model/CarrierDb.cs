﻿namespace Database.Model.Databases.Carrier;

public partial class CarrierDb : ContextDb<MasterTemplateContext>
{
	private const int CURRENT_VERSION = 0;
	public        int CurrentVersion => CURRENT_VERSION;

	public CarrierDb( IRequestContext requestContext ) : base( requestContext, builder => new MasterTemplateContext( builder.Options ) )
	{
	}
}