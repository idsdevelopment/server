﻿namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	private const string PROGRAM  = "Auto Clean-up old LIMBO shipments";
	private const int    DAYS_OLD = 90;

	public void DeletedTripCleanup( string program, DateTime utcLimboDateTime, DateTime utcLimboDeleteDateTime )
	{
		try
		{
			var MaxDate = utcLimboDateTime > utcLimboDeleteDateTime ? utcLimboDateTime : utcLimboDeleteDateTime;

			var DbTrips = ( from T in Entity.Trips
			                where ( ( T.LastModified <= MaxDate ) && ( T.Status1 == (int)STATUS.LIMBO ) ) || ( T.Status1 == (int)STATUS.DELETED )
			                select new { T.LastModified, T.TripId, T.Status1, T.Status2 } ).ToList();

			foreach( var Trip in DbTrips )
			{
				var Status  = (STATUS)Trip.Status1;
				var Status1 = (STATUS1)Trip.Status2;

				switch( Status )
				{
				case STATUS.DELETED:
				case STATUS.LIMBO when ( ( Status1 & STATUS1.WAS_DELETED ) != 0 ) && ( Trip.LastModified <= utcLimboDeleteDateTime ):
					DeleteTripDontExport( program, Trip.TripId );
					break;

				case STATUS.LIMBO when Trip.LastModified <= utcLimboDateTime:
					FinaliseTripDontExport( program, Trip.TripId );
					break;
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogExceptionProgram( program, Exception );
		}
	}

	public static void ProcessLimbo()
	{
		var CleanupDate = DateTime.UtcNow.AddDays( -DAYS_OLD );

		foreach( var Context in Database.Users.RequestContexts() )
		{
			using var Db = new CarrierDb( Context );
			Db.DeletedTripCleanup( PROGRAM, CleanupDate, CleanupDate );
		}
	}
}