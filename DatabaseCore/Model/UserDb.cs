﻿namespace Database.Model.Database;

public sealed class UsersEntities() : ContextDb<UsersContext>( USERS_DATABASE, builder => new UsersContext( builder.Options ) )
{
	private const string USERS_DATABASE = "Users";
}