﻿namespace Database.Model.Database;

public static partial class Users
{
	public static string? GetCarrierPassword( IRequestContext context, string code ) => Employee( context, code )?.Password;

	public static Employee? Employee( IRequestContext context, string code )
	{
		try
		{
			using var Db = new UsersEntities();

			code = code.ToLower();
		#if !NETCOREAPP
			var Employees = Db.Employees;
		#else
			var Employees = Db.Entity.Employees;
		#endif

			return ( from E in Employees
			         where E.Code.ToLower() == code
			         select E ).FirstOrDefault();
		}
		catch( Exception E )
		{
			context.SystemLogException( E );
		}

		return null;
	}
}