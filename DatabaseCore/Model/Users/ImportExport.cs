﻿namespace Database.Model.Database;

using LOGIN_INFO = (string CarrierId, string UserName, string Password, bool ImportEnabled, bool ExportEnabled);
using RUN_TIMES = (DateTime LastRun, DateTime NextRun);

public partial class Users
{
	public class ImportExport
	{
		public static void UpdatePassword( string carrierId, string password )
		{
			using var Db  = new UsersEntities();
			var       Rec = GetImportExportRecordOrNew( Db, ref carrierId );
			Rec.Password = password;
			Db.Entity.SaveChanges();
		}

		public static void UpdateUsername( string carrierId, string username )
		{
			using var Db  = new UsersEntities();
			var       Rec = GetImportExportRecordOrNew( Db, ref carrierId );
			Rec.UserName = username;
			Db.Entity.SaveChanges();
		}


		public static void EnableCarrierImport( string carrierId, bool enable )
		{
			using var Db = new UsersEntities();
			var       E  = Db.Entity;

			var Rec = GetImportExportRecordOrNew( Db, ref carrierId );
			Rec.ImportEnabled = enable;
			E.SaveChanges();
		}


		public static void EnableCarrierExport( string carrierId, bool enable )
		{
			using var Db = new UsersEntities();
			var       E  = Db.Entity;

			var Rec = GetImportExportRecordOrNew( Db, ref carrierId );
			Rec.ExportEnabled = enable;
			E.SaveChanges();
		}

		public static void UpdateRunTimes( string carrierId, DateTime utcNextRun, DateTime utcLastRun )
		{
			using var Db  = new UsersEntities();
			var       Rec = GetImportExportRecord( Db, ref carrierId );

			if( Rec is not null )
			{
				Rec.LastRun = utcLastRun;
				Rec.NextRun = utcNextRun;
				Db.Entity.SaveChanges();
			}
		}

		public static RUN_TIMES GetRunTimes( string carrierId )
		{
			using var Db  = new UsersEntities();
			var       Rec = GetImportExportRecord( Db, ref carrierId );

			return Rec is not null ? ( Rec.LastRun, Rec.NextRun ) : ( DateTime.MinValue, DateTime.MinValue );
		}

		public static List<LOGIN_INFO> GetCarriersToImportExport()
		{
			var       Now = DateTime.UtcNow;
			using var Db  = new UsersEntities();

			return ( from I in Db.Entity.ImportExports
			         where ( I.NextRun <= Now ) && ( I.ImportEnabled || I.ExportEnabled )
			         select I ).ToList()
			                   .Select( a => ( a.CarrierId, a.UserName, a.Password, a.ImportEnabled, a.ExportEnabled ) )
			                   .ToList();
		}

		private static Databases.Users.ImportExport? GetImportExportRecord( UsersEntities users, ref string carrierId )
		{
			var Cid = carrierId.TrimToLower();
			carrierId = Cid;
			var E = users.Entity;

			return ( from I in E.ImportExports
			         where I.CarrierId == Cid
			         select I ).FirstOrDefault();
		}

		private static Databases.Users.ImportExport GetImportExportRecordOrNew( UsersEntities users, ref string carrierId )
		{
			var Rec = GetImportExportRecord( users, ref carrierId );

			if( Rec is null )
			{
				Rec = new Databases.Users.ImportExport { CarrierId = carrierId };
				users.Entity.ImportExports.Add( Rec );
			}
			return Rec;
		}
	}
}