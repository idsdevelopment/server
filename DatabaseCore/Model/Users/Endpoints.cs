﻿namespace Database.Model.Database;

public static partial class Users
{
	public static List<string> GetCarriers()
	{
		using var Db = new UsersEntities();

		return ( from E in Db.Entity.EndPoints
		         select E.CarrierId ).ToList();
	}

	public static EndPoint? GetEndPoints( IRequestContext context )
	{
		var CarrierId = context.CarrierId.ToLower();

		var Retries = 10;

		while( true )
		{
			try
			{
				using var Db = new UsersEntities();

				var RetVal = ( from E in Db.Entity.EndPoints
				               where E.CarrierId == CarrierId
				               select E ).FirstOrDefault();

				if( RetVal is not null )
				{
					RetVal.CarrierId = CarrierId; // Make sure lower case for blob storage
					return RetVal;
				}
			}
			catch( Exception E )
			{
				if( Retries <= 1 )
					context.SystemLogException( E );
			}

			if( --Retries <= 0 )
				break;

			Thread.Sleep( 1_000 );
		}

		return null;
	}

	public static bool HasEndpoints( IRequestContext context ) => GetEndPoints( context ) is not null;

	public static void DeleteEndpoint( IRequestContext context )
	{
		try
		{
			using var Db = new UsersEntities();

		#if NETCOREAPP
			var Eps = Db.Entity.EndPoints;
		#else
			var Eps = Db.EndPoints;
		#endif

			var CarrierId = context.CarrierId.ToLower();

			var Rec = ( from E in Eps
			            where E.CarrierId == CarrierId
			            select E ).FirstOrDefault();

			if( Rec is not null )
			{
				Eps.Remove( Rec );
				Db.SaveChanges();
			}
		}
		catch( Exception Exception )
		{
			context.SystemLogException( nameof( DeleteEndpoint ), Exception );
		}
	}

	public static EndPoint? CreateStorageEndPoint( IRequestContext context, StorageAccountResource? storageAccount )
	{
		if( storageAccount is { Data: { } Data } )
		{
			var Cid = context.CarrierId.ToLower();

			using var Db          = new UsersEntities();
			var       DbEndPoints = Db.Entity.EndPoints;

			var Rec = ( from U in DbEndPoints
			            where U.CarrierId == Cid
			            select U ).FirstOrDefault();

			var EndPoints = Data.PrimaryEndpoints;
			var Keys      = storageAccount.GetKeys();

			if( Keys is not null && Keys.Any() )
			{
				var AccountKey = Keys.First().Value;

				if( AccountKey is not null )
				{
					var Name  = Data.Name;
					var Table = EndPoints.TableUri.AbsoluteUri;
					var File  = EndPoints.FileUri.AbsoluteUri;
					var Blob  = EndPoints.BlobUri.AbsoluteUri;

					if( Rec is not null )
					{
						Rec.StorageAccount       = Name;
						Rec.StorageAccountKey    = AccountKey;
						Rec.StorageTableEndpoint = Table;
						Rec.StorageFileEndpoint  = File;
						Rec.StorageBlobEndpoint  = Blob;
					}
					else
					{
						Rec = new EndPoint
						      {
							      CarrierId            = Cid,
							      StorageAccount       = Name,
							      StorageAccountKey    = AccountKey,
							      StorageTableEndpoint = Table,
							      StorageFileEndpoint  = File,
							      StorageBlobEndpoint  = Blob
						      };

						DbEndPoints.Add( Rec );
					}
					Db.SaveChanges();
					return Rec;
				}
			}
		}
		return null;
	}

	public static async Task<EndPoint?> AddStorageEndPoint( IRequestContext context ) => GetEndPoints( context ) ?? CreateStorageEndPoint( context, await StorageAccount.CreateBlobStorageAccount( context ) );
}