﻿namespace Database.Model.Database;

public static partial class Users
{
	public const int PROCESS_LIMIT_IN_MINUTES = 60 * 24;

	public static long TimeLimitTicks
	{
		get
		{
			if( _TimeLimitTicks <= 0 )
				_TimeLimitTicks = new TimeSpan( 0, PROCESS_LIMIT_IN_MINUTES, 0 ).Ticks;

			return _TimeLimitTicks;
		}
	}

	private static long _TimeLimitTicks;

	public static long StartProcess( IRequestContext context )
	{
		try
		{
			var NowTicks = DateTime.UtcNow.Ticks;

			var Proc = new Process
			           {
				           TimeStarted    = NowTicks,
				           Processing     = true,
				           ResponseString = ""
			           };

			using var Db = new UsersEntities();
			Db.Entity.Processes.Add( Proc );
			RemoveExpired( Db, NowTicks );
			Db.SaveChanges();
			return Proc.ProcessId;
		}
		catch( Exception E )
		{
			context.SystemLogException( E );
		}

		return 0;
	}

	public static long RunProcess( IRequestContext context, Action<IRequestContext> action )
	{
		var DbContext = context.Clone();

		try
		{
			var ProcessId = StartProcess( DbContext );

			if( ProcessId > 0 )
			{
				Tasks.RunVoid( () =>
				               {
					               try
					               {
						               action( DbContext );
					               }
					               catch( Exception E )
					               {
						               DbContext.SystemLogException( E );
					               }
					               finally
					               {
						               EndProcess( DbContext, ProcessId );
					               }
				               } );
			}

			return ProcessId;
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
		return 0;
	}

	public static void EndProcess( IRequestContext context, long processId )
	{
		try
		{
			using var Db = new UsersEntities();

			var Processes = Db.Entity.Processes;

			var Proc = ( from P in Processes
			             where P.ProcessId == processId
			             select P ).FirstOrDefault();

			if( Proc is not null )
				Processes.Remove( Proc );
			RemoveExpired( Db, DateTime.UtcNow.Ticks );
			Db.SaveChanges();
		}
		catch( Exception E )
		{
			context.SystemLogException( E );
		}
	}

	public static void EndProcess( IRequestContext context, long processId, string response )
	{
		try
		{
			using var Db = new UsersEntities();

			var Proc = ( from P in Db.Entity.Processes
			             where P.ProcessId == processId
			             select P ).FirstOrDefault();

			if( Proc is not null )
			{
				Proc.Processing     = false;
				Proc.ResponseString = response;
			}

			RemoveExpired( Db, DateTime.UtcNow.Ticks );
			Db.SaveChanges();
		}
		catch( Exception E )
		{
			context.SystemLogException( E );
		}
	}


	public static bool IsProcessRunning( IRequestContext context, long processId )
	{
		try
		{
			using var Db = new UsersEntities();

			return ( from P in Db.Entity.Processes
			         where ( P.ProcessId == processId ) && P.Processing
			         select P ).FirstOrDefault() is not null;
		}
		catch( Exception E )
		{
			context.SystemLogException( E );
		}

		return false;
	}

	public static string GetProcessingResponse( IRequestContext context, long processId )
	{
		var Result = "";

		try
		{
			using var Db = new UsersEntities();

			var Processes = Db.Entity.Processes;

			var Rec = ( from P in Processes
			            where P.ProcessId == processId
			            select P ).FirstOrDefault();

			if( Rec is not null )
			{
				Result = Rec.ResponseString;

				if( !Rec.Processing )
				{
					Processes.Remove( Rec );
					Db.SaveChanges();
				}
			}
		}
		catch( Exception E )
		{
			context.SystemLogException( E );
		}

		return Result;
	}

	private static void RemoveExpired( UsersEntities db, long nowTicks )
	{
		var Processes = db.Entity.Processes;

		// Save changes gets called in the calling function
		Processes.RemoveRange( from P in Processes
		                       where ( nowTicks - P.TimeStarted ) >= TimeLimitTicks
		                       select P );
	}
}