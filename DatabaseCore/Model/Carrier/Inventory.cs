﻿using Inventory = Protocol.Data.Inventory;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public void AddUpdateInventory( Inventory inv )
	{
		try
		{
			var Code  = inv.InventoryCode;
			var BCode = inv.Barcode;

			var E         = Entity;
			var Inventory = E.Inventories;

			var Rec = ( from I in Inventory
			            where I.InventoryCode == Code
			            select I ).FirstOrDefault();

			void AddRec()
			{
				Rec = new MasterTemplate.Inventory
				      {
					      InventoryCode    = Code,
					      Barcode          = BCode,
					      ShortDescription = inv.Description,
					      LongDescription  = inv.Description,
					      PackageQuantity  = inv.PackageQuantity,
					      Active           = true,

					      DangerousGoods = false,
					      IsContainer    = false,
					      PackageType    = "",
					      UNClass        = "",
					      UnitVolume     = 0,
					      UnitWeight     = 0,

					      // cjt Missing fields
					      BinX = "",
					      BinY = "",
					      BinZ = "",

					      // Customisable Fields
					      BitData1 = false,
					      BitData2 = false,
					      BitData3 = false,
					      BitData4 = false,
					      BitData5 = false,
					      BitData6 = false,
					      BitData7 = false,
					      BitData8 = false,

					      DecimalData1 = 0,
					      DecimalData2 = 0,
					      DecimalData3 = 0,
					      DecimalData4 = 0,

					      IntData1 = 0,
					      IntData2 = 0,
					      IntData3 = 0,
					      IntData4 = 0,

					      StringData1 = "",
					      StringData2 = "",
					      StringData3 = "",
					      StringData4 = ""
				      };
				Inventory.Add( Rec );
			}

			if( Rec is null )
				AddRec();
			else
			{
				// Changing the barcodes inventory code
				if( Rec.InventoryCode != Code )
				{
					Inventory.Remove( Rec );
					AddRec();
				}
				else
				{
					Rec.Barcode          = inv.Barcode;
					Rec.ShortDescription = inv.Description;
					Rec.LongDescription  = inv.Description;
					Rec.PackageQuantity  = inv.PackageQuantity;
				}
			}

			E.SaveChanges();
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public void AddUpdateInventory( InventoryList invList )
	{
		foreach( var Item in invList )
			AddUpdateInventory( Item );
	}

	public void DeleteInventory( string itemCode )
	{
		try
		{
			var E   = Entity;
			var Inv = E.Inventories;

			var Rec = ( from I in Inv
			            where I.InventoryCode == itemCode
			            select I ).FirstOrDefault();

			if( Rec is not null )
			{
				Inv.Remove( Rec );
				E.SaveChanges();
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}


	public void DeleteInventory( InventoryList inventory )
	{
		try
		{
			var InvCodes = ( from I in inventory
			                 select I.InventoryCode ).ToList();
			var E   = Entity;
			var Inv = E.Inventories;

			Inv.RemoveRange( from I in Inv
			                 where InvCodes.Contains( I.InventoryCode )
			                 select I );

			E.SaveChanges();
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public Dictionary<string, Inventory> GetInventoryDictionary()
	{
		return (
			       from I in Entity.Inventories
			       select new Inventory
			              {
				              Barcode         = I.Barcode,
				              Description     = I.ShortDescription,
				              PackageQuantity = I.PackageQuantity,
				              InventoryCode   = I.InventoryCode
			              } ).ToDictionary( inventory => inventory.Barcode );
	}

	public InventoryList GetInventoryList()
	{
		var Result = new InventoryList();

		try
		{
			Result.AddRange( from I in Entity.Inventories
			                 select new Inventory
			                        {
				                        Barcode         = I.Barcode,
				                        Description     = I.ShortDescription,
				                        PackageQuantity = I.PackageQuantity,
				                        InventoryCode   = I.InventoryCode
			                        }
			               );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}
}