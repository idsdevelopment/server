﻿namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public RateResult CalculateRate( Rate requestObject )
	{
		var Result = new RateResult();
		var E      = Entity;

		try
		{
			int GetZoneId( string zoneName )
			{
				return ( from Z in E.Zones
				         where Z.ZoneName == zoneName
				         select Z ).FirstOrDefault()?.Id ?? -1;
			}

			var FromZId = GetZoneId( requestObject.FromZone );

			if( FromZId > -1 )
			{
				var ToZId = GetZoneId( requestObject.ToZone );

				if( ToZId > -1 )
				{
					var ServiceLevelRec = ( from S in E.ServiceLevels
					                        where S.Name == requestObject.ServiceLevel.Trim()
					                        select S ).FirstOrDefault();

					if( ServiceLevelRec is not null )
					{
						var Date = requestObject.DateTime;

						var DayAllowed = Date.DayOfWeek switch
						                 {
							                 DayOfWeek.Monday    => ServiceLevelRec.Monday,
							                 DayOfWeek.Tuesday   => ServiceLevelRec.Tuesday,
							                 DayOfWeek.Wednesday => ServiceLevelRec.Wednesday,
							                 DayOfWeek.Thursday  => ServiceLevelRec.Thursday,
							                 DayOfWeek.Friday    => ServiceLevelRec.Friday,
							                 DayOfWeek.Saturday  => ServiceLevelRec.Saturday,
							                 DayOfWeek.Sunday    => ServiceLevelRec.Sunday,
							                 _                   => false
						                 };

						if( DayAllowed )
						{
						#if NET7_0_OR_GREATER
							var Time = TimeOnly.FromTimeSpan( Date.TimeOfDay );
						#else
							var Time = Date.TimeOfDay;
						#endif

							if( ( Time >= ServiceLevelRec.StartTime ) && ( Time <= ServiceLevelRec.EndTime ) )
							{
								var PackageType = requestObject.PackageType.Trim();

								var PackageTypeRec = ( from P in E.PackageTypes
								                       where P.Description == PackageType
								                       select P ).FirstOrDefault();
								var CyclicMap = new HashSet<string>();
								var Volume    = requestObject.Height * requestObject.Width * requestObject.Depth;
								var Weight    = requestObject.Weight;

							CutoffLoop:

								if( PackageTypeRec is { Deleted: false } )
								{
									PackageType = PackageTypeRec.Description.Trim();

									if( CyclicMap.Add( PackageType ) )
									{
										var Rate = ( from R in E.RateMatrices
										             where ( R.FromZoneId == FromZId ) && ( R.ToZoneId == ToZId )
										                                               && ( R.ServiceLevelId == ServiceLevelRec.Id )
										                                               && ( R.PackageTypeId == PackageTypeRec.Id )
										                                               && ( R.VehicleTypeId == 0 )
										             select R ).FirstOrDefault();

										if( Rate is not null )
										{
											var Amount = Rate.Value;

											foreach( var Cutoff in ( from C in E.PackageTypeCutoffs
											                         orderby C.CutoffAmount descending
											                         where C.PackageTypeId == PackageTypeRec.Id
											                         select C ).ToList() )
											{
												void GetPackageRec()
												{
													PackageTypeRec = ( from P in E.PackageTypes
													                   where P.Id == Cutoff.ToPackageType
													                   select P ).FirstOrDefault();
												}

												switch( (CUTOFF_TYPE)Cutoff.CutoffType )
												{
												case CUTOFF_TYPE.AMOUNT:
													if( Amount >= Cutoff.CutoffAmount )
													{
														GetPackageRec();
														goto CutoffLoop;
													}
													break;

												case CUTOFF_TYPE.WEIGHT:
													if( Weight >= Cutoff.CutoffAmount )
													{
														GetPackageRec();
														goto CutoffLoop;
													}
													break;

												case CUTOFF_TYPE.VOLUME:
													if( Volume >= Cutoff.CutoffAmount )
													{
														GetPackageRec();
														goto CutoffLoop;
													}
													break;

												default:
													Result.Status = RATE_ERROR.INVALID_CUTOFF_TYPE;
													return Result;
												}
											}
											Result.Value  = Amount;
											Result.Status = RATE_ERROR.OK;
										}
										else
											Result.Status = RATE_ERROR.NO_RATES;
									}
									else
										Result.Status = RATE_ERROR.CYCLIC_CUTOFF;
								}
								else
									Result.Status = RATE_ERROR.INVALID_PACKAGE_TYPE;
							}
							else
								Result.Status = RATE_ERROR.TIME_NOT_VALID;
						}
						else
							Result.Status = RATE_ERROR.DAY_NOT_VALID;
					}
					else
						Result.Status = RATE_ERROR.INVALID_SERVICE_LEVEL;
				}
				else
					Result.Status = RATE_ERROR.INVALID_TO_ZONE;
			}
			else
				Result.Status = RATE_ERROR.INVALID_FROM_ZONE;
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}
}