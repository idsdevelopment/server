﻿using StaffShift = Protocol.Data.StaffShift;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public StaffShifts GetCompanyShifts( long companyId )
	{
		var Shifts = new StaffShifts();

		try
		{
			if( companyId != 0 )
			{
				var E = Entity;

				Shifts.AddRange( ( from S in E.StaffShifts
				                   let ShiftIds = from Cs in E.CompanyToShifts
				                                  where Cs.CompanyId == companyId
				                                  select Cs.ShiftId
				                   where ShiftIds.Contains( S.Id )
				                   select new StaffShift
				                          {
					                          ShiftName = S.Shift,
					                          StartDay  = (DAY_OF_WEEK)S.StartDay,
					                          EndDay    = (DAY_OF_WEEK)S.EndDay,
					                          Formula   = S.Formula,

				                          #if NET7_0_OR_GREATER
					                          Start = S.Start.ToTimeSpan(),
					                          End   = S.End.ToTimeSpan(),
				                          #else
					                          Start = S.Start,
					                          End = S.End,
				                          #endif

					                          ServiceLevel = ( from Sv in E.ServiceLevels
					                                           where Sv.Id == S.ServiceLevelId
					                                           select Sv.Name ).FirstOrDefault<string>() ?? "????"
				                          } ).ToList() );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Shifts;
	}

	public StaffShifts GetCompanyShifts( string accountId ) => GetCompanyShifts( CompanyShifts_GetResellerId( accountId ) );


	public void UpdateCompanyShifts( UpdateCompanyShifts requestObject )
	{
		var CompanyId = CompanyShifts_GetResellerId( requestObject.CompanyName );

		if( CompanyId != 0 )
		{
			var E  = Entity;
			var Cs = E.CompanyToShifts;

			// Delete current 
			E.CompanyToShifts.RemoveRange( from C in Cs
			                               where C.CompanyId == CompanyId
			                               select C );
			E.SaveChanges();

			// Get valid shifts
			var Shifts = ( from S in E.StaffShifts
			               where requestObject.Shifts.Contains( S.Shift )
			               select S.Id ).ToList();

			if( Shifts.Count > 0 )
			{
				Cs.AddRange( from S in Shifts
				             select new CompanyToShift
				                    {
					                    CompanyId = CompanyId,
					                    ShiftId   = S
				                    } );

				E.SaveChanges();
			}
		}
	}

	public StaffShifts GetStaffShifts()
	{
		var Shifts = new StaffShifts();
		var E      = Entity;

		try
		{
			Shifts.AddRange( from S in E.StaffShifts
			                 select new StaffShift
			                        {
				                        ShiftName = S.Shift,

			                        #if NET7_0_OR_GREATER
				                        Start = S.Start.ToTimeSpan(),
				                        End   = S.End.ToTimeSpan(),
			                        #else
				                        Start = S.Start,
				                        End = S.End,
			                        #endif

				                        StartDay = (DAY_OF_WEEK)S.StartDay,
				                        EndDay   = (DAY_OF_WEEK)S.EndDay,
				                        Formula  = S.Formula,

				                        ServiceLevel = ( from Sv in E.ServiceLevels
				                                         where Sv.Id == S.ServiceLevelId
				                                         select Sv.Name ).FirstOrDefault<string>() ?? "????"
			                        } );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return Shifts;
	}


	public static StaffShift? GetStaffShift( StaffShifts shifts, DateTimeOffset dateTime )
	{
		foreach( var Shift in shifts )
		{
			if( ShiftExtensions.InShift( dateTime, Shift.StartDay, Shift.Start, Shift.EndDay, Shift.End ) )
				return Shift;
		}
		return null;
	}

	public MasterTemplate.StaffShift? GetStaffShift( DateTimeOffset dateTime, string serviceLevel ) => GetStaffShift( dateTime, ( from S in Entity.ServiceLevels
	                                                                                                                              where string.Compare( S.Name, serviceLevel.Trim(), StringComparison.OrdinalIgnoreCase ) == 0
	                                                                                                                              select S.Id ).FirstOrDefault() );

	public MasterTemplate.StaffShift? GetStaffShift( DateTimeOffset dateTime, int serviceLevelId )
	{
	#if NET7_0_OR_GREATER
		var TimeOfDay = new TimeOnly( dateTime.TimeOfDay.Ticks );
	#else
		var TimeOfDay = dateTime.TimeOfDay;
	#endif

		var Shifts = ( from S in Entity.StaffShifts
		               where ( S.ServiceLevelId == serviceLevelId ) && ( TimeOfDay >= S.Start ) && ( TimeOfDay <= S.End )
		               select S ).ToList();

		MasterTemplate.StaffShift? Current  = null;
		var                        Duration = TimeSpan.MinValue;

		foreach( var Shift in Shifts )
		{
			if( Current is not null )
			{
			#if NET7_0_OR_GREATER
				var D = ShiftExtensions.Duration( Shift.StartDay,
				                                  Shift.Start.ToTimeSpan(),
				                                  Shift.EndDay,
				                                  Shift.End.ToTimeSpan() );
			#else
				var D = ShiftExtensions.Duration( Shift.StartDay,
				                                  Shift.Start,
				                                  Shift.EndDay,
				                                  Shift.End );
			#endif

				if( D > Duration )
				{
					Duration = D;
					Current  = Shift;
				}
			}
			else
				Current = Shift;
		}

		return Current;
	}

	public void UpdateStaffShifts( StaffShiftsUpdate shifts )
	{
		var ToDelete = ( from S in shifts
		                 where S.Delete
		                 select S.ShiftName ).ToList();

		if( ToDelete.Count > 0 )
		{
			var E = Entity;

			E.StaffShifts.RemoveRange( from S in E.StaffShifts
			                           where ToDelete.Contains( S.Shift )
			                           select S );
			E.SaveChanges();
		}

		var ToUpdate = ( from S in shifts
		                 where !S.Delete
		                 select S ).ToList();

		if( ToUpdate.Count > 0 )
		{
			var E = Entity;

			var Shifts = E.StaffShifts;

			foreach( var U in ToUpdate )
			{
				U.Formula      = U.Formula.Trim();
				U.ServiceLevel = U.ServiceLevel.Trim();
				U.OriginalName = U.OriginalName.Trim();
				U.ShiftName    = U.ShiftName.Trim();

				if( U.OriginalName.IsNullOrWhiteSpace() )
					U.OriginalName = U.ShiftName;

				var ServiceLevelId = ( from S in E.ServiceLevels
				                       where S.Name == U.ServiceLevel
				                       select S.Id ).FirstOrDefault();

				var Rec = ( from S in Shifts
				            where S.Shift == U.OriginalName // Maybe modify
				            select S ).FirstOrDefault();

				if( Rec is not null )
				{
					Rec.Shift          = U.ShiftName;
					Rec.StartDay       = (short)U.StartDay;
					Rec.EndDay         = (short)U.EndDay;
					Rec.Formula        = U.Formula;
					Rec.ServiceLevelId = ServiceLevelId;

				#if NET7_0_OR_GREATER
					Rec.Start = new TimeOnly( U.Start.Ticks );
					Rec.End   = new TimeOnly( U.End.Ticks );
				#else
					Rec.Start = U.Start;
					Rec.End = U.End;
				#endif
				}
				else
				{
					Shifts.Add( new MasterTemplate.StaffShift
					            {
						            Shift          = U.ShiftName,
						            StartDay       = (short)U.StartDay,
						            EndDay         = (short)U.EndDay,
						            Formula        = U.Formula.Trim(),
						            ServiceLevelId = ServiceLevelId,

					            #if NET7_0_OR_GREATER
						            Start = new TimeOnly( U.Start.Ticks ),
						            End   = new TimeOnly( U.End.Ticks )
					            #else
						            Start = U.Start,
						            End = U.End
					            #endif
					            } );
				}
			}

			E.SaveChanges();
		}
	}

	private long CompanyShifts_GetResellerId( string customerCode )
	{
		if( customerCode.IsNotNullOrWhiteSpace() )
		{
			return ( from R in Entity.ResellerCustomers
			         where R.CustomerCode == customerCode
			         select R.CompanyId ).FirstOrDefault();
		}
		return 0;
	}
}