﻿namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public class StaffNoteLogger
	{
		public string StaffId  { get; set; } = "";
		public string NoteName { get; set; } = "";
		public string Note     { get; set; } = "";

		public StaffNoteLogger()
		{
		}

		public StaffNoteLogger( StaffNote n )
		{
			StaffId  = n.StaffId;
			NoteName = n.NoteName;
			Note     = n.Note;
		}
	}


	public Notes GetStaffNotes( string staffId )
	{
		var Result = new Notes();
		staffId = staffId.TrimToLower();

		try
		{
			var Notes = from N in Entity.StaffNotes
			            where N.StaffId == staffId
			            select N;

			if( Notes.Any() )
			{
				foreach( var Note in Notes )
				{
					Result.Add( new Note
					            {
						            NoteId = Note.NoteName,
						            Text   = Note.Note
					            } );
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}

	public void AddStaffUpdateNote( string programName, string staffId, string noteName, string note )
	{
		try
		{
			staffId  = staffId.TrimToLower();
			noteName = noteName.Trim();
			var E  = Entity;
			var Sn = E.StaffNotes;

			var Rec = ( from N in Sn
			            where ( N.StaffId == staffId ) && ( N.NoteName == noteName )
			            select N ).FirstOrDefault();

			if( Rec is null )
			{
				Rec = new StaffNote
				      {
					      StaffId  = staffId,
					      NoteName = noteName,
					      Note     = note
				      };

				Sn.Add( Rec );
			}
			else
				Rec.Note = note;

			E.SaveChanges();

			Context.LogNew( Context.GetStorageId( LOG.STAFF ), programName, "Add staff note", new StaffNoteLogger( Rec ) );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public void RenameStaffNote( string programName, string staffId, string oldNoteName, string newNoteName )
	{
		staffId = staffId.TrimToLower();

		try
		{
			var Sn = Entity.StaffNotes;

			var Rec = ( from N in Sn
			            where ( N.StaffId == staffId ) && ( N.NoteName == oldNoteName )
			            select N ).FirstOrDefault();

			if( Rec is not null )
			{
				var Before = new StaffNoteLogger( Rec );

				Sn.Remove( Rec );
				Entity.SaveChanges();
				Rec.NoteName = newNoteName;
				Sn.Add( Rec );
				Entity.SaveChanges();

				Context.LogDifferences( Context.GetStorageId( LOG.STAFF ), programName, "Rename staff note", Before, new StaffNoteLogger( Rec ) );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public void DeleteStaffNote( string programName, string staffId, string noteName )
	{
		staffId = staffId.TrimToLower();

		try
		{
			var Sn = Entity.StaffNotes;

			var Rec = ( from Note in Sn
			            where ( Note.StaffId == staffId ) && ( Note.NoteName == noteName )
			            select Note ).FirstOrDefault();

			if( Rec is not null )
			{
				Sn.Remove( Rec );
				Entity.SaveChanges();
				Context.LogDeleted( Context.GetStorageId( LOG.STAFF ), programName, "Remove staff note", new StaffNoteLogger( Rec ) );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}
}