﻿namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public decimal? GetCartonQuantity( string carton )
	{
		carton = carton.Trim();

		var Rec = ( from X in Entity.XRefs
		            where ( X.Type == (short)XRef.TYPE.PML_CARTON_TO_PACKETS ) && ( X.Id1 == carton )
		            select X ).FirstOrDefault();

		return Rec?.DecimalData1;
	}

	public void AddUpdateCarton( string carton, string cartonDescription, string packet, string packetDescription, decimal quantity )
	{
		carton = carton.Trim();
		packet = packet.Trim();

		var E     = Entity;
		var XRefs = E.XRefs;

		var Rec = ( from X in XRefs
		            where ( X.Type == (short)XRef.TYPE.PML_CARTON_TO_PACKETS ) && ( X.Id1 == carton )
		            select X ).FirstOrDefault();

		if( Rec is null )
		{
			XRefs.Add( new XRef
			           {
				           Type         = (short)XRef.TYPE.PML_CARTON_TO_PACKETS,
				           Id1          = carton,
				           Id2          = packet,
				           DecimalData1 = quantity,
				           StringData1  = cartonDescription,
				           StringData2  = packetDescription
			           } );
		}
		else
		{
			Rec.Id2          = packet;
			Rec.DecimalData1 = quantity;
			Rec.StringData1  = cartonDescription;
			Rec.StringData2  = packetDescription;
		}

		E.SaveChanges();
	}

	public ( bool Ok, string CartonDescription, string Packet, string PacketDescription, decimal Quantity ) GetPacketFromCarton( string carton )
	{
		carton = carton.Trim();

		var Rec = ( from X in Entity.XRefs
		            where ( X.Type == (short)XRef.TYPE.PML_CARTON_TO_PACKETS ) && ( X.Id1 == carton )
		            select X ).FirstOrDefault();

		return Rec is not null ? ( true, Rec.StringData1, Rec.Id2, Rec.StringData2, Rec.DecimalData1 ) : ( false, "", "", "", 0 );
	}

	public (bool Ok, string CartonCode, string CartonDescription, decimal CartonQuantity) GetFromCartonPacket( string packet )
	{
		packet = packet.Trim();

		var Rec = ( from X in Entity.XRefs
		            where ( X.Type == (short)XRef.TYPE.PML_CARTON_TO_PACKETS ) && ( X.Id2 == packet )
		            select X ).FirstOrDefault();

		return Rec is not null ? ( true, Rec.Id1, Rec.StringData1, Rec.DecimalData1 ) : ( false, "", "", 0 );
	}
}