﻿namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public XRef? GetBarcodeFromAccountIdXref( XRef.TYPE type, string accountId ) => ( from X in Entity.XRefs
	                                                                                  where ( X.Type == (short)type ) && ( X.Id1 == accountId )
	                                                                                  select X ).FirstOrDefault();

	public XRef? GetAccountIdFromBarcodeXref( XRef.TYPE type, string barcode ) => ( from X in Entity.XRefs
	                                                                                where ( X.Type == (short)type ) && ( X.Id2 == barcode )
	                                                                                select X ).FirstOrDefault();

	public XRef? GetBarcodeFromAccountIdXref( string accountId ) => GetBarcodeFromAccountIdXref( XRef.TYPE.ACCOUNT_ID_TO_BARCODE, accountId );
	public XRef? GetSecondaryAccountIdFromBarcodeXref( string barcode ) => GetAccountIdFromBarcodeXref( XRef.TYPE.SECONDARY_ACCOUNT_ID_TO_BARCODE, barcode );

	public void AddUpdateAccountIdToLocationBarcode( string accountId, string locationBarcode )
	{
		AddUpdateAccountIdToLocationBarcode( XRef.TYPE.ACCOUNT_ID_TO_BARCODE, accountId, locationBarcode );
	}

	public void AddUpdateSecondaryAccountIdToLocationBarcode( string accountId, string locationBarcode )
	{
		AddUpdateAccountIdToLocationBarcode( XRef.TYPE.SECONDARY_ACCOUNT_ID_TO_BARCODE, accountId, locationBarcode );
	}

	private void AddUpdateAccountIdToLocationBarcode( XRef.TYPE type, string accountId, string locationBarcode )
	{
		var Rec   = GetBarcodeFromAccountIdXref( type, accountId );
		var E     = Entity;
		var XRefs = E.XRefs;

		if( Rec is not null )
		{
			XRefs.Remove( Rec );
			E.SaveChanges();
		}

		XRefs.Add( new XRef( type )
		           {
			           Id1 = accountId,
			           Id2 = locationBarcode
		           } );

		E.SaveChanges();
	}
}