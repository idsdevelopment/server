﻿namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public void SaveTripToXref( string azureId, string ids1Id, string barcode, string barcode1, string itemCode, string description, string reference, decimal pieces, decimal weight )
	{
		azureId = azureId.Trim();
		ids1Id  = ids1Id.Trim();

		var E     = Entity;
		var Xrefs = E.XRefs;

		var Rec = ( from X in Xrefs
		            where ( X.Id1 == azureId ) && ( X.Id2 == ids1Id )
		            select X ).FirstOrDefault();

		if( Rec is not null )
		{
			Rec.StringData1 = barcode.Trim();
			Rec.StringData2 = itemCode.Trim();
			Rec.StringData3 = description.Trim();
			Rec.StringData4 = reference.Trim();
			Rec.StringData5 = barcode1.Trim();

			Rec.StringData6 = Rec.StringData7 = Rec.StringData8 = "";

			Rec.DecimalData1 = pieces;
			Rec.DecimalData2 = weight;
		}
		else
			Xrefs.Add( new XRef().AsTrip( azureId, ids1Id, barcode, barcode1, itemCode, description, reference, pieces, weight ) );

		E.SaveChanges();
	}

	public void DeleteTripFromXref( string azureId, string ids1Id )
	{
		azureId = azureId.Trim();
		ids1Id  = ids1Id.Trim();

		var E     = Entity;
		var Xrefs = E.XRefs;

		var Rec = ( from X in Xrefs
		            where ( X.Type == (short)XRef.TYPE.TRIP ) && ( X.Id1 == azureId ) && ( X.Id2 == ids1Id )
		            select X ).FirstOrDefault();

		if( Rec is not null )
		{
			Xrefs.Remove( Rec );
			E.SaveChanges();
		}
	}

	public void DeleteTripsFromXref( string azureId )
	{
		azureId = azureId.Trim();

		var E     = Entity;
		var Xrefs = E.XRefs;

		var Recs = ( from X in Xrefs
		             where ( X.Type == (short)XRef.TYPE.TRIP ) && ( X.Id1 == azureId )
		             select X ).ToList();

		if( Recs.Count > 0 )
		{
			Xrefs.RemoveRange( Recs );
			E.SaveChanges();
		}
	}

	public void SetNewId1KeyInXref( string oldKey, string newKey )
	{
		var E     = Entity;
		var Xrefs = E.XRefs;

		var Recs = ( from X in Xrefs
		             where ( X.Type == (short)XRef.TYPE.TRIP ) && ( X.Id1 == oldKey )
		             select X ).ToList();

		if( Recs.Count > 0 )
		{
			foreach( var XRef in Recs )
				XRef.Id1 = newKey;

			E.SaveChanges();
		}
	}

	public ( string? AzureId, bool Ok, string Barcode, string ItemCode, string Description, string Reference, decimal Pieces, decimal Weight ) AzureIdFromXref( string ids1Id )
	{
		ids1Id = ids1Id.Trim();

		var Rec = ( from X in Entity.XRefs
		            where ( X.Type == (short)XRef.TYPE.TRIP ) && ( X.Id2 == ids1Id )
		            select X ).FirstOrDefault();

		return Rec is not null ? ( Rec.Id1.Trim(), true, Rec.StringData1, Rec.StringData2, Rec.StringData3, Rec.StringData4, Rec.DecimalData1, Rec.DecimalData2 )
			       : ( "", false, "", "", "", "", 0M, 0M );
	}

	public List<(string IdsId, string Barcode, string ItemCode, string Description, string Reference, decimal Pieces, decimal Weight)> IdsIdFromXref( string azureId )
	{
		azureId = azureId.Trim();

		var Result = new List<(string, string, string, string, string, decimal, decimal )>();

		foreach( var X1 in from X in Entity.XRefs
		                   orderby X.Id2
		                   where ( X.Type == (short)XRef.TYPE.TRIP ) && ( X.Id1 == azureId )
		                   select X )
			Result.Add( ( X1.Id2, X1.StringData1, X1.StringData2, X1.StringData3, X1.StringData4, X1.DecimalData1, X1.DecimalData2 ) );

		return Result;
	}

	public List<string> GetRelatedTripIds( List<string> tripIds ) => ( from X in Entity.XRefs
	                                                                   where tripIds.Contains( X.Id1 )
	                                                                   select X.Id2 ).ToList();

	public List<(string TripId, string Barcode, decimal Pieces)> GetRelatedTripIdsAndPieces( List<string> tripIds )
	{
		var Result = new List<(string TripId, string Barcode, decimal Pieces)>();

		var E = Entity;

		foreach( var A in from X in E.XRefs
		                  where tripIds.Contains( X.Id1 )
		                  select new
		                         {
			                         TripId  = X.Id2,
			                         Pieces  = X.DecimalData1,
			                         Barcode = X.StringData1
		                         } )
			Result.Add( ( A.TripId, A.Barcode, A.Pieces ) );

		return Result;
	}
}