﻿using Address = Database.Model.Databases.MasterTemplate.Address;

// ReSharper disable NullCoalescingConditionIsAlwaysNotNullAccordingToAPIContract

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public class AddressLogger
	{
		public string SecondaryId { get; set; }

		public string  Suite         { get; set; }
		public string  AddressLine1  { get; set; }
		public string  AddressLine2  { get; set; }
		public string  Vicinity      { get; set; }
		public string  City          { get; set; }
		public string  Region        { get; set; }
		public string  PostalCode    { get; set; }
		public string  CountryCode   { get; set; }
		public string  Phone         { get; set; }
		public string  Phone1        { get; set; }
		public string  Mobile        { get; set; }
		public string  Mobile1       { get; set; }
		public string  Fax           { get; set; }
		public string  ContactName   { get; set; }
		public string  EmailAddress  { get; set; }
		public string  EmailAddress1 { get; set; }
		public string  EmailAddress2 { get; set; }
		public decimal Latitude      { get; set; }
		public decimal Longitude     { get; set; }
		public string  Notes         { get; set; }
		public string  Barcode       { get; set; }
		public string  PostalBarcode { get; set; }
		public int     GroupNumber   { get; set; }

		public AddressLogger( Address addr )
		{
			FixNulls( addr );

			SecondaryId   = addr.SecondaryId;
			Suite         = addr.Suite;
			AddressLine1  = addr.AddressLine1;
			AddressLine2  = addr.AddressLine2;
			Vicinity      = addr.Vicinity;
			City          = addr.City;
			Region        = addr.Region;
			PostalCode    = addr.PostalCode;
			CountryCode   = addr.CountryCode;
			Phone         = addr.Phone;
			Phone1        = addr.Phone1;
			Mobile        = addr.Mobile;
			Mobile1       = addr.Mobile1;
			Fax           = addr.Fax;
			ContactName   = addr.Contact;
			EmailAddress  = addr.EmailAddress;
			EmailAddress1 = addr.EmailAddress1;
			EmailAddress2 = addr.EmailAddress2;
			Latitude      = addr.Latitude;
			Longitude     = addr.Longitude;
			Notes         = addr.Notes;
			Barcode       = addr.Barcode;
			PostalBarcode = addr.PostalBarcode;
		}
	}

	public ( AddressLogger? Before, AddressLogger? After, long AddressId, bool Ok ) UpdateAddress( long addressId, Protocol.Data.Address addr, string programName )
	{
		try
		{
			var Rec = ( from A in Entity.Addresses
			            where A.Id == addressId
			            select A ).FirstOrDefault();

			if( Rec is not null )
			{
				var Before = new AddressLogger( Rec );
				Rec.SecondaryId   = addr.SecondaryId;
				Rec.Suite         = addr.Suite.Max50();
				Rec.AddressLine1  = addr.AddressLine1;
				Rec.AddressLine2  = addr.AddressLine2;
				Rec.Vicinity      = addr.Vicinity;
				Rec.City          = addr.City;
				Rec.Region        = addr.Region;
				Rec.Country       = addr.Country;
				Rec.CountryCode   = addr.CountryCode;
				Rec.PostalCode    = addr.PostalCode.TrimMax20();
				Rec.PostalBarcode = addr.PostalBarcode;
				Rec.Barcode       = addr.Barcode;
				Rec.Latitude      = addr.Latitude.Max90();
				Rec.Longitude     = addr.Longitude.Max180();
				Rec.Notes         = addr.Notes;
				Rec.Contact       = addr.ContactName;
				Rec.EmailAddress  = addr.EmailAddress;
				Rec.EmailAddress1 = addr.EmailAddress1;
				Rec.EmailAddress2 = addr.EmailAddress2;
				Rec.Mobile        = addr.Mobile.Max20();
				Rec.Mobile1       = addr.Mobile1.Max20();
				Rec.Fax           = addr.Fax.Max20();
				Rec.Phone         = addr.Phone.Max20();
				Rec.Phone1        = addr.Phone1.Max20();
				Rec.LastModified  = DateTime.Now;
				Rec.ZoneId        = GetIdByZoneName( addr.ZoneName );

				FixNulls( Rec );

				Entity.SaveChanges();
				var After = new AddressLogger( Rec );
				return ( Before, After, addressId, true );
			}
			var (AddressId, Address, Ok) = AddAddress( addr, programName );

			if( Ok )
				return ( Before: null, After: Address, AddressId, Ok: true );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return ( Before: null, After: null, 0, Ok: false );
	}

	public (long AddressId, AddressLogger? Address, bool Ok) AddAddress( Protocol.Data.Address addr, string programName )
	{
		var Retval = ( AddressId: (long)0, Address: (AddressLogger?)null, Ok: false );

		try
		{
			var A = new Address
			        {
				        SecondaryId   = addr.SecondaryId,
				        Suite         = addr.Suite.Max50(),
				        AddressLine1  = addr.AddressLine1,
				        AddressLine2  = addr.AddressLine2,
				        Vicinity      = addr.Vicinity,
				        City          = addr.City,
				        Region        = addr.Region,
				        Country       = addr.Country,
				        CountryCode   = addr.CountryCode,
				        PostalCode    = addr.PostalCode.Max20(),
				        PostalBarcode = addr.PostalBarcode,
				        Barcode       = addr.Barcode,
				        Latitude      = addr.Latitude.Max90(),
				        Longitude     = addr.Longitude.Max180(),
				        Notes         = addr.Notes,
				        Contact       = addr.ContactName,
				        EmailAddress  = addr.EmailAddress,
				        EmailAddress1 = addr.EmailAddress1,
				        EmailAddress2 = addr.EmailAddress2,
				        Mobile        = addr.Mobile.Max20(),
				        Mobile1       = addr.Mobile1.Max20(),
				        Fax           = addr.Fax.Max20(),
				        Phone         = addr.Phone.Max20(),
				        Phone1        = addr.Phone1.Max20(),
				        LastModified  = DateTime.Now,
				        ZoneId        = GetIdByZoneName( addr.ZoneName )
			        };

			FixNulls( A );

			Entity.Addresses.Add( A );
			Entity.SaveChanges();
			Retval.Address   = new AddressLogger( A );
			Retval.AddressId = A.Id;
			Retval.Ok        = true;
		}
		catch( Exception E )
		{
			Context.SystemLogException( nameof( AddAddress ), E );
		}
		return Retval;
	}

	public (Address? Address, bool Ok) GetAddress( long addressId )
	{
		(Address? Address, bool Ok) Retval = ( Address: null, Ok: false );

		Retval.Address = ( from A in Entity.Addresses
		                   where A.Id == addressId
		                   select A ).FirstOrDefault();
		Retval.Ok = Retval.Address.IsNotNull();
		return Retval;
	}

	public (Address? Address, bool Ok) GetAddressBySecondaryId( string id )
	{
		(Address? Address, bool Ok) Retval = ( Address: null, Ok: false );

		Retval.Address = ( from A in Entity.Addresses
		                   where A.SecondaryId == id
		                   select A ).FirstOrDefault();
		Retval.Ok = Retval.Address.IsNotNull();
		return Retval;
	}

	public List<string> FindMissingAddressesBySecondaryId( List<string> ids )
	{
		var Ids       = new HashSet<string>( ids );
		var Addresses = Entity.Addresses;

		return ( from Id in Ids
		         let Rec = ( from A in Addresses
		                     where A.SecondaryId == Id
		                     select A ).FirstOrDefault()
		         where Rec is null
		         select Id ).ToList();
	}

	public List<Address> CheckSuiteAndAddressLine1( string suite, string addressLine1 )
	{
		try
		{
			return ( from A in Entity.Addresses
			         where ( A.Suite == suite ) && ( A.AddressLine1 == addressLine1 )
			         select A ).ToList();
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( nameof( CheckSuiteAndAddressLine1 ), Exception );
		}
		return [];
	}

	// Client send nulls
	private static void FixNulls( Address a )
	{
		a.SecondaryId   ??= "";
		a.Suite         ??= "";
		a.AddressLine1  ??= "";
		a.AddressLine2  ??= "";
		a.Vicinity      ??= "";
		a.City          ??= "";
		a.Region        ??= "";
		a.PostalCode    ??= "";
		a.CountryCode   ??= "";
		a.Phone         ??= "";
		a.Phone1        ??= "";
		a.Mobile        ??= "";
		a.Mobile1       ??= "";
		a.Fax           ??= "";
		a.Contact       ??= "";
		a.EmailAddress  ??= "";
		a.EmailAddress1 ??= "";
		a.EmailAddress2 ??= "";
		a.Notes         ??= "";
		a.Barcode       ??= "";
		a.PostalBarcode ??= "";
	}
}