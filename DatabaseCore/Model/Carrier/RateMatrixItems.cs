﻿namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public void AddUpdateRateMatrixItems( RateMatrixItems rateMatrixItems )
	{
		try
		{
			var E = Entity;

			foreach( var Item in rateMatrixItems )
			{
				var Rec = ( from Rm in E.RateMatrices
				            where ( Rm.FromZoneId == Item.FromZoneId )
				                  && ( Rm.ToZoneId == Item.ToZoneId )
				                  && ( Rm.ServiceLevelId == Item.ServiceLevelId )
				                  && ( Rm.PackageTypeId == Item.PackageTypeId )
				                  && ( Rm.VehicleTypeId == Item.VehicleTypeId )
				            select Rm ).FirstOrDefault();

				if( Rec is null )
				{
					E.RateMatrices.Add( new RateMatrix
					                    {
						                    FromZoneId     = Item.FromZoneId,
						                    PackageTypeId  = Item.PackageTypeId,
						                    ServiceLevelId = Item.ServiceLevelId,
						                    ToZoneId       = Item.ToZoneId,
						                    VehicleTypeId  = Item.VehicleTypeId,
						                    Formula        = Item.Formula.NullTrim(),
						                    Value          = Item.Value
					                    } );
				}
				else
				{
					Rec.Formula = Item.Formula.NullTrim();
					Rec.Value   = Item.Value;
				}
				E.SaveChanges();
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public RateMatrixItems GetAllRateMatrixItems()
	{
		RateMatrixItems Rims = [];

		try
		{
			var Temp = ( from R in Entity.RateMatrices
			             select R ).ToList();

			foreach( var Rm in Temp )
			{
				var Rmi = Convert( Rm );
				Rims.Add( Rmi );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Rims;
	}

	private static RateMatrixItem Convert( RateMatrix rm )
	{
		RateMatrixItem Rmi = new()
		                     {
			                     Formula        = rm.Formula.NullTrim(),
			                     FromZoneId     = rm.FromZoneId,
			                     PackageTypeId  = rm.PackageTypeId,
			                     ServiceLevelId = rm.ServiceLevelId,
			                     ToZoneId       = rm.ToZoneId,
			                     Value          = rm.Value,
			                     VehicleTypeId  = rm.VehicleTypeId
		                     };

		return Rmi;
	}
}