﻿using Database.Utils;
using Configuration = Database.Model.Databases.MasterTemplate.Configuration;

namespace Database.Model.Databases.Carrier;

public static class CarrierDbExtensions
{
	public static string Max20( this  string txt ) => txt.MaxLength( 20 );
	public static string Max50( this  string txt ) => txt.MaxLength( 50 );
	public static string Max100( this string txt ) => txt.MaxLength( 100 );
	public static string Max255( this string txt ) => txt.MaxLength( 255 );

	public static string Max900Bytes( this string txt ) // Max Db Index Size
	{
		const int MAX_SIZE = 900 - 4; // Allow some for string length in Db
		var       Bytes    = Encoding.Unicode.GetBytes( txt );

		if( Bytes.Length > MAX_SIZE )
		{
			Array.Resize( ref Bytes, MAX_SIZE );
			txt = Encoding.Unicode.GetString( Bytes );
		}
		return txt;
	}

	public static string TrimMax20( this  string txt ) => txt.NullTrim().MaxLength( 20 );
	public static string TrimMax50( this  string txt ) => txt.NullTrim().MaxLength( 50 );
	public static string TrimMax100( this string txt ) => txt.NullTrim().MaxLength( 100 );
	public static string TrimMax255( this string txt ) => txt.NullTrim().MaxLength( 255 );

	public static decimal Limit( this decimal val, uint max )
	{
		if( Math.Abs( val ) > max )
			return val < 0 ? -max : max;
		return val;
	}

	public static decimal Max180( this decimal val ) => val.Limit( 180 );
	public static decimal Max90( this  decimal val ) => val.Limit( 90 );
}

public partial class CarrierDb : ContextDb<MasterTemplateEntities>
{
	private const string CONNECTION_ID = "MasterTemplateEntities",
						 CACHE_KEY     = "~~Carrier~~Db~~";

	private const int CURRENT_VERSION = 0;

	public override int CurrentVersion => CURRENT_VERSION;

#region Enforce Single Request
	protected SemaphoreQueue EnforceThreadLimit<T>( int maxThreads = 1 ) where T : class => SemaphoreQueue.SemaphoreQueueByTypeAndId<T>( Context.CarrierId.TrimToLower(), maxThreads );
#endregion

	protected override MasterTemplateEntities CreateEntity( string connectionString ) => new( connectionString );

	protected override void GetConnection( out string databaseName, out string connectionName, out string cacheKey )
	{
		databaseName   = DbUtils.MASTER_DATABASE_NAME;
		connectionName = CONNECTION_ID;
		cacheKey       = CACHE_KEY;
	}

	protected override bool UpgradeToVersion( MasterTemplateEntities entity, int toVersion ) => false;

	protected override int GetDatabaseVersion( MasterTemplateEntities entity )
	{
		var Cfg = ( from Conf in entity.Configurations
					select Conf ).FirstOrDefault();

		if( Cfg is null )
		{
			var Config = new Configuration
						 {
							 CurrentVersion = CURRENT_VERSION
						 };

			entity.Configurations.Add( Config );
			entity.SaveChanges();

			return CURRENT_VERSION;
		}

		return Cfg.CurrentVersion;
	}

	protected override void SetDatabaseVersion( MasterTemplateEntities entity, int version )
	{
		var Config = ( from Conf in entity.Configurations
					   select Conf ).FirstOrDefault();

		if( Config is null )
		{
			Config = new Configuration
					 {
						 CurrentVersion = version
					 };
			entity.Configurations.Add( Config );
		}
		else
			Config.CurrentVersion = version;

		entity.SaveChanges();
	}

	public CarrierDb( IRequestContext context ) : base( context )
	{
	}
}