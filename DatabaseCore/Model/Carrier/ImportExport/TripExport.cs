﻿namespace Database.Model.Databases.Carrier;

public class TripExport
{
	public string Code
	{
		get => Export.Code;
		set => Export.Code = value;
	}

	public TRIP_DATA_SUB_TYPES SubType
	{
		get => (TRIP_DATA_SUB_TYPES)Export.Int1;
		set => Export.Int1 = (long)value;
	}

	public STATUS Status
	{
		get => (STATUS)Export.Int2;
		set => Export.Int2 = (long)value;
	}

	public bool Bool1
	{
		get => Export.Bool1;
		set => Export.Bool1 = value;
	}

	public Signature Signature
	{
		get => new()
		       {
			       Height = (int)Export.Int4,
			       Width  = (int)Export.Int5,
			       Points = Export.String5
		       };
		set
		{
			Export.Int4    = value.Height;
			Export.Int5    = value.Width;
			Export.String5 = value.Points;
		}
	}

	public string String1
	{
		get => Export.String1;
		set => Export.String1 = value;
	}

	public string String2
	{
		get => Export.String2;
		set => Export.String2 = value;
	}

	public string String3
	{
		get => Export.String3;
		set => Export.String3 = value;
	}

	public string String4
	{
		get => Export.String4;
		set => Export.String4 = value;
	}

	public string String5
	{
		get => Export.String5;
		set => Export.String5 = value;
	}

	private readonly Export Export;

	public TripExport( Export export )
	{
		Export = export;
	}

	public TripExport( TripExport export )
	{
		Export = export.Export;
	}

	public TripExport()
	{
		Export = new Export
		         {
			         DataType = (short)Export.DATA_TYPES.TRIP_UPDATE
		         };
	}

	public enum TRIP_DATA_SUB_TYPES : uint
	{
		NEW,
		UPDATE,
		UPDATE_WITH_SIGNATURE,
		STATUS,
		DELETE
	}

	public static implicit operator Export?( TripExport? export ) => export?.Export;
}

public partial class CarrierDb
{
	public void AddNewTripToExport( string code )
	{
		AddToExport( CreateExportTrip<TripExport>( code, TripExport.TRIP_DATA_SUB_TYPES.NEW )! );
	}

	public void AddTripToExport( string code )
	{
		AddToExport( CreateExportTrip<TripExport>( code, TripExport.TRIP_DATA_SUB_TYPES.UPDATE )! );
	}

	public void AddTripToExport( string code, Signature signature )
	{
		var Rec = CreateExportTrip<TripExport>( code, TripExport.TRIP_DATA_SUB_TYPES.UPDATE_WITH_SIGNATURE );

		if( Rec is not null )
		{
			Rec.Signature = signature;
			AddToExport( Rec );
		}
	}

	public void AddTripStatusToExport( string code, STATUS status )
	{
		var Rec = CreateExportTrip<TripExport>( code, TripExport.TRIP_DATA_SUB_TYPES.STATUS );

		if( Rec is not null )
		{
			Rec.Status = status;
			AddToExport( Rec );
		}
	}

	public void AddTripStatusToExport( string code, STATUS status, string string1, string string2, string string3, string string4, string string5 )
	{
		var Rec = CreateExportTrip<TripExport>( code, TripExport.TRIP_DATA_SUB_TYPES.STATUS );

		if( Rec is not null )
		{
			Rec.Status  = status;
			Rec.String1 = string1;
			Rec.String2 = string2;
			Rec.String3 = string3;
			Rec.String4 = string4;
			Rec.String5 = string5;
			AddToExport( Rec );
		}
	}


	public T? CreateExportTrip<T>( string code, TripExport.TRIP_DATA_SUB_TYPES subType ) where T : TripExport, new()
	{
		code = code.Trim();

		if( code.IsNotNullOrWhiteSpace() && Context.CarrierServicePreferences.Export )
		{
			return new T
			       {
				       SubType = subType,
				       Code    = code
			       };
		}
		return null;
	}
}