﻿namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public void AddToExport( Export? export )
	{
		if( export is not null )
		{
			try
			{
				var Db = new CarrierDb( Context );
				var E  = Db.Entity;

				export.Exported = DateTime.UtcNow;

				// Just add it, if it's a duplicate key, it will throw an exception (Save a lookup)
				E.Exports.Add( export );
				E.SaveChanges();
			}
			catch // Probably a duplicate key
			{
			}
		}
	}
}