﻿namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public void AddUpdateCharges( Charges charges )
	{
		try
		{
			var E             = Entity;
			var ChargeDetails = E.ChargeDetails;

			var Lookup = ( from C in GetAllCharges()
			               select C.ChargeId ).ToHashSet();

			foreach( var C in charges )
			{
				try
				{
					var Rec = ( from Chg in ChargeDetails
					            where Chg.ChargeId == C.ChargeId
					            select Chg ).FirstOrDefault();

					if( Rec is null )
					{
						var CRec = new ChargeDetail
						           {
							           ChargeId                 = C.ChargeId,
							           AccountId                = C.AccountId,
							           AlwaysApplied            = C.AlwaysApplied,
							           ChargeType               = C.ChargeType,
							           DisplayOnDriversScreen   = C.DisplayOnDriversScreen,
							           DisplayOnEntry           = C.DisplayOnEntry,
							           DisplayOnInvoice         = C.DisplayOnInvoice,
							           DisplayOnWaybill         = C.DisplayOnWaybill,
							           DisplayType              = C.DisplayType,
							           ExcludeFromFuelSurcharge = C.ExcludeFromFuelSurcharge,
							           ExcludeFromTaxes         = C.ExcludeFromTaxes,
							           Formula                  = C.Formula.NullTrim(),
							           IsText                   = C.IsText,
							           IsDiscountable           = C.IsDiscountable,
							           IsFormula                = C.IsFormula,
							           IsFuelSurcharge          = C.IsFuelSurcharge,
							           IsMultiplier             = C.IsMultiplier,
							           IsOverride               = C.IsOverride,
							           IsTax                    = C.IsTax,
							           IsVolumeCharge           = C.IsVolumeCharge,
							           IsWeightCharge           = C.IsWeightCharge,
							           Label                    = C.Label,
							           Max                      = C.Max,
							           Min                      = C.Min,
							           OverridenPackage         = C.OverridenPackage,
							           SortIndex                = C.SortIndex,
							           Text                     = C.Text,
							           Value                    = C.Value,
							           ExcludeFromPayroll       = C.ExcludeFromPayroll
						           };

						ChargeDetails.Add( CRec );
					}
					else
					{
						Lookup.Remove( Rec.ChargeId );

						Rec.AccountId                = C.AccountId;
						Rec.AlwaysApplied            = C.AlwaysApplied;
						Rec.ChargeType               = C.ChargeType;
						Rec.DisplayOnDriversScreen   = C.DisplayOnDriversScreen;
						Rec.DisplayOnEntry           = C.DisplayOnEntry;
						Rec.DisplayOnInvoice         = C.DisplayOnInvoice;
						Rec.DisplayOnWaybill         = C.DisplayOnWaybill;
						Rec.DisplayType              = C.DisplayType;
						Rec.ExcludeFromFuelSurcharge = C.ExcludeFromFuelSurcharge;
						Rec.ExcludeFromTaxes         = C.ExcludeFromTaxes;
						Rec.Formula                  = C.Formula.NullTrim();
						Rec.IsText                   = C.IsText;
						Rec.IsDiscountable           = C.IsDiscountable;
						Rec.IsFormula                = C.IsFormula;
						Rec.IsFuelSurcharge          = C.IsFuelSurcharge;
						Rec.IsMultiplier             = C.IsMultiplier;
						Rec.IsOverride               = C.IsOverride;
						Rec.IsTax                    = C.IsTax;
						Rec.IsVolumeCharge           = C.IsVolumeCharge;
						Rec.IsWeightCharge           = C.IsWeightCharge;
						Rec.Label                    = C.Label;
						Rec.Max                      = C.Max;
						Rec.Min                      = C.Min;
						Rec.OverridenPackage         = C.OverridenPackage;
						Rec.SortIndex                = C.SortIndex;
						Rec.Text                     = C.Text;
						Rec.Value                    = C.Value;
						Rec.ExcludeFromPayroll       = C.ExcludeFromPayroll;
					}

					E.SaveChanges();
				}
				catch( Exception Exception )
				{
					Context.SystemLogException( Exception );
				}
			}

			if( Lookup.Count > 0 )
			{
				// Need to delete any Charges that are left
				ChargeDetails.RemoveRange( from C in ChargeDetails
				                           where Lookup.Contains( C.ChargeId )
				                           select C );

				E.SaveChanges();
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public Charges GetAllCharges()
	{
		Charges Charges = [];

		try
		{
			var Temp = ( from Charge in Entity.ChargeDetails
			             select Charge ).ToList();

			foreach( var Cd in Temp )
			{
				var Charge = Convert( Cd );
				Charges.Add( Charge );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Charges;
	}

	public Charge? GetCharge( string chargeName )
	{
		try
		{
			var Temp = ( from Charge in Entity.ChargeDetails
			             where Charge.Label == chargeName
			             select Charge ).FirstOrDefault();

			if( Temp is not null )
				return Convert( Temp );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return null;
	}


	public void DeleteCharge( string programName, string chargeId )
	{
		try
		{
			var Charges = Entity.ChargeDetails;

			var Rec = ( from C in Charges
			            where C.ChargeId == chargeId
			            select C ).FirstOrDefault();

			if( Rec is not null )
			{
				Charges.Remove( Rec );
				Entity.SaveChanges();

				//var Before = new ChargeDetailLogger(
				Context.LogDeleted( Context.GetStorageId( LOG.TRIP ), programName, "Remove Charge " + chargeId, Rec );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	private static Charge Convert( ChargeDetail cd )
	{
		Charge Charge = new()
		                {
			                ChargeId                 = cd.ChargeId,
			                AccountId                = cd.AccountId,
			                AlwaysApplied            = cd.AlwaysApplied,
			                ChargeType               = cd.ChargeType,
			                DisplayOnDriversScreen   = cd.DisplayOnDriversScreen,
			                DisplayOnEntry           = cd.DisplayOnEntry,
			                DisplayOnInvoice         = cd.DisplayOnInvoice,
			                DisplayOnWaybill         = cd.DisplayOnWaybill,
			                DisplayType              = cd.DisplayType,
			                ExcludeFromFuelSurcharge = cd.ExcludeFromFuelSurcharge,
			                ExcludeFromTaxes         = cd.ExcludeFromTaxes,
			                Formula                  = cd.Formula.NullTrim(),
			                IsText                   = cd.IsText,
			                IsDiscountable           = cd.IsDiscountable,
			                IsFormula                = cd.IsFormula,
			                IsFuelSurcharge          = cd.IsFuelSurcharge,
			                IsMultiplier             = cd.IsMultiplier,
			                IsOverride               = cd.IsOverride,
			                IsTax                    = cd.IsTax,
			                IsVolumeCharge           = cd.IsVolumeCharge,
			                IsWeightCharge           = cd.IsWeightCharge,
			                Label                    = cd.Label,
			                Max                      = cd.Max,
			                Min                      = cd.Min,
			                OverridenPackage         = cd.OverridenPackage,
			                SortIndex                = cd.SortIndex,
			                Text                     = cd.Text,
			                Value                    = cd.Value,
			                ExcludeFromPayroll       = cd.ExcludeFromPayroll
		                };

		return Charge;
	}
}