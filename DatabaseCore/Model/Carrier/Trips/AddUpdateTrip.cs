﻿using Ids1WebService;
using StorageV2.Carrier;
using Trip = Database.Model.Databases.MasterTemplate.Trip;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public (DateTime LastModified, bool Ok) AddUpdateTrip( TripUpdate t, bool needToTestCarrier = true )
	{
		var WasReusedTripId = false;
		var LastModified    = DateTime.UtcNow;
		var Retval          = ( LastModified, Ok: true );

		var IsNew = false;

		try
		{
			IsNew = t.TripId.IsNullOrWhiteSpace();

			t.TripId = IsNew ? Trip.NextTripId( Context ) : t.TripId.Trim();

			if( needToTestCarrier && CarrierPreAddUpdateTripHook( t ) )
				return Retval;

			var   TripCharges = JsonConvert.SerializeObject( t.TripCharges );
			Trip? Rec         = null;

			if( !IsNew )
			{
				Rec = ( from T in Entity.Trips
				        where T.TripId == t.TripId
				        select T ).FirstOrDefault();

				IsNew = Rec is null && t.Status1 is not STATUS.DELETED and not STATUS.FINALISED;
			}

			if( IsNew )
			{
				if( ( from Si in Entity.TripStorageIndices
				      where Si.TripId == t.TripId
				      select Si ).Any() )
				{
					Retval.LastModified = DateTime.MinValue;
					Retval.Ok           = false;
					WasReusedTripId     = true;
					throw new Exception( $"Program:{t.Program}, Reused trip Id:{t.TripId}" );
				}

				var Status3 = t.Status3 & ~STATUS2.DONT_UPDATE_POD & ~STATUS2.DONT_UPDATE_POP;

				var Trip = new Trip
				           {
					           TripId                     = t.TripId,
					           Location                   = t.Location,
					           ReceivedByDevice           = t.ReceivedByDevice,
					           ReadByDriver               = t.ReadByDriver,
					           Barcode                    = t.Barcode,
					           Board                      = t.Board,
					           CallTime                   = t.CallTime ?? DateTimeOffset.MinValue,
					           CallerName                 = t.CallerName,
					           CallerPhone                = t.CallerPhone.Max20(),
					           CallerEmail                = t.CallerEmail,
					           DangerousGoods             = t.DangerousGoods,
					           DriverCode                 = t.Driver,
					           DueTime                    = t.DueTime ?? DateTimeOffset.MinValue,
					           HasDangerousGoodsDocuments = t.HasDocuments,
					           LastModified               = LastModified,
					           Measurement                = t.Measurement,
					           MissingPieces              = t.MissingPieces,
					           Pieces                     = t.Pieces,
					           OriginalPieceCount         = t.OriginalPieceCount,
					           ReadyTime                  = t.ReadyTime ?? DateTimeOffset.MinValue,
					           Reference                  = t.Reference,
					           AccountId                  = t.AccountId,

					           Status1 = (int)t.Status1,
					           Status2 = (int)t.Status2,
					           Status3 = (int)Status3,
					           Status4 = (int)t.Status4,
					           Status5 = 0,
					           Status6 = 0,

					           UNClass      = t.UNClass,
					           Volume       = t.Volume,
					           Weight       = t.Weight,
					           PackageType  = t.PackageType.IsNotNull() ? t.PackageType : "",
					           ServiceLevel = t.ServiceLevel,
					           POP          = t.POP.Max50(),
					           POD          = t.POD.Max50(),

					           DeliveryCompanyId            = t.DeliveryCompanyId,
					           DeliveryCompanyAccountId     = t.DeliveryAccountId,
					           DeliveryAddressAddressLine1  = t.DeliveryAddressAddressLine1.Max100(),
					           DeliveryAddressAddressLine2  = t.DeliveryAddressAddressLine2.Max100(),
					           DeliveryAddressMobile1       = t.DeliveryAddressMobile,
					           DeliveryAddressPhone         = t.DeliveryAddressPhone.Max20(),
					           DeliveryAddressEmailAddress  = t.DeliveryAddressEmailAddress,
					           DeliveryAddressCity          = t.DeliveryAddressCity,
					           DeliveryAddressBarcode       = t.DeliveryAddressBarcode,
					           DeliveryAddressPostalCode    = t.DeliveryAddressPostalCode,
					           DeliveryAddressCountry       = t.DeliveryAddressCountry,
					           DeliveryAddressLatitude      = t.DeliveryAddressLatitude,
					           DeliveryAddressLongitude     = t.DeliveryAddressLongitude,
					           DeliveryAddressEmailAddress1 = t.DeliveryAddressEmailAddress1,
					           DeliveryAddressEmailAddress2 = t.DeliveryAddressEmailAddress2,
					           DeliveryAddressCountryCode   = t.DeliveryAddressCountryCode,
					           DeliveryAddressMobile        = t.DeliveryAddressMobile,
					           DeliveryAddressFax           = t.DeliveryAddressFax,
					           DeliveryAddressPhone1        = t.DeliveryAddressPhone1,
					           DeliveryAddressPostalBarcode = t.DeliveryAddressPostalBarcode,
					           DeliveryAddressRegion        = t.DeliveryAddressRegion,
					           DeliveryAddressSuite         = t.DeliveryAddressSuite.Max50(),
					           DeliveryAddressVicinity      = t.DeliveryAddressVicinity,
					           DeliveryCompanyName          = t.DeliveryCompanyName,
					           DeliveryContact              = t.DeliveryContact,
					           DeliveryAddressNotes         = t.DeliveryAddressNotes,
					           DeliveryNotes                = t.DeliveryNotes,

					           PickupCompanyId            = t.PickupCompanyId,
					           PickupCompanyAccountId     = t.PickupAccountId,
					           PickupAddressAddressLine1  = t.PickupAddressAddressLine1.Max100(),
					           PickupAddressAddressLine2  = t.PickupAddressAddressLine2.Max100(),
					           PickupAddressMobile1       = t.PickupAddressMobile,
					           PickupAddressPhone         = t.PickupAddressPhone.Max20(),
					           PickupAddressEmailAddress  = t.PickupAddressEmailAddress,
					           PickupAddressCity          = t.PickupAddressCity,
					           PickupAddressBarcode       = t.PickupAddressBarcode,
					           PickupAddressPostalCode    = t.PickupAddressPostalCode,
					           PickupAddressCountry       = t.PickupAddressCountry,
					           PickupAddressLatitude      = t.PickupAddressLatitude,
					           PickupAddressLongitude     = t.PickupAddressLongitude,
					           PickupAddressEmailAddress1 = t.PickupAddressEmailAddress1,
					           PickupAddressEmailAddress2 = t.PickupAddressEmailAddress2,
					           PickupAddressCountryCode   = t.PickupAddressCountryCode,
					           PickupAddressMobile        = t.PickupAddressMobile,
					           PickupAddressFax           = t.PickupAddressFax,
					           PickupAddressPhone1        = t.PickupAddressPhone1,
					           PickupAddressPostalBarcode = t.PickupAddressPostalBarcode,
					           PickupAddressRegion        = t.PickupAddressRegion,
					           PickupAddressSuite         = t.PickupAddressSuite.Max50(),
					           PickupAddressVicinity      = t.PickupAddressVicinity,
					           PickupCompanyName          = t.PickupCompanyName,
					           PickupContact              = t.PickupContact,
					           PickupAddressNotes         = t.PickupAddressNotes,
					           PickupNotes                = t.PickupNotes,

					           BillingCompanyAccountId     = t.BillingAccountId,
					           BillingAddressAddressLine1  = t.BillingAddressAddressLine1.Max100(),
					           BillingAddressAddressLine2  = t.BillingAddressAddressLine2.Max100(),
					           BillingAddressBarcode       = t.BillingAddressBarcode,
					           BillingAddressCity          = t.BillingAddressCity,
					           BillingAddressCountry       = t.BillingAddressCountry,
					           BillingAddressCountryCode   = t.BillingAddressCountryCode,
					           BillingAddressEmailAddress  = t.BillingAddressEmailAddress,
					           BillingAddressEmailAddress1 = t.BillingAddressEmailAddress1,
					           BillingAddressEmailAddress2 = t.BillingAddressEmailAddress2,
					           BillingAddressFax           = t.BillingAddressFax,
					           BillingAddressLatitude      = t.BillingAddressLatitude,
					           BillingAddressLongitude     = t.BillingAddressLongitude,
					           BillingAddressMobile        = t.BillingAddressMobile,
					           BillingAddressMobile1       = t.BillingAddressMobile1,
					           BillingAddressPhone         = t.BillingAddressPhone.Max20(),
					           BillingAddressPhone1        = t.BillingAddressPhone1,
					           BillingAddressPostalBarcode = t.BillingAddressPostalBarcode,
					           BillingAddressPostalCode    = t.BillingAddressPostalCode,
					           BillingAddressRegion        = t.BillingAddressRegion,
					           BillingAddressSuite         = t.BillingAddressSuite.Max50(),
					           BillingAddressVicinity      = t.BillingAddressVicinity,
					           BillingCompanyName          = t.BillingCompanyName,
					           BillingContact              = t.BillingContact,
					           BillingAddressNotes         = t.BillingAddressNotes,
					           BillingNotes                = t.BillingNotes,

					           CurrentZone        = t.CurrentZone,
					           PickupZone         = t.PickupZone,
					           DeliveryZone       = t.DeliveryZone,
					           PickupTime         = t.PickupTime ?? DateTimeOffset.MinValue,
					           PickupLatitude     = t.PickupLatitude,
					           PickupLongitude    = t.PickupLongitude,
					           DeliveredTime      = t.DeliveryTime ?? DateTimeOffset.MinValue,
					           DeliveredLatitude  = t.DeliveryLatitude,
					           DeliveredLongitude = t.DeliveryLongitude,

					           VerifiedTime      = t.VerifiedTime,
					           VerifiedLatitude  = t.VerifiedLatitude,
					           VerifiedLongitude = t.VerifiedLongitude,

					           ClaimedTime                = t.ClaimTime,
					           ClaimedLatitude            = t.ClaimLatitude,
					           ClaimedLongitude           = t.ClaimLongitude,
					           TripChargesAsJson          = JsonConvert.SerializeObject( t.TripCharges ),
					           IsQuote                    = t.IsQuote,
					           UndeliverableNotes         = t.UndeliverableNotes,
					           ExtensionType              = (short)t.ExtensionType,
					           ExtensionAsJson            = t.ExtensionAsJson,
					           TripItemsType              = (int)t.PackageTypes,
					           TripItemsAsJson            = t.PackagesAsJson,
					           FinalisedLatitude          = 0,
					           FinalisedLongitude         = 0,
					           FinalisedTime              = DateTimeOffset.UtcNow,
					           DriverCommissionCalculated = false,
					           Group0                     = t.Group0,
					           Group1                     = t.Group1,
					           Group2                     = t.Group2,
					           Group3                     = t.Group3,
					           Group4                     = t.Group4,
					           DevicePickupSortOrder      = 0,
					           DeviceDeliverySortOrder    = 0,
					           Route                      = t.Route,
					           CallTakerId                = t.CallTakerId,
					           DriverNotes                = t.DriverNotes,

					           PickupWaitTimeStart               = t.PickupWaitTimeStart,
					           PickupWaitTimeDurationInSeconds   = t.PickupWaitTimeInSeconds,
					           DeliveryWaitTimeStart             = t.DeliveryWaitTimeStart,
					           DeliveryWaitTimeDurationInSeconds = t.DeliveryWaitTimeInSeconds,

					           InvoiceNumber      = t.InvoiceNumber,
					           TotalAmount        = t.TotalAmount,
					           TotalTaxAmount     = t.TotalTaxAmount,
					           DiscountAmount     = t.DiscountAmount,
					           TotalFixedAmount   = t.TotalFixedAmount,
					           TotalPayrollAmount = t.TotalPayrollAmount,
					           DeliveryAmount     = t.DeliveryAmount,
					           Pallets            = t.Pallets,
					           Schedule           = ""
				           };
				Entity.Trips.Add( Trip );
				LogTrip( t.Program, Trip );
				Entity.SaveChanges();
			}
			else
			{
				switch( t.Status1 )
				{
				case STATUS.DELETED:
				case STATUS.FINALISED:
					{
						bool SaveFailed;

						do
						{
							SaveFailed = false;

							try
							{
								if( Rec is null ) // Probably already deleted
								{
									Retval.Ok = false;
									return Retval;
								}
								Entity.Trips.Remove( Rec );
								Entity.SaveChanges();
							}
							catch( DbUpdateConcurrencyException Exception )
							{
								RandomDelay();
								SaveFailed = true;

								// Get the current entity values and the values in the database
								var Entry          = Exception.Entries.Single();
								var DatabaseValues = Entry.GetDatabaseValues();

								//Record deleted ??
								if( DatabaseValues is null )
									break;
								Rec = (Trip)DatabaseValues.ToObject();
							}
						}
						while( SaveFailed );

						if( Rec is not null )
						{
							Rec.Status1 = (int)t.Status1;
							Trips.Add( Context, Rec.ToTrip() );

							var Status3 = (STATUS2)Rec.Status3 & ~STATUS2.DONT_UPDATE_POD & ~STATUS2.DONT_UPDATE_POP;

							var NdxRec = new TripStorageIndex
							             {
								             CallTime            = Rec.CallTime,
								             TripId              = Rec.TripId,
								             Reference           = Rec.Reference,
								             Status1             = (byte)Rec.Status1,
								             Status2             = (byte)Rec.Status2,
								             Status3             = (byte)Status3,
								             AccountId           = Rec.AccountId,
								             DeliveryCompanyName = Rec.DeliveryCompanyName,
								             PickupCompanyName   = Rec.PickupCompanyName,
								             Lastupdated         = LastModified,
								             PickupedTime        = Rec.PickupTime,
								             VerifiedTime        = Rec.VerifiedTime
							             };
							Entity.TripStorageIndices.Add( NdxRec );
							Entity.SaveChanges();
							Rec.LastModified = LastModified;

							if( t.Status1 == STATUS.DELETED )
								LogDeletedTrip( t.Program, Rec );
							else
								LogFinalisedTrip( t.Program, Rec );
						}
						break;
					}

				default:
					if( Rec is not null )
					{
						bool SaveFailed;
						var  Before = new TripLogger( Rec );

						if( t.Status1 == STATUS.DISPATCHED )
							t.Status2 = STATUS1.UNSET;

						var Status3 = t.Status3;

						var Pop = ( Status3 & STATUS2.DONT_UPDATE_POP ) == 0 ? t.POP : Rec.POP;
						var Pod = ( Status3 & STATUS2.DONT_UPDATE_POD ) == 0 ? t.POD : Rec.POD;

						Status3 = Status3 & ~STATUS2.DONT_UPDATE_POD & ~STATUS2.DONT_UPDATE_POP;

						do
						{
							try
							{
								Rec.Location                   = t.Location;
								Rec.ReceivedByDevice           = t.ReceivedByDevice;
								Rec.ReadByDriver               = t.ReadByDriver;
								Rec.Barcode                    = t.Barcode;
								Rec.Board                      = t.Board;
								Rec.CallTime                   = t is { IsCallTimeSpecified: true, CallTime: not null } ? t.CallTime.Value : Rec.CallTime;
								Rec.CallerName                 = t.CallerName;
								Rec.CallerPhone                = t.CallerPhone;
								Rec.CallerEmail                = t.CallerEmail;
								Rec.DangerousGoods             = t.DangerousGoods;
								Rec.DriverCode                 = t.Driver;
								Rec.DueTime                    = t is { DueTimeSpecified: true, DueTime: not null } ? t.DueTime.Value : Rec.DueTime;
								Rec.HasDangerousGoodsDocuments = t.HasDocuments;
								Rec.LastModified               = LastModified;
								Rec.Measurement                = t.Measurement;
								Rec.MissingPieces              = t.MissingPieces;
								Rec.OriginalPieceCount         = t.OriginalPieceCount;
								Rec.Pieces                     = t.Pieces;
								Rec.ReadyTime                  = t is { ReadyTimeSpecified: true, ReadyTime: not null } ? t.ReadyTime.Value : Rec.ReadyTime;
								Rec.Reference                  = t.Reference;
								Rec.AccountId                  = t.AccountId;

								Rec.Status1 = (int)t.Status1;
								Rec.Status2 = (int)t.Status2;
								Rec.Status3 = (int)Status3;
								Rec.Status4 = (int)t.Status4;
								Rec.Status5 = 0;
								Rec.Status6 = 0;

								Rec.UNClass      = t.UNClass;
								Rec.Volume       = t.Volume;
								Rec.Weight       = t.Weight;
								Rec.PackageType  = t.PackageType;
								Rec.ServiceLevel = t.ServiceLevel;
								Rec.POP          = Pop.Max50();
								Rec.POD          = Pod.Max50();

								Rec.DeliveryCompanyId            = t.DeliveryCompanyId;
								Rec.DeliveryCompanyAccountId     = t.DeliveryAccountId;
								Rec.DeliveryAddressAddressLine1  = t.DeliveryAddressAddressLine1.Max100();
								Rec.DeliveryAddressAddressLine2  = t.DeliveryAddressAddressLine2.Max100();
								Rec.DeliveryAddressMobile1       = t.DeliveryAddressMobile;
								Rec.DeliveryAddressPhone         = t.DeliveryAddressPhone.Max20();
								Rec.DeliveryAddressEmailAddress  = t.DeliveryAddressEmailAddress;
								Rec.DeliveryAddressCity          = t.DeliveryAddressCity;
								Rec.DeliveryAddressBarcode       = t.DeliveryAddressBarcode;
								Rec.DeliveryAddressPostalCode    = t.DeliveryAddressPostalCode;
								Rec.DeliveryAddressCountry       = t.DeliveryAddressCountry;
								Rec.DeliveryAddressLatitude      = t.DeliveryAddressLatitude;
								Rec.DeliveryAddressLongitude     = t.DeliveryAddressLongitude;
								Rec.DeliveryAddressEmailAddress1 = t.DeliveryAddressEmailAddress1;
								Rec.DeliveryAddressEmailAddress2 = t.DeliveryAddressEmailAddress2;
								Rec.DeliveryAddressCountryCode   = t.DeliveryAddressCountryCode;
								Rec.DeliveryAddressMobile        = t.DeliveryAddressMobile;
								Rec.DeliveryAddressFax           = t.DeliveryAddressFax;
								Rec.DeliveryAddressPhone1        = t.DeliveryAddressPhone1;
								Rec.DeliveryAddressPostalBarcode = t.DeliveryAddressPostalBarcode;
								Rec.DeliveryAddressRegion        = t.DeliveryAddressRegion;
								Rec.DeliveryAddressSuite         = t.DeliveryAddressSuite.Max50();
								Rec.DeliveryAddressVicinity      = t.DeliveryAddressVicinity;
								Rec.DeliveryCompanyName          = t.DeliveryCompanyName;
								Rec.DeliveryContact              = t.DeliveryContact;
								Rec.DeliveryAddressNotes         = t.DeliveryAddressNotes;
								Rec.DeliveryNotes                = t.DeliveryNotes;

								Rec.PickupCompanyId            = t.PickupCompanyId;
								Rec.PickupCompanyAccountId     = t.PickupAccountId;
								Rec.PickupAddressAddressLine1  = t.PickupAddressAddressLine1.Max100();
								Rec.PickupAddressAddressLine2  = t.PickupAddressAddressLine2.Max100();
								Rec.PickupAddressMobile1       = t.PickupAddressMobile;
								Rec.PickupAddressPhone         = t.PickupAddressPhone.Max20();
								Rec.PickupAddressEmailAddress  = t.PickupAddressEmailAddress;
								Rec.PickupAddressCity          = t.PickupAddressCity;
								Rec.PickupAddressBarcode       = t.PickupAddressBarcode;
								Rec.PickupAddressPostalCode    = t.PickupAddressPostalCode;
								Rec.PickupAddressCountry       = t.PickupAddressCountry;
								Rec.PickupAddressLatitude      = t.PickupAddressLatitude;
								Rec.PickupAddressLongitude     = t.PickupAddressLongitude;
								Rec.PickupAddressEmailAddress1 = t.PickupAddressEmailAddress1;
								Rec.PickupAddressEmailAddress2 = t.PickupAddressEmailAddress2;
								Rec.PickupAddressCountryCode   = t.PickupAddressCountryCode;
								Rec.PickupAddressMobile        = t.PickupAddressMobile;
								Rec.PickupAddressFax           = t.PickupAddressFax;
								Rec.PickupAddressPhone1        = t.PickupAddressPhone1;
								Rec.PickupAddressPostalBarcode = t.PickupAddressPostalBarcode;
								Rec.PickupAddressRegion        = t.PickupAddressRegion;
								Rec.PickupAddressSuite         = t.PickupAddressSuite.Max50();
								Rec.PickupAddressVicinity      = t.PickupAddressVicinity;
								Rec.PickupCompanyName          = t.PickupCompanyName;
								Rec.PickupContact              = t.PickupContact;
								Rec.PickupAddressNotes         = t.PickupAddressNotes;
								Rec.PickupNotes                = t.PickupNotes;

								Rec.BillingCompanyAccountId     = t.BillingAccountId;
								Rec.BillingAddressAddressLine1  = t.BillingAddressAddressLine1.Max100();
								Rec.BillingAddressAddressLine2  = t.BillingAddressAddressLine2.Max100();
								Rec.BillingAddressBarcode       = t.BillingAddressBarcode;
								Rec.BillingAddressCity          = t.BillingAddressCity;
								Rec.BillingAddressCountry       = t.BillingAddressCountry;
								Rec.BillingAddressCountryCode   = t.BillingAddressCountryCode;
								Rec.BillingAddressEmailAddress  = t.BillingAddressEmailAddress;
								Rec.BillingAddressEmailAddress1 = t.BillingAddressEmailAddress1;
								Rec.BillingAddressEmailAddress2 = t.BillingAddressEmailAddress2;
								Rec.BillingAddressFax           = t.BillingAddressFax;
								Rec.BillingAddressLatitude      = t.BillingAddressLatitude;
								Rec.BillingAddressLongitude     = t.BillingAddressLongitude;
								Rec.BillingAddressMobile        = t.BillingAddressMobile;
								Rec.BillingAddressMobile1       = t.BillingAddressMobile1;
								Rec.BillingAddressPhone         = t.BillingAddressPhone.Max20();
								Rec.BillingAddressPhone1        = t.BillingAddressPhone1;
								Rec.BillingAddressPostalBarcode = t.BillingAddressPostalBarcode;
								Rec.BillingAddressPostalCode    = t.BillingAddressPostalCode;
								Rec.BillingAddressRegion        = t.BillingAddressRegion;
								Rec.BillingAddressSuite         = t.BillingAddressSuite.Max50();
								Rec.BillingAddressVicinity      = t.BillingAddressVicinity;
								Rec.BillingCompanyName          = t.BillingCompanyName;
								Rec.BillingContact              = t.BillingContact;
								Rec.BillingAddressNotes         = t.BillingAddressNotes;
								Rec.BillingNotes                = t.BillingNotes;
								Rec.CurrentZone                 = t.CurrentZone;
								Rec.PickupZone                  = t.PickupZone;
								Rec.DeliveryZone                = t.DeliveryZone;
								Rec.PickupTime                  = t is { PickupTimeSpecified: true, PickupTime: not null } ? t.PickupTime.Value : Rec.PickupTime;
								Rec.PickupLatitude              = t.PickupLatitude;
								Rec.PickupLongitude             = t.PickupLongitude;
								Rec.DeliveredTime               = t is { DeliveryTimeSpecified: true, DeliveryTime: not null } ? t.DeliveryTime.Value : Rec.DeliveredTime;
								Rec.DeliveredLatitude           = t.DeliveryLatitude;
								Rec.DeliveredLongitude          = t.DeliveryLongitude;

								Rec.VerifiedTime      = t.VerifiedTime;
								Rec.VerifiedLatitude  = t.VerifiedLatitude;
								Rec.VerifiedLongitude = t.VerifiedLongitude;

								Rec.ClaimedTime                = t.ClaimTime;
								Rec.ClaimedLatitude            = t.ClaimLatitude;
								Rec.ClaimedLongitude           = t.ClaimLongitude;
								Rec.ExtensionType              = (short)t.ExtensionType;
								Rec.TripChargesAsJson          = TripCharges;
								Rec.IsQuote                    = t.IsQuote;
								Rec.UndeliverableNotes         = t.UndeliverableNotes;
								Rec.ExtensionAsJson            = t.ExtensionAsJson;
								Rec.TripItemsType              = (int)t.PackageTypes;
								Rec.TripItemsAsJson            = t.PackagesAsJson;
								Rec.FinalisedTime              = DateTimeOffset.UtcNow;
								Rec.FinalisedLatitude          = 0;
								Rec.FinalisedLongitude         = 0;
								Rec.DriverCommissionCalculated = false;
								Rec.Group0                     = t.Group0;
								Rec.Group1                     = t.Group1;
								Rec.Group2                     = t.Group2;
								Rec.Group3                     = t.Group3;
								Rec.Group4                     = t.Group4;
								Rec.DeviceDeliverySortOrder    = 0;
								Rec.DevicePickupSortOrder      = 0;
								Rec.Route                      = t.Route;
								Rec.CallTakerId                = t.CallTakerId;
								Rec.DriverNotes                = t.DriverNotes;

								Rec.PickupWaitTimeStart               = t.PickupWaitTimeStart;
								Rec.PickupWaitTimeDurationInSeconds   = t.PickupWaitTimeInSeconds;
								Rec.DeliveryWaitTimeStart             = t.DeliveryWaitTimeStart;
								Rec.DeliveryWaitTimeDurationInSeconds = t.DeliveryWaitTimeInSeconds;

								Rec.PickupArriveTime   = t.PickupArriveTime;
								Rec.DeliveryArriveTime = t.DeliveryArriveTime;

								Rec.InvoiceNumber      = t.InvoiceNumber;
								Rec.TotalAmount        = t.TotalAmount;
								Rec.TotalTaxAmount     = t.TotalTaxAmount;
								Rec.DiscountAmount     = t.DiscountAmount;
								Rec.TotalFixedAmount   = t.TotalFixedAmount;
								Rec.TotalPayrollAmount = t.TotalPayrollAmount;
								Rec.DeliveryAmount     = t.DeliveryAmount;
								Rec.Pallets            = t.Pallets;

								Entity.SaveChanges();
								SaveFailed = false;
							}
							catch( DbUpdateConcurrencyException )
							{
								RandomDelay();
								SaveFailed = true;

								Rec = ( from T in Entity.Trips
								        where T.TripId == t.TripId
								        select T ).FirstOrDefault();

								if( Rec is null ) // Probably Deleted
								{
									Retval.Ok = false;
									return Retval;
								}
							}
						}
						while( SaveFailed );
						var After = new TripLogger( Rec );
						LogTrip( t.Program, Before, After );
					}
					else
						throw new Exception( "Unexpected null" );
					break;
				}
			}
		}
		catch( Exception E )
		{
			if( !WasReusedTripId || !Context.DisableReusedTripIds )
				Context.SystemLogExceptionProgram( t.Program, new Exception( $"Trip Id: {t.TripId}\r\n{E.Message}" ) );
			Retval.Ok = false;
		}
		finally
		{
			if( Retval.Ok && Context.SendToIds1 )
			{
				List<Signature> Sigs;

				var Status = t.S1;

				if( Status is STATUS.PICKED_UP or STATUS.DELIVERED or STATUS.VERIFIED )
				{
					var Signatures = new Signatures( Context ).GetSignatures( t.TripId );

					Sigs = ( from S in Signatures
					         where S.S == Status
					         orderby S.Date descending
					         select S ).ToList();
				}
				else
					Sigs = [];

				IDSClient.Ids1.UpdateTrip( Context, IsNew, TransferRecords.Extensions.ToTransferRecord( t, Sigs, Context.CarrierId ) );
			}
		}
		return Retval;
	}

#region Auto Create Driver
	public void AddUpdateTripAutoCreate( TripUpdate trip, bool updateAddress )
	{
		if( ( trip.Status1 >= STATUS.DISPATCHED ) && trip.Driver.IsNotNullOrWhiteSpace() )
			CreateDriver( trip.Program, trip.Driver );

		if( updateAddress )
			trip = AddCompanyAddress( trip );

		AddUpdateTrip( trip );
	}

	public void AddUpdateTripAutoCreateDontUpdateAddresses( TripUpdate trip )
	{
		AddUpdateTripAutoCreate( trip, false );
	}
#endregion
}