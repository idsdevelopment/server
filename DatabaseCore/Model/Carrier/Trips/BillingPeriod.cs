﻿using Trip = Database.Model.Databases.MasterTemplate.Trip;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public TripList GetTripsByBillingPeriod( GetTripsByBillingPeriod r )
	{
		var E = Entity;

		var ByCallTime = r.SearchBy == Protocol.Data.GetTripsByBillingPeriod.SEARCH_BY.CALL_DATE;
		var Co         = r.BillingCompanyName;

		List<Trip> Trips = ( from T in E.Trips
							 where ( ( ByCallTime && ( T.CallTime >= r.FromDate ) && ( T.CallTime <= r.ToDate ) )
									 || ( !ByCallTime && ( T.DeliveredTime >= r.FromDate ) && ( T.DeliveredTime <= r.ToDate ) ) )
								   && ( T.Status1 >= (int)r.FromStatus ) && ( T.Status1 <= (int)r.ToStatus )
								   && ( ( Co == "" ) || ( Co == T.BillingCompanyName ) )
								   && ( T.InvoiceNumber <= 0 )
							 select T ).ToList();

		if( Trips.Count != 0 )
		{
			var Companies = ( from T in Trips
							  let BillingCompanyName = T.BillingCompanyName.Trim()
							  where BillingCompanyName != ""
							  select BillingCompanyName ).Distinct().ToList();

			var BillingCompanies = ( from C in E.Companies
									 where Companies.Contains( C.CompanyName ) && ( C.BillingPeriod >= r.FromBillingPeriod ) && ( C.BillingPeriod <= r.ToBillingPeriod )
									 select C.CompanyName ).Distinct().ToList();

			var BillableTrips = ( from T in Trips
								  where BillingCompanies.Contains( T.BillingCompanyName )
								  select T ).ToList();

			return new TripList( from T in BillableTrips
								 orderby T.CallTime
								 select T.ToTrip() );
		}
		return [];
	}
}