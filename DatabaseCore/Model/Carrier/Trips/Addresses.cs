﻿using Address = Database.Model.Databases.MasterTemplate.Address;
using Company = Protocol.Data.Company;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public TripUpdate AddCompanyAddress( TripUpdate trip )
	{
		Task<Address?> AddCompanyAddressAsync( string barcode, string companyName,
		                                       string suite, string addressLine1, string addressLine2, string city, string region, string country, string countryCode, string vicinity,
		                                       string postalCode, string postalBarcode,
		                                       string emailAddress, string emailAddress1, string emailAddress2, string fax,
		                                       string mobile, string mobile1, string phone, string phone1,
		                                       decimal latitude, decimal longitude,
		                                       string notes )
		{
			var SendContext = Context.Clone();

			return Task.Run( () =>
			                 {
				                 if( barcode.IsNotNullOrWhiteSpace() )
				                 {
					                 try
					                 {
						                 using var Db1     = new CarrierDb( SendContext );
						                 var       Company = Db1.GetResellerCompanyByLocation( trip.AccountId, barcode );

						                 if( Company is null )
						                 {
							                 Db1.AddResellerCompany( new AddCustomerCompany( trip.Program )
							                                         {
								                                         CustomerCode = trip.AccountId,

								                                         Company = new Company
								                                                   {
									                                                   CompanyName     = companyName,
									                                                   CompanyNumber   = "",
									                                                   Suite           = suite,
									                                                   AddressLine1    = addressLine1,
									                                                   AddressLine2    = addressLine2,
									                                                   Barcode         = barcode,
									                                                   City            = city,
									                                                   Country         = country,
									                                                   CountryCode     = countryCode,
									                                                   EmailAddress    = emailAddress,
									                                                   EmailAddress1   = emailAddress1,
									                                                   EmailAddress2   = emailAddress2,
									                                                   Enabled         = true,
									                                                   Fax             = fax,
									                                                   Latitude        = latitude,
									                                                   Longitude       = longitude,
									                                                   LocationBarcode = barcode,
									                                                   Mobile          = mobile,
									                                                   Mobile1         = mobile1,
									                                                   Notes           = notes,
									                                                   Password        = "",
									                                                   Phone           = phone,
									                                                   Phone1          = phone1,
									                                                   PostalCode      = postalCode.TrimMax20(),
									                                                   PostalBarcode   = postalBarcode,
									                                                   Region          = region,
									                                                   UserName        = "",
									                                                   Vicinity        = vicinity
								                                                   }
							                                         } );

							                 Company = Db1.GetResellerCompanyByLocation( trip.AccountId, barcode );

							                 if( Company is null )
								                 return null;
						                 }

						                 var Temp = Db1.GetCompanyAddresses( Company.CompanyId );
						                 return Temp.Count > 0 ? Temp[ 0 ] : null;
					                 }
					                 catch( Exception Exception )
					                 {
						                 SendContext.SystemLogExceptionProgram( trip.Program, Exception );
					                 }
				                 }
				                 return null;
			                 } );
		}

		try
		{
			var Tasks = new Task<Address?>[ 3 ];

			Tasks[ 0 ] = AddCompanyAddressAsync( trip.PickupAddressBarcode, trip.PickupCompanyName,
			                                     trip.PickupAddressSuite, trip.PickupAddressAddressLine1, trip.PickupAddressAddressLine2, trip.PickupAddressCity, trip.PickupAddressRegion, trip.PickupAddressCountry, trip.PickupAddressCountryCode,
			                                     trip.PickupAddressVicinity,
			                                     trip.PickupAddressPostalCode, trip.PickupAddressPostalBarcode,
			                                     trip.PickupAddressEmailAddress, trip.PickupAddressEmailAddress1, trip.PickupAddressEmailAddress2,
			                                     trip.PickupAddressFax, trip.PickupAddressMobile, trip.PickupAddressMobile1, trip.PickupAddressPhone, trip.PickupAddressPhone1,
			                                     trip.PickupAddressLatitude, trip.PickupAddressLongitude,
			                                     trip.PickupAddressNotes );

			Tasks[ 1 ] = AddCompanyAddressAsync( trip.DeliveryAddressBarcode, trip.DeliveryCompanyName,
			                                     trip.DeliveryAddressSuite, trip.DeliveryAddressAddressLine1, trip.DeliveryAddressAddressLine2, trip.DeliveryAddressCity, trip.DeliveryAddressRegion, trip.DeliveryAddressCountry, trip.DeliveryAddressCountryCode,
			                                     trip.DeliveryAddressVicinity,
			                                     trip.DeliveryAddressPostalCode, trip.DeliveryAddressPostalBarcode,
			                                     trip.DeliveryAddressEmailAddress, trip.DeliveryAddressEmailAddress1, trip.DeliveryAddressEmailAddress2,
			                                     trip.DeliveryAddressFax, trip.DeliveryAddressMobile, trip.DeliveryAddressMobile1, trip.DeliveryAddressPhone, trip.DeliveryAddressPhone1,
			                                     trip.DeliveryAddressLatitude, trip.DeliveryAddressLongitude,
			                                     trip.DeliveryAddressNotes );

			Tasks[ 2 ] = AddCompanyAddressAsync( trip.BillingAddressBarcode, trip.BillingCompanyName,
			                                     trip.BillingAddressSuite, trip.BillingAddressAddressLine1, trip.BillingAddressAddressLine2, trip.BillingAddressCity, trip.BillingAddressRegion, trip.BillingAddressCountry,
			                                     trip.BillingAddressCountryCode,
			                                     trip.BillingAddressVicinity,
			                                     trip.BillingAddressPostalCode, trip.BillingAddressPostalBarcode,
			                                     trip.BillingAddressEmailAddress, trip.BillingAddressEmailAddress1, trip.BillingAddressEmailAddress2,
			                                     trip.BillingAddressFax, trip.BillingAddressMobile, trip.BillingAddressMobile1, trip.BillingAddressPhone, trip.BillingAddressPhone1,
			                                     trip.BillingAddressLatitude, trip.BillingAddressLongitude,
			                                     trip.BillingAddressNotes );

			Task.WaitAll( Tasks );

			var A = Tasks[ 0 ].Result;

			if( A is not null )
			{
				trip.PickupAddressSuite         = A.Suite;
				trip.PickupAddressAddressLine1  = A.AddressLine1;
				trip.PickupAddressAddressLine2  = A.AddressLine2;
				trip.PickupAddressCity          = A.City;
				trip.PickupAddressRegion        = A.Region;
				trip.PickupAddressCountry       = A.Country;
				trip.PickupAddressCountryCode   = A.CountryCode;
				trip.PickupAddressVicinity      = A.Vicinity;
				trip.PickupAddressPostalCode    = A.PostalCode;
				trip.PickupAddressPostalBarcode = A.PostalBarcode;
				trip.PickupAddressEmailAddress  = A.EmailAddress;
				trip.PickupAddressEmailAddress1 = A.EmailAddress1;
				trip.PickupAddressEmailAddress2 = A.EmailAddress2;
				trip.PickupAddressFax           = A.Fax;
				trip.PickupAddressMobile        = A.Mobile;
				trip.PickupAddressMobile1       = A.Mobile1;
				trip.PickupAddressPhone         = A.Phone;
				trip.PickupAddressPhone1        = A.Phone1;
			}

			A = Tasks[ 1 ].Result;

			if( A is not null )
			{
				trip.DeliveryAddressSuite         = A.Suite;
				trip.DeliveryAddressAddressLine1  = A.AddressLine1;
				trip.DeliveryAddressAddressLine2  = A.AddressLine2;
				trip.DeliveryAddressCity          = A.City;
				trip.DeliveryAddressRegion        = A.Region;
				trip.DeliveryAddressCountry       = A.Country;
				trip.DeliveryAddressCountryCode   = A.CountryCode;
				trip.DeliveryAddressVicinity      = A.Vicinity;
				trip.DeliveryAddressPostalCode    = A.PostalCode;
				trip.DeliveryAddressPostalBarcode = A.PostalBarcode;
				trip.DeliveryAddressEmailAddress  = A.EmailAddress;
				trip.DeliveryAddressEmailAddress1 = A.EmailAddress1;
				trip.DeliveryAddressEmailAddress2 = A.EmailAddress2;
				trip.DeliveryAddressFax           = A.Fax;
				trip.DeliveryAddressMobile        = A.Mobile;
				trip.DeliveryAddressMobile1       = A.Mobile1;
				trip.DeliveryAddressPhone         = A.Phone;
				trip.DeliveryAddressPhone1        = A.Phone1;
			}

			A = Tasks[ 2 ].Result;

			if( A is not null )
			{
				trip.BillingAddressSuite         = A.Suite;
				trip.BillingAddressAddressLine1  = A.AddressLine1;
				trip.BillingAddressAddressLine2  = A.AddressLine2;
				trip.BillingAddressCity          = A.City;
				trip.BillingAddressRegion        = A.Region;
				trip.BillingAddressCountry       = A.Country;
				trip.BillingAddressCountryCode   = A.CountryCode;
				trip.BillingAddressVicinity      = A.Vicinity;
				trip.BillingAddressPostalCode    = A.PostalCode;
				trip.BillingAddressPostalBarcode = A.PostalBarcode;
				trip.BillingAddressEmailAddress  = A.EmailAddress;
				trip.BillingAddressEmailAddress1 = A.EmailAddress1;
				trip.BillingAddressEmailAddress2 = A.EmailAddress2;
				trip.BillingAddressFax           = A.Fax;
				trip.BillingAddressMobile        = A.Mobile;
				trip.BillingAddressMobile1       = A.Mobile1;
				trip.BillingAddressPhone         = A.Phone;
				trip.BillingAddressPhone1        = A.Phone1;
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogExceptionProgram( trip.Program, Exception );
		}
		return trip;
	}
}