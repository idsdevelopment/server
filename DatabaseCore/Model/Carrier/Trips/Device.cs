﻿using Ids1WebService;
using StorageV2.Carrier;
using TransferRecords;
using Trip = Database.Model.Databases.MasterTemplate.Trip;
using TripCharge = Protocol.Data.TripCharge;

namespace Database.Model.Databases.Carrier;

public static class Extensions
{
	public static TripTransferRecord ToTransferRecord( this Trip t, string program, string carrierId, Signature? signature = null ) => new( false,
	                                                                                                                                        program,
	                                                                                                                                        t.Location,
	                                                                                                                                        false,
	                                                                                                                                        false,
	                                                                                                                                        false,
	                                                                                                                                        t.TripId,
	                                                                                                                                        t.Barcode,
	                                                                                                                                        t.AccountId,
	                                                                                                                                        (STATUS)t.Status1,
	                                                                                                                                        (STATUS1)t.Status2,
	                                                                                                                                        (STATUS2)t.Status3,
	                                                                                                                                        t.Reference,
	                                                                                                                                        t.CallTime,
	                                                                                                                                        true,
	                                                                                                                                        t.CallerPhone,
	                                                                                                                                        t.CallerEmail,
	                                                                                                                                        t.PickupTime,
	                                                                                                                                        true,
	                                                                                                                                        t.PickupLatitude,
	                                                                                                                                        t.PickupLongitude,
	                                                                                                                                        t.DeliveredTime,
	                                                                                                                                        true,
	                                                                                                                                        (double)t.DeliveryAddressLatitude,
	                                                                                                                                        (double)t.DeliveryAddressLongitude,
	                                                                                                                                        t.VerifiedTime,
	                                                                                                                                        t.VerifiedLatitude,
	                                                                                                                                        t.VerifiedLongitude,
	                                                                                                                                        t.ClaimedTime,
	                                                                                                                                        t.ClaimedLatitude,
	                                                                                                                                        t.ClaimedLongitude,
	                                                                                                                                        t.ReadyTime,
	                                                                                                                                        true,
	                                                                                                                                        t.DueTime,
	                                                                                                                                        true,
	                                                                                                                                        t.DriverCode,
	                                                                                                                                        t.Weight,
	                                                                                                                                        t.Pieces,
	                                                                                                                                        t.PackageType,
	                                                                                                                                        t.ServiceLevel,
	                                                                                                                                        t.Volume,
	                                                                                                                                        t.Measurement,
	                                                                                                                                        t.OriginalPieceCount,
	                                                                                                                                        t.MissingPieces,
	                                                                                                                                        t.DangerousGoods,
	                                                                                                                                        t.UNClass,
	                                                                                                                                        t.HasDangerousGoodsDocuments,
	                                                                                                                                        t.Board,
	                                                                                                                                        t.POP,
	                                                                                                                                        t.POD,
	                                                                                                                                        t.PickupCompanyName,
	                                                                                                                                        t.PickupCompanyName,
	                                                                                                                                        t.PickupAddressBarcode,
	                                                                                                                                        t.PickupAddressPostalBarcode,
	                                                                                                                                        t.PickupAddressSuite,
	                                                                                                                                        t.PickupAddressAddressLine1,
	                                                                                                                                        t.PickupAddressAddressLine2,
	                                                                                                                                        t.PickupAddressVicinity,
	                                                                                                                                        t.PickupAddressCity,
	                                                                                                                                        t.PickupAddressRegion,
	                                                                                                                                        t.PickupAddressPostalCode,
	                                                                                                                                        t.PickupAddressCountry,
	                                                                                                                                        t.PickupAddressCountryCode,
	                                                                                                                                        t.PickupAddressPhone,
	                                                                                                                                        t.PickupAddressPhone1,
	                                                                                                                                        t.PickupAddressMobile,
	                                                                                                                                        t.PickupAddressMobile1,
	                                                                                                                                        t.PickupAddressFax,
	                                                                                                                                        t.PickupAddressEmailAddress,
	                                                                                                                                        t.PickupAddressEmailAddress1,
	                                                                                                                                        t.PickupAddressEmailAddress2,
	                                                                                                                                        t.PickupContact,
	                                                                                                                                        t.PickupAddressLatitude,
	                                                                                                                                        t.PickupAddressLongitude,
	                                                                                                                                        t.PickupAddressNotes,
	                                                                                                                                        t.PickupNotes,
	                                                                                                                                        t.DeliveryCompanyName,
	                                                                                                                                        t.DeliveryCompanyName,
	                                                                                                                                        t.DeliveryAddressBarcode,
	                                                                                                                                        t.DeliveryAddressPostalBarcode,
	                                                                                                                                        t.DeliveryAddressSuite,
	                                                                                                                                        t.DeliveryAddressAddressLine1,
	                                                                                                                                        t.DeliveryAddressAddressLine2,
	                                                                                                                                        t.DeliveryAddressVicinity,
	                                                                                                                                        t.DeliveryAddressCity,
	                                                                                                                                        t.DeliveryAddressRegion,
	                                                                                                                                        t.DeliveryAddressPostalCode,
	                                                                                                                                        t.DeliveryAddressCountry,
	                                                                                                                                        t.DeliveryAddressCountryCode,
	                                                                                                                                        t.DeliveryAddressPhone,
	                                                                                                                                        t.DeliveryAddressPhone1,
	                                                                                                                                        t.DeliveryAddressMobile,
	                                                                                                                                        t.DeliveryAddressMobile1,
	                                                                                                                                        t.DeliveryAddressFax,
	                                                                                                                                        t.DeliveryAddressEmailAddress,
	                                                                                                                                        t.DeliveryAddressEmailAddress1,
	                                                                                                                                        t.DeliveryAddressEmailAddress2,
	                                                                                                                                        t.DeliveryContact,
	                                                                                                                                        t.DeliveryAddressLatitude,
	                                                                                                                                        t.DeliveryAddressLongitude,
	                                                                                                                                        t.DeliveryAddressNotes,
	                                                                                                                                        t.DeliveryNotes,
	                                                                                                                                        t.BillingCompanyName,
	                                                                                                                                        t.BillingCompanyName,
	                                                                                                                                        t.BillingAddressBarcode,
	                                                                                                                                        t.BillingAddressPostalBarcode,
	                                                                                                                                        t.BillingAddressSuite,
	                                                                                                                                        t.BillingAddressAddressLine1,
	                                                                                                                                        t.BillingAddressAddressLine2,
	                                                                                                                                        t.BillingAddressVicinity,
	                                                                                                                                        t.BillingAddressCity,
	                                                                                                                                        t.BillingAddressRegion,
	                                                                                                                                        t.BillingAddressPostalCode,
	                                                                                                                                        t.BillingAddressCountry,
	                                                                                                                                        t.BillingAddressCountryCode,
	                                                                                                                                        t.BillingAddressPhone,
	                                                                                                                                        t.BillingAddressPhone1,
	                                                                                                                                        t.BillingAddressMobile,
	                                                                                                                                        t.BillingAddressMobile1,
	                                                                                                                                        t.BillingAddressFax,
	                                                                                                                                        t.BillingAddressEmailAddress,
	                                                                                                                                        t.BillingAddressEmailAddress1,
	                                                                                                                                        t.BillingAddressEmailAddress2,
	                                                                                                                                        t.BillingContact,
	                                                                                                                                        t.BillingAddressLatitude,
	                                                                                                                                        t.BillingAddressLongitude,
	                                                                                                                                        t.BillingAddressNotes,
	                                                                                                                                        t.BillingNotes,
	                                                                                                                                        t.ReceivedByDevice,
	                                                                                                                                        t.ReadByDriver,
	                                                                                                                                        t.CurrentZone,
	                                                                                                                                        t.PickupZone,
	                                                                                                                                        t.DeliveryZone,
	                                                                                                                                        new List<TripCharge>(),
	                                                                                                                                        t.CallerName,
	                                                                                                                                        t.IsQuote,
	                                                                                                                                        t.UndeliverableNotes,
	                                                                                                                                        t.ExtensionType,
	                                                                                                                                        t.ExtensionAsJson,
	                                                                                                                                        t.LastModified,
	                                                                                                                                        t.Group0,
	                                                                                                                                        t.Group1,
	                                                                                                                                        t.Group2,
	                                                                                                                                        t.Group3,
	                                                                                                                                        t.Group4,
	                                                                                                                                        signature is null ? [] : new List<Signature> { signature },
	                                                                                                                                        t.CallTakerId,
	                                                                                                                                        carrierId );
}

public partial class CarrierDb
{
	// Returns the board and device status
	public TripUpdateMessage TripReceivedOrRead( string program, string tripId, bool isReceived )
	{
		var E = Entity;

		try
		{
			while( true )
			{
				var Rec = ( from T in E.Trips
				            where T.TripId == tripId
				            select T ).FirstOrDefault();

				if( Rec is not null )
				{
					var Before = new TripLogger( Rec );

					Rec.LastModified = DateTime.UtcNow;

					if( isReceived )
						Rec.ReceivedByDevice = true;
					else
						Rec.ReadByDriver = true;

					try
					{
						E.SaveChanges();
					}
					catch( DbUpdateConcurrencyException )
					{
						RandomDelay();
						continue;
					}

					var After = new TripLogger( Rec );

					LogTrip( program, Before, After );

					if( Context.SendToIds1 )
						IDSClient.Ids1.UpdateTrip( Context, false, Rec.ToTransferRecord( program, Context.CarrierId ) );

					return BuildMessage( program, TripUpdateMessage.ACTION.UPDATE_STATUS, Rec );
				}
				break;
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return new TripUpdateMessage();
	}

	public TripUpdateMessage TripReceivedByDevice( string program, string tripId ) => TripReceivedOrRead( program, tripId, true );

	public TripUpdateMessage TripReadByDriver( string program, string tripId ) => TripReceivedOrRead( program, tripId, false );

	public List<Protocol.Data.Trip> GetTripsForDriver( ref string driverCode )
	{
		if( driverCode.IsNullOrWhiteSpace() )
			driverCode = Context.UserName;

		var Dc = driverCode.Trim();

		List<Protocol.Data.Trip> Result = [];

		try
		{
			foreach( var Trip in from T in Entity.Trips
			                     where ( T.DriverCode == Dc ) && ( ( T.Status1 == (int)STATUS.DISPATCHED ) || ( T.Status1 == (int)STATUS.PICKED_UP ) )
			                     select T )
			{
				if( ( (STATUS2)Trip.Status3 & STATUS2.DONT_SEND_TO_DRIVER ) == 0 )
					Result.Add( Trip.ToTrip() );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}

	public List<TripUpdateMessage> UpdateTripFromDevice( DeviceTripUpdate u )
	{
		try
		{
			u.PopPod = u.PopPod.Max50();

			var TripId = u.TripId;

			var E = Entity;

			var Rec = ( from T in E.Trips
			            where T.TripId == TripId
			            select T ).FirstOrDefault();

			if( Rec is not null )
			{
				var Dp = DevicePreferences();

				if( !Dp.EditPiecesWeight )
				{
					u.Pieces = Rec.Pieces;
					u.Weight = Rec.Weight;
				}

				if( !Dp.EditReference )
					u.Reference = Rec.Reference;

				TripLogger After,
				           Before = new( Rec );

				Rec.LastModified = DateTime.UtcNow;

				var IsUndeliverable = ( u.Status1 & STATUS1.UNDELIVERED ) != 0;
				var NeedNewTrip     = false;

				var Diff      = u.Pieces - Rec.Pieces;
				var WasPickup = u.Status < STATUS.PICKED_UP;

				// Underscan / Overscan
				if( Diff != 0 )
				{
					// Must have been undeliverable
					if( u.Pieces <= 0 )
					{
						u.Status1       |= STATUS1.UNDELIVERED;
						IsUndeliverable =  true;
					}

					// Extra picked up / delivered
					else if( Diff > 0 )
						Rec.BillingNotes = $"{Diff:N0} extra piece(s) {( WasPickup ? "picked up" : "delivered" )}.\r\n{Rec.BillingNotes}".Trim();

					// Unpickupable / Undeliverable
					else
						NeedNewTrip = Dp.CreateNewTripForPieces;
				}

				var Status2 = STATUS2.UNSET;

				if( IsUndeliverable )
				{
					if( u.Status < STATUS.PICKED_UP ) // Not Picked Up
					{
						if( Dp.DeleteCannotPickup )
						{
							Rec.UndeliverableNotes = $"Deleted by preference setting\r\n{Rec.UndeliverableNotes}";
							Rec.BillingNotes       = $"Deleted by preference setting\r\n{Rec.BillingNotes}";
							Rec.Status1            = (int)u.Status;
							Rec.Status2            = (int)u.Status1;

							DeleteTrip( u.Program, Rec, Rec.LastModified );

							After = new TripLogger( Rec );
							LogTrip( u.Program, Before, After );

							return [BuildMessage( u.Program, TripUpdateMessage.ACTION.UPDATE_STATUS, Rec )];
						}
						var Text = $"Could not pickup: {u.UpdateTime:f} {u.UpdateTime:zz}\r\n";

						Rec.UndeliverableNotes =  $"{Text}{Rec.UndeliverableNotes}";
						Rec.BillingNotes       =  $"{Text}{Rec.BillingNotes}";
						Status2                |= STATUS2.DONT_SEND_TO_DRIVER | STATUS2.WAS_UNDELIVERABLE;
					}
				}

				Rec.Status1 =  (int)u.Status;
				Rec.Status2 =  (int)u.Status1;
				Rec.Status3 |= (int)Status2;

				switch( u.Status )
				{
				case STATUS.PICKED_UP:
					Rec.POP                             = u.PopPod;
					Rec.PickupTime                      = u.UpdateTime;
					Rec.PickupLatitude                  = u.UpdateLatitude;
					Rec.PickupLongitude                 = u.UpdateLongitude;
					Rec.PickupWaitTimeStart             = u.WaitTimeStarTime;
					Rec.PickupWaitTimeDurationInSeconds = u.WaitTimeInSeconds;
					Rec.PickupArriveTime                = u.ArriveTime;
					Rec.Status3                         = (int)( (STATUS2)Rec.Status3 & ~( STATUS2.WAS_PICKED_UP | STATUS2.WAS_UNDELIVERABLE ) );
					goto case STATUS.DISPATCHED;

				case STATUS.DISPATCHED:
					if( ( u.Status1 & STATUS1.CLAIMED ) != 0 )
					{
						Rec.ClaimedTime     = u.UpdateTime;
						Rec.ClaimedLatitude = u.UpdateLatitude;
						Rec.ClaimedLatitude = u.UpdateLongitude;
					}
					break;

				case STATUS.DELIVERED:
					Rec.POD                               = u.PopPod;
					Rec.DeliveredTime                     = u.UpdateTime;
					Rec.DeliveredLatitude                 = u.UpdateLatitude;
					Rec.DeliveredLongitude                = u.UpdateLongitude;
					Rec.DeliveryWaitTimeStart             = u.WaitTimeStarTime;
					Rec.DeliveryWaitTimeDurationInSeconds = u.WaitTimeInSeconds;
					Rec.DeliveryArriveTime                = u.ArriveTime;
					Rec.Status3                           = (int)( (STATUS2)Rec.Status3 & ~STATUS2.WAS_UNDELIVERABLE );
					break;

				case STATUS.VERIFIED:
					Rec.POD                               = u.PopPod;
					Rec.VerifiedTime                      = u.UpdateTime;
					Rec.VerifiedLatitude                  = u.UpdateLatitude;
					Rec.VerifiedLongitude                 = u.UpdateLongitude;
					Rec.DeliveryWaitTimeStart             = u.WaitTimeStarTime;
					Rec.DeliveryWaitTimeDurationInSeconds = u.WaitTimeInSeconds;
					Rec.DeliveryArriveTime                = u.ArriveTime;
					Rec.Status3                           = (int)( (STATUS2)Rec.Status3 & ~STATUS2.WAS_UNDELIVERABLE );
					break;
				}

				if( u.Pieces > 0 )
					Rec.Pieces = u.Pieces;

				Rec.Weight = u.Weight;

				Rec.UndeliverableNotes = u.UndeliverableNotes;

				Rec.Reference   = u.Reference;
				Rec.DriverNotes = u.DriverNotes;

				while( true )
				{
					try
					{
						E.SaveChanges();
						break;
					}
					catch( DbUpdateConcurrencyException )
					{
						RandomDelay();

						var DbRec = ( from T in E.Trips
						              where T.TripId == TripId
						              select T ).FirstOrDefault();

						if( DbRec is null ) // Deleted
							break;

						Before = new TripLogger( DbRec );

						DbRec.LastModified = DateTime.UtcNow;

						DbRec.Status1 = Rec.Status1;
						DbRec.Status2 = Rec.Status2;

						DbRec.ReceivedByDevice |= Rec.ReceivedByDevice;
						DbRec.ReadByDriver     |= Rec.ReadByDriver;

						DbRec.Pieces    = Rec.Pieces;
						DbRec.Weight    = Rec.Weight;
						DbRec.Reference = Rec.Reference;

						DbRec.PickupAddressNotes = Rec.PickupAddressNotes;

						DbRec.DeliveryAddressNotes = Rec.DeliveryAddressNotes;
						DbRec.BillingAddressNotes  = Rec.BillingAddressNotes;
						DbRec.UndeliverableNotes   = Rec.UndeliverableNotes;

						DbRec.PickupNotes   = Rec.PickupNotes;
						DbRec.DeliveryNotes = Rec.DeliveryNotes;
						DbRec.BillingNotes  = Rec.BillingNotes;

						DbRec.POP             = Rec.POP;
						DbRec.PickupTime      = Rec.PickupTime;
						DbRec.PickupLatitude  = Rec.PickupLatitude;
						DbRec.PickupLongitude = Rec.PickupLongitude;

						DbRec.POD                = Rec.POD;
						DbRec.DeliveredTime      = Rec.DeliveredTime;
						DbRec.DeliveredLatitude  = Rec.DeliveredLatitude;
						DbRec.DeliveredLongitude = Rec.DeliveredLongitude;

						DbRec.VerifiedTime      = Rec.VerifiedTime;
						DbRec.VerifiedLatitude  = Rec.VerifiedLatitude;
						DbRec.VerifiedLongitude = Rec.VerifiedLongitude;

						DbRec.ClaimedTime     = Rec.ClaimedTime;
						DbRec.ClaimedLatitude = Rec.ClaimedLatitude;
						DbRec.ClaimedLatitude = Rec.ClaimedLatitude;

						DbRec.DeliveryWaitTimeDurationInSeconds = Rec.DeliveryWaitTimeDurationInSeconds;
						DbRec.DeliveryWaitTimeStart             = Rec.DeliveryWaitTimeStart;

						DbRec.PickupWaitTimeDurationInSeconds = Rec.PickupWaitTimeDurationInSeconds;
						DbRec.PickupWaitTimeStart             = Rec.PickupWaitTimeStart;

						DbRec.PickupArriveTime   = Rec.PickupArriveTime;
						DbRec.DeliveryArriveTime = Rec.DeliveryArriveTime;

						DbRec.DriverNotes = Rec.DriverNotes;

						Rec = DbRec;
					}
					catch( Exception Exception )
					{
						Context.SystemLogException( Exception );
						break;
					}
				}

				switch( u.Status )
				{
				case STATUS.PICKED_UP:
					UpdateCompanyGps( Rec.PickupCompanyId, Rec.PickupAddressLatitude, Rec.PickupAddressLongitude );
					break;

				case STATUS.DELIVERED:
					UpdateCompanyGps( Rec.DeliveryCompanyId, Rec.DeliveryAddressLatitude, Rec.DeliveryAddressLongitude );
					break;

				case STATUS.VERIFIED:
					UpdateCompanyGps( Rec.DeliveryCompanyId, (decimal)Rec.VerifiedLatitude, (decimal)Rec.VerifiedLongitude );
					break;
				}

				new Signatures( Context ).AddUpdate( TripId, u.Signature );

				After = new TripLogger( Rec );

				LogTrip( u.Program, Before, After );

				if( Context.SendToIds1 )
					IDSClient.Ids1.UpdateTrip( Context, false, Rec.ToTransferRecord( u.Program, Context.CarrierId, u.Signature ) );

				if( IsUndeliverable && Dp.UndeliverableBackToDispatch )
				{
					Rec.Status2 |= (int)STATUS1.UNDELIVERED;
					SetTripStatus( u.Program, Rec, Rec.DriverCode );
					var Msg = BuildMessage( u.Program, TripUpdateMessage.ACTION.UPDATE_STATUS, Rec );
					Msg.BroadcastToDispatchBoard = true;
					return [Msg];
				}

				var Result = new List<TripUpdateMessage> { BuildMessage( u.Program, TripUpdateMessage.ACTION.UPDATE_TRIP, Rec ) };

				if( NeedNewTrip )
				{
					var NewRec = new Trip( Rec )
					             {
						             TripId = Trip.NextTripId( Context ),

						             Status1 = (int)STATUS.ACTIVE,
						             Status2 = (int)STATUS1.UNSET,
						             Status3 = (int)( WasPickup ? STATUS2.WAS_UNDELIVERABLE : STATUS2.WAS_UNDELIVERABLE | STATUS2.WAS_PICKED_UP ),
						             Status4 = 0,
						             Status5 = 0,
						             Status6 = 0,

						             Pieces = Math.Abs( Diff )
					             };

					var Orig = $"Original Shipment Id: {TripId}";

					NewRec.BillingNotes = $"{Orig}\r\n{NewRec.BillingNotes}";

					if( WasPickup )
					{
						NewRec.PickupNotes        = $"{Orig}\r\n{NewRec.PickupNotes}";
						NewRec.PickupAddressNotes = $"{Orig}\r\n{NewRec.PickupAddressNotes}";
					}
					else
					{
						NewRec.DeliveryNotes        = $"{Orig}\r\n{NewRec.DeliveryNotes}";
						NewRec.DeliveryAddressNotes = $"{Orig}\r\n{NewRec.DeliveryAddressNotes}";
					}
					E.Trips.Add( NewRec );
					E.SaveChanges();

					LogTrip( u.Program, new TripLogger( NewRec ) );

					if( Context.SendToIds1 )
						IDSClient.Ids1.UpdateTrip( Context, true, NewRec.ToTransferRecord( u.Program, Context.CarrierId ) );

					var Msg = BuildMessage( u.Program, TripUpdateMessage.ACTION.UPDATE_TRIP, NewRec );
					Msg.BroadcastToDispatchBoard = true;
					Msg.BroadcastToDriver        = false;

					Result.Add( Msg );
				}
				return Result;
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return [];
	}
}