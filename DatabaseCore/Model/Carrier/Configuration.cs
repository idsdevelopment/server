﻿using Company = Database.Utils.Company;
using Configuration = Database.Model.Databases.MasterTemplate.Configuration;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public DateTime LastMessagingCleanup
	{
		get => ( from C in Entity.Configurations
		         select C.LastMessagingCleanup ).FirstOrDefault();

		set
		{
			var E = Entity;

			var Rec = ( from C in E.Configurations
			            select C ).FirstOrDefault();

			if( Rec is not null )
			{
				Rec.LastMessagingCleanup = value;
				E.SaveChanges();
			}
		}
	}

	public DateTime LastAddressCleanup
	{
		get => ( from C in Entity.Configurations
		         select C.LastDistanceCleanup ).FirstOrDefault();

		set
		{
			var E = Entity;

			var Rec = ( from C in E.Configurations
			            select C ).FirstOrDefault();

			if( Rec is not null )
			{
				Rec.LastDistanceCleanup = value;
				E.SaveChanges();
			}
		}
	}

	public Configuration GetConfiguration()
	{
		var E             = Entity;
		var Configuration = E.Configurations;

		Configuration? GetConfig()
		{
			return ( from C in Configuration
			         select C ).FirstOrDefault();
		}

		var Result = GetConfig();

		if( Result is null )
		{
			Company.CreateCompanyStorage( Context, this ).Wait();
			Result = GetConfig();

			if( Result is not null )
			{
				var Now = DateTime.UtcNow;
				Result.NextInvoiceNumber    = 1;
				Result.LastMessagingCleanup = Now;
				Result.LastDistanceCleanup  = Now;
				E.SaveChanges();
			}
			else
				throw new Exception( "Failed to create company storage" );
		}
		return Result;
	}

	public void UpdateConfiguration( Configuration c )
	{
		while( true )
		{
			try
			{
				var Configurations = Entity.Configurations;

				var Cfg = ( from C in Configurations
				            select C ).FirstOrDefault();

				if( Cfg is not null )
				{
					Cfg.CompanyAddressId     = c.CompanyAddressId;
					Cfg.StorageAccount       = c.StorageAccount;
					Cfg.StorageAccountKey    = c.StorageAccountKey;
					Cfg.StorageBlobEndpoint  = c.StorageBlobEndpoint;
					Cfg.StorageFileEndpoint  = c.StorageFileEndpoint;
					Cfg.StorageTableEndpoint = c.StorageTableEndpoint;
					Cfg.LastMessagingCleanup = c.LastMessagingCleanup;
				}
				else
					Configurations.Add( c );

				Entity.SaveChanges();
			}
			catch( DbUpdateConcurrencyException )
			{
				RandomDelay();
				continue;
			}
			catch( Exception E )
			{
				Context.SystemLogException( E );
			}
			break;
		}
	}

#region Next Invoice Number
	private static readonly object NextInvoiceLockObject = new();

	public long GetAndUpdateNextInvoiceNumber()
	{
		lock( NextInvoiceLockObject )
		{
			var E = Entity;

			while( true )
			{
				try
				{
					var Rec = ( from C in E.Configurations
					            select C ).FirstOrDefault();

					if( Rec is not null )
					{
						var Result = Rec.NextInvoiceNumber++;
						E.SaveChanges();
						return Result;
					}

					throw new Exception( "Missing Configuration" );
				}
				catch( DbUpdateConcurrencyException )
				{
					RandomDelay();
				}
				catch( Exception Exception )
				{
					Context.SystemLogException( Exception );
					break;
				}
			}
		}
		return -1;
	}

	public long NextInvoiceNumber
	{
		get
		{
			try
			{
				lock( NextInvoiceLockObject )
				{
					var E = Entity;

					var Rec = ( from C in E.Configurations
					            select C ).FirstOrDefault();

					if( Rec is not null )
						return Rec.NextInvoiceNumber;
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
			return -1;
		}

		set
		{
			lock( NextInvoiceLockObject )
			{
				while( true )
				{
					try
					{
						var E = Entity;

						var Rec = ( from C in E.Configurations
						            select C ).FirstOrDefault();

						if( Rec is not null )
						{
							Rec.NextInvoiceNumber = value;
							E.SaveChanges();
						}
						break;
					}
					catch( DbUpdateConcurrencyException )
					{
						RandomDelay();
					}
					catch( Exception Exception )
					{
						Context.SystemLogException( Exception );
						break;
					}
				}
			}
		}
	}
#endregion
}