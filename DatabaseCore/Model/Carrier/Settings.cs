﻿namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public Settings GetImportSettings()
	{
		var Retval = new Settings();

		var Cfg = ( from Config in Entity.Configurations
		            select Config ).FirstOrDefault();

		if( Cfg is not null )
		{
			Retval.LastAccountUpdate = Cfg.LastIds1AccountTick;
			Retval.LastTripUpdate    = Cfg.LastIds1TripTick;
			Retval.LastAddressUpdate = Cfg.LastIds1AddressTick;
		}

		return Retval;
	}

	public void UpdateImportSettings( Settings settings )
	{
		var E = Entity;

		var Cfg = ( from Config in E.Configurations
		            select Config ).FirstOrDefault();

		if( Cfg is null )
			E.Configurations.Add( Cfg = GetConfiguration() );

		Cfg.LastIds1AccountTick = settings.LastAccountUpdate;
		Cfg.LastIds1TripTick    = settings.LastTripUpdate;
		Cfg.LastIds1AddressTick = settings.LastAddressUpdate;
		E.SaveChanges();
	}
}