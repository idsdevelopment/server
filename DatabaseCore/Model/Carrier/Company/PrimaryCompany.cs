﻿using Ids1WebService;
using TransferRecords;
using Company = Protocol.Data.Company;
using ResellerCustomer = Database.Model.Databases.MasterTemplate.ResellerCustomer;

// ReSharper disable StringCompareToIsCultureSpecific

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	private PrimaryCompany GetCompanyDetails( ResellerCustomer? cust )
	{
		var Result = new PrimaryCompany();

		try
		{
			if( cust is not null )
			{
				var LoginCode = cust.LoginCode;

				var (_, _, _, _, Enabled, Ok) = Database.Users.GetResellerFromLoginCode( LoginCode );

				Result.LoginEnabled = Ok && Enabled;
				Result.LoginCode    = LoginCode;
				Result.CustomerCode = cust.CustomerCode;
				Result.UserName     = cust.UserName;

				Result.Password = Encryption.ToTimeLimitedToken( cust.Password );

				Company GetCompanyAndAddress( long companyId )
				{
					var (Company, _) = GetCompany( companyId );
					var (Address, _) = GetCompanyAddress( companyId );
					return ConvertCompany( Context, Company, Address );
				}

				Result.Company         = GetCompanyAndAddress( cust.CompanyId );
				Result.BillingCompany  = GetCompanyAndAddress( cust.BillingCompanyId );
				Result.ShippingCompany = GetCompanyAndAddress( cust.ShippingCompanyId );
			}
			Result.Ok = true;
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return Result;
	}

	public string AddUpdatePrimaryCompany( AddUpdatePrimaryCompany customer )
	{
		try
		{
			var (Password, Ok) = Encryption.FromTimeLimitedToken( customer.Password );

			if( Ok )
			{
				Password = Password.SubStr( 0, 50 );
				var ProgramName = customer.ProgramName;
				var E           = Entity;
				var Rc          = E.ResellerCustomers;

				var Rec = ( from C in Rc
							where C.CustomerCode == customer.CustomerCode
							select C ).FirstOrDefault();

				// ReSharper disable once LocalFunctionHidesMethod
				long AddCompany( Company? company )
				{
					long CompanyId = 0,
						 AddressId = 0;

					if( company is not null && company.CompanyNumber.IsNotNullOrWhiteSpace() )
					{
						var (Company, _, CompanyOk) = this.AddCompany( company, ProgramName );

						if( CompanyOk && Company is not null )
						{
							CompanyId = Company.CompanyId;
							AddressId = Company.PrimaryAddressId;
						}
					}

					if( CompanyId != 0 )
						AddCustCompany( E, CompanyId, AddressId, customer.CustomerCode, customer.Company );

					return CompanyId;
				}

				long AddShippingCompany()
				{
					return AddCompany( customer.ShippingCompany );
				}

				long AddBillingCompany()
				{
					return AddCompany( customer.BillingCompany );
				}

				if( Rec is null )
				{
					// Add primary company address
					var (Company, _, CompanyOk) = this.AddCompany( customer.Company, ProgramName );

					if( CompanyOk )
					{
						// Generate unique login code system wide
						var (Code, CodeOk) = Database.Users.AddUpdateResellerLoginCode( Context, customer.LoginEnabled, customer.CustomerCode, customer.SuggestedLoginCode, customer.Company );

						if( CodeOk )
						{
							if( Company is not null )
							{
								var CompanyId = Company.CompanyId;
								var AddressId = Company.PrimaryAddressId;

								var BillingCompanyId  = AddBillingCompany();
								var ShippingCompanyId = AddShippingCompany();

								var Cust = new ResellerCustomer
										   {
											   CustomerCode      = customer.CustomerCode,
											   LoginCode         = Code,
											   Password          = Password,
											   UserName          = customer.UserName,
											   CompanyId         = CompanyId,
											   BillingCompanyId  = BillingCompanyId,
											   ShippingCompanyId = ShippingCompanyId,
											   CompanyName       = Company.CompanyName,
											   StringOption1     = "",
											   StringOption2     = "",
											   StringOption3     = "",
											   StringOption4     = "",
											   StringOption5     = "",
											   ShiftRate         = ""
										   };

								// cjt Copied from ResellerCustomers.AddUpdateCustomer
								Rc.Add( Cust );
								AddCustCompany( E, CompanyId, AddressId, Code, customer.Company );

								if( Context.SendAccountsToIds1 )
									IDSClient.Ids1.AddPrimaryCompany( Context, ProgramName, customer.ToTransferRecord( Context.CarrierId ) );
							}
							return Code;
						}
					}
				}
				else
				{
					// Generate unique login code system wide
					var (Code, CodeOk) = Database.Users.AddUpdateResellerLoginCode( Context, customer.LoginEnabled, customer.CustomerCode, customer.SuggestedLoginCode, customer.Company );

					if( CodeOk )
					{
						Rec.LoginCode   = Code;
						Rec.CompanyName = customer.Company.CompanyName;

						Rec.UserName = customer.UserName; // cjt Added 20190808
						Rec.Password = Password;

						if( Rec.BillingCompanyId == 0 )
							Rec.BillingCompanyId = AddBillingCompany();

						if( Rec.ShippingCompanyId == 0 )
							Rec.ShippingCompanyId = AddShippingCompany();

						E.SaveChanges();

						if( customer.Company.CompanyName.IsNotNullOrWhiteSpace() )
							UpdateCompany( Rec.CompanyId, customer.Company, ProgramName );

						if( customer.BillingCompany.CompanyName.IsNotNullOrWhiteSpace() )
							UpdateCompany( Rec.BillingCompanyId, customer.BillingCompany, ProgramName );

						if( customer.ShippingCompany.CompanyName.IsNotNullOrWhiteSpace() )
							UpdateCompany( Rec.ShippingCompanyId, customer.ShippingCompany, ProgramName );

						return Code;
					}
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return "";
	}

	public PrimaryCompany GetPrimaryCompanyByCompanyName( string companyName )
	{
		try
		{
			companyName = companyName.Trim();

			return GetCompanyDetails( ( from C in Entity.ResellerCustomers
										where C.CompanyName == companyName
										select C ).FirstOrDefault() );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return GetCompanyDetails( null );
	}

	public PrimaryCompany GetPrimaryCompanyByCustomerCode( string customerCode )
	{
		if( customerCode.IsNotNullOrWhiteSpace() )
		{
			try
			{
				customerCode = customerCode.Trim();

				return GetCompanyDetails( ( from C in Entity.ResellerCustomers
											where C.CustomerCode == customerCode
											select C ).FirstOrDefault() );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
		return GetCompanyDetails( null );
	}

	public PrimaryCompany GetPrimaryCompanyByLoginCode( string loginCode )
	{
		try
		{
			loginCode = loginCode.Trim();

			return GetCompanyDetails( ( from C in Entity.ResellerCustomers
										where C.LoginCode == loginCode
										select C ).FirstOrDefault() );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return GetCompanyDetails( null );
	}
}