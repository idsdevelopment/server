﻿using Zone = Database.Model.Databases.MasterTemplate.Zone;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public void AddUpdatePostalZones( PostcodesToZones postcodesToZones )
	{
		var PostalCode = postcodesToZones.PostalCode.NullTrim();

		if( PostalCode != "" )
		{
			var Program = postcodesToZones.Program.NullTrim();
			var Route   = postcodesToZones.RouteName.NullTrim();

			var (RouteId, Ok) = GetRouteId( Route );

			switch( Ok )
			{
			case true when RouteId == -1:
				{
					( RouteId, Ok ) = AddUpdateRoute( Program, Context.CarrierId, Route );

					if( !Ok )
						RouteId = -1;
					break;
				}

			case true:
				break;

			default:
				RouteId = -1;
				break;
			}

			var E     = Entity;
			var Zones = E.Zones;

			string DoZone( string zone, string abbreviation )
			{
				var Zone = zone.NullTrim();

				var Abbreviation = abbreviation.NullTrim();
				Abbreviation = ( Abbreviation == "" ? Zone : Abbreviation ).TrimMax20();

				switch( ( from Z in Zones
				          where Z.ZoneName == Zone
				          select Z ).FirstOrDefault() )
				{
				case { } Z when Z.Abbreviation != abbreviation:
					Z.Abbreviation = Abbreviation;
					E.SaveChanges();
					break;

				case not null:
					break;

				default:
					Zones.Add( new Zone
					           {
						           ZoneName     = Zone,
						           Abbreviation = Abbreviation
					           } );
					E.SaveChanges();
					break;
				}
				return Zone;
			}

			var Zone1 = DoZone( postcodesToZones.Zone1, postcodesToZones.AbbreviationZone1 );
			var Zone2 = DoZone( postcodesToZones.Zone2, postcodesToZones.AbbreviationZone2 );
			var Zone3 = DoZone( postcodesToZones.Zone3, postcodesToZones.AbbreviationZone3 );
			var Zone4 = DoZone( postcodesToZones.Zone4, postcodesToZones.AbbreviationZone4 );

			var PCodes = E.PostalCodeToRoutes;

			var PRec = ( from P in PCodes
			             where ( P.RouteId == RouteId ) && ( P.PostalCode == PostalCode )
			             select P ).FirstOrDefault();

			if( PRec is null )
			{
				PRec = new PostalCodeToRoute
				       {
					       RouteId    = RouteId,
					       PostalCode = PostalCode,
					       Zone1      = Zone1,
					       Zone2      = Zone2,
					       Zone3      = Zone3,
					       Zone4      = Zone4
				       };
				PCodes.Add( PRec );
			}
			else
			{
				PRec.Zone1 = Zone1;
				PRec.Zone2 = Zone2;
				PRec.Zone3 = Zone3;
				PRec.Zone4 = Zone4;
			}
			E.SaveChanges();
		}
	}

	public PostcodesToZonesList GetPostalZones( string routeName, string postalCode, string zone )
	{
		routeName = routeName.NullTrim();

		var Result = new PostcodesToZonesList();

		var (DoRoute, WildRoute) = DoCode( ref routeName );

		long GetId()
		{
			var (RouteId, Ok) = GetRouteId( routeName );

			if( !Ok )
				RouteId = -1;

			return RouteId;
		}

		var RouteId = DoRoute ? GetId() : -1;

		var (_, WildPostal) = DoCode( ref postalCode );
		var (_, WildZone)   = DoCode( ref zone );

		var E = Entity;

		List<PostalCodeToRoute> PZones;

		try
		{
			PZones = ( from P in E.PostalCodeToRoutes
			           where WildRoute || ( ( P.RouteId == RouteId )
			                                && ( WildPostal || ( P.PostalCode == postalCode ) )
			                                && ( WildZone || ( P.Zone1 == zone )
			                                              || ( P.Zone2 == zone )
			                                              || ( P.Zone3 == zone )
			                                              || ( P.Zone4 == zone ) ) )
			           select P ).ToList();
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
			return Result;
		}

		try
		{
			var Routes = ( from R in E.Routes
			               select new { R.RouteId, R.Name } ).ToList();

			Result.AddRange( from P in PZones
			                 select new PostcodesToZonesBase
			                        {
				                        RouteName = ( from R in Routes
				                                      where R.RouteId == P.RouteId
				                                      select R.Name ).FirstOrDefault() ?? "Unknown Route",
				                        PostalCode = P.PostalCode,
				                        Zone1      = P.Zone1,
				                        Zone2      = P.Zone2,
				                        Zone3      = P.Zone3,
				                        Zone4      = P.Zone4
			                        } );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return Result;
	}

	public void DeletePostalZones( string routeName, string postalCode )
	{
		var (RouteId, _) = GetRouteId( routeName.NullTrim() );
		postalCode       = postalCode.NullTrim();

		var E                  = Entity;
		var PostalCodeToRoutes = E.PostalCodeToRoutes;

		var Rec = ( from R in PostalCodeToRoutes
		            where ( R.RouteId == RouteId ) && ( R.PostalCode == postalCode )
		            select R ).FirstOrDefault();

		if( Rec is not null )
		{
			PostalCodeToRoutes.Remove( Rec );
			E.SaveChanges();
		}
	}

	private (long RouteId, bool Ok) GetRouteId( string routeName )
	{
		if( routeName != "" )
		{
			try
			{
				return ( ( from R in Entity.Routes.AsEnumerable()
				           where R.Name == routeName
				           select R.RouteId ).DefaultIfEmpty( -1 ).FirstOrDefault(),
				         true );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
		return ( -1, false );
	}

	private static ( bool DoCode, bool IsWild ) DoCode( ref string code )
	{
		var C = code.NullTrim();
		code = C;
		return ( C != "", C == "*" );
	}
}