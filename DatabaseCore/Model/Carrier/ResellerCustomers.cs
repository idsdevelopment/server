﻿using Address = Database.Model.Databases.MasterTemplate.Address;
using Company = Database.Model.Databases.MasterTemplate.Company;
using CompanyAddress = Protocol.Data.CompanyAddress;
using ResellerCustomer = Database.Model.Databases.MasterTemplate.ResellerCustomer;

// ReSharper disable ConstantNullCoalescingCondition

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	private static readonly object ResellerUpdateLock = new();

	public class ResellerCustomerLogger
	{
		public const string LOG_STORAGE = "Customer";

		public string CustomerCode     { get; set; }
		public string LoginCode        { get; set; } = "";
		public string UserName         { get; set; } = "";
		public string Password         { get; set; } = "";
		public long   CompanyId        { get; set; }
		public long   BillingCompanyId { get; set; }
		public string CompanyName      { get; set; } = "";

		public string ShiftRate { get; set; } = "";

		public CompanyLogger       Address          { get; set; } = BLANK_COMPANY_LOGGER;
		public CompanyLogger       BillingAddress   { get; set; } = BLANK_COMPANY_LOGGER;
		public List<CompanyLogger> CompanyAddresses { get; set; } = [];

		// ReSharper disable once InconsistentNaming
		internal static CompanyLogger BLANK_COMPANY_LOGGER => _BLANK_COMPANY_LOGGER ??= new CompanyLogger( new Company(), new AddressLogger( new Address() ) );

		private static CompanyLogger? _BLANK_COMPANY_LOGGER;

		public ResellerCustomerLogger( ResellerCustomer c )
		{
			CustomerCode     = c.CustomerCode;
			LoginCode        = c.LoginCode;
			UserName         = c.UserName;
			Password         = c.Password;
			CompanyId        = c.CompanyId;
			BillingCompanyId = c.BillingCompanyId;
			CompanyName      = c.CompanyName;
			ShiftRate        = c.ShiftRate;
		}

		public ResellerCustomerLogger( ResellerCustomer c, CompanyLogger address, CompanyLogger billingAddress, List<CompanyLogger> companyAddresses ) : this( c )
		{
			Address          = address;
			BillingAddress   = billingAddress;
			CompanyAddresses = companyAddresses;
		}

		public ResellerCustomerLogger( string c )
		{
			CustomerCode = c;
		}
	}

	public static bool HasCarrierId( IRequestContext context ) => Database.Users.HasEndpoints( context );

	public void DeleteAllResellerCustomers()
	{
		try
		{
			Entity.Database.ExecuteSqlRaw( "DELETE FROM dbo.ResellerCustomers" );
			Database.Users.DeleteAllResellerCustomers( Context, Context.CarrierId );
		}
		catch( Exception E )
		{
			Context.SystemLogException( nameof( DeleteAllResellerCustomers ), E );
		}
	}

	public CustomerLookupSummary CustomerSummaryLookup( string customerAccountCode )
	{
		var Rec = ( from C in Entity.ResellerCustomers
		            where C.CustomerCode == customerAccountCode
		            select C ).FirstOrDefault();

		if( Rec is not null )
		{
			var Company = GetCompany( Rec.CompanyId ).Company;
			var Address = GetCompanyPrimaryAddress( Company ).Address;

			return new CustomerLookupSummary
			       {
				       CustomerCode       = Rec.CustomerCode,
				       CompanyName        = Company.CompanyName,
				       DisplayCompanyName = Company.DisplayCompanyName,
				       AddressLine1       = Address?.AddressLine1.NullTrim()!,
				       BoolOption1        = Rec.BoolOption1
			       };
		}

		return new CustomerLookupSummary { Found = false };
	}

	public CustomerLookupSummaryList CustomersLookupByAccount( CustomerLookup lookup )
	{
		var RetVal = new CustomerLookupSummaryList();

		try
		{
			var Key   = lookup.Key;
			var Count = lookup.PreFetchCount;

			IEnumerable<ResellerCustomer> Result;

			switch( lookup.Fetch )
			{
			case PreFetch.PREFETCH.FIRST:
				Result = ( from C in Entity.ResellerCustomers
				           orderby C.CustomerCode
				           select C ).Take( Count );
				break;

			case PreFetch.PREFETCH.LAST:
				Result = ( from C in Entity.ResellerCustomers
				           orderby C.CustomerCode descending
				           select C ).Take( Count );
				break;

			case PreFetch.PREFETCH.FIND_MATCH:
				Result = ( from C in Entity.ResellerCustomers
				           where C.CustomerCode.StartsWith( Key )
				           orderby C.CustomerCode
				           select C ).Take( Count );
				break;

			case PreFetch.PREFETCH.FIND_RANGE:
				if( Count >= 0 ) // Next
				{
					Result = ( from C in Entity.ResellerCustomers
					           where string.Compare( C.CustomerCode, Key, StringComparison.Ordinal ) >= 0
					           orderby C.CustomerCode
					           select C ).Take( Count );
				}
				else // Prior
				{
					Result = ( from C in Entity.ResellerCustomers
					           where string.Compare( C.CustomerCode, Key, StringComparison.Ordinal ) <= 0
					           orderby C.CustomerCode
					           select C ).Take( -Count );
				}
				break;

			default:
				Context.SystemLogException( nameof( CustomersLookup ), new Exception( $"Unknown prefetch type: {lookup.Fetch}" ) );
				return RetVal;
			}

			RetVal.AddRange( from C in Result
			                 let Company = GetCompany( C.CompanyId ).Company
			                 let Address = GetCompanyPrimaryAddress( Company ).Address
			                 where Address is not null
			                 select new CustomerLookupSummary
			                        {
				                        CustomerCode       = C.CustomerCode,
				                        CompanyName        = Company.CompanyName,
				                        DisplayCompanyName = Company.DisplayCompanyName,
				                        AddressLine1       = Address.AddressLine1,
				                        BoolOption1        = C.BoolOption1
			                        } );
		}
		catch( Exception E )
		{
			Context.SystemLogException( E );
		}

		return RetVal;
	}

	public CustomerLookupSummaryList CustomersLookupByCompanyName( CustomerLookup lookup )
	{
		var RetVal = new CustomerLookupSummaryList();

		try
		{
			var Key   = lookup.Key;
			var Count = lookup.PreFetchCount;

			IEnumerable<ResellerCustomer> Result;

			switch( lookup.Fetch )
			{
			case PreFetch.PREFETCH.FIRST:
				Result = ( from C in Entity.ResellerCustomers
				           orderby C.CompanyName
				           select C ).Take( Count ).ToList();

				break;

			case PreFetch.PREFETCH.LAST:
				Result = ( from C in Entity.ResellerCustomers
				           orderby C.CustomerCode descending
				           select C ).Take( Count ).ToList();

				break;

			case PreFetch.PREFETCH.FIND_MATCH:
				Result = ( from C in Entity.ResellerCustomers
				           where C.CustomerCode.StartsWith( Key )
				           orderby C.CustomerCode
				           select C ).Take( Count ).ToList();

				break;

			case PreFetch.PREFETCH.FIND_RANGE:
				if( Count >= 0 ) // Next
				{
					Result = ( from C in Entity.ResellerCustomers
					           where string.Compare( C.CustomerCode, Key, StringComparison.Ordinal ) >= 0
					           orderby C.CustomerCode
					           select C ).Take( Count ).ToList();
				}
				else // Prior
				{
					Result = ( from C in Entity.ResellerCustomers
					           where string.Compare( C.CustomerCode, Key, StringComparison.Ordinal ) <= 0
					           orderby C.CustomerCode
					           select C ).Take( -Count ).ToList();
				}

				break;

			default:
				Context.SystemLogException( nameof( CustomersLookup ), new Exception( $"Unknown prefetch type: {lookup.Fetch}" ) );

				return RetVal;
			}

			RetVal.AddRange( from C in Result
			                 let Cs = ( from C1 in Entity.ResellerCustomerCompanySummaries
			                            where C1.CompanyName == C.CompanyName
			                            select C1 ).FirstOrDefault()
			                 where Cs is not null
			                 select new CustomerLookupSummary
			                        {
				                        CustomerCode       = C.CustomerCode,
				                        DisplayCompanyName = Cs.DisplayCompanyName,
				                        CompanyName        = C.CompanyName,
				                        AddressLine1       = Cs.AddressLine1,

				                        Enabled = Cs.Enabled
			                        } );
		}
		catch( Exception E )
		{
			Context.SystemLogException( nameof( CustomersLookup ), E );
		}

		return RetVal;
	}

	public CustomerLookupSummaryList CustomersLookup( CustomerLookup lookup ) => lookup.ByAccountNumber ? CustomersLookupByAccount( lookup ) : CustomersLookupByCompanyName( lookup );

	public bool DeleteResellerCustomer( string customerCode, string programName )
	{
		try
		{
			var E = Entity;
			var R = E.ResellerCustomers;

			var Rec = ( from Rc in R
			            where Rc.CustomerCode == customerCode
			            select Rc ).FirstOrDefault();

			if( Rec is not null )
			{
				var AddressIds = ( from A in Entity.CompanyAddresses
				                   where A.CompanyId == Rec.CompanyId
				                   select A.AddressId ).ToList();

				E.ResellerCustomerCompanySummaries.RemoveRange( from S in E.ResellerCustomerCompanySummaries
				                                                where ( S.CompanyId == Rec.CompanyId ) && AddressIds.Contains( S.AddressId )
				                                                select S );

				R.Remove( Rec );
				E.SaveChanges();
				Context.LogDeleted( ResellerCustomerLogger.LOG_STORAGE, programName, "", new ResellerCustomerLogger( customerCode ) );
			}

			return Database.Users.DeleteResellerLoginCodeByCustomerCode( Context, customerCode );
		}
		catch( Exception E )
		{
			Context.SystemLogException( nameof( DeleteResellerCustomer ), E );
		}

		return false;
	}

	public static void AddCustCompany( MasterTemplateContext entity, long cid, long acid, string? custCode, CompanyBase company )
	{
		if( custCode is not null && custCode.IsNotNullOrWhiteSpace() )
		{
			entity.ResellerCustomerCompanies.Add( new ResellerCustomerCompany
			                                      {
				                                      CustomerCode = custCode,
				                                      CompanyId    = cid
			                                      } );

			entity.ResellerCustomerCompanySummaries.Add( new ResellerCustomerCompanySummary
			                                             {
				                                             CompanyId       = cid,
				                                             AddressId       = acid,
				                                             CompanyName     = company.CompanyName.ToUpper(),
				                                             LocationBarcode = company.LocationBarcode,

				                                             Enabled = company.Enabled,

				                                             DisplayCompanyName = company.CompanyName,
				                                             CustomerCode       = custCode,
				                                             Suite              = company.Suite.NullTrim(),
				                                             AddressLine1       = company.AddressLine1.NullTrim(),
				                                             AddressLine2       = company.AddressLine2.NullTrim(),
				                                             City               = company.City.NullTrim(),
				                                             Region             = company.Region.NullTrim(),
				                                             PostalCode         = company.PostalCode.TrimMax20(),
				                                             CountryCode        = company.CountryCode.NullTrim()
			                                             } );
			entity.SaveChanges();
		}
	}

	public string AddUpdateResellerCustomer( AddUpdateCustomer reseller )
	{
		try
		{
			var (Password, Ok) = Encryption.FromTimeLimitedToken( reseller.Password );

			if( Ok )
			{
				var ProgramName = reseller.ProgramName;
				var E           = Entity;
				var Rc          = E.ResellerCustomers;

				var ResellerRec = ( from C in Rc
				                    where C.CustomerCode == reseller.CustomerCode
				                    select C ).FirstOrDefault();

				void AddCustCompanyLocal( long cid, long acid, string custCode, CompanyBase company )
				{
					AddCustCompany( E, cid, acid, custCode, company );
				}

				if( ResellerRec is null ) // No Reseller
				{
					// Generate unique login code system wide
					var (Code, CodeOk) = Database.Users.AddUpdateResellerLoginCode( Context, reseller );

					if( CodeOk )
					{
						// Add primary company address
						var (Company, AddressLog, CompanyOk) = AddCompany( reseller.Company, ProgramName );

						if( CompanyOk )
						{
							var  CId         = Company!.CompanyId;
							var  Aid         = Company.PrimaryAddressId;
							long BId         = 0;
							var  BAddressLog = new CompanyLogger( new Company(), new AddressLogger( new Address() ) );

							// Do Bill To Company
							if( reseller.BillingCompany.IsNotNull() && reseller.BillingCompany.CompanyNumber.IsNotNullOrWhiteSpace() )
							{
								var (BCompany, BAddressLog1, BCompanyOk) = AddCompany( reseller.BillingCompany, ProgramName );

								if( BCompanyOk )
								{
									BId         = BCompany!.CompanyId;
									BAddressLog = BAddressLog1;
								}
							}

							var Cust = new ResellerCustomer
							           {
								           CustomerCode     = reseller.CustomerCode,
								           LoginCode        = Code,
								           Password         = Password,
								           UserName         = reseller.UserName,
								           CompanyId        = CId,
								           BillingCompanyId = BId,
								           CompanyName      = Company.CompanyName.NullTrim(),
								           StringOption1    = "",
								           StringOption2    = "",
								           StringOption3    = "",
								           StringOption4    = "",
								           StringOption5    = "",
								           ShiftRate        = ""
							           };

							Rc.Add( Cust );

							AddCustCompanyLocal( CId, Aid, Code, reseller.Company );

							var CompanyAddresses = new List<CompanyLogger>();

							if( reseller.Companies.IsNotNull() )
							{
								var CustId = Cust.CustomerCode;

								foreach( var CustomerCompany in reseller.Companies )
								{
									var (Co, __, CcOk) = AddCompany( CustomerCompany, ProgramName );

									if( CcOk )
									{
										AddCustCompanyLocal( Co!.CompanyId, Co.PrimaryAddressId, Co.CompanyName, CustomerCompany );
										var Id = Co.CompanyId;

										E.ResellerCustomerCompanies.Add( new ResellerCustomerCompany
										                                 {
											                                 CustomerCode = CustId.NullTrim(),
											                                 CompanyId    = Id
										                                 } );

										E.SaveChanges();
									}
								}
							}

							Context.LogNew( ResellerCustomerLogger.LOG_STORAGE, ProgramName, "Add new reseller", new ResellerCustomerLogger( Cust, AddressLog!, BAddressLog!, CompanyAddresses ) );
						}
					}

					return Code;
				}

				var (ResellerCompany, CoOk) = GetCompany( ResellerRec.CompanyId );

				if( CoOk )
				{
					var (CoBefore, CoAfter, CoOk1) = UpdateCompany( ResellerCompany.CompanyId, reseller.Company, ProgramName );

					if( CoOk1 )
					{
						var (BCompany, _) = GetCompany( ResellerRec.BillingCompanyId );

						var (BBefore, BAfter, _) = UpdateCompany( BCompany.CompanyId, reseller.BillingCompany, ProgramName );

						ResellerCustomerLogger Before = new( ResellerRec, CoBefore, BBefore, [] );
						ResellerCustomerLogger After  = new( ResellerRec, CoAfter, BAfter, [] );

						var Added = new List<CompanyLogger>();

						if( reseller.Companies.IsNotNull() )
						{
							var CustId = ResellerRec.CustomerCode.ToUpper();

							foreach( var CustomerCompany in reseller.Companies )
							{
								var RRec = ( from R in E.ResellerCustomerCompanies
								             where R.CustomerCode == CustId
								             select R ).FirstOrDefault();

								if( RRec is null )
								{
									var (_, RCoLogger, Rok) = AddCompany( CustomerCompany, ProgramName );

									if( Rok )
										Added.Add( RCoLogger! );
								}
								else
								{
									var (UBefore, UAfter, UOk) = UpdateCompany( RRec.CompanyId, CustomerCompany, ProgramName );

									if( UOk )
									{
										Before.CompanyAddresses.Add( UBefore );
										After.CompanyAddresses.Add( UAfter );
									}
								}
							}
						}

						After.CompanyAddresses.AddRange( Added );
						Context.LogDifferences( ResellerCustomerLogger.LOG_STORAGE, ProgramName, "Add/Update reseller", Before, After );
					}
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return "";
	}

	public string AddResellerCustomer( AddUpdateCustomer customer )
	{
		var PName = customer.ProgramName;
		DeleteResellerCustomer( customer.CustomerCode, PName );
		var (Code, Ok) = Database.Users.AddUpdateResellerLoginCode( Context, customer );

		if( Ok )
		{
			var Company = AddCompany( customer.Company, PName );

			if( Company.Ok )
			{
				var CId = Company.Company!.CompanyId;
				var Aid = Company.Company!.PrimaryAddressId;

				long BId = 0;

				Company = AddCompany( customer.BillingCompany, PName );

				if( Company.Ok )
					BId = Company.Company!.CompanyId;

				var E = Entity;

				var Cust = new ResellerCustomer
				           {
					           CustomerCode     = customer.CustomerCode,
					           LoginCode        = Code,
					           Password         = customer.Password,
					           UserName         = customer.UserName,
					           CompanyId        = CId,
					           BillingCompanyId = BId,
					           CompanyName      = Company.Company!.CompanyName.NullTrim(),
					           StringOption1    = "",
					           StringOption2    = "",
					           StringOption3    = "",
					           StringOption4    = "",
					           StringOption5    = "",
					           ShiftRate        = ""
				           };

				E.ResellerCustomers.Add( Cust );

				E.ResellerCustomerCompanies.Add( new ResellerCustomerCompany
				                                 {
					                                 CustomerCode = Code,
					                                 CompanyId    = CId
				                                 } );
				var CustCo = customer.Company;

				E.ResellerCustomerCompanySummaries.Add( new ResellerCustomerCompanySummary
				                                        {
					                                        CompanyId       = CId,
					                                        AddressId       = Aid,
					                                        CompanyName     = Company.Company!.CompanyName.NullTrim(),
					                                        LocationBarcode = Company.Company!.LocationBarcode.NullTrim(),

					                                        Enabled = Company.Company!.Enabled,

					                                        DisplayCompanyName = Company.Company!.DisplayCompanyName.NullTrim(),
					                                        CustomerCode       = customer.CustomerCode,
					                                        Suite              = CustCo.Suite.NullTrim(),
					                                        AddressLine1       = CustCo.AddressLine1.NullTrim(),
					                                        AddressLine2       = CustCo.AddressLine2.NullTrim(),
					                                        City               = CustCo.City.NullTrim(),
					                                        Region             = CustCo.Region.NullTrim(),
					                                        PostalCode         = CustCo.PostalCode.TrimMax20(),
					                                        CountryCode        = CustCo.CountryCode.NullTrim()
				                                        } );
				E.SaveChanges();

				if( customer.Companies.Count > 0 )
				{
					var CustId = Cust.CustomerCode;

					new Tasks.Task<Protocol.Data.Company>().Run( customer.Companies, company =>
					                                                                 {
						                                                                 try
						                                                                 {
							                                                                 using var Db = new CarrierDb( Context );

							                                                                 var E1 = Db.Entity;

							                                                                 var (Co, _, CcOk) = Db.AddCompany( company, PName );

							                                                                 if( CcOk )
							                                                                 {
								                                                                 var Id = Co!.CompanyId;

								                                                                 E1.ResellerCustomerCompanies.Add( new ResellerCustomerCompany
								                                                                                                   {
									                                                                                                   CustomerCode = CustId.NullTrim(),
									                                                                                                   CompanyId    = Id
								                                                                                                   } );

								                                                                 E1.ResellerCustomerCompanySummaries.Add( new ResellerCustomerCompanySummary
								                                                                                                          {
									                                                                                                          CompanyId       = Id,
									                                                                                                          AddressId       = Co.PrimaryAddressId,
									                                                                                                          CompanyName     = Co.CompanyName.NullTrim(),
									                                                                                                          LocationBarcode = Co.LocationBarcode.NullTrim(),

									                                                                                                          Enabled = Co.Enabled,

									                                                                                                          DisplayCompanyName = Co.DisplayCompanyName.NullTrim(),
									                                                                                                          CustomerCode       = CustId.NullTrim(),
									                                                                                                          Suite              = company.Suite.NullTrim(),
									                                                                                                          AddressLine1       = company.AddressLine1.NullTrim(),
									                                                                                                          AddressLine2       = company.AddressLine2.NullTrim(),
									                                                                                                          City               = company.City.NullTrim(),
									                                                                                                          Region             = company.Region.NullTrim(),
									                                                                                                          PostalCode         = company.PostalCode.TrimMax20(),
									                                                                                                          CountryCode        = company.CountryCode.NullTrim()
								                                                                                                          } );

								                                                                 E1.SaveChanges();
							                                                                 }
						                                                                 }
						                                                                 catch( Exception Exception )
						                                                                 {
							                                                                 Context.SystemLogException( $"{nameof( AddResellerCustomer )} - Task", Exception );
						                                                                 }
					                                                                 }, 2 );
				}

				return Code;
			}
		}

		return "";
	}

	public CompanyByAccountAndCompanyNameList FindMissingCompanies( CompanyByAccountAndCompanyNameList companyNameList )
	{
		var RetVal = new CompanyByAccountAndCompanyNameList();

		try

		{
			// Simplify the lookups
			var CoNames = new Dictionary<string, HashSet<string>>(); // CompanyName CustomerCode

			foreach( var CName in companyNameList )
			{
				var CompanyName = CName.CompanyName.ToUpper();

				if( !CoNames.TryGetValue( CompanyName, out var CustHashSet ) )
					CoNames.Add( CompanyName, CustHashSet = [] );
				CustHashSet.Add( CName.CustomerCode );
			}

			var Companies  = Entity.Companies;
			var ResellerCo = Entity.ResellerCustomerCompanies;

			// Walk the company names
			foreach( var CoName in CoNames )
			{
				var CompanyName = CoName.Key;
				var Customers   = CoName.Value;

				var CoRecs = from Comp in Companies
				             where Comp.CompanyName == CompanyName
				             select Comp;

				CompanyByAccountAndCompanyName DefaultCo( string? customerCode )
				{
					return new CompanyByAccountAndCompanyName
					       {
						       CustomerCode = customerCode.NullTrim(),
						       CompanyName  = CompanyName.NullTrim()
					       };
				}

				if( !CoRecs.Any() ) // Company name not found
				{
					RetVal.AddRange( from CustomerCode in Customers
					                 select DefaultCo( CustomerCode ) );
				}
				else
				{
					foreach( var CustomerCode in Customers ) // HashSet of Customers
					{
						var Found = false;

						foreach( var CoRec in CoRecs ) // All the found Companies by Names
						{
							Found = ( from R in ResellerCo
							          where ( R.CustomerCode == CustomerCode ) && ( R.CompanyId == CoRec.CompanyId )
							          select R ).FirstOrDefault() is not null;

							if( Found )
								break; // Not missing
						}

						if( !Found )
							RetVal.Add( DefaultCo( CustomerCode ) );
					}
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( nameof( FindMissingCompanies ), Exception );
		}

		return RetVal;
	}

	public long AddResellerCompany( AddCustomerCompany customerCompany )
	{
		var Rc = ( from R in Entity.ResellerCustomers
		           where R.CustomerCode == customerCompany.CustomerCode
		           select R ).FirstOrDefault();

		if( Rc is not null )
		{
			try
			{
				var (Comp, _, Ok) = AddCompany( customerCompany.Company, customerCompany.ProgramName );

				if( Ok )
				{
					var Cid      = Comp!.CompanyId;
					var CustCode = customerCompany.CustomerCode;

					Entity.ResellerCustomerCompanies.Add( new ResellerCustomerCompany
					                                      {
						                                      CompanyId    = Cid,
						                                      CustomerCode = CustCode
					                                      } );
					var Cc = customerCompany.Company;

					Entity.ResellerCustomerCompanySummaries.Add( new ResellerCustomerCompanySummary
					                                             {
						                                             CompanyId       = Cid,
						                                             CompanyName     = Comp.CompanyName.NullTrim(),
						                                             LocationBarcode = Comp.LocationBarcode.NullTrim(),

						                                             Enabled = Comp.Enabled,

						                                             DisplayCompanyName = Comp.DisplayCompanyName.NullTrim(),
						                                             AddressId          = Comp.PrimaryAddressId,
						                                             CustomerCode       = CustCode,
						                                             Suite              = Cc.Suite.NullTrim(),
						                                             AddressLine1       = Cc.AddressLine1.NullTrim(),
						                                             Region             = Cc.Region.NullTrim(),
						                                             City               = Cc.City.NullTrim(),
						                                             AddressLine2       = Cc.AddressLine2.NullTrim(),
						                                             CountryCode        = Cc.CountryCode.NullTrim(),
						                                             PostalCode         = Cc.PostalCode.TrimMax20()
					                                             } );
					Entity.SaveChanges();

					return Cid;
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( nameof( AddResellerCompany ), Exception );
			}
		}
		return -1;
	}

	public void DeleteCustomerCompany( DeleteCustomerCompany customerCompany )
	{
		var Rc = ( from R in Entity.ResellerCustomers
		           where R.CustomerCode == customerCompany.CustomerCode
		           select R ).FirstOrDefault();

		if( Rc is not null )
		{
			try
			{
				var CompanyName  = customerCompany.Company.CompanyName;
				var CustomerCode = customerCompany.CustomerCode;

				DeleteCustomerCompanyAddress( CustomerCode, CompanyName, customerCompany.ProgramName );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( nameof( DeleteCustomerCompany ), Exception );
			}
		}
	}

	public void UpdateCustomerCompany( UpdateCustomerCompany customerCompany )
	{
		UpdateCustomerCompanyAddress( customerCompany, !customerCompany.GlobalUpdate );
	}

	public CustomerCompaniesList GetCustomerCompanyAddress( CompanyByAccountAndCompanyNameList companyNameList )
	{
		var RetVal = new CustomerCompaniesList();

		try

		{
			var CustomerWithinCompanyName = new Dictionary<string, HashSet<string>>();

			foreach( var CName in companyNameList )
			{
				var CompanyName = CName.CompanyName.ToUpper();

				if( !CustomerWithinCompanyName.TryGetValue( CompanyName, out var CustHashSet ) )
					CustomerWithinCompanyName.Add( CompanyName, CustHashSet = [] );
				CustHashSet.Add( CName.CustomerCode );
			}

			var Companies   = Entity.Companies;
			var ResellerCo  = Entity.ResellerCustomerCompanies;
			var CoAddresses = Entity.CompanyAddresses;
			var Addresses   = Entity.Addresses;

			foreach( var Kv in CustomerWithinCompanyName )
			{
				var CompanyName = Kv.Key;

				// Find all with the same CompanyName
				var CoRecs = from Co in Companies
				             where Co.CompanyName == CompanyName
				             select Co;

				// Get company Record for reseller
				foreach( var CoRec in CoRecs )
				{
					foreach( var CustomerCode in Kv.Value )
					{
						var Rec = ( from R in ResellerCo
						            where ( R.CompanyId == CoRec.CompanyId ) && ( R.CustomerCode == CustomerCode )
						            select R ).FirstOrDefault();

						if( Rec is not null )
						{
							var CoAddrs = from Ca in CoAddresses
							              where Ca.CompanyId == CoRec.CompanyId
							              select Ca;

							if( CoAddrs.Any() )
							{
								var Cc = new CustomerCompanies
								         {
									         CustomerCode = CustomerCode
								         };
								RetVal.Add( Cc );

								foreach( var CoAddr in CoAddrs )
								{
									var Addr = ( from A in Addresses
									             where A.Id == CoAddr.AddressId
									             select A ).FirstOrDefault();

									if( Addr is not null )
									{
										Cc.Companies.Add( new Protocol.Data.Company
										                  {
											                  CompanyId       = CoRec.CompanyId,
											                  CompanyNumber   = CoRec.CompanyNumber.NullTrim(),
											                  CompanyName     = CoRec.DisplayCompanyName.NullTrim(),
											                  LocationBarcode = CoRec.LocationBarcode.NullTrim(),
											                  UserName        = CoRec.UserName.NullTrim(),
											                  Password        = CoRec.Password.NullTrim(),
											                  Barcode         = Addr.Barcode.NullTrim(),
											                  Suite           = Addr.Suite.NullTrim(),
											                  AddressLine1    = Addr.AddressLine1.NullTrim(),
											                  AddressLine2    = Addr.AddressLine2.NullTrim(),
											                  Vicinity        = Addr.Vicinity.NullTrim(),
											                  City            = Addr.City.NullTrim(),
											                  Country         = Addr.Country.NullTrim(),
											                  CountryCode     = Addr.CountryCode.NullTrim(),
											                  PostalBarcode   = Addr.PostalBarcode.NullTrim(),
											                  Region          = Addr.Region.NullTrim(),
											                  PostalCode      = Addr.PostalCode.TrimMax20(),
											                  Latitude        = Addr.Latitude,
											                  Longitude       = Addr.Longitude,
											                  Phone           = Addr.Phone.NullTrim(),
											                  Phone1          = Addr.Phone1.NullTrim(),
											                  EmailAddress    = Addr.EmailAddress.NullTrim(),
											                  EmailAddress1   = Addr.EmailAddress1.NullTrim(),
											                  EmailAddress2   = Addr.EmailAddress2.NullTrim(),
											                  Mobile          = Addr.Mobile.NullTrim(),
											                  Mobile1         = Addr.Mobile1.NullTrim(),
											                  Fax             = Addr.Fax.NullTrim(),
											                  Notes           = Addr.Notes.NullTrim()
										                  } );
									}
								}
							}
						}
					}
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( nameof( GetCustomerCompanyAddress ), Exception );
		}

		return RetVal;
	}

	public bool CheckCustomerCompanyAndAddressLine1( string customerCode, string companyName, string suite, string addressLine1 )
	{
		try
		{
			var Cust = ( from R in Entity.ResellerCustomers
			             where R.CustomerCode == customerCode
			             select R ).FirstOrDefault();

			if( Cust is not null )
			{
				var Companies = GetCompaniesByCompanyAndAddressLine1( companyName, suite, addressLine1 );
				var ReCo      = Entity.ResellerCustomerCompanies;

				foreach( var Company in Companies )
				{
					var Rec = ( from Rc in ReCo
					            where ( Rc.CustomerCode == customerCode ) && ( Rc.CompanyId == Company.CompanyId )
					            select Rc ).FirstOrDefault();

					if( Rec is not null )
						return true;
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( nameof( CheckCustomerCompanyAndAddressLine1 ), Exception );
		}

		return false;
	}

	public bool CheckCustomerCompanyAndAddressLine1( CompanyByAccountSummary coData ) => CheckCustomerCompanyAndAddressLine1( coData.CustomerCode, coData.CompanyName, coData.Suite, coData.AddressLine1 );

	public ResellersCustomerCompanyLookup GetCustomerCompanySecondaryId( string id )
	{
		try
		{
			var (Company, Address, Ok) = GetCompanyBySecondaryId( id );

			if( Ok )
			{
				var Rec = ( from Rc in Entity.ResellerCustomerCompanies
				            where Rc.CompanyId == Company!.CompanyId
				            select Rc ).FirstOrDefault();

				if( Rec is not null )
					return ConvertToCompanyLookup( Rec.CustomerCode, Company!.CompanyName, Company.LocationBarcode, Address! );
			}
		}
		catch( Exception E )
		{
			Context.SystemLogException( nameof( GetCustomerCompanySecondaryId ), E );
		}

		return new ResellersCustomerCompanyLookup();
	}

	public ResellersCustomerCompanyLookup GetCustomerCompanyAddress( string customerCode, string companyName )
	{
		try
		{
			var Companies = GetCompaniesByName( companyName );

			foreach( var Company in Companies )
			{
				var Rec = ( from Rc in Entity.ResellerCustomerCompanies
				            where ( Rc.CompanyId == Company.CompanyId ) && ( Rc.CustomerCode == customerCode )
				            select Rc ).FirstOrDefault();

				if( Rec is not null )
				{
					var (Address1, Ok) = GetCompanyAddress( Company.CompanyId );

					if( Ok )
						return ConvertToCompanyLookup( customerCode, companyName, "", Address1 );
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return new ResellersCustomerCompanyLookup();
	}

	public void DeleteCustomerCompanyAddress( string customerCode, string companyName, string programName )
	{
		try
		{
			var Companies = GetCompaniesByName( companyName );

			foreach( var Company in Companies )
			{
				var (Address1, Ok) = GetCompanyAddress( Company.CompanyId );

				if( Ok )
				{
					// Delete and log
					Entity.Addresses.Remove( Address1 );
					Entity.SaveChanges();
					Context.LogDeleted( ResellerCustomerLogger.LOG_STORAGE, programName, "", new ResellerCustomerLogger( customerCode ) );

					break;
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public void UpdateCustomerCompanyAddress( UpdateCustomerCompany customerCompany, bool filterByReseller = true )
	{
		lock( ResellerUpdateLock ) // Address import overrunning database 
		{
			try
			{
				var E = Entity;

				var CompaniesByName = GetCompaniesByName( customerCompany.Company.CompanyName );

				if( filterByReseller )
				{
					var CIds = ( from C in CompaniesByName
					             select C.CompanyId ).ToList();

					var RCids = ( from Rc in E.ResellerCustomerCompanies
					              where ( Rc.CustomerCode == customerCompany.CustomerCode ) && CIds.Contains( Rc.CompanyId )
					              select Rc.CompanyId ).ToList();

					CompaniesByName = ( from C in CompaniesByName
					                    where RCids.Contains( C.CompanyId )
					                    select C ).ToList();
				}

				foreach( var Company in CompaniesByName )
				{
					var Addr = ( from Ca in E.CompanyAddresses
					             where Ca.CompanyId == Company.CompanyId
					             join A in E.Addresses on Ca.AddressId equals A.Id
					             select A ).FirstOrDefault();

					if( Addr is not null )
					{
						// Update and log
						// Entity.Addresses.(Addr.Address);
						Addr.AddressLine1  = customerCompany.Company.AddressLine1;
						Addr.AddressLine2  = customerCompany.Company.AddressLine2;
						Addr.Barcode       = customerCompany.Company.Barcode;
						Addr.City          = customerCompany.Company.City;
						Addr.Country       = customerCompany.Company.Country;
						Addr.CountryCode   = customerCompany.Company.CountryCode;
						Addr.EmailAddress  = customerCompany.Company.EmailAddress;
						Addr.EmailAddress1 = customerCompany.Company.EmailAddress1;
						Addr.EmailAddress2 = customerCompany.Company.EmailAddress2;
						Addr.Fax           = customerCompany.Company.Fax.TrimMax20();
						Addr.Latitude      = customerCompany.Company.Latitude;
						Addr.Longitude     = customerCompany.Company.Longitude;
						Addr.Mobile        = customerCompany.Company.Mobile.TrimMax20();
						Addr.Mobile1       = customerCompany.Company.Mobile1.TrimMax20();
						Addr.Notes         = customerCompany.Company.Notes;
						Addr.Phone         = customerCompany.Company.Phone.TrimMax20();
						Addr.Phone1        = customerCompany.Company.Phone1.TrimMax20();
						Addr.PostalBarcode = customerCompany.Company.PostalBarcode;
						Addr.PostalCode    = customerCompany.Company.PostalCode.TrimMax20();
						Addr.Region        = customerCompany.Company.Region;
						Addr.SecondaryId   = customerCompany.Company.SecondaryId;
						Addr.Suite         = customerCompany.Company.Suite;
						Addr.Vicinity      = customerCompany.Company.Vicinity;

						Company.UserName        = customerCompany.Company.UserName;
						Company.LocationBarcode = customerCompany.Company.LocationBarcode;

						E.SaveChanges();
						Context.LogDifferences( ResellerCustomerLogger.LOG_STORAGE, customerCompany.ProgramName, "", new ResellerCustomerLogger( customerCompany.CustomerCode ) );
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
	}

	public CustomerCodeList GetCustomerList()
	{
		var CustList = new CustomerCodeList();

		try
		{
			CustList.AddRange( from C in Entity.ResellerCustomers
			                   select C.CustomerCode );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return CustList;
	}

	public CustomerCodeCompanyNameList GetCustomerCodeCompanyNameList()
	{
		var Result = new CustomerCodeCompanyNameList();

		try
		{
			var Recs = from R in Entity.ResellerCustomers
			           select new { R.CustomerCode, R.CompanyName };

			Result.AddRange( from Rec in Recs
			                 select new CustomerCodeCompanyName
			                        {
				                        CustomerCode = Rec.CustomerCode,
				                        CompanyName  = Rec.CompanyName
			                        } );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}

	public CompanyAddress GetResellerCustomerCompany( string customerCode )
	{
		var E = Entity;

		var Rec = ( from R in E.ResellerCustomers
		            where R.CustomerCode == customerCode
		            join C in E.Companies on R.CompanyId equals C.CompanyId
		            join A in E.Addresses on C.PrimaryAddressId equals A.Id
		            select new
		                   {
			                   Comp = C,
			                   Addr = A
		                   } ).FirstOrDefault();

		return Rec?.Addr is not null ? ConvertCompanyAddress( Context, Rec.Comp, Rec.Addr ) : new CompanyAddress();
	}

	public Company? GetResellerCompanyByLocation( string resellerId, string locationCode )
	{
		var E = Entity;

		locationCode = locationCode.TrimStart();

		var LocationCompanies = ( from C in E.Companies
		                          where C.LocationBarcode == locationCode
		                          select C ).ToList();

		switch( LocationCompanies.Count )
		{
		case >= 1 when resellerId.IsNotNullOrWhiteSpace():
			var CoIds = ( from C in LocationCompanies
			              select C.CompanyId ).ToList();

			var CompanyId = ( from R in E.ResellerCustomerCompanies
			                  where ( R.CustomerCode == resellerId ) && CoIds.Contains( R.CompanyId )
			                  select (long?)R.CompanyId ).FirstOrDefault();

			return CompanyId is not null ? ( from C in LocationCompanies
			                                 where C.CompanyId == CompanyId
			                                 select C ).FirstOrDefault() : null;

		case >= 1:
			return LocationCompanies[ 0 ];

		default:
			return null;
		}
	}

	private static ResellersCustomerCompanyLookup ConvertToCompanyLookup( string customerCode, string companyName, string locationBarcode, Address a ) => new()
	                                                                                                                                                      {
		                                                                                                                                                      Ok              = true,
		                                                                                                                                                      CustomerCode    = customerCode,
		                                                                                                                                                      CompanyName     = companyName,
		                                                                                                                                                      LocationBarcode = locationBarcode,
		                                                                                                                                                      AddressLine1    = a.AddressLine1.NullTrim(),
		                                                                                                                                                      AddressLine2    = a.AddressLine2.NullTrim(),
		                                                                                                                                                      Barcode         = a.Barcode.NullTrim(),
		                                                                                                                                                      City            = a.City.NullTrim(),
		                                                                                                                                                      Suite           = a.Suite.NullTrim(),
		                                                                                                                                                      SecondaryId     = a.SecondaryId.NullTrim(),
		                                                                                                                                                      Country         = a.Country.NullTrim(),
		                                                                                                                                                      CountryCode     = a.CountryCode.NullTrim(),
		                                                                                                                                                      EmailAddress    = a.EmailAddress.NullTrim(),
		                                                                                                                                                      EmailAddress1   = a.EmailAddress1.NullTrim(),
		                                                                                                                                                      EmailAddress2   = a.EmailAddress2.NullTrim(),
		                                                                                                                                                      Fax             = a.Fax.NullTrim(),
		                                                                                                                                                      Latitude        = a.Latitude,
		                                                                                                                                                      Longitude       = a.Longitude,
		                                                                                                                                                      Mobile          = a.Mobile.NullTrim(),
		                                                                                                                                                      Mobile1         = a.Mobile1.NullTrim(),
		                                                                                                                                                      Notes           = a.Notes.NullTrim(),
		                                                                                                                                                      Phone           = a.Phone.NullTrim(),
		                                                                                                                                                      Phone1          = a.Phone1.NullTrim(),
		                                                                                                                                                      PostalBarcode   = a.PostalBarcode.NullTrim(),
		                                                                                                                                                      PostalCode      = a.PostalCode.TrimMax20(),
		                                                                                                                                                      Region          = a.Region.NullTrim(),
		                                                                                                                                                      Vicinity        = a.Vicinity.NullTrim()
	                                                                                                                                                      };
}