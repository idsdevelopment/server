﻿namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public bool CarrierPreAddUpdateTripHook( TripUpdate t )
	{
		var CustPreference = CustomerPreferences();

		// ReSharper disable once ConvertIfStatementToReturnStatement
		if( CustPreference.IsPml && PmlPreAddUpdateTrip( t ) ) // Returns true if no more to do
			return true;

		return false;
	}

	public bool CarrierPreUpdateTripStatusHook( TripUpdateStatus t )
	{
		var CustPreference = CustomerPreferences();

		// ReSharper disable once ConvertIfStatementToReturnStatement
		if( CustPreference.IsPml && PmlPreUpdateTripStatus( t ) ) // Returns true if no more to do
			return true;

		return false;
	}
}