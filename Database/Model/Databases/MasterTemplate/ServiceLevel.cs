//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Database.Model.Databases.MasterTemplate
{
    using System;
    using System.Collections.Generic;
    
    public partial class ServiceLevel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public long Background_ARGB { get; set; }
        public long Foreground_ARGB { get; set; }
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }
        public bool Sunday { get; set; }
        public System.TimeSpan StartTime { get; set; }
        public System.TimeSpan EndTime { get; set; }
        public System.TimeSpan DeliveryHours { get; set; }
        public System.TimeSpan WarningHours { get; set; }
        public System.TimeSpan CriticalHours { get; set; }
        public System.TimeSpan BeyondHours { get; set; }
        public short SortOrder { get; set; }
    }
}
