//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Database.Model.Databases.MasterTemplate
{
    using System;
    using System.Collections.Generic;
    
    public partial class PackageType
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public decimal MinVisibleWeight { get; set; }
        public decimal MaxVisibleWeight { get; set; }
        public string Label { get; set; }
        public bool HoursXRate { get; set; }
        public bool WeightXRate { get; set; }
        public bool PiecesXRate { get; set; }
        public bool ExcessRate { get; set; }
        public bool DistanceXRate { get; set; }
        public string Formula { get; set; }
        public bool UsePiecesForCutoff { get; set; }
        public bool Cumulative { get; set; }
        public bool LimitMaxToNextCutoff { get; set; }
        public bool HideOnTripEntry { get; set; }
        public bool SimpleEnabled { get; set; }
        public bool FormulasEnabled { get; set; }
        public bool CutoffsEnabled { get; set; }
        public short SortOrder { get; set; }
        public bool Deleted { get; set; }
    }
}
