//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Database.Model.Databases.Users
{
    using System;
    using System.Collections.Generic;
    
    public partial class Employee
    {
        public string Code { get; set; }
        public string Password { get; set; }
        public bool AllowCreation { get; set; }
    }
}
