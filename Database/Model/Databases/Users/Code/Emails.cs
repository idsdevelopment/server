﻿#nullable enable

using System.IO;
using System.Net.Mail;
using Logs;

namespace Database.Model.Databases.Users;

public class Emails
{
	private const int
	#if DEBUG
		ERROR_WAIT_IN_MINUTES = 1,
	#else
		ERROR_WAIT_IN_MINUTES = 30,
	#endif
		ERROR_EXPIRED_IN_DAYS = 2;

	public static void Send( IRequestContext context, MailMessage message )
	{
		try
		{
			string BuildAddress( MailAddressCollection addresses )
			{
				return string.Join( ";", addresses );
			}

			using var Db  = new UsersEntities();
			var       Now = DateTime.UtcNow;

			var MailRec = new Email
						  {
							  Locked     = true,
							  CarrierId  = context.CarrierId,
							  From       = message.From.Address,
							  To         = BuildAddress( message.To ),
							  CC         = BuildAddress( message.CC ),
							  BCC        = BuildAddress( message.Bcc ),
							  Subject    = message.Subject,
							  Body       = message.Body,
							  IsBodyHtml = message.IsBodyHtml,
							  Created    = Now,
							  SleepUntil = Now
						  };

			Db.Emails.Add( MailRec );
			Db.SaveChanges();

			try
			{
				foreach( var Attachment in message.Attachments )
				{
					var ContentStream = Attachment.ContentStream;

					if( ContentStream is not null )
					{
						byte[] BData;

						using( var MemoryStream = new MemoryStream() )
						{
							ContentStream.Position = 0;
							ContentStream.CopyTo( MemoryStream );
							BData = MemoryStream.ToArray();
						}

						Db.EmailAttachments.Add( new EmailAttachment
												 {
													 Attachment = BData,
													 FileName   = Attachment.Name,
													 MediaType  = Attachment.ContentType.MediaType,
													 MessageId  = MailRec.Id
												 } );
						Db.SaveChanges();
					}
				}
			}
			catch( Exception Exception )
			{
				context.SystemLogException( Exception );
			}
			finally
			{
				MailRec = ( from E in Db.Emails
							where E.Id == MailRec.Id
							select E ).FirstOrDefault();

				if( MailRec is not null )
				{
					MailRec.Locked = false;
					Db.SaveChanges();
				}
			}
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
	}

	public static (long MessageId, string CarrierId, bool Expired, MailMessage? Message) GetNexMessage( string? carrierId = null )
	{
		try
		{
			using var Db = new UsersEntities();

			var Now = DateTime.UtcNow;

			var Rec = ( from E in Db.Emails
						where ( E.SleepUntil <= Now ) && ( ( carrierId == null ) || ( E.CarrierId == carrierId ) ) && !E.Locked
						select E ).FirstOrDefault();

			if( Rec is not null )
			{
				Rec.SleepUntil = Now.AddMinutes( ERROR_WAIT_IN_MINUTES );
				Db.SaveChanges();

				static MailAddress ToAddress( string address )
				{
					return new MailAddress( address );
				}

				static void Addresses( MailAddressCollection collection, string addresses )
				{
					foreach( var A in addresses.Split( [';'], StringSplitOptions.RemoveEmptyEntries ) )
						collection.Add( ToAddress( A ) );
				}

				var Result = new MailMessage();
				Result.From = ToAddress( Rec.From );
				Addresses( Result.To, Rec.To );
				Addresses( Result.CC, Rec.CC );
				Addresses( Result.Bcc, Rec.BCC );
				Result.Subject    = Rec.Subject;
				Result.Body       = Rec.Body;
				Result.IsBodyHtml = Rec.IsBodyHtml;

				foreach( var A in ( from A in Db.EmailAttachments
									where A.MessageId == Rec.Id
									select A ).ToList() )
				{
					// Don't dispose of the stream, the attachment will do that.
					var Stream     = new MemoryStream( A.Attachment );
					var Attachment = new Attachment( Stream, A.FileName, A.MediaType );
					Result.Attachments.Add( Attachment );
				}

				return ( Rec.Id, Rec.CarrierId, ( Now - Rec.Created ).Days >= ERROR_EXPIRED_IN_DAYS, Result );
			}
		}
		catch( Exception Exception )
		{
			SystemLog.Add( Exception );
		}
		return ( 0, "", true, null );
	}

	public static void DeleteEmail( long id )
	{
		using var Db = new UsersEntities();

		var Rec = ( from E in Db.Emails
					where E.Id == id
					select E ).FirstOrDefault();

		if( Rec is not null )
		{
			Db.Emails.Remove( Rec );
			Db.SaveChanges();
		}
	}
}