﻿#nullable enable

using Exceptions;
using Configuration = Database.Model.Databases.Users.Configuration;

namespace Database.Model.Database;

public static partial class Users
{
	public static Configuration Configuration
	{
		get
		{
			try
			{
				using var Db = new UsersEntities();

				return ( from C in Db.Configurations
				         select C ).First();
			}
			catch( Exception E )
			{
				new SystemLogException( null, E );
			}

			return new Configuration();
		}

		set
		{
			try
			{
				using var Db = new UsersEntities();

				while( true )
				{
					try
					{
						var Config = ( from C in Db.Configurations
						               select C ).FirstOrDefault();

						if( Config is null )
						{
							Config = new Configuration();
							Db.Configurations.Add( Config );
						}

						Config.LastAddressCleanup    = value.LastAddressCleanup;
						Config.AverageAccessInterval = value.AverageAccessInterval;
						Db.SaveChanges();
						return;
					}
					catch( DbUpdateConcurrencyException )
					{
					}
					catch( Exception Exception )
					{
						new SystemLogException( null, Exception );
						return;
					}
				}
			}
			catch( Exception Exception )
			{
				new SystemLogException( null, Exception );
			}
		}
	}

	public static LicenseKeys GetLicenseKeys( IRequestContext ctx )
	{
		var Result = new LicenseKeys();

		try
		{
			var Config = Configuration;
			Result.Dynamsoft = Encryption.Encrypt( Config.DynamsoftBarcodeKey );
		}
		catch( Exception Exception )
		{
			ctx.SystemLogException( Exception );
		}
		return Result;
	}
}