﻿#nullable enable

using System.Net.Http;
using InventoryBarcode = Protocol.Data.InventoryBarcode;

// ReSharper disable InconsistentNaming

namespace Database.Model.Database;

public class BarcodeData
{
	[JsonProperty( "0" )]
	public BarcodeDetails? BarcodeDetails { get; set; }

	[JsonProperty( "1" )]
	public BarcodeDetails? BarcodeDetails1 { get; set; }

	[JsonProperty( "2" )]
	public BarcodeDetails? BarcodeDetails2 { get; set; }

	[JsonProperty( "3" )]
	public BarcodeDetails? BarcodeDetails3 { get; set; }

	[JsonProperty( "barcode_number" )]
	public string BarcodeNumber { get; set; } = "";

	[JsonProperty( "name" )]
	public string Name { get; set; } = "";

	[JsonProperty( "address" )]
	public string Address { get; set; } = "";

	[JsonProperty( "phone" )]
	public string Phone { get; set; } = "";

	[JsonProperty( "url" )]
	public string Url { get; set; } = "";

	[JsonProperty( "image_url" )]
	public Uri? ImageUrl { get; set; }

	[JsonProperty( "success" )]
	public bool Success { get; set; }

	[JsonProperty( "source" )]
	public string Source { get; set; } = "";
}

public class BarcodeDetails
{
	[JsonProperty( "val_t" )]
	public string Description { get; set; } = "";

	[JsonProperty( "title" )]
	public string Title { get; set; } = "";

	[JsonProperty( "field_name" )]
	public string FieldName { get; set; } = "";

	[JsonProperty( "image_url" )]
	public Uri? ImageUrl { get; set; }
}

public static partial class Users
{
	private const string BARCODE_TOKEN = "$BARCODE";

	public static InventoryBarcode GetInventoryBarcode( string barcode )
	{
		try
		{
			using var Db = new UsersEntities();

			var Rec = ( from B in Db.InventoryBarcodes
			            where B.Barcode == barcode
			            select B ).FirstOrDefault();

			if( Rec is null )
			{
				var BCodeUrl = Configuration.BarcodeLookupUrl;

				if( BCodeUrl.IsNotNullOrWhiteSpace() )
				{
					var P = BCodeUrl.IndexOf( BARCODE_TOKEN, StringComparison.Ordinal );

					if( P >= 0 )
					{
						BCodeUrl = BCodeUrl.Replace( BARCODE_TOKEN, barcode );

						using var Client    = new HttpClient();
						var       JsonBytes = Client.GetByteArrayAsync( BCodeUrl ).Result;

						if( JsonBytes is not null )
						{
							var Json = Encoding.UTF8.GetString( JsonBytes );

							if( Json.IsNotNullOrWhiteSpace() )
							{
								var BarcodeData = JsonConvert.DeserializeObject<BarcodeData>( Json );

								if( BarcodeData?.BarcodeDetails is not null )
								{
									Rec = new Databases.Users.InventoryBarcode
									      {
										      Barcode     = barcode,
										      Owner       = BarcodeData.Name,
										      Description = BarcodeData.BarcodeDetails.Description
									      };
									Db.InventoryBarcodes.Add( Rec );
									Db.SaveChanges();

									return new InventoryBarcode
									       {
										       Barcode     = Rec.Barcode,
										       Description = Rec.Description,
										       Owner       = Rec.Owner,
										       Found       = true
									       };
								}
							}
						}
					}
				}
			}
			else
			{
				return new InventoryBarcode
				       {
					       Barcode     = Rec.Barcode,
					       Description = Rec.Description,
					       Owner       = Rec.Owner,
					       Found       = true
				       };
			}
		}
		catch
		{
		}

		return new InventoryBarcode();
	}
}