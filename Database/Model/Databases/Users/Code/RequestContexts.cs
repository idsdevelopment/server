﻿namespace Database.Model.Database;

public static partial class Users
{
	public static List<IRequestContext> RequestContexts()
	{
		using var Db = new UsersEntities();

		var Recs = ( from E in Db.EndPoints
				     select E.CarrierId ).ToList();

		var NewContextDelegate = AIRequestInterface.NewContext;

		if( NewContextDelegate is not null )
		{
			return ( from C in Recs
				     select NewContextDelegate( C, "" ) ).ToList();
		}
		return [];
	}
}