﻿#nullable enable

// ReSharper disable InconsistentNaming

namespace Database.Model.Databases.MasterTemplate
{
	public partial class XRef
	{
		public XRef AsAURT( string aurt, string tripId )
		{
			Type = (short)TYPE.PML_AURT;
			Id1  = aurt;
			Id2  = tripId;
			return this;
		}
	}
}

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		public void SaveAurtToXref( string aurt, string tripId )
		{
			aurt   = aurt.Trim();
			tripId = tripId.Trim();

			var E     = Entity;
			var XRefs = E.XRefs;

			var Rec = ( from X in XRefs
			            where ( X.Type == (short)XRef.TYPE.PML_AURT ) && ( X.Id1 == aurt ) && ( X.Id2 == tripId )
			            select X ).FirstOrDefault();

			if( Rec is null )
			{
				XRefs.Add( new XRef().AsAURT( aurt, tripId ) );
				E.SaveChanges();
			}
		}

		public List<string> GetTripIdsFromAurt( string aurt )
		{
			aurt = aurt.Trim();

			return ( from X in Entity.XRefs
			         where ( X.Type == (short)XRef.TYPE.PML_AURT ) && ( X.Id1 == aurt )
			         select X.Id2 ).ToList();
		}

		public IEnumerable<(bool IsMaster, string TripId, string Barcode, decimal Pieces, string Description, string Aurt, string ServiceLevel )> PmlGetMasterAndRelatedTripIdsAndPieces( List<string> tripIds )
		{
			var E = Entity;

			// Find all Xrefs related to masters
			var MasterXrefs = ( from X in E.XRefs
			                    where tripIds.Contains( X.Id1 )
			                    select new {TripId = X.Id2, Description = X.StringData3, Barcode = X.StringData5, OriginalPieces = X.DecimalData1, Aurt = X.StringData1, ServiceLevel = X.StringData2} ).ToList();

			var Result = ( from X in MasterXrefs
			               select ( IsMaster: false, X.TripId.Trim(), X.Barcode.Trim(), X.OriginalPieces, X.Description.Trim(), X.Aurt.Trim(), X.ServiceLevel ) ).ToList();

			var Masters = ( from Trip in E.Trips
			                where tripIds.Contains( Trip.TripId )
			                select new {Trip.TripId, Barcode = Trip.Group4, Trip.OriginalPieceCount, Description = Trip.PackageType, Trip.BillingNotes, Trip.ServiceLevel} ).ToList();

			Result.AddRange( from Master in Masters
			                 select ( IsMaster: true, Master.TripId.Trim(), Master.Barcode.Trim(), Master.OriginalPieceCount, Master.Description.Trim(), Master.BillingNotes.Trim(), Master.ServiceLevel.Trim() ) );

			// Update masters first (less confusing)
			return ( from R in Result
			         orderby R.IsMaster
			         select R ).ToList();
		}
	}
}