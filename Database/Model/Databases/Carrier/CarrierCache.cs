﻿#nullable enable

namespace Database.Model.Databases.Carrier;

// Must be outside generic
internal abstract class CarrierCache
{
	private static readonly Dictionary<string, Dictionary<Type, object>> Caches = new();

	internal object? this[ string carrierId, Type type ]
	{
		get
		{
			lock( Caches )
				return Get( carrierId ).TryGetValue( type, out var Cache ) ? Cache : default;
		}
		set
		{
			if( value is not null )
			{
				lock( Caches )
					Get( carrierId )[ type ] = value;
			}
		}
	}

	private static Dictionary<Type, object> Get( string carrierId )
	{
		if( !Caches.TryGetValue( carrierId, out var CarrierMemoryCacheBases ) )
			Caches.Add( carrierId, CarrierMemoryCacheBases = new Dictionary<Type, object>() );

		return CarrierMemoryCacheBases;
	}

	internal static object Add( string carrierId, Type type, object cache )
	{
		var CarrierCache = Get( carrierId );

		if( !CarrierCache.ContainsKey( type ) )
			CarrierCache.Add( type, cache );

		return cache;
	}
}

internal abstract class CarrierMemoryCache<TKey, TValue> : MemoryCache<TKey, TValue> where TKey : notnull
{
	protected abstract TValue?                          OnGetRecordForCache( CarrierDb db, TKey key );
	protected abstract CarrierMemoryCache<TKey, TValue> OnCreateInstance();

	protected virtual void OnAddCacheRecord( TKey key, TValue value, TimeSpan expires )
	{
		AddAbsolute( key, value!, Expires, null );
	}

	internal CarrierMemoryCache( int expiresInMinutes = EXPIRES_IN_MINUTES )
	{
		Expires = TimeSpan.FromMinutes( expiresInMinutes );
	}

#region Indexeres
	internal new TValue? this[ TKey key ]
	{
		get
		{
			var Rec = base[ key ];
			return Rec.Ok ? Rec.Value : default;
		}
		set
		{
			if( value is not null )
				OnAddCacheRecord( key, value, Expires );
		}
	}

	private CarrierMemoryCache<TKey, TValue> GetCache( CarrierDb db )
	{
		var CacheDb = CarrierCache[ db ];

		if( CacheDb is null )
		{
			CacheDb = OnCreateInstance();
			Carrier.CarrierCache.Add( db.Context.CarrierId, typeof( CarrierMemoryCache<TKey, TValue> ), CacheDb );
		}
		return CacheDb;
	}

	internal CarrierMemoryCache<TKey, TValue> this[ CarrierDb db ] => GetCache( db );


	internal TValue? this[ CarrierDb db, TKey key ]
	{
		get
		{
			var CacheDb = GetCache( db );
			var Rec     = CacheDb[ key ];

			if( Rec is null )
			{
				Rec = OnGetRecordForCache( db, key );

				if( Rec is not null )
					CacheDb[ key ] = Rec;
			}
			return Rec;
		}
		set
		{
			if( value is not null )
			{
				var CacheDb = GetCache( db );
				CacheDb[ key ] = value;
			}
		}
	}
#endregion

#region Expires
	public const     int      EXPIRES_IN_MINUTES = 5;
	private readonly TimeSpan Expires;
#endregion

#region Carrier Cache
	private readonly CCache CarrierCache = new();

	private sealed class CCache : CarrierCache
	{
	#region Indexeres
		private CarrierMemoryCache<TKey, TValue>? this[ string carrierId ] => base[ carrierId, typeof( CarrierMemoryCache<TKey, TValue> ) ] as CarrierMemoryCache<TKey, TValue>;

		private CarrierMemoryCache<TKey, TValue>? this[ IRequestContext context ] => this[ context.CarrierId ];

		internal CarrierMemoryCache<TKey, TValue>? this[ CarrierDb db ] => this[ db.Context ];
	#endregion
	}
#endregion
}