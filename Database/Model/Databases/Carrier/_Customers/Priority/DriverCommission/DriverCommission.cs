﻿#nullable enable

using Protocol.Data.Customers.Priority;
using RouteManifest = StorageV2.Carrier.RouteManifest;
using Shift = FormulaDataTypes.Employee.Shift;

// ReSharper disable MergeIntoPattern

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	private const string STAT       = "STAT",
	                     MULTI_STAT = "MULTISTAT",
	                     SWEEP      = "SWEEP";

	public class PriorityDriverCommission : CommissionDetailsBase
	{
		public Protocol.Data.Customers.Priority.PriorityDriverCommission.Address PickupAddress   { get; set; } = new();
		public Protocol.Data.Customers.Priority.PriorityDriverCommission.Address DeliveryAddress { get; set; } = new();

		public string ManifestName { get; set; } = "";
		public int    LegNumber    { get; set; }

		public PriorityDriverCommission()
		{
			Type = COMMISSION_DETAILS_TYPE.PRIORITY;
		}

		public PriorityDriverCommission( string pickupCo, string pickupStreet, string pickupCity, string deliveryCo, string deliveryStreet, string deliveryCity, string manifestName, int legNumber ) : this()
		{
			PickupAddress.CompanyName = pickupCo.Trim();
			PickupAddress.Street      = pickupStreet.Trim();
			PickupAddress.City        = pickupCity.Trim();

			DeliveryAddress.CompanyName = deliveryCo.Trim();
			DeliveryAddress.Street      = deliveryStreet.Trim();
			DeliveryAddress.City        = deliveryCity.Trim();

			ManifestName = manifestName;
			LegNumber    = legNumber;
		}
	}

	public PriorityDriverCommissions PriorityGetDriverCommission( PriorityDriverCommissionRange r )
	{
		var Result = new PriorityDriverCommissions();
		r.Driver = r.Driver.Trim();

		try
		{
			foreach( var Commission in from C in Entity.StaffCommissions
			                           orderby C.SortOrder
			                           where ( C.StaffId == r.Driver ) && ( C.DateTime >= r.FromDate ) && ( C.DateTime <= r.ToDate )
			                           select C )
			{
				if( Commission.DetailsAsJson.IsNotNullOrWhiteSpace() )
				{
					switch( (MASTER_COMMISSION_DETAILS_TYPE)Commission.DetailsType )
					{
					case MASTER_COMMISSION_DETAILS_TYPE.COMMISSION_DETAILS:
						var Details = JsonConvert.DeserializeObject<PriorityDriverCommission>( Commission.DetailsAsJson ) ?? new PriorityDriverCommission();

						Result.Add( new Protocol.Data.Customers.Priority.PriorityDriverCommission
						            {
							            Reference = Commission.Reference,
							            LegNumber = Commission.SortOrder,

							            DateTime     = Commission.DateTime,
							            TripId       = Commission.TripId,
							            Distance     = Commission.BaseValue,
							            Commission   = Commission.ExtendedValue,
							            ServiceLevel = Commission.ServiceLevel,

							            DeliveryAddress = Details.DeliveryAddress,
							            ManifestName    = Details.ManifestName
						            } );

						break;

					case MASTER_COMMISSION_DETAILS_TYPE.ERROR:
						var Error = JsonConvert.DeserializeObject<string>( Commission.DetailsAsJson ) ?? "Unable to convert error";
						Details = new PriorityDriverCommission();

						Result.Add( new Protocol.Data.Customers.Priority.PriorityDriverCommission
						            {
							            Reference = Commission.Reference,

							            DateTime   = Commission.DateTime,
							            TripId     = Commission.TripId,
							            Distance   = Commission.BaseValue,
							            Commission = Commission.ExtendedValue,

							            DeliveryAddress = Details.DeliveryAddress,

							            ManifestName = Details.ManifestName,
							            LegNumber    = Commission.SortOrder,

							            Error = Error
						            } );
						break;
					}
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}

	public TripList PriorityGetUnCalculatedDriverCommissionForDriver( PriorityDriverCommissionRange request )
	{
		var Result = new TripList();

		try
		{
			var Driver = request.Driver.Trim();

			var From = request.FromDate.DateTime;
			var To   = request.ToDate.EndOfDay().DateTime;

			var Da = ( from T in Entity.Trips
			           orderby T.PickupTime
			           where !T.DriverCommissionCalculated
			                 && ( string.Compare( Driver, T.DriverCode.Trim(), StringComparison.OrdinalIgnoreCase ) == 0 )
			                 && ( T.Status1 >= (int)STATUS.DELIVERED ) && ( T.Status1 < (int)STATUS.DELETED )
			                 && ( T.PickupTime >= From ) && ( T.PickupTime <= To )
			                 && ( ( string.Compare( T.ServiceLevel, STAT, StringComparison.OrdinalIgnoreCase ) == 0 )
			                      || ( string.Compare( T.ServiceLevel, MULTI_STAT, StringComparison.OrdinalIgnoreCase ) == 0 )
			                      || ( string.Compare( T.ServiceLevel, SWEEP, StringComparison.OrdinalIgnoreCase ) == 0 ) )
			           orderby T.DriverCode, T.AccountId
			           select new {DriverCode = T.DriverCode.Trim(), AccountId = T.AccountId.Trim()} ).FirstOrDefault();

			if( Da is not null )
			{
				var Trips = from T in Entity.Trips
				            orderby T.PickupTime
				            where !T.DriverCommissionCalculated
				                  && ( string.Compare( Da.DriverCode, T.DriverCode.Trim(), StringComparison.OrdinalIgnoreCase ) == 0 )
				                  && ( string.Compare( Da.AccountId, T.AccountId.Trim(), StringComparison.OrdinalIgnoreCase ) == 0 )
				                  && ( T.Status1 >= (int)STATUS.DELIVERED ) && ( T.Status1 < (int)STATUS.DELETED )
				                  && ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                  && ( ( string.Compare( T.ServiceLevel, STAT, StringComparison.OrdinalIgnoreCase ) == 0 )
				                       || ( string.Compare( T.ServiceLevel, MULTI_STAT, StringComparison.OrdinalIgnoreCase ) == 0 )
				                       || ( string.Compare( T.ServiceLevel, SWEEP, StringComparison.OrdinalIgnoreCase ) == 0 ) )
				            select T;

				if( Trips.Any() )
				{
					foreach( var Trip in Trips )
						Result.Add( Trip.ToTrip() );
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}

	public TripList PriorityGetUnCalculatedDriverCommission( DateTimeOffset date )
	{
		var Result = new TripList();

		try
		{
			date = date.EndOfDay();

			var Driver = ( from T in Entity.Trips
			               where !T.DriverCommissionCalculated
			                     && ( T.DriverCode.Trim() != "" )
			                     && ( T.Status1 >= (int)STATUS.DELIVERED ) && ( T.Status1 < (int)STATUS.DELETED )
			                     && ( T.PickupTime <= date )
			                     && ( ( string.Compare( T.ServiceLevel, STAT, StringComparison.OrdinalIgnoreCase ) == 0 )
			                          || ( string.Compare( T.ServiceLevel, MULTI_STAT, StringComparison.OrdinalIgnoreCase ) == 0 )
			                          || ( string.Compare( T.ServiceLevel, SWEEP, StringComparison.OrdinalIgnoreCase ) == 0 ) )
			               orderby T.DriverCode, T.AccountId
			               select new {DriverCode = T.DriverCode.Trim(), AccountId = T.AccountId.Trim()} ).FirstOrDefault();

			if( Driver is not null )
			{
				var Trips = from T in Entity.Trips
				            orderby T.PickupTime
				            where !T.DriverCommissionCalculated
				                  && ( string.Compare( Driver.DriverCode, T.DriverCode.Trim(), StringComparison.OrdinalIgnoreCase ) == 0 )
				                  && ( string.Compare( Driver.AccountId, T.AccountId.Trim(), StringComparison.OrdinalIgnoreCase ) == 0 )
				                  && ( T.Status1 >= (int)STATUS.DELIVERED ) && ( T.Status1 < (int)STATUS.DELETED )
				                  && ( T.PickupTime <= date )
				                  && ( ( string.Compare( T.ServiceLevel, STAT, StringComparison.OrdinalIgnoreCase ) == 0 )
				                       || ( string.Compare( T.ServiceLevel, MULTI_STAT, StringComparison.OrdinalIgnoreCase ) == 0 )
				                       || ( string.Compare( T.ServiceLevel, SWEEP, StringComparison.OrdinalIgnoreCase ) == 0 ) )
				            select T;

				if( Trips.Any() )
				{
					foreach( var Trip in Trips )
						Result.Add( Trip.ToTrip() );
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return Result;
	}

	public void PriorityUpdateDriverCommission( UpdateDriverCommission u )
	{
		decimal Value;
		var     Formula = u.Formula;

		if( Formula.IsNotNullOrWhiteSpace() )
		{
			( var Errors, Value ) = new Shift
			                        {
				                        DELIVERY_ZONE = "",
				                        KILOMETRES    = u.Distance,
				                        Formula       = Formula.Trim(),
				                        PACKAGE_TYPE  = "",
				                        PICKUP_ZONE   = ""
			                        }.Evaluate();

			if( Errors.Count > 0 ) // Invalid formula
			{
				UpdateCommissionError( u.StaffId, u.Reference, u.SortOrder, u.DateTime, u.TripId, "", "", $"Formula Error: {Errors[ 0 ]}" );
				return;
			}
		}
		else
			Value = 0;

		u.Reference = u.Reference.Trim();
		var ManifestName = $"{u.StaffId}-{u.Reference}";

		// Don't run in thread. Remote server (Priority Driver Commission) gets ahead of blob storage
		var Manifest = new RouteManifest( Context, ManifestName, u.SortOrder );
		Manifest.WriteDirections( JsonConvert.SerializeObject( u.Route ) );

		UpdateCommissionDetails( u.StaffId, u.Reference, u.SortOrder, u.DateTime, u.Distance, Math.Round( Value, 3, MidpointRounding.AwayFromZero ), u.TripId, u.ServiceLevel, u.Reference,
		                         new PriorityDriverCommission( u.PickupCompany, u.PickupStreet, u.PickupCity, u.DeliveryCompany, u.DeliveryStreet, u.DeliveryCity, ManifestName, u.SortOrder ) );
	}
}