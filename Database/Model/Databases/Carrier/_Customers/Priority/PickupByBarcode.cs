﻿#nullable enable

using Protocol.Data.Customers.Priority;

namespace Database.Model.Databases.Carrier;

public static class PriorityExtensions
{
	//
	// Structure (I think)
	//
	// CX-S[Source Location[-Heartland Location]]+D[Destination Location[-Nursing Location]]+I[Invoice Number]
	// 

	public static ( bool Ok, bool IsHeartland, string SourceLocation, string Destinationlocation, string NursingStation, string InvoiveNumber ) SplitCxBarcode( this string barcode )
	{
		(bool Ok, bool IsHeartland, string SourceLocation, string Destinationlocation, string NursingStation, string InvoiveNumber) Result = ( false, false, "", "", "", "" );

		var Barcode = barcode.Trim();

		if( Barcode.StartsWith( "CX-S", StringComparison.OrdinalIgnoreCase ) )
		{
			Barcode = Barcode.TrimStart( "CX-S", StringComparison.OrdinalIgnoreCase );
			var P = Barcode.IndexOf( '+' ); // Start of destination

			if( P > 0 )
			{
				var SourceLocation      = Barcode.Substring( 0, P );
				var DestinationLocation = Barcode.Substring( P + 1 );

				if( DestinationLocation.StartsWith( "D", StringComparison.OrdinalIgnoreCase ) )
				{
					DestinationLocation = DestinationLocation.TrimStart( "D", StringComparison.OrdinalIgnoreCase );
					P                   = DestinationLocation.IndexOf( '+' );

					if( P > 0 )
					{
						Result.InvoiveNumber = DestinationLocation.Substring( P + 1 );

						DestinationLocation = DestinationLocation.Substring( 0, P );

						var P1 = SourceLocation.IndexOf( '-' );

						if( P1 > 0 )
						{
							Result.IsHeartland    = true;
							Result.SourceLocation = SourceLocation.Substring( 0, P1 );
						}
						else
							Result.SourceLocation = SourceLocation;

						var DestAndNursing = DestinationLocation.Substring( 0, P );
						P = DestAndNursing.IndexOf( '-' );

						if( P > 0 )
						{
							Result.Destinationlocation = DestAndNursing.Substring( 0, P );
							Result.NursingStation      = DestAndNursing.Substring( P + 1 );
						}
						else
							Result.Destinationlocation = DestAndNursing;

						Result.Ok = true;
					}
				}
			}
		}
		return Result;
	}
}

public partial class CarrierDb
{
	public List<TripUpdate> PriorityPickupByBarcode( PickupByBarcode pickup )
	{
		var Result = new List<TripUpdate>();

		try
		{
			var E     = Entity;
			var Trips = E.Trips;

			foreach( var CxBarcode in pickup.TripIds )
			{
				var Rec = ( from T in Trips
				            where T.TripId == CxBarcode
				            select T ).FirstOrDefault();

				if( Rec is not null )
				{
					var Before = new TripLogger( Rec );
					var Driver = Rec.DriverCode;

					if( Driver.IsNotNullOrWhiteSpace() && ( Driver != pickup.Driver ) )
						Rec.Status2 |= (int)STATUS1.CLAIMED;

					Rec.DriverCode = pickup.Driver.Trim();
					Rec.Status1    = (int)STATUS.PICKED_UP;

					E.SaveChanges();

					LogTrip( pickup.Program, Before, new TripLogger( Rec ) );

					Result.Add( Rec.ToTripUpdate( pickup.Program, true, true, false ) );
				}
				else
				{
					var (Ok, _, _, DestinationLocation, _, _) = CxBarcode.SplitCxBarcode();

					if( Ok )
					{
						var Trip = MasterTemplate.Trip.NewTrip();
						Trip.TripId       = CxBarcode;
						Trip.Status1      = (int)STATUS.PICKED_UP;
						Trip.AccountId    = pickup.AccountId;
						Trip.ServiceLevel = pickup.ServiceLevel;
						Trip.Route        = pickup.Route;

						var PickupAddress = GetResellerCustomerCompany( pickup.AccountId );

						Trip.PickupCompanyName      = PickupAddress.CompanyName;
						Trip.PickupCompanyAccountId = pickup.AccountId;
						Trip.PickupAddressBarcode   = PickupAddress.Barcode;

						Trip.PickupAddressAddressLine1  = PickupAddress.AddressLine1;
						Trip.PickupAddressAddressLine2  = PickupAddress.AddressLine2;
						Trip.PickupAddressCity          = PickupAddress.City;
						Trip.PickupAddressRegion        = PickupAddress.Region;
						Trip.PickupAddressPostalCode    = PickupAddress.PostalCode;
						Trip.PickupAddressPostalBarcode = PickupAddress.PostalBarcode;
						Trip.PickupAddressCountry       = PickupAddress.Country;
						Trip.PickupAddressCountryCode   = PickupAddress.CountryCode;

						var (CoOk, Company, Address) = GetCompanyByLocationBarcode( DestinationLocation );

						if( CoOk )
						{
							Trip.DeliveryCompanyName          = Company.CompanyName;
							Trip.DeliveryCompanyAccountId     = pickup.AccountId;
							Trip.DeliveryAddressBarcode       = Address.Barcode;
							Trip.DeliveryAddressAddressLine1  = Address.AddressLine1;
							Trip.DeliveryAddressAddressLine2  = Address.AddressLine2;
							Trip.DeliveryAddressCity          = Address.City;
							Trip.DeliveryAddressRegion        = Address.Region;
							Trip.DeliveryAddressPostalCode    = Address.PostalCode;
							Trip.DeliveryAddressPostalBarcode = Address.PostalBarcode;
							Trip.DeliveryAddressCountry       = Address.Country;
							Trip.DeliveryAddressCountryCode   = Address.CountryCode;
						}

						Trips.Add( Trip );
						E.SaveChanges();
						LogTrip( pickup.Program, Trip );
						Result.Add( Trip.ToTripUpdate( pickup.Program, false, true, false ) );
					}
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogExceptionProgram( pickup.Program, Exception );
		}

		return Result;
	}
}