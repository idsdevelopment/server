﻿#nullable enable

using Protocol.Data.Customers.Priority;
using Company = Protocol.Data.Company;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public void PriorityUpdatePickupAddress( UpdatePickupAddress u )
	{
		try
		{
			var E = Entity;

			while( true )
			{
				try
				{
					var Rec = ( from T in E.Trips
					            where T.TripId == u.TripId
					            select T ).FirstOrDefault();

					if( Rec is not null )
					{
						new TripLogger( Rec );

						var Address = new Company
						              {
							              CompanyName     = Rec.PickupCompanyName,
							              CompanyNumber   = "",
							              Barcode         = Rec.Barcode,
							              LocationBarcode = Rec.Location,
							              ContactName     = Rec.PickupContact,

							              Suite         = Rec.PickupAddressSuite,
							              AddressLine1  = Rec.PickupAddressAddressLine1,
							              AddressLine2  = Rec.PickupAddressAddressLine2,
							              City          = Rec.PickupAddressCity,
							              Vicinity      = Rec.PickupAddressVicinity,
							              Region        = Rec.PickupAddressRegion,
							              Country       = Rec.PickupAddressCountry,
							              CountryCode   = Rec.PickupAddressCountryCode,
							              PostalCode    = Rec.PickupAddressPostalCode,
							              PostalBarcode = Rec.PickupAddressPostalBarcode,
							              EmailAddress  = Rec.PickupAddressEmailAddress,
							              EmailAddress1 = Rec.PickupAddressEmailAddress1,
							              EmailAddress2 = Rec.PickupAddressEmailAddress2,
							              Enabled       = true,
							              Fax           = Rec.PickupAddressFax,
							              Latitude      = (decimal)Rec.PickupLatitude,
							              Longitude     = (decimal)Rec.PickupLongitude,
							              Mobile        = Rec.PickupAddressMobile,
							              Mobile1       = Rec.PickupAddressMobile1,
							              Notes         = Rec.PickupAddressNotes,
							              Password      = "",
							              Phone         = Rec.PickupAddressPhone,
							              Phone1        = Rec.PickupAddressPhone1,
							              SecondaryId   = "",
							              UserName      = "",
							              ZoneName      = Rec.PickupZone
						              };

						Rec.ExtensionAsJson = JsonConvert.SerializeObject( Address );

						Rec.PickupCompanyName = u.CompanyName;
						Rec.Location          = u.LocationBarcode;
						Rec.Barcode           = u.Barcode;

						Rec.PickupContact = u.ContactName;

						Rec.PickupAddressSuite         = u.Suite;
						Rec.PickupAddressAddressLine1  = u.AddressLine1;
						Rec.PickupAddressAddressLine2  = u.AddressLine2;
						Rec.PickupAddressCity          = u.City;
						Rec.PickupAddressRegion        = u.Region;
						Rec.PickupAddressVicinity      = u.Vicinity;
						Rec.PickupAddressCountry       = u.Country;
						Rec.PickupAddressCountryCode   = u.CountryCode;
						Rec.PickupAddressPostalCode    = u.PostalCode;
						Rec.PickupAddressPostalBarcode = u.PostalBarcode;
						Rec.PickupAddressEmailAddress  = u.EmailAddress;
						Rec.PickupAddressEmailAddress1 = u.EmailAddress1;

						Rec.PickupAddressEmailAddress2 = u.EmailAddress2;
						Rec.PickupAddressFax           = u.Fax;

						Rec.PickupLatitude  = (double)u.Latitude;
						Rec.PickupLongitude = (double)u.Longitude;

						Rec.PickupAddressMobile  = u.Mobile;
						Rec.PickupAddressMobile1 = u.Mobile1;
						Rec.PickupAddressNotes   = u.Notes;

						Rec.PickupAddressPhone  = u.Phone;
						Rec.PickupAddressPhone1 = u.Phone1;

						Rec.PickupZone = u.ZoneName;

						E.SaveChanges();
					}
				}
				catch( DbUpdateConcurrencyException )
				{
					continue;
				}
				break;
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogExceptionProgram( u.ProgramName, Exception );
		}
	}
}