﻿using Ids1WebService;
using Protocol.Data._Customers.Pml;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public TripUpdateList PML_Futile( Futile futile )
	{
		var Result = new TripUpdateList
		             {
			             Program = futile.ProgramName
		             };

		try
		{
			var LastModified = DateTime.UtcNow;

			var Now = futile.DateTime.ToString( "g" );

			var Reason = $"Pickup Cancelled {futile.Reason} {Now}\r\n";

			var E     = Entity;
			var Trips = E.Trips;

			var Recs = ( from T in Trips
			             where futile.TripIds.Contains( T.TripId )
			             select T ).ToList();

			foreach( var Rec in Recs )
			{
				var Before = new TripLogger( Rec );

				if( futile.Status != STATUS.DELETED )
					Rec.Status1 = (int)STATUS.ACTIVE;

				Rec.LastModified = LastModified;

				Rec.PickupNotes = $"{Reason}{Rec.PickupNotes}";
				E.SaveChanges();

				var After = new TripLogger( Rec );
				LogTrip( futile.ProgramName, Before, After );
			}

			if( futile.Status == STATUS.DELETED )
			{
				void Futile( string tripId )
				{
					IDSClient.Ids1.Futile( Context, futile.ProgramName, tripId, futile.DateTime, Reason );
				}

				foreach( var Rec in Recs )
				{
					foreach( var Ids1TripId in GetRelatedTripIds( [Rec.TripId] ) )
						Futile( Ids1TripId );

					var Before = new TripLogger( Rec );
					Rec.Status1 = (int)STATUS.LIMBO;
					Rec.Status2 = (int)STATUS1.WAS_DELETED;
					E.SaveChanges();

					//	DeleteTrip( futile.ProgramName, Rec, LastModified, futile.DateTime );
					var After = new TripLogger( Rec );
					LogTrip( futile.ProgramName, Before, After );

					Futile( Rec.TripId );
				}
			}

			foreach( var PTrip in from R in Recs
			                      select R.ToTrip() )
			{
				PTrip.Status1                  = futile.Status;
				PTrip.BroadcastToDispatchBoard = true;
				PTrip.BroadcastToDriverBoard   = true;
				PTrip.BroadcastToDriver        = true;
				Result.Trips.Add( PTrip );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}
}