﻿#nullable enable

using Ids1WebService;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public static long FixPmlXref( IRequestContext context )
	{
		const string PROGRAM = "XRef Cleanup V1.00";

		var Pid = Database.Users.StartProcess( context );

		Tasks.RunVoid( () =>
		               {
			               var PmlContext = context.Clone( "PML" ); // Use Pml for PmlTest

			               try
			               {
				               using var Db1 = new CarrierDb( context );
				               var       E   = Db1.Entity;

				               while( true )
				               {
					               try
					               {
						               var TripsIds = ( from T in E.Trips
						                                where T.Status1 == (int)STATUS.LIMBO
						                                select T.TripId ).ToList();

						               // Cleanup All Trips That Have Been Processed
						               foreach( var TripId in TripsIds )
						               {
							               var Ids1Trip = IDSClient.Ids1.GetTrip( PmlContext, TripId );

							               if( Ids1Trip is null || ( (STATUS)Ids1Trip.status >= STATUS.PICKED_UP ) )
							               {
								               Db1.FinaliseTrip( PROGRAM, TripId );

								               var Xrs = ( from X in E.XRefs
								                           where ( X.Id1 == TripId ) || ( X.Id2 == TripId )
								                           select X ).ToList();

								               if( Xrs.Count > 0 )
								               {
									               E.XRefs.RemoveRange( Xrs );
									               E.SaveChanges();
								               }
							               }
						               }

						               // Remove All Xrefs That Don't have Trips
						               var Xrefs = ( from X in E.XRefs
						                             where ( from T in E.Trips
						                                     where ( T.TripId == X.Id1 ) || ( T.TripId == X.Id2 )
						                                     select T ).FirstOrDefault() == null
						                             select X ).ToList();

						               if( Xrefs.Count > 0 )
						               {
							               E.XRefs.RemoveRange( Xrefs );
							               E.SaveChanges();
						               }
					               }
					               catch( DbUpdateConcurrencyException )
					               {
						               continue;
					               }
					               break;
				               }
			               }
			               catch( Exception E )
			               {
				               context.SystemLogExceptionProgram( $"{PROGRAM} (Did not complete.)", E );
			               }
			               finally
			               {
				               Database.Users.EndProcess( context, Pid );
			               }
		               } );
		return Pid;
	}
}