﻿using Protocol.Data._Customers.Pml;
using Trip = Database.Model.Databases.MasterTemplate.Trip;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public record OriginalDriverTrip( string OriginalDriver, Trip Trip );

	public List<OriginalDriverTrip> PML_ClaimTrips( ClaimTrips requestObject )
	{
		var Result = new List<OriginalDriverTrip>();

		try
		{
			var E       = Entity;
			var TripIds = requestObject.TripIds;

			do
			{
				try
				{
					var Trips = ( from T in E.Trips
					              where TripIds.Contains( T.TripId )
					              select T ).ToList();

					// Only process the ones that are found
					TripIds = ( from T in Trips
					            select T.TripId ).ToList();

					foreach( var Trip in Trips )
					{
						var Before         = new TripLogger( Trip );
						var OriginalDriver = Trip.DriverCode;

						Trip.DriverCode       = requestObject.UserName;
						Trip.DeliveryNotes    = $"Cage: {requestObject.Cage}\r\n{Trip.DeliveryNotes}";
						Trip.ClaimedTime      = requestObject.ClaimTime;
						Trip.ClaimedLatitude  = requestObject.Latitude;
						Trip.ClaimedLongitude = requestObject.Longitude;
						Trip.Status2          = (int)STATUS1.CLAIMED;

						E.SaveChanges();

						var After = new TripLogger( Trip );
						LogTrip( requestObject.ProgramName, Before, After );

						TripIds.Remove( Trip.TripId );
						Result.Add( new OriginalDriverTrip( OriginalDriver, Trip ) );
					}
				}

				catch( DbUpdateConcurrencyException )
				{
				}
			}
			while( TripIds.Count > 0 );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}
}