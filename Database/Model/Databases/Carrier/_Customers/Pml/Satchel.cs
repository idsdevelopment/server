﻿#nullable enable

using Ids1WebService;
using Protocol.Data._Customers.Pml;
using StorageV2.Carrier;
using TransferRecords;
using Trip = Database.Model.Databases.MasterTemplate.Trip;

namespace Database.Model.Databases.Carrier;

public static class PmlTripExtensions
{
	public static bool IsUplift( this string str ) => str.ToUpper() is "UP" or "UPLIFT" or "UPPACK" or "UPPC" or "UPTOTAL";
	public static bool IsUplift( this TripItem item ) => item.ItemCode.IsUplift();
}

public partial class CarrierDb
{
	public List<(Trip Trip, string OriginalDriver)> PML_ClaimSatchels( ClaimSatchels satchels )
	{
		var Result = new List<(Trip Trip, string OriginalDriver)>();

		try
		{
			var E          = Entity;
			var SatchelIds = satchels.SatchelIds;

			do
			{
				try
				{
					var Trips = ( from T in E.Trips
					              where SatchelIds.Contains( T.TripId )
					              select T ).ToList();

					// Only process the ones that are found
					SatchelIds = ( from T in Trips
					               select T.TripId ).ToList();

					foreach( var T in Trips )
					{
						var OriginalDriver = T.DriverCode;

						var Before = new TripLogger( T );

						new Signatures( Context ).AddUpdate( T.TripId, satchels.Signature );

						T.DriverCode       =  satchels.UserName;
						T.DeliveryNotes    =  $"Cage: {satchels.Cage}\r\n{T.DeliveryNotes}";
						T.ClaimedTime      =  satchels.ClaimTime;
						T.ClaimedLatitude  =  satchels.Latitude;
						T.ClaimedLongitude =  satchels.Longitude;
						T.Status2          |= (int)STATUS1.CLAIMED;
						E.SaveChanges();

						var After = new TripLogger( T );

						LogTrip( satchels.ProgramName, Before, After );

						Result.Add( ( T, OriginalDriver ) );
						SatchelIds.Remove( T.TripId );
					}
				}
				catch( DbUpdateConcurrencyException )
				{
				}
				catch( Exception Exception )
				{
					Context.SystemLogException( Exception );
					break;
				}
			}
			while( SatchelIds.Count > 0 );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}


	public (Trip? Satchel, string OriginalDriver) PML_VerifySatchel( VerifySatchel verify )
	{
		(Trip? Satchel, string OriginalDriver) Result = ( null, "" );

		while( true )
		{
			try
			{
				var E      = Entity;
				var TripId = verify.TripId.Trim();

				var Satchel = ( from S in E.Trips
				                where S.TripId == TripId
				                select S ).FirstOrDefault();

				if( Satchel is not null )
				{
					Result.OriginalDriver = Satchel.DriverCode;

					var Before = new TripLogger( Satchel );

					Satchel.DriverCode = verify.UserName;
					Satchel.POD        = verify.UserName;
					Satchel.Status1    = (int)STATUS.VERIFIED;

					var VerifyTime = verify.DateTime;

					Satchel.VerifiedTime      = VerifyTime;
					Satchel.VerifiedLatitude  = verify.Latitude;
					Satchel.VerifiedLongitude = verify.Longitude;

					E.SaveChanges();

					var After = new TripLogger( Satchel );

					LogTrip( verify.ProgramName, Before, After );

					FinaliseTrip( verify.ProgramName, Satchel, DateTime.UtcNow, VerifyTime, verify.Latitude, verify.Longitude );

					Satchel.Status1 = (int)STATUS.FINALISED;

					IDSClient.Ids1.VerifyTrip( Context, TripId, verify.ProgramName, VerifyTime );
					Result.Satchel = Satchel;
				}
			}
			catch( DbUpdateConcurrencyException )
			{
				continue;
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}
	}


	public (List<string> FinalisedTripIds, Trip? Satchel) PML_UpdateSatchel( Satchel s )
	{
		var Result = ( FinalisedTripIds: new List<string>(), Satchel: (Trip?)null );

		try
		{
			var Ids1TripIds = Ids1Update( s );

			var E     = Entity;
			var Trips = E.Trips;

			try
			{
				var TotalQuantityForBarcode = new Dictionary<string, decimal>();

				foreach( var Package in s.Packages )
				{
					foreach( var Item in Package.Items )
					{
						if( !TotalQuantityForBarcode.TryGetValue( Item.Barcode, out var Qty ) )
							Qty = 0;

						TotalQuantityForBarcode[ Item.Barcode ] = Qty + Item.Pieces;
					}
				}

				var   Now        = DateTime.UtcNow;
				Trip? PickupTrip = null;

				// Primary trip ids
				foreach( var Rec in ( from T in Trips
				                      where s.TripIds.Contains( T.TripId )
				                      select T ).ToList() )
				{
					PickupTrip = Rec;
					var TripId = Rec.TripId;

					new Signatures( Context ).AddUpdate( TripId, s.Signature );

					var Before = new TripLogger( Rec );

					Rec.LastModified = Now;
					Rec.Reference    = s.SatchelId;
					Rec.POP          = s.POP;

					var Packages = JsonConvert.DeserializeObject<List<TripPackage>>( Rec.TripItemsAsJson );

					if( Packages is not null )
					{
						foreach( var Package in Packages )
						{
							foreach( var Item in Package.Items )
							{
								if( TotalQuantityForBarcode.TryGetValue( Item.Barcode, out var Residual ) && ( Residual > 0 ) )
								{
									var Allowed = Item.Original - Item.Pieces;

									if( Allowed > 0 )
									{
										var Adj = Math.Min( Residual, Allowed );
										Item.Pieces                             += Adj;
										Residual                                -= Adj;
										TotalQuantityForBarcode[ Item.Barcode ] =  Residual;
									}
								}
							}
						}
					}
					else
						Packages = [];

					Rec.TripItemsAsJson = JsonConvert.SerializeObject( Packages );

					E.SaveChanges();

					var After = new TripLogger( Rec );

					LogTrip( s.ProgramName, Before, After );
					FinaliseTrip( s.ProgramName, Rec, DateTime.Now );
					Result.FinalisedTripIds.Add( TripId );
				}

				if( PickupTrip is not null )
				{
					var Sid = s.SatchelId.Trim();

					static string NextId( string sid )
					{
						var P = sid.LastIndexOf( '-' );

						if( P > 0 )
						{
							var Temp = sid.Split( ["-"], StringSplitOptions.RemoveEmptyEntries );
							var Ndx  = Temp.Length - 1;
							var Num  = int.Parse( Temp[ Ndx ] );
							Array.Resize( ref Temp, Ndx );

							var Tmp = string.Join( "-", Temp );

							if( ( Num % 10 ) != 9 )
								return $"{Tmp}-{Num + 1}";
						}
						return $"{sid}-1";
					}

					var SatchelId = ( from T in Trips
					                  where T.TripId == Sid
					                  select T.TripId ).FirstOrDefault();

					if( SatchelId is not null )
						Sid = NextId( SatchelId );
					else
					{
						SatchelId = ( from T in E.TripStorageIndexes
						              where T.TripId == Sid
						              select T.TripId ).FirstOrDefault();

						if( SatchelId is not null )
							Sid = NextId( SatchelId );
					}

					const decimal TOTAL_PIECES = 1M;

					var STrip = new Trip( PickupTrip )
					            {
						            TripId          = Sid,
						            DriverCode      = s.Driver,
						            LastModified    = Now,
						            Reference       = string.Join( ",", s.TripIds ).Max900Bytes(), // Db Index Limit
						            Status1         = (int)STATUS.PICKED_UP,
						            ServiceLevel    = "Satchel",
						            PackageType     = "Satchel",
						            Pieces          = TOTAL_PIECES,
						            TripItemsType   = (int)TripUpdate.PACKAGE_TYPES.PACKAGE,
						            TripItemsAsJson = JsonConvert.SerializeObject( s.Packages ),
						            PickupTime      = s.PickupTime,
						            PickupLatitude  = s.PickupLatitude,
						            PickupLongitude = s.PickupLongitude,
						            CurrentZone     = PickupTrip.PickupZone,
						            POP             = s.POP
					            };

					Trips.Add( STrip );
					E.SaveChanges();

					new Signatures( Context ).AddUpdate( Sid, s.Signature );
					LogTrip( s.ProgramName, new TripLogger( STrip ) );
					Result.Satchel = STrip;

					var CoName = PickupTrip.PickupCompanyName;

					if( CoName.LastIndexOf( '(' ) < 0 )
						CoName = $"{CoName} ({PickupTrip.PickupAddressBarcode})";

					//Wait until 15 seconds after the Minute (May help stop duplicate AURTs)
					Tasks.RunVoid( ( ( 60 - DateTime.UtcNow.Second ) * 1_000 ) + 15_000, () =>
					                                                                     {
						                                                                     try
						                                                                     {
							                                                                     foreach( var Package in s.Packages )
							                                                                     {
								                                                                     foreach( var Item in Package.Items )
								                                                                     {
									                                                                     if( Item.IsUplift() && ( Item.Pieces > 0 ) )
									                                                                     {
										                                                                     var Trip = STrip.ToTrip();
										                                                                     Trip.PickupCompanyName = CoName;
										                                                                     Trip.Status1           = STATUS.PICKED_UP;

										                                                                     Trip.TripId        = MasterTemplate.Trip.NextTripId( Context );
										                                                                     Trip.DeliveryNotes = Sid;
										                                                                     Trip.PackageType   = Item.Description;
										                                                                     Trip.Pieces        = Item.Pieces;
										                                                                     Trip.Reference     = Item.Barcode;
										                                                                     Trip.BillingNotes  = $"{Trip.Driver} - {Now:yyyy-MM-ddThh:mm:ss}"; // Get moved into Ids1 pallets field
										                                                                     IDSClient.Ids1.UpliftComplete( Context, Trip.ToTransferRecord(), s.ProgramName );
									                                                                     }
								                                                                     }
							                                                                     }
						                                                                     }
						                                                                     catch( Exception Exception )
						                                                                     {
							                                                                     Context.SystemLogException( Exception );
						                                                                     }
					                                                                     } );
				}
				else
					throw new Exception( "No trip ids" );
			}
			finally
			{
				E.XRefs.RemoveRange( from X in E.XRefs
				                     where Ids1TripIds.Contains( X.Id1 ) || Ids1TripIds.Contains( X.Id2 )
				                     select X );

				E.SaveChanges();

				var Trps = ( from T in E.Trips
				             where Ids1TripIds.Contains( T.TripId )
				             select T ).ToList();

				foreach( var Trp in Trps )
					FinaliseTrip( s.ProgramName, Trp, DateTime.Now );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}

#region Update Ids1
	private static string FixDesc( string desc ) => desc.ToAlphaNumeric().ToUpper();

	private IList<string> Ids1Update( Satchel s )
	{
		var TripIds = new List<string>();

		try
		{
			// Barcode, Aurt, Ids1
			var Dict = new Dictionary<string, Dictionary<string, List<(string Ids1TripId, decimal Pieces, decimal OriginalPieces)>>>();

			foreach( var (_, TripId, Barcode, OriginalPieces, Description, Aurt, ServiceLevel) in PmlGetMasterAndRelatedTripIdsAndPieces( s.TripIds ) )
			{
				if( !ServiceLevel.IsUplift() )
				{
					var BCode = Barcode.Trim();

					// Corrupt File Correction
					if( BCode.IsNullOrWhiteSpace() || BCode.StartsWith( "AURT", StringComparison.OrdinalIgnoreCase ) )
						BCode = IDSClient.Ids1.GetTripReference( Context, TripId ).Trim();

					var Code = BCode.IsNullOrWhiteSpace() ? FixDesc( Description ) : BCode;

					if( !Dict.TryGetValue( Code, out var AurtDict ) )
						Dict[ Code ] = AurtDict = new Dictionary<string, List<(string Ids1TripId, decimal Pieces, decimal Original)>>();

					if( !AurtDict.TryGetValue( Aurt, out var Ids1Trips ) )
						AurtDict[ Aurt ] = Ids1Trips = new List<(string Ids1TripId, decimal Pieces, decimal Original)>();

					Ids1Trips.Add( ( TripId, 0, OriginalPieces ) );
					TripIds.Add( TripId );
				}
			}

			var LostBarcodes = new List<string>();

			foreach( var Package in s.Packages )
			{
				foreach( var Item in Package.Items )
				{
					if( !Item.IsUplift() )
					{
						var Code = Item.Barcode.Trim();

						if( !Dict.TryGetValue( Code, out var AurtDict ) )
						{
							var DCode = FixDesc( Item.Description );

							if( !Dict.TryGetValue( DCode, out AurtDict ) )
							{
								LostBarcodes.Add( $"Barcode: {Code},  {DCode}, Pieces: {Item.Original}, Scanned: {Item.Pieces}" );
								continue;
							}
						}

						var Quantity = Item.Pieces;

						foreach( var Pair in AurtDict )
						{
							var Ids1Trips = Pair.Value;
							var Count     = Ids1Trips.Count;

							for( var I = 0; I < Count; I++ )
							{
								var Ids1 = Ids1Trips[ I ];

								var Allowed = Ids1.OriginalPieces - Ids1.Pieces;

								if( Allowed > 0 )
								{
									var Adj = Math.Min( Quantity, Allowed );
									Ids1.Pieces    += Adj;
									Ids1Trips[ I ] =  Ids1;
									Quantity       -= Adj;

									if( Quantity <= 0 )
										goto Done;
								}
							}
						}
					}
				Done: ;
				}
			}

			if( LostBarcodes.Count > 0 )
			{
				var Error = new StringBuilder( $"--- Lost Barcodes ---\r\nSatchel: {s.SatchelId}\r\n" );

				foreach( var TripId in s.TripIds )
					Error.Append( $"Trip Id: {TripId}\r\n" );

				Error.Append( string.Join( "\r\n", LostBarcodes ) );

				Context.SystemLogExceptionProgram( s.ProgramName, new Exception( Error.ToString() ) );
			}

			Tasks.RunVoid( () =>
			               {
				               try
				               {
					               foreach( var BPair in Dict )
					               {
						               foreach( var APair in BPair.Value )
						               {
							               foreach( var (Ids1TripId, Pieces, _) in APair.Value )
								               IDSClient.Ids1.PickupTrip( Context, Ids1TripId, s.Driver, s.PickupTime, s.POP, Pieces );
						               }
					               }
				               }
				               catch( Exception Exception )
				               {
					               Context.SystemLogException( Exception );
				               }
			               } );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return TripIds;
	}
#endregion
}