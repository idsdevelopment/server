﻿#nullable enable

using Protocol.Data._Customers.Pml;
using Inventory = Protocol.Data.Inventory;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public void PML_AddUpdateInventory( PmlInventoryList inventory )
	{
		AddUpdateInventory( ToInventoryList( inventory ) );
	}

	public void PML_DeleteInventory( PmlInventoryList inventory )
	{
		DeleteInventory( ToInventoryList( inventory ) );
	}


	public PmlInventoryList PML_GetInventoryList()
	{
		var Result = new PmlInventoryList();

		try
		{
			Result.AddRange( from I in GetInventoryList()
			                 select new PmlInventory( I ) );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}

	private static InventoryList ToInventoryList( PmlInventoryList inventory )
	{
		var Result = new InventoryList();

		Result.AddRange( from I in inventory
		                 select (Inventory)I );

		return Result;
	}
}