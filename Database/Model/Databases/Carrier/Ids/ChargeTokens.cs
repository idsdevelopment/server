﻿#nullable enable

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public static ChargeTokens GetCarrierChargeTokens( IRequestContext context, GetChargeTokens requestObject )
	{
		var Result = new ChargeTokens();

		try
		{
			// ReSharper disable once ConvertToUsingDeclaration
			using var Db = new UsersEntities();

			var Defaults = ( from C in Db.Configurations
							 select new {C.DefaultSingleTokenValue, C.DefaultTokenExpiresInDays} ).First();

			Result.DefaultTokenValue         = Defaults.DefaultSingleTokenValue;
			Result.DefaultTokenExpiresInDays = Defaults.DefaultTokenExpiresInDays;

			var Now = DateTimeOffset.Now;

			var Temp = from B in Db.Billings
					   select new
							  {
								  Billing = B,

								  // ReSharper disable once AccessToDisposedClosure
								  ActiveTokens = from C in Db.CarrierTokens
												 where ( C.CarrierId == B.CarrierId ) && ( C.AvailableTokens > 0 ) && ( C.Expires >= Now )
												 select C
							  };

			Result.CarrierChargeTokens.AddRange( from T in Temp
												 select new CarrierChargeToken
														{
															CarrierId = T.Billing.CarrierId,
															ActiveTokens = ( from A in T.ActiveTokens
																			 select new ChargeToken
																					{
																						Id               = A.Id,
																						AvailableTokens  = A.AvailableTokens,
																						SingleTokenValue = A.SingleTokenValue,
																						Expires          = A.Expires
																					} ).ToList()
														}
											   );
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}

		return Result;
	}

	public static bool AddUpdateCarrierChargeTokens( IRequestContext context, ChargeTokens tokens )
	{
		try
		{
			using var Db = new UsersEntities();

			var Config = ( from C in Db.Configurations
						   select C ).FirstOrDefault();

			if( Config is not null )
			{
				Config.DefaultSingleTokenValue   = tokens.DefaultTokenValue;
				Config.DefaultTokenExpiresInDays = tokens.DefaultTokenExpiresInDays;
			}

			var TokenCarriers = ( from T in tokens.CarrierChargeTokens
								  select T.CarrierId ).ToList();

			var Billing = Db.Billings;

			// Deletes
			var Deletes = ( from B in Billing
							where !TokenCarriers.Contains( B.CarrierId )
							select B ).ToList();

			if( Deletes.Count > 0 )
			{
				Billing.RemoveRange( Deletes );
				Db.SaveChanges();
			}

			var DbBilling = ( from B in Billing
							  select B ).ToDictionary( b => b.CarrierId, b => b );

			var CarrierIds = ( from B in DbBilling
							   select B.Key.ToUpper() ).ToList();

			// Additions
			var New = ( from C in tokens.CarrierChargeTokens
						where !CarrierIds.Contains( C.CarrierId.ToUpper() )
						select new Billing
							   {
								   CarrierId                = C.CarrierId,
								   DefaultTokenValue        = tokens.DefaultTokenValue,
								   DefaultTokenExpireInDays = tokens.DefaultTokenExpiresInDays
							   } ).ToList();

			if( New.Count > 0 )
			{
				Billing.AddRange( New );
				Db.SaveChanges();
			}

			// Updates
			var NewIds = ( from N in New
						   select N.CarrierId ).ToList();

			var Update = false;

			foreach( var Token in tokens.CarrierChargeTokens )
			{
				var Cid = Token.CarrierId;

				if( !NewIds.Contains( Cid ) && DbBilling.TryGetValue( Cid, out var Rec ) )
				{
					Update                = true;
					Rec.DefaultTokenValue = tokens.DefaultTokenValue;
				}
			}

			if( Update )
				Db.SaveChanges();
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
			return false;
		}
		return true;
	}
}