﻿#nullable enable

using System.Data;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public enum COMMISSION_DETAILS_TYPE
	{
		PRIORITY
	}

	public enum MASTER_COMMISSION_DETAILS_TYPE : byte
	{
		NONE,
		ERROR,
		COMMISSION_DETAILS
	}

	public class CommissionDetailsBase
	{
		public COMMISSION_DETAILS_TYPE Type { get; set; }
	}


	public void SetCommissionCalculated( string tripId )
	{
		var E = Entity;

		while( true )
		{
			try
			{
				var TripRec = ( from T in E.Trips
				                where T.TripId == tripId
				                select T ).FirstOrDefault();

				if( TripRec is not null )
				{
					TripRec.LastModified               = DateTime.UtcNow;
					TripRec.DriverCommissionCalculated = true;
					E.SaveChanges();
				}
			}
			catch( DBConcurrencyException )
			{
				continue;
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
			break;
		}
	}


	public void UpdateCommission( string staffId, string reference, int sortOrder, DateTimeOffset date, string tripId, string serviceLevel, string group,
	                              decimal baseValue, decimal extendedValue,
	                              MASTER_COMMISSION_DETAILS_TYPE masterType, string detailsAsJson )
	{
		try
		{
			staffId   = staffId.Trim();
			reference = reference.Trim();
			tripId    = tripId.Trim();

			var E = Entity;
			var S = E.StaffCommissions;

			var Rec = ( from C in S
			            where ( C.StaffId == staffId ) && ( C.Reference == reference ) && ( C.DateTime == date ) && ( C.TripId == tripId ) && ( C.SortOrder == sortOrder )
			            select C ).FirstOrDefault();

			if( Rec is null )
			{
				Rec = new StaffCommission
				      {
					      StaffId        = staffId,
					      Reference      = reference,
					      SortOrder      = sortOrder,
					      TripId         = tripId,
					      DateTime       = date,
					      BaseValue      = baseValue,
					      ExtendedValue  = extendedValue,
					      DetailsType    = (byte)masterType,
					      DetailsAsJson  = detailsAsJson,
					      ServiceLevel   = serviceLevel,
					      CommissionPaid = false
				      };
				S.Add( Rec );
			}
			else
			{
				Rec.BaseValue     = baseValue;
				Rec.ExtendedValue = extendedValue;
				Rec.DetailsType   = (byte)masterType;
				Rec.DetailsAsJson = detailsAsJson;
				Rec.ServiceLevel  = serviceLevel;
				Rec.SortOrder     = sortOrder;
			}
			E.SaveChanges();

			SetCommissionCalculated( tripId );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}


	public void UpdateCommissionError( string staffId, string reference, int sortOrder, DateTimeOffset date, string tripId, string serviceLevel, string group, string error )
	{
		UpdateCommission( staffId, reference, sortOrder, date, tripId, serviceLevel, group, 0, 0, MASTER_COMMISSION_DETAILS_TYPE.ERROR, JsonConvert.SerializeObject( error ) );
	}

	public void UpdateCommissionDetails( string staffId, string reference, int sortOrder, DateTimeOffset date,
	                                     decimal baseValue, decimal extendedValue, string tripId, string serviceLevel, string group,
	                                     CommissionDetailsBase details )
	{
		UpdateCommission( staffId, reference, sortOrder, date, tripId, serviceLevel, group, baseValue, extendedValue, MASTER_COMMISSION_DETAILS_TYPE.COMMISSION_DETAILS, JsonConvert.SerializeObject( details ) );
	}
}