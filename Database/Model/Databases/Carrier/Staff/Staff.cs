﻿#nullable enable

using Address = Protocol.Data.Address;
using Staff = Database.Model.Databases.MasterTemplate.Staff;
using Zone = Database.Model.Databases.MasterTemplate.Zone;

// ReSharper disable StringCompareToIsCultureSpecific

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public class StaffLogger
	{
		public string StaffId           { get; set; }
		public bool   Enabled           { get; set; }
		public string Password          { get; set; }
		public string TaxNumber         { get; set; }
		public string FirstName         { get; set; }
		public string LastName          { get; set; }
		public string MiddleNames       { get; set; }
		public string CommissionFormula { get; set; }

		public string EmergencyFirstName { get; set; }
		public string EmergencyLastName  { get; set; }
		public string EmergencyPhone     { get; set; }
		public string EmergencyMobile    { get; set; }
		public string EmergencyEmail     { get; set; }

		public decimal MainCommission1 { get; set; }
		public decimal MainCommission2 { get; set; }
		public decimal MainCommission3 { get; set; }

		public decimal OverrideCommission1  { get; set; }
		public decimal OverrideCommission2  { get; set; }
		public decimal OverrideCommission3  { get; set; }
		public decimal OverrideCommission4  { get; set; }
		public decimal OverrideCommission5  { get; set; }
		public decimal OverrideCommission6  { get; set; }
		public decimal OverrideCommission7  { get; set; }
		public decimal OverrideCommission8  { get; set; }
		public decimal OverrideCommission9  { get; set; }
		public decimal OverrideCommission10 { get; set; }
		public decimal OverrideCommission11 { get; set; }
		public decimal OverrideCommission12 { get; set; }

		public string Roles { get; set; } = "";
		public string Zones { get; set; } = "";

		public AddressLogger? Before { get; set; }
		public AddressLogger? After  { get; set; }

		public StaffLogger( CarrierDb db, Staff s )
		{
			Db = db;

			StaffId           = s.StaffId;
			Enabled           = s.Enabled;
			Password          = s.Password;
			TaxNumber         = s.TaxNumber;
			FirstName         = s.FirstName;
			LastName          = s.LastName;
			MiddleNames       = s.MiddleNames;
			CommissionFormula = s.CommissionFormula;

			EmergencyFirstName = s.EmergencyFirstName;
			EmergencyLastName  = s.EmergencyLastName;
			EmergencyPhone     = s.EmergencyPhone;
			EmergencyMobile    = s.EmergencyMobile;
			EmergencyEmail     = s.EmergencyEmail;

			MainCommission1 = s.MainCommission1;
			MainCommission2 = s.MainCommission2;
			MainCommission3 = s.MainCommission3;

			OverrideCommission1  = s.OverrideCommission1;
			OverrideCommission2  = s.OverrideCommission2;
			OverrideCommission3  = s.OverrideCommission3;
			OverrideCommission4  = s.OverrideCommission4;
			OverrideCommission5  = s.OverrideCommission5;
			OverrideCommission6  = s.OverrideCommission6;
			OverrideCommission7  = s.OverrideCommission7;
			OverrideCommission8  = s.OverrideCommission8;
			OverrideCommission9  = s.OverrideCommission9;
			OverrideCommission10 = s.OverrideCommission10;
			OverrideCommission11 = s.OverrideCommission11;
			OverrideCommission12 = s.OverrideCommission12;

			var Addr = new AddressLogger( new MasterTemplate.Address() );
			Before = Addr;
			After  = Addr;
		}

		public StaffLogger( CarrierDb db, Staff s, AddressLogger? before ) : this( db, s )
		{
			Before = before;
		}

		public StaffLogger( CarrierDb db, Staff s, AddressLogger? before, AddressLogger? after ) : this( db, s, before )
		{
			Before = before;
			After  = after;
		}

		public StaffLogger( CarrierDb db, Staff staff, IEnumerable<StaffRole> roles ) : this( db, staff )
		{
			UpdateRoles( roles );
		}

		public StaffLogger( CarrierDb db, Staff staff, IEnumerable<StaffRole> roles, IEnumerable<Zone> zones ) : this( db, staff, roles )
		{
			UpdateZones( zones );
		}

		private readonly CarrierDb Db;

		public void UpdateRoles( IEnumerable<StaffRole> roles )
		{
			var Rls = new StringBuilder();

			var First = true;

			var DictRoles = Db.GetRolesAsDictionaryById();

			foreach( var Role in roles )
			{
				if( First )
					First = false;
				else
					Rls.Append( ", " );

				Rls.Append( DictRoles.TryGetValue( Role.RoleId, out var Rle ) ? Rle.RoleName : "????" );
			}

			Roles = Rls.ToString();
		}

		public void UpdateZones( IEnumerable<Zone> zones )
		{
			var Zns = new StringBuilder();

			var First = true;

			foreach( var Zone in zones )
			{
				if( First )
					First = false;
				else
					Zns.Append( ", " );

				Zns.Append( Zone.ZoneName );
			}

			Zones = Zns.ToString();
		}
	}

	private Address GetProtocolDataAddress( long id )
	{
		var Address = new Address();

		try
		{
			var (Addr, Ok) = GetAddress( id );

			if( Ok && Addr is not null )
			{
				Address.AddressLine1  = Addr.AddressLine1;
				Address.AddressLine2  = Addr.AddressLine2;
				Address.Barcode       = Addr.Barcode;
				Address.City          = Addr.City;
				Address.Country       = Addr.Country;
				Address.CountryCode   = Addr.CountryCode;
				Address.EmailAddress  = Addr.EmailAddress;
				Address.EmailAddress1 = Addr.EmailAddress1;
				Address.EmailAddress2 = Addr.EmailAddress2;
				Address.Fax           = Addr.Fax;
				Address.Latitude      = Addr.Latitude;
				Address.Longitude     = Addr.Longitude;
				Address.Mobile        = Addr.Mobile;
				Address.Mobile1       = Addr.Mobile1;
				Address.Notes         = Addr.Notes;
				Address.Phone         = Addr.Phone;
				Address.Phone1        = Addr.Phone1;
				Address.PostalBarcode = Addr.PostalBarcode;
				Address.PostalCode    = Addr.PostalCode;
				Address.Region        = Addr.Region;
				Address.SecondaryId   = Addr.SecondaryId;
				Address.Suite         = Addr.Suite;
				Address.Vicinity      = Addr.Vicinity;
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return Address;
	}

	private static Protocol.Data.Staff Convert( Staff s ) => new()
															 {
																 Ok                   = true,
																 StaffId              = s.StaffId,
																 MainCommission1      = s.MainCommission1,
																 FirstName            = s.FirstName,
																 LastName             = s.LastName,
																 EmergencyLastName    = s.EmergencyLastName,
																 EmergencyEmail       = s.EmergencyEmail,
																 EmergencyMobile      = s.EmergencyMobile,
																 EmergencyPhone       = s.EmergencyPhone,
																 OverrideCommission1  = s.OverrideCommission1,
																 EmergencyFirstName   = s.EmergencyFirstName,
																 MainCommission2      = s.MainCommission2,
																 Enabled              = s.Enabled,
																 OverrideCommission2  = s.OverrideCommission2,
																 OverrideCommission3  = s.OverrideCommission3,
																 TaxNumber            = s.TaxNumber,
																 MiddleNames          = s.MiddleNames,
																 Password             = Encryption.ToTimeLimitedToken( s.Password ),
																 MainCommission3      = s.MainCommission3,
																 OverrideCommission10 = s.OverrideCommission10,
																 OverrideCommission11 = s.OverrideCommission11,
																 OverrideCommission12 = s.OverrideCommission12,
																 OverrideCommission4  = s.OverrideCommission4,
																 OverrideCommission5  = s.OverrideCommission5,
																 OverrideCommission6  = s.OverrideCommission6,
																 OverrideCommission7  = s.OverrideCommission7,
																 OverrideCommission8  = s.OverrideCommission8,
																 OverrideCommission9  = s.OverrideCommission9
															 };

	private Staff? GetStaff( string staffId ) => ( from S in Entity.Staffs
												   where S.StaffId == staffId
												   select S ).FirstOrDefault();

	public StaffLookupSummaryList StaffLookup( StaffLookup lookup )
	{
		var RetVal = new StaffLookupSummaryList();

		try
		{
			var Key   = lookup.Key;
			var Count = lookup.PreFetchCount;

			IEnumerable<Staff> Result;

			switch( lookup.Fetch )
			{
			case PreFetch.PREFETCH.FIRST:
				Result = ( from S in Entity.Staffs
						   orderby S.StaffId
						   select S ).Take( Count );
				break;

			case PreFetch.PREFETCH.LAST:
				Result = ( from S in Entity.Staffs
						   orderby S.StaffId descending
						   select S ).Take( Count );
				break;

			case PreFetch.PREFETCH.FIND_MATCH:
				Result = ( from S in Entity.Staffs
						   where S.StaffId.StartsWith( Key )
						   orderby S.StaffId
						   select S ).Take( Count );
				break;

			case PreFetch.PREFETCH.FIND_RANGE:
				if( Count >= 0 ) // Next
				{
					Result = ( from S in Entity.Staffs
							   where S.StaffId.CompareTo( Key ) >= 0
							   orderby S.StaffId
							   select S ).Take( Count );
				}
				else // Prior
				{
					Result = ( from S in Entity.Staffs
							   where S.StaffId.CompareTo( Key ) <= 0
							   orderby S.StaffId
							   select S ).Take( -Count );
				}
				break;

			case PreFetch.PREFETCH.OFFSET:
				Result = ( from S in Entity.Staffs
						   orderby S.StaffId
						   select S ).Skip( lookup.Offset ).Take( -Count );
				break;

			default:
				Context.SystemLogException( new Exception( $"Unknown prefetch type: {lookup.Fetch}" ) );

				return RetVal;
			}

			RetVal.AddRange( from S in Result
							 let Addr = GetAddress( S.AddressId )
							 let A = Addr.Ok ? Addr.Address : new MasterTemplate.Address()
							 select new StaffLookupSummary
									{
										AddressLine1 = A.AddressLine1 ?? "",
										StaffId      = S.StaffId,
										FirstName    = S.FirstName,
										LastName     = S.LastName
									} );
		}
		catch( Exception E )
		{
			Context.SystemLogException( nameof( CustomersLookup ), E );
		}

		return RetVal;
	}

	public List<Staff> GetStaff() => ( from S in Entity.Staffs
									   orderby S.StaffId
									   select S ).ToList();

	public StaffList GetStaffAsProtocolStaff()
	{
		var Result = new StaffList();
		Result.AddRange( GetStaff().Select( Convert ) );

		return Result;
	}

	public StaffAndRolesList GetStaffAndRoles()
	{
		var Result = new StaffAndRolesList();

		try
		{
			var StaffList = GetStaff();

			var StaffRoles = GetStaffMemberRoles( from S in StaffList
												  select S.StaffId );

			foreach( var Staff in StaffList )
			{
				var PStaff = Convert( Staff );
				PStaff.Address = GetProtocolDataAddress( Staff.AddressId );

				Result.Add( new StaffAndRoles( PStaff, StaffRoles[ Staff.StaffId ] ) );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return Result;
	}

	public StaffLookupSummaryList GetAdministrators()
	{
		var Result = new StaffLookupSummaryList();

		try
		{
			var E = Entity;

			Result.AddRange( from S in E.Staffs
							 where S.Enabled && ( from Sr in E.StaffRoles
												  where ( from R in E.Roles
														  where R.IsAdministrator
														  select R.Id ).Contains( Sr.RoleId )
												  select Sr.StaffId ).Contains( S.StaffId )
							 orderby S.StaffId
							 select new StaffLookupSummary
									{
										FirstName = S.FirstName,
										LastName  = S.LastName,
										StaffId   = S.StaffId,

										AddressLine1 = ( from A in E.Addresses
														 where A.Id == S.AddressId
														 select A.AddressLine1 ).FirstOrDefault() ?? ""
									} );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}

	public (bool UserNameOk, bool PasswordOk, int UserId, bool Ok) LoginOk( string staffId, string password )
	{
		var Result = ( UserNameOk: false, PasswordOk: false, UserId: -1, Ok: false );

		staffId = staffId.ToLower();

		var Rec = GetStaff( staffId );

		if( Rec is {Enabled: true} )
		{
			Result.UserId     = Rec.Id;
			Result.UserNameOk = true;

			if( Rec.Password == password )
			{
				Result.PasswordOk = true;
				Result.Ok         = true;
			}
		}

		return Result;
	}

	public void DeleteStaffMember( string programName, string staffId )
	{
		staffId = staffId.TrimToLower();

		try
		{
			var Rec = GetStaff( staffId );

			if( Rec is not null )
			{
				Entity.Staffs.Remove( Rec );
				Entity.SaveChanges();
				var Before = new StaffLogger( this, Rec );
				Context.LogDeleted( Context.GetStorageId( LOG.STAFF ), programName, "Remove staff member " + staffId, Before );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public void AddUpdateStaffMember( UpdateStaffMemberWithRolesAndZones staffMember, bool encryptedPassword = true )
	{
		try
		{
			var Prog = staffMember.ProgramName;
			var S    = staffMember.Staff;
			var A    = S.Address;

			if( encryptedPassword )
			{
				var (Value, Ok) = Encryption.FromTimeLimitedToken( S.Password );

				if( !Ok )
					throw new Exception( "Invalid password" );

				S.Password = Value;
			}

			var Rec = GetStaff( S.StaffId );

			if( Rec is null )
			{
				var (AddressId, Address, AddressOk) = AddAddress( A, staffMember.ProgramName );

				if( AddressOk )
				{
					var Staff = new Staff
								{
									AddressId = AddressId,
									StaffId   = S.StaffId,
									Enabled   = S.Enabled,

									FirstName   = S.FirstName.Max50(),
									LastName    = S.LastName.Max50(),
									MiddleNames = S.MiddleNames,
									Password    = S.Password,

									EmergencyFirstName = S.EmergencyFirstName.Max50(),
									EmergencyLastName  = S.EmergencyLastName.Max50(),
									EmergencyMobile    = S.EmergencyMobile,
									EmergencyPhone     = S.EmergencyPhone,
									EmergencyEmail     = S.EmergencyEmail,

									TaxNumber         = S.TaxNumber,
									CommissionFormula = "",

									MainCommission1 = S.MainCommission1,
									MainCommission2 = S.MainCommission2,
									MainCommission3 = S.MainCommission3,

									OverrideCommission1  = S.OverrideCommission1,
									OverrideCommission2  = S.OverrideCommission2,
									OverrideCommission3  = S.OverrideCommission3,
									OverrideCommission4  = S.OverrideCommission4,
									OverrideCommission5  = S.OverrideCommission5,
									OverrideCommission6  = S.OverrideCommission6,
									OverrideCommission7  = S.OverrideCommission7,
									OverrideCommission8  = S.OverrideCommission8,
									OverrideCommission9  = S.OverrideCommission9,
									OverrideCommission10 = S.OverrideCommission10,
									OverrideCommission11 = S.OverrideCommission11,
									OverrideCommission12 = S.OverrideCommission12
								};
					Entity.Staffs.Add( Staff );
					Entity.SaveChanges();
					Context.LogNew( Context.GetStorageId( LOG.STAFF ), Prog, "Add staff member", new StaffLogger( this, Staff, Address ) );

					AddUpdateStaffRoles( S.StaffId, staffMember.Roles );
				}
			}
			else
			{
				var (AddressBefore, AddressAfter, AddressId, AddressOk) = UpdateAddress( Rec.AddressId, A, Prog );

				if( AddressOk )
				{
					var Before = new StaffLogger( this, Rec );

					Rec.AddressId            = AddressId;
					Rec.Enabled              = S.Enabled;
					Rec.FirstName            = S.FirstName.Max50();
					Rec.LastName             = S.LastName.Max50();
					Rec.MiddleNames          = S.MiddleNames;
					Rec.Password             = S.Password;
					Rec.EmergencyFirstName   = S.EmergencyFirstName.Max50();
					Rec.EmergencyLastName    = S.EmergencyLastName.Max50();
					Rec.EmergencyMobile      = S.EmergencyMobile;
					Rec.EmergencyPhone       = S.EmergencyPhone;
					Rec.EmergencyEmail       = S.EmergencyEmail;
					Rec.TaxNumber            = S.TaxNumber;
					Rec.CommissionFormula    = "";
					Rec.MainCommission1      = S.MainCommission1;
					Rec.MainCommission2      = S.MainCommission2;
					Rec.MainCommission3      = S.MainCommission3;
					Rec.OverrideCommission1  = S.OverrideCommission1;
					Rec.OverrideCommission2  = S.OverrideCommission2;
					Rec.OverrideCommission3  = S.OverrideCommission3;
					Rec.OverrideCommission4  = S.OverrideCommission4;
					Rec.OverrideCommission5  = S.OverrideCommission5;
					Rec.OverrideCommission6  = S.OverrideCommission6;
					Rec.OverrideCommission7  = S.OverrideCommission7;
					Rec.OverrideCommission8  = S.OverrideCommission8;
					Rec.OverrideCommission9  = S.OverrideCommission9;
					Rec.OverrideCommission10 = S.OverrideCommission10;
					Rec.OverrideCommission11 = S.OverrideCommission11;
					Rec.OverrideCommission12 = S.OverrideCommission12;

					Entity.SaveChanges();

					var After = new StaffLogger( this, Rec, AddressBefore, AddressAfter );

					var (BFore, Afr, IsOk) = AddUpdateStaffRoles( S.StaffId, staffMember.Roles );

					if( IsOk )
					{
						Before.UpdateRoles( BFore );
						After.UpdateRoles( Afr );
					}

					Context.LogDifferences( Context.GetStorageId( LOG.STAFF ), Prog, "Update staff member", Before, After );
				}
			}
		}
		catch( Exception E )
		{
			Context.SystemLogException( E );
		}
	}

	public void EnableDisableStaffMember( string program, string staffId, bool enable )
	{
		try
		{
			var Rec = GetStaff( staffId );

			if( Rec is not null )
			{
				var Before = new StaffLogger( this, Rec );

				Rec.Enabled = enable;

				Entity.SaveChanges();

				var After = new StaffLogger( this, Rec );
				Context.LogDifferences( Context.GetStorageId( LOG.STAFF ), program, "Enable staff member", Before, After );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public bool StaffMemberExists( string staffId ) => GetStaff( staffId ) is not null;

	public int GetStaffMemberInternalId( string staffId )
	{
		var Rec = GetStaff( staffId );
		return Rec?.Id ?? 0;
	}

	public Protocol.Data.Staff GetStaffMember( string staffId )
	{
		Protocol.Data.Staff Staff;

		var Rec = GetStaff( staffId );

		if( Rec is not null )
		{
			Staff         = Convert( Rec );
			Staff.Address = GetProtocolDataAddress( Rec.AddressId );
		}
		else
			Staff = new Protocol.Data.Staff();

		return Staff;
	}
}