﻿#nullable enable

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public OverridePackageList GetPackageOverridesByCompanyId( long companyId )
	{
		var Result = new OverridePackageList();

		try
		{
			var Recs = ( from P in Entity.PackageOverrides
					     where P.CompanyId == companyId
					     select P ).ToList();

			foreach( var Rec in Recs )
			{
				Result.Add( new OverridePackage
						    {
							    Id                = Rec.Id,
							    CompanyId         = Rec.CompanyId,
							    FromPackageTypeId = Rec.FromPackageTypeId,
							    ToPackageTypeId   = Rec.ToPackageTypeId,
							    SortIndex         = Rec.SortIndex
						    } );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}

	public void AddUpdatePackageOverride( OverridePackage po )
	{
		try
		{
			var E  = Entity;
			var Po = E.PackageOverrides;

			var Rec = ( from P in Po
					    where ( P.CompanyId == po.CompanyId )
						      && ( P.FromPackageTypeId == po.FromPackageTypeId )
						      && ( P.ToPackageTypeId == po.ToPackageTypeId )
					    select P ).FirstOrDefault();

			if( Rec is null )
			{
				Po.Add( new PackageOverride
					    {
						    CompanyId         = po.CompanyId,
						    FromPackageTypeId = po.FromPackageTypeId,
						    ToPackageTypeId   = po.ToPackageTypeId,
						    SortIndex         = po.SortIndex
					    } );
			}
			else
				Rec.SortIndex = po.SortIndex;

			E.SaveChanges();
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public void DeletePackageOverride( int requestObject )
	{
		try
		{
			var E  = Entity;
			var Po = E.PackageOverrides;

			var Rec = ( from P in Po
					    where P.Id == requestObject
					    select P ).FirstOrDefault();

			if( Rec is not null )
			{
				Po.Remove( Rec );
				E.SaveChanges();
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public RedirectPackageList GetPackageRedirectsByCompanyId( long companyId )
	{
		var Result = new RedirectPackageList();

		try
		{
			var Recs = ( from P in Entity.PackageRedirects
					     where P.CompanyId == companyId
					     select P ).ToList();

			foreach( var Rec in Recs )
			{
				Result.Add( new RedirectPackage
						    {
							    Id             = Rec.Id,
							    CompanyId      = Rec.CompanyId,
							    FromZoneId     = Rec.FromZoneId,
							    ToZoneId       = Rec.ToZoneId,
							    OverrideId     = Rec.OverrideId,
							    ServiceLevelId = Rec.ServiceLevelId,
							    PackageTypeId  = Rec.PackageTypeId,
							    Charge         = Rec.Charge
						    } );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}

	public void AddUpdatePackageRedirect( RedirectPackage pr )
	{
		try
		{
			var E  = Entity;
			var Pr = E.PackageRedirects;

			var Rec = ( from R in Pr
					    where ( R.CompanyId == pr.CompanyId )
						      && ( R.FromZoneId == pr.FromZoneId )
						      && ( R.ToZoneId == pr.ToZoneId )
						      && ( R.ServiceLevelId == pr.ServiceLevelId )
						      && ( R.PackageTypeId == pr.PackageTypeId )
						      && ( R.OverrideId == pr.OverrideId )
					    select R ).FirstOrDefault();

			if( Rec is null )
			{
				Pr.Add( new PackageRedirect
					    {
						    CompanyId      = pr.CompanyId,
						    FromZoneId     = pr.FromZoneId,
						    ToZoneId       = pr.ToZoneId,
						    PackageTypeId  = pr.PackageTypeId,
						    ServiceLevelId = pr.ServiceLevelId,
						    OverrideId     = pr.OverrideId,
						    Charge         = pr.Charge
					    } );
			}
			else
				Rec.Charge = pr.Charge;

			E.SaveChanges();
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public void DeletePackageRedirect( int id )
	{
		try
		{
			var E  = Entity;
			var Pr = E.PackageRedirects;

			var Rec = ( from R in Pr
					    where R.Id == id
					    select R ).FirstOrDefault();

			if( Rec is not null )
			{
				Pr.Remove( Rec );
				E.SaveChanges();
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}
}