﻿#nullable enable

using Invoice = Protocol.Data.Invoice;
using Trip = Protocol.Data.Trip;

namespace Database.Model.Databases.Carrier;

public static class InvoiceExtensions
{
	public static Invoice ToInvoice( this MasterTemplate.Invoice i )
	{
		var CombinedTripCharges = JsonConvert.DeserializeObject<List<TripCharge>>( i.TripChargesAsJson ) ?? [];
		var TripIds             = JsonConvert.DeserializeObject<List<string>>( i.TripIdsAsJson ) ?? [];

		return new Invoice
			   {
				   TripIds             = TripIds,
				   CombinedTripCharges = CombinedTripCharges,

				   BillingCompanyName = i.BillingCompany,
				   InvoiceDateTime    = i.InvoiceDateTime,
				   I                  = i.InvoiceNumber,
				   IsCreditNode       = i.IsCreditNote,
				   NonTaxableValue    = i.NonTaxableValue,
				   TaxableValue       = i.TaxableValue,
				   TotalCharges       = i.TotalCharges,
				   TotalTaxA          = i.TotalTaxA,
				   TotalTaxB          = i.TotalTaxB,
				   TotalValue         = i.TotalValue,
				   InvoiceFrom        = i.InvoiceFromDateTime,
				   InvoiceTo          = i.InvoiceToDateTime,
				   TotalDiscount      = i.TotalDiscount,
				   BillingPeriod      = i.BillingPeriod,
				   Residual           = i.Residual,

				   EmailInvoice = i.EmailInvoice,
				   EmailAddress = i.EmailAddress,
				   EmailSubject = i.EmailSubject
			   };
	}
}

public partial class CarrierDb
{
	public long LastInvoiceNumber
	{
		get
		{
			var Rec = ( from I in Entity.Invoices
					    orderby I.InvoiceNumber descending
					    select I ).FirstOrDefault();
			return Rec?.InvoiceNumber ?? -1;
		}
	}

	public InvoiceAndTripsList GetInvoicesAndTrips( GetInvoices gi )
	{
		var Result = new InvoiceAndTripsList();

		try
		{
			var Search = new SearchTrips
						 {
							 ByTripId = true
						 };

			foreach( var Invoice in GetInvoices( gi ) )
			{
				var Trips = new List<Trip>();

				foreach( var TripId in Invoice.TripIds )
				{
					Search.TripId = TripId;
					var Trip = StorageSearchTrips( Search );

					if( Trip.Count > 0 )
						Trips.Add( Trip[ 0 ] );
				}

				Result.Add( new InvoiceAndTrips( Invoice, Trips ) );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}

	public Invoices GetInvoices( GetInvoices gi )
	{
		var Result = new Invoices();

		try
		{
			var Invoices = ( from I in Entity.Invoices
						     where ( ( gi.BillingCustomerCode == "" ) || ( I.BillingCompany == gi.BillingCustomerCode ) )
							       && ( I.InvoiceDateTime >= gi.FromDateTime ) && ( I.InvoiceDateTime <= gi.ToDateTime )
						     select I ).ToList();

			foreach( var Invoice in Invoices )
				Result.Add( Invoice.ToInvoice() );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}

	public long AddInvoice( Invoice invoice )
	{
		try
		{
			var (Company, Ok) = GetCompanyByName( invoice.BillingCompanyName );

			if( Ok )
			{
				var ValidTrips = new List<string>();
				var E          = Entity;

				var InvoiceNumber = GetAndUpdateNextInvoiceNumber();

				if( InvoiceNumber > -1 )
				{
					foreach( var TripId in invoice.TripIds )
					{
						var Trip = ( from T in E.Trips
								     where T.TripId == TripId
								     select T ).FirstOrDefault();

						if( Trip is not null && (STATUS)Trip.Status1 is STATUS.POSTED )
						{
							ValidTrips.Add( TripId );
							Trip.InvoiceNumber = InvoiceNumber;
							Trip.Status2       = (int)( (STATUS1)Trip.Status2 | STATUS1.INVOICED );
							E.SaveChanges();
						}
					}

					FinaliseTrips( invoice.ProgramName, ValidTrips );

					E.Invoices.Add( new MasterTemplate.Invoice
								    {
									    BillingCompanyId    = Company.CompanyId,
									    IsCreditNote        = invoice.IsCreditNode,
									    BillingCompany      = invoice.BillingCompanyName,
									    InvoiceNumber       = InvoiceNumber,
									    InvoiceDateTime     = invoice.InvoiceDateTime,
									    NonTaxableValue     = invoice.NonTaxableValue,
									    TaxableValue        = invoice.TaxableValue,
									    TotalTaxA           = invoice.TotalTaxA,
									    TotalTaxB           = invoice.TotalTaxB,
									    TotalValue          = invoice.TotalValue,
									    TripIdsAsJson       = JsonConvert.SerializeObject( invoice.TripIds ),
									    TripChargesAsJson   = JsonConvert.SerializeObject( invoice.CombinedTripCharges ),
									    InvoiceFromDateTime = invoice.InvoiceFrom,
									    InvoiceToDateTime   = invoice.InvoiceTo,
									    TotalDiscount       = invoice.TotalDiscount,
									    TotalCharges        = invoice.TotalCharges,
									    BillingPeriod       = invoice.BillingPeriod,
									    Residual            = invoice.Residual,

									    EmailInvoice = invoice.EmailInvoice,
									    EmailAddress = invoice.EmailAddress,
									    EmailSubject = invoice.EmailSubject
								    } );
					E.SaveChanges();
				}
				return InvoiceNumber;
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogExceptionProgram( invoice.ProgramName, Exception );
		}
		return -1;
	}
}