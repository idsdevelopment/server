﻿#nullable enable

namespace Database.Model.Databases.MasterTemplate;

public partial class Trip
{
	public static Trip NewTrip() => new()
	                                {
		                                ConcurrencyToken = Array.Empty<byte>(),

		                                TripId = NextTripId(),

		                                ServiceLevel      = "",
		                                Barcode           = "",
		                                Pieces            = 0,
		                                Route             = "",
		                                DriverCode        = "",
		                                BillingNotes      = "",
		                                Group4            = "",
		                                Location          = "",
		                                TripItemsAsJson   = "",
		                                Reference         = "",
		                                LastModified      = DateTime.UtcNow,
		                                PackageType       = "",
		                                TripChargesAsJson = "",
		                                AccountId         = "",

		                                BillingAddressAddressLine1  = "",
		                                BillingAddressAddressLine2  = "",
		                                BillingAddressBarcode       = "",
		                                BillingAddressCity          = "",
		                                BillingAddressCountry       = "",
		                                BillingAddressCountryCode   = "",
		                                BillingAddressEmailAddress  = "",
		                                BillingAddressEmailAddress1 = "",
		                                BillingAddressEmailAddress2 = "",
		                                BillingAddressFax           = "",
		                                BillingAddressLatitude      = 0,
		                                BillingAddressLongitude     = 0,
		                                BillingAddressMobile        = "",
		                                BillingAddressMobile1       = "",
		                                BillingAddressNotes         = "",
		                                BillingAddressPhone         = "",
		                                BillingAddressPhone1        = "",
		                                BillingAddressPostalBarcode = "",
		                                BillingAddressPostalCode    = "",
		                                BillingAddressRegion        = "",
		                                BillingAddressSuite         = "",
		                                BillingAddressVicinity      = "",
		                                BillingCompanyAccountId     = "",
		                                BillingCompanyId            = 0,
		                                BillingCompanyName          = "",
		                                BillingContact              = "",

		                                Board            = "",
		                                CallTime         = DateTimeOffset.MinValue,
		                                CallerEmail      = "",
		                                CallerName       = "",
		                                CallerPhone      = "",
		                                ClaimedLatitude  = 0,
		                                ClaimedLongitude = 0,
		                                ClaimedTime      = DateTimeOffset.MinValue,
		                                CurrentZone      = "",
		                                DangerousGoods   = false,

		                                DeliveredLatitude            = 0,
		                                DeliveredLongitude           = 0,
		                                DeliveredTime                = DateTimeOffset.MinValue,
		                                DeliveryAddressAddressLine1  = "",
		                                DeliveryAddressAddressLine2  = "",
		                                DeliveryAddressBarcode       = "",
		                                DeliveryAddressCity          = "",
		                                DeliveryAddressCountry       = "",
		                                DeliveryAddressCountryCode   = "",
		                                DeliveryAddressEmailAddress  = "",
		                                DeliveryAddressEmailAddress1 = "",
		                                DeliveryAddressEmailAddress2 = "",
		                                DeliveryAddressFax           = "",
		                                DeliveryAddressLatitude      = 0,
		                                DeliveryAddressLongitude     = 0,
		                                DeliveryAddressMobile        = "",
		                                DeliveryAddressMobile1       = "",
		                                DeliveryAddressNotes         = "",
		                                DeliveryAddressPhone         = "",
		                                DeliveryAddressPhone1        = "",
		                                DeliveryAddressPostalBarcode = "",
		                                DeliveryAddressPostalCode    = "",
		                                DeliveryAddressRegion        = "",
		                                DeliveryAddressSuite         = "",
		                                DeliveryAddressVicinity      = "",
		                                DeliveryCompanyAccountId     = "",
		                                DeliveryCompanyId            = 0,
		                                DeliveryCompanyName          = "",
		                                DeliveryContact              = "",
		                                DeliveryNotes                = "",
		                                DeliveryZone                 = "",

		                                DeviceDeliverySortOrder = 0,
		                                DevicePickupSortOrder   = 0,

		                                DriverCommissionCalculated = false,
		                                DueTime                    = DateTimeOffset.MaxValue,
		                                ExtensionAsJson            = "",
		                                ExtensionType              = 0,
		                                FinalisedLatitude          = 0,
		                                FinalisedLongitude         = 0,
		                                FinalisedTime              = DateTimeOffset.MinValue,
		                                Group0                     = "",
		                                Group1                     = "",
		                                Group2                     = "",
		                                Group3                     = "",
		                                HasDangerousGoodsDocuments = false,
		                                HasItems                   = false,
		                                IsQuote                    = false,
		                                Measurement                = "",
		                                MissingPieces              = Array.Empty<byte>(),
		                                OriginalPieceCount         = 0,
		                                POD                        = "",
		                                POP                        = "",

		                                PickupAddressAddressLine1  = "",
		                                PickupAddressAddressLine2  = "",
		                                PickupAddressBarcode       = "",
		                                PickupAddressCity          = "",
		                                PickupAddressCountry       = "",
		                                PickupAddressCountryCode   = "",
		                                PickupAddressEmailAddress  = "",
		                                PickupAddressEmailAddress1 = "",
		                                PickupAddressEmailAddress2 = "",
		                                PickupAddressFax           = "",
		                                PickupAddressLatitude      = 0,
		                                PickupAddressLongitude     = 0,
		                                PickupAddressMobile        = "",
		                                PickupAddressMobile1       = "",
		                                PickupAddressNotes         = "",
		                                PickupAddressPhone         = "",
		                                PickupAddressPhone1        = "",
		                                PickupAddressPostalBarcode = "",
		                                PickupAddressPostalCode    = "",
		                                PickupAddressRegion        = "",
		                                PickupAddressSuite         = "",
		                                PickupAddressVicinity      = "",
		                                PickupCompanyAccountId     = "",
		                                PickupCompanyId            = 0,
		                                PickupCompanyName          = "",
		                                PickupContact              = "",
		                                PickupNotes                = "",
		                                PickupZone                 = "",
		                                PickupLatitude             = 0,
		                                PickupLongitude            = 0,
		                                PickupTime                 = DateTimeOffset.MinValue,

		                                ReadByDriver       = false,
		                                ReadyTime          = DateTimeOffset.MinValue,
		                                ReceivedByDevice   = false,
		                                Status1            = 0,
		                                Status2            = 0,
		                                Status3            = 0,
		                                Status4            = 0,
		                                Status5            = 0,
		                                Status6            = 0,
		                                TripItemsType      = 0,
		                                UNClass            = "",
		                                UndeliverableNotes = "",
		                                VerifiedLatitude   = 0,
		                                VerifiedLongitude  = 0,
		                                VerifiedTime       = DateTimeOffset.MinValue,
		                                Volume             = 0,
		                                Weight             = 0,
		                                CallTakerId        = "",
		                                DriverNotes        = "",

		                                PickupWaitTimeStart               = DateTimeOffset.MinValue,
		                                PickupWaitTimeDurationInSeconds   = 0,
		                                DeliveryWaitTimeStart             = DateTimeOffset.MinValue,
		                                DeliveryWaitTimeDurationInSeconds = 0,

		                                PickupArriveTime   = DateTimeOffset.MinValue,
		                                DeliveryArriveTime = DateTimeOffset.MinValue,

		                                Schedule        = "",
		                                ImportReference = ""
	                                };

#region Next Trip Id
	private static readonly Random Rand = new();

	public static string NextTripId( char prefix = 'Z' )
	{
		lock( Rand )
		{
			static char GetChar()
			{
				return (char)( (uint)Rand.Next( 26 ) + 'A' );
			}

			static string GetInt()
			{
				return Rand.Next( 100000 ).ToString( "00000" );
			}

			return $"{prefix}{GetChar()}{GetChar()}{GetInt()}{GetInt()}";
		}
	}

	public static string NextTripId( IRequestContext context )
	{
		try
		{
			return NextTripId();
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
		return "?????????????";
	}
#endregion
}

public static class TripExtensions
{
	public static Trip Clone( this Trip t ) => new()
	                                           {
		                                           TripId                            = t.TripId,
		                                           Location                          = t.Location,
		                                           LastModified                      = t.LastModified,
		                                           PackageType                       = t.PackageType,
		                                           PickupTime                        = t.PickupTime,
		                                           Status1                           = t.Status1,
		                                           Status2                           = t.Status2,
		                                           Status3                           = t.Status3,
		                                           ReceivedByDevice                  = t.ReceivedByDevice,
		                                           ReadByDriver                      = t.ReadByDriver,
		                                           TripItemsType                     = t.TripItemsType,
		                                           UNClass                           = t.UNClass,
		                                           UndeliverableNotes                = t.UndeliverableNotes,
		                                           VerifiedLatitude                  = t.VerifiedLatitude,
		                                           VerifiedLongitude                 = t.VerifiedLongitude,
		                                           VerifiedTime                      = t.VerifiedTime,
		                                           Volume                            = t.Volume,
		                                           Weight                            = t.Weight,
		                                           CallTakerId                       = t.CallTakerId,
		                                           DriverNotes                       = t.DriverNotes,
		                                           PickupWaitTimeStart               = t.PickupWaitTimeStart,
		                                           PickupWaitTimeDurationInSeconds   = t.PickupWaitTimeDurationInSeconds,
		                                           DeliveryWaitTimeStart             = t.DeliveryWaitTimeStart,
		                                           DeliveryWaitTimeDurationInSeconds = t.DeliveryWaitTimeDurationInSeconds,
		                                           PickupArriveTime                  = t.PickupArriveTime,
		                                           DeliveryArriveTime                = t.DeliveryArriveTime,
		                                           PickupAddressAddressLine1         = t.PickupAddressAddressLine1,
		                                           PickupAddressAddressLine2         = t.PickupAddressAddressLine2,
		                                           PickupAddressBarcode              = t.PickupAddressBarcode,
		                                           PickupAddressCity                 = t.PickupAddressCity,
		                                           PickupAddressCountry              = t.PickupAddressCountry,
		                                           PickupAddressCountryCode          = t.PickupAddressCountryCode,
		                                           PickupAddressEmailAddress         = t.PickupAddressEmailAddress,
		                                           PickupAddressEmailAddress1        = t.PickupAddressEmailAddress1,
		                                           PickupAddressEmailAddress2        = t.PickupAddressEmailAddress2,
		                                           PickupAddressFax                  = t.PickupAddressFax,
		                                           PickupAddressLatitude             = t.PickupAddressLatitude,
		                                           PickupAddressLongitude            = t.PickupAddressLongitude,
		                                           PickupAddressMobile               = t.PickupAddressMobile,
		                                           PickupAddressMobile1              = t.PickupAddressMobile1,
		                                           PickupAddressNotes                = t.PickupAddressNotes,
		                                           PickupAddressPhone                = t.PickupAddressPhone,
		                                           PickupAddressPhone1               = t.PickupAddressPhone1,
		                                           PickupAddressPostalBarcode        = t.PickupAddressPostalBarcode,
		                                           PickupAddressPostalCode           = t.PickupAddressPostalCode,
		                                           PickupAddressRegion               = t.PickupAddressRegion,
		                                           PickupAddressSuite                = t.PickupAddressSuite,
		                                           PickupAddressVicinity             = t.PickupAddressVicinity,
		                                           PickupCompanyAccountId            = t.PickupCompanyAccountId,
		                                           PickupCompanyId                   = t.PickupCompanyId,
		                                           PickupCompanyName                 = t.PickupCompanyName,
		                                           PickupContact                     = t.PickupContact,
		                                           PickupNotes                       = t.PickupNotes,
		                                           PickupZone                        = t.PickupZone,
		                                           PickupLatitude                    = t.PickupLatitude,
		                                           PickupLongitude                   = t.PickupLongitude,
		                                           DeliveryAddressAddressLine1       = t.DeliveryAddressAddressLine1,
		                                           DeliveryAddressAddressLine2       = t.DeliveryAddressAddressLine2,
		                                           DeliveryAddressBarcode            = t.DeliveryAddressBarcode,
		                                           DeliveryAddressCity               = t.DeliveryAddressCity,
		                                           DeliveryAddressCountry            = t.DeliveryAddressCountry,
		                                           DeliveryAddressCountryCode        = t.DeliveryAddressCountryCode,
		                                           DeliveryAddressEmailAddress       = t.DeliveryAddressEmailAddress,
		                                           DeliveryAddressEmailAddress1      = t.DeliveryAddressEmailAddress1,
		                                           DeliveryAddressEmailAddress2      = t.DeliveryAddressEmailAddress2,
		                                           DeliveryAddressFax                = t.DeliveryAddressFax,
		                                           DeliveryAddressLatitude           = t.DeliveryAddressLatitude,
		                                           DeliveryAddressLongitude          = t.DeliveryAddressLongitude,
		                                           DeliveryAddressMobile             = t.DeliveryAddressMobile,
		                                           DeliveryAddressMobile1            = t.DeliveryAddressMobile1,
		                                           DeliveryAddressNotes              = t.DeliveryAddressNotes,
		                                           DeliveryAddressPhone              = t.DeliveryAddressPhone,
		                                           DeliveryAddressPhone1             = t.DeliveryAddressPhone1,
		                                           DeliveryAddressPostalBarcode      = t.DeliveryAddressPostalBarcode,
		                                           DeliveryAddressPostalCode         = t.DeliveryAddressPostalCode,
		                                           DeliveryAddressRegion             = t.DeliveryAddressRegion,
		                                           DeliveryAddressSuite              = t.DeliveryAddressSuite,
		                                           DeliveryAddressVicinity           = t.DeliveryAddressVicinity,
		                                           DeliveryCompanyAccountId          = t.DeliveryCompanyAccountId,
		                                           DeliveryCompanyId                 = t.DeliveryCompanyId,
		                                           DeliveryCompanyName               = t.DeliveryCompanyName,
		                                           DeliveryContact                   = t.DeliveryContact,
		                                           DeliveryNotes                     = t.DeliveryNotes,
		                                           DeliveryZone                      = t.DeliveryZone,
		                                           TripChargesAsJson                 = t.TripChargesAsJson,
		                                           TripItemsAsJson                   = t.TripItemsAsJson,
		                                           DriverCode                        = t.DriverCode,
		                                           AccountId                         = t.AccountId,
		                                           Barcode                           = t.Barcode,
		                                           BillingAddressAddressLine1        = t.BillingAddressAddressLine1,
		                                           BillingAddressAddressLine2        = t.BillingAddressAddressLine2,
		                                           BillingAddressBarcode             = t.BillingAddressBarcode,
		                                           BillingAddressCity                = t.BillingAddressCity,
		                                           BillingAddressCountry             = t.BillingAddressCountry,
		                                           BillingAddressPhone               = t.BillingAddressPhone,
		                                           BillingAddressPostalCode          = t.BillingAddressPostalCode,
		                                           BillingAddressRegion              = t.BillingAddressRegion,
		                                           BillingAddressSuite               = t.BillingAddressSuite,
		                                           BillingAddressVicinity            = t.BillingAddressVicinity,
		                                           BillingCompanyAccountId           = t.BillingCompanyAccountId,
		                                           BillingCompanyName                = t.BillingCompanyName,
		                                           BillingContact                    = t.BillingContact,
		                                           BillingNotes                      = t.BillingNotes,
		                                           BillingAddressCountryCode         = t.BillingAddressCountryCode,
		                                           BillingAddressEmailAddress        = t.BillingAddressEmailAddress,
		                                           BillingAddressEmailAddress1       = t.BillingAddressEmailAddress1,
		                                           BillingAddressEmailAddress2       = t.BillingAddressEmailAddress2,
		                                           BillingAddressFax                 = t.BillingAddressFax,
		                                           BillingAddressLatitude            = t.BillingAddressLatitude,
		                                           BillingAddressLongitude           = t.BillingAddressLongitude,
		                                           BillingAddressMobile              = t.BillingAddressMobile,
		                                           BillingAddressMobile1             = t.BillingAddressMobile1,
		                                           BillingAddressNotes               = t.BillingAddressNotes,
		                                           BillingAddressPostalBarcode       = t.BillingAddressPostalBarcode,
		                                           BillingAddressPhone1              = t.BillingAddressPhone1,
		                                           BillingCompanyId                  = t.BillingCompanyId,
		                                           Board                             = t.Board,
		                                           CallTime                          = t.CallTime,
		                                           CallerEmail                       = t.CallerEmail,
		                                           CallerName                        = t.CallerName,
		                                           CallerPhone                       = t.CallerPhone,
		                                           ClaimedLatitude                   = t.ClaimedLatitude,
		                                           ClaimedLongitude                  = t.ClaimedLongitude,
		                                           ClaimedTime                       = t.ClaimedTime,
		                                           CurrentZone                       = t.CurrentZone,
		                                           DangerousGoods                    = t.DangerousGoods,
		                                           DeliveredLatitude                 = t.DeliveredLatitude,
		                                           DeliveredLongitude                = t.DeliveredLongitude,
		                                           DeliveredTime                     = t.DeliveredTime,
		                                           DeliveryAmount                    = t.DeliveryAmount,
		                                           DeviceDeliverySortOrder           = t.DeviceDeliverySortOrder,
		                                           DevicePickupSortOrder             = t.DevicePickupSortOrder,
		                                           DiscountAmount                    = t.DiscountAmount,
		                                           DriverCommissionCalculated        = t.DriverCommissionCalculated,
		                                           DueTime                           = t.DueTime,
		                                           ExtensionAsJson                   = t.ExtensionAsJson,
		                                           ExtensionType                     = t.ExtensionType,
		                                           FinalisedLatitude                 = t.FinalisedLatitude,
		                                           FinalisedLongitude                = t.FinalisedLongitude,
		                                           FinalisedTime                     = t.FinalisedTime,
		                                           Group0                            = t.Group0,
		                                           Group1                            = t.Group1,
		                                           Group2                            = t.Group2,
		                                           Group3                            = t.Group3,
		                                           Group4                            = t.Group4,
		                                           HasDangerousGoodsDocuments        = t.HasDangerousGoodsDocuments,
		                                           HasItems                          = t.HasItems,
		                                           InvoiceNumber                     = t.InvoiceNumber,
		                                           IsQuote                           = t.IsQuote,
		                                           Measurement                       = t.Measurement,
		                                           MissingPieces                     = t.MissingPieces,
		                                           OriginalPieceCount                = t.OriginalPieceCount,
		                                           POD                               = t.POD,
		                                           POP                               = t.POP,
		                                           Pallets                           = t.Pallets,
		                                           Pieces                            = t.Pieces,
		                                           ReadyTime                         = t.ReadyTime,
		                                           Reference                         = t.Reference,
		                                           Route                             = t.Route,
		                                           ServiceLevel                      = t.ServiceLevel,
		                                           Status4                           = t.Status4,
		                                           Status5                           = t.Status5,
		                                           Status6                           = t.Status6,
		                                           TotalAmount                       = t.TotalAmount,
		                                           TotalFixedAmount                  = t.TotalFixedAmount,
		                                           TotalPayrollAmount                = t.TotalPayrollAmount,
		                                           TotalTaxAmount                    = t.TotalTaxAmount,

		                                           Schedule        = t.Schedule,
		                                           ImportReference = t.ImportReference
	                                           };

	public static List<TripPackage> Consolidate( this List<TripPackage> packages, bool appendReference = true )
	{
		var PDict = new Dictionary<string, TripPackage>();

		// Consolidate Items in same package types
		foreach( var TripPackage in packages )
		{
			var PType = TripPackage.PackageType;

			if( !PDict.TryGetValue( PType, out var Entry ) )
				PDict.Add( PType, TripPackage );
			else
				Entry.Items.AddRange( TripPackage.Items );
		}
		var Result = new List<TripPackage>();

		// Consolidate items within the package type
		foreach( var TripPackagePair in PDict )
		{
			var P = TripPackagePair.Value;
			P.Weight = 0;
			P.Pieces = 0;
			var ItemDict = new Dictionary<string, TripItem>();
			var Items    = P.Items;
			P.Items = [];

			foreach( var TripItem in Items )
			{
				var BCode = TripItem.Barcode;
				TripItem.Reference = TripItem.Reference.Trim();

				if( !ItemDict.TryGetValue( BCode, out var Entry ) )
				{
					ItemDict.Add( BCode, TripItem );
					P.Items.Add( TripItem );
				}
				else
				{
					if( appendReference )
					{
						var Ref = TripItem.Reference.Trim();

						if( Ref.IsNotNullOrWhiteSpace() )
						{
							var ERef = Entry.Reference.Trim();

							if( ERef.IsNotNullOrWhiteSpace() )
							{
								if( !ERef.EndsWith( Ref ) && !ERef.Contains( $"{Ref}," ) )
									Entry.Reference = $"{ERef},{Ref}";
							}
							else
								Entry.Reference = Ref;
						}
					}
					Entry.Pieces      += TripItem.Pieces;
					Entry.Weight      += TripItem.Weight;
					Entry.BinaryData1 |= TripItem.BinaryData1; // Pml Kiosk Alert Flag
				}
				P.Pieces += TripItem.Pieces;
				P.Weight += TripItem.Weight;
			}
			Result.Add( P );
		}
		return Result;
	}

	public static Protocol.Data.Trip ToTrip( this Trip t )
	{
		var Trip = (Protocol.Data.Trip)BuildTripUpdate( new Protocol.Data.Trip(), t );
		Trip.Ok = true;
		return Trip;
	}

	public static TripUpdate ToTripUpdate( this Trip t, string program, bool toDispatchBoard, bool toDriversBoard, bool toDriver )
	{
		var Rec = BuildTripUpdate( new TripUpdate(), t );

		Rec.BroadcastToDispatchBoard = toDispatchBoard;
		Rec.BroadcastToDriver        = toDriver;
		Rec.BroadcastToDriverBoard   = toDriversBoard;
		Rec.Program                  = program;

		return Rec;
	}

	private static TripUpdate BuildTripUpdate( TripUpdate u, Trip t )
	{
		var Charges = ( t.TripChargesAsJson.IsNullOrWhiteSpace()
			                ? []
			                : JsonConvert.DeserializeObject<List<TripCharge>>( t.TripChargesAsJson ) ) ?? [];

		u.TripId           = t.TripId;
		u.Location         = t.Location;
		u.LastModified     = t.LastModified;
		u.PackageType      = t.PackageType;
		u.PickupTime       = t.PickupTime;
		u.Status1          = (STATUS)t.Status1;
		u.Status2          = (STATUS1)t.Status2;
		u.Status3          = (STATUS2)t.Status3;
		u.ReceivedByDevice = t.ReceivedByDevice;
		u.ReadByDriver     = t.ReadByDriver;
		u.AccountId        = t.AccountId;
		u.Barcode          = t.Barcode;
		u.Board            = t.Board;

		u.BillingAccountId            = t.BillingCompanyAccountId;
		u.BillingAddressAddressLine1  = t.BillingAddressAddressLine1;
		u.BillingAddressAddressLine2  = t.BillingAddressAddressLine2;
		u.BillingAddressBarcode       = t.BillingAddressBarcode;
		u.BillingAddressCity          = t.BillingAddressCity;
		u.BillingAddressCountry       = t.BillingAddressCountry;
		u.BillingAddressCountryCode   = t.BillingAddressCountryCode;
		u.BillingAddressEmailAddress  = t.BillingAddressEmailAddress;
		u.BillingAddressEmailAddress1 = t.BillingAddressEmailAddress1;
		u.BillingAddressEmailAddress2 = t.BillingAddressEmailAddress2;
		u.BillingAddressFax           = t.BillingAddressFax;
		u.BillingAddressLatitude      = t.BillingAddressLatitude;
		u.BillingAddressLongitude     = t.BillingAddressLongitude;
		u.BillingAddressMobile        = t.BillingAddressMobile;
		u.BillingAddressMobile1       = t.BillingAddressMobile1;
		u.BillingAddressNotes         = t.BillingAddressNotes;
		u.BillingAddressPhone         = t.BillingAddressPhone;
		u.BillingAddressPhone1        = t.BillingAddressPhone1;
		u.BillingAddressPostalBarcode = t.BillingAddressPostalBarcode;
		u.BillingAddressPostalCode    = t.BillingAddressPostalCode;
		u.BillingAddressRegion        = t.BillingAddressRegion;
		u.BillingAddressSuite         = t.BillingAddressSuite;
		u.BillingAddressVicinity      = t.BillingAddressVicinity;
		u.BillingCompanyName          = t.BillingCompanyName;
		u.BillingContact              = t.BillingContact;
		u.BillingNotes                = t.BillingNotes;

		u.PickupAccountId            = t.PickupCompanyAccountId;
		u.PickupAddressAddressLine1  = t.PickupAddressAddressLine1;
		u.PickupAddressAddressLine2  = t.PickupAddressAddressLine2;
		u.PickupAddressBarcode       = t.PickupAddressBarcode;
		u.PickupAddressCity          = t.PickupAddressCity;
		u.PickupAddressCountry       = t.PickupAddressCountry;
		u.PickupAddressCountryCode   = t.PickupAddressCountryCode;
		u.PickupAddressEmailAddress  = t.PickupAddressEmailAddress;
		u.PickupAddressEmailAddress1 = t.PickupAddressEmailAddress1;
		u.PickupAddressEmailAddress2 = t.PickupAddressEmailAddress2;
		u.PickupAddressFax           = t.PickupAddressFax;
		u.PickupAddressLatitude      = t.PickupAddressLatitude;
		u.PickupAddressLongitude     = t.PickupAddressLongitude;
		u.PickupAddressMobile        = t.PickupAddressMobile;
		u.PickupAddressMobile1       = t.PickupAddressMobile1;
		u.PickupAddressNotes         = t.PickupAddressNotes;
		u.PickupAddressPhone         = t.PickupAddressPhone;
		u.PickupAddressPhone1        = t.PickupAddressPhone1;
		u.PickupAddressPostalBarcode = t.PickupAddressPostalBarcode;
		u.PickupAddressPostalCode    = t.PickupAddressPostalCode;
		u.PickupAddressRegion        = t.PickupAddressRegion;
		u.PickupAddressSuite         = t.PickupAddressSuite;
		u.PickupAddressVicinity      = t.PickupAddressVicinity;
		u.PickupCompanyName          = t.PickupCompanyName;
		u.PickupContact              = t.PickupContact;
		u.PickupNotes                = t.PickupNotes;
		u.PickupZone                 = t.PickupZone;
		u.PickupLongitude            = t.PickupLongitude;
		u.PickupLatitude             = t.PickupLatitude;

		u.DeliveryAccountId            = t.DeliveryCompanyAccountId;
		u.DeliveryAddressAddressLine1  = t.DeliveryAddressAddressLine1;
		u.DeliveryAddressAddressLine2  = t.DeliveryAddressAddressLine2;
		u.DeliveryAddressBarcode       = t.DeliveryAddressBarcode;
		u.DeliveryAddressCity          = t.DeliveryAddressCity;
		u.DeliveryAddressCountry       = t.DeliveryAddressCountry;
		u.DeliveryAddressCountryCode   = t.DeliveryAddressCountryCode;
		u.DeliveryAddressEmailAddress  = t.DeliveryAddressEmailAddress;
		u.DeliveryAddressEmailAddress1 = t.DeliveryAddressEmailAddress1;
		u.DeliveryAddressEmailAddress2 = t.DeliveryAddressEmailAddress2;
		u.DeliveryAddressFax           = t.DeliveryAddressFax;
		u.DeliveryAddressLatitude      = t.DeliveryAddressLatitude;
		u.DeliveryAddressLongitude     = t.DeliveryAddressLongitude;
		u.DeliveryAddressMobile        = t.DeliveryAddressMobile;
		u.DeliveryAddressMobile1       = t.DeliveryAddressMobile1;
		u.DeliveryAddressNotes         = t.DeliveryAddressNotes;
		u.DeliveryAddressPhone         = t.DeliveryAddressPhone;
		u.DeliveryAddressPhone1        = t.DeliveryAddressPhone1;
		u.DeliveryAddressPostalBarcode = t.DeliveryAddressPostalBarcode;
		u.DeliveryAddressPostalCode    = t.DeliveryAddressPostalCode;
		u.DeliveryAddressRegion        = t.DeliveryAddressRegion;
		u.DeliveryAddressSuite         = t.DeliveryAddressSuite;
		u.DeliveryAddressVicinity      = t.DeliveryAddressVicinity;
		u.DeliveryCompanyName          = t.DeliveryCompanyName;
		u.DeliveryContact              = t.DeliveryContact;
		u.DeliveryNotes                = t.DeliveryNotes;
		u.DeliveryLatitude             = t.DeliveredLatitude;
		u.DeliveryLongitude            = t.DeliveredLongitude;
		u.DeliveryTime                 = t.DeliveredTime;
		u.DeliveryZone                 = t.DeliveryZone;

		u.CallerEmail        = t.CallerEmail;
		u.CallerName         = t.CallerName;
		u.CallerPhone        = t.CallerPhone;
		u.CallTime           = t.CallTime;
		u.ClaimLatitude      = t.ClaimedLatitude;
		u.ClaimLongitude     = t.ClaimedLongitude;
		u.ClaimTime          = t.ClaimedTime;
		u.CurrentZone        = t.CurrentZone;
		u.DangerousGoods     = t.DangerousGoods;
		u.Driver             = t.DriverCode;
		u.DueTime            = t.DueTime;
		u.HasDocuments       = t.HasDangerousGoodsDocuments;
		u.IsQuote            = t.IsQuote;
		u.Measurement        = t.Measurement;
		u.MissingPieces      = t.MissingPieces;
		u.OriginalPieceCount = t.OriginalPieceCount;
		u.POD                = t.POD;
		u.POP                = t.POP;

		u.Pieces = t.Pieces;
		u.Weight = t.Weight;

		u.ReadyTime          = t.ReadyTime;
		u.Reference          = t.Reference;
		u.ServiceLevel       = t.ServiceLevel;
		u.TripCharges        = Charges;
		u.UNClass            = t.UNClass;
		u.UndeliverableNotes = t.UndeliverableNotes;
		u.VerifiedLongitude  = t.VerifiedLongitude;
		u.VerifiedLatitude   = t.VerifiedLatitude;
		u.VerifiedTime       = t.VerifiedTime;
		u.ExtensionType      = t.ExtensionType;
		u.ExtensionAsJson    = t.ExtensionAsJson;
		u.PackageTypes       = (TripUpdate.PACKAGE_TYPES)t.TripItemsType;
		u.PackagesAsJson     = t.TripItemsAsJson;
		u.Group0             = t.Group0;
		u.Group1             = t.Group1;
		u.Group2             = t.Group2;
		u.Group3             = t.Group3;
		u.Group4             = t.Group4;
		u.CallTakerId        = t.CallTakerId;

		u.Route             = t.Route;
		u.DeliverySortOrder = t.DeviceDeliverySortOrder;
		u.PickupSortOrder   = t.DevicePickupSortOrder;

		u.DriverNotes = t.DriverNotes;

		u.PickupWaitTimeStart       = t.PickupWaitTimeStart;
		u.PickupWaitTimeInSeconds   = t.PickupWaitTimeDurationInSeconds;
		u.DeliveryWaitTimeStart     = t.DeliveryWaitTimeStart;
		u.DeliveryWaitTimeInSeconds = t.DeliveryWaitTimeDurationInSeconds;

		u.PickupArriveTimeSpecified = t.PickupArriveTime != DateTimeOffset.MinValue;
		u.PickupArriveTime          = t.PickupArriveTime;

		u.DeliveryArriveTimeSpecified = t.DeliveryArriveTime != DateTimeOffset.MinValue;
		u.DeliveryArriveTime          = t.DeliveryArriveTime;

		u.InvoiceNumber      = t.InvoiceNumber;
		u.TotalAmount        = t.TotalAmount;
		u.TotalTaxAmount     = t.TotalTaxAmount;
		u.DiscountAmount     = t.DiscountAmount;
		u.TotalFixedAmount   = t.TotalFixedAmount;
		u.TotalPayrollAmount = t.TotalPayrollAmount;
		u.DeliveryAmount     = t.DeliveryAmount;
		u.Pallets            = t.Pallets;

		return u;
	}
}