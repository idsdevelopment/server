﻿using Trip = Database.Model.Databases.MasterTemplate.Trip;

#nullable enable

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public (Trip? Trip, string OriginalDriver ) ResponseClaimTrip( string programName, string driverCode, string tripId )
	{
		(Trip? Trip, string OriginalDriver) Result = ( null, "" );

		try
		{
			var E = Entity;

			while( true )
			{
				try
				{
					var Rec = ( from T in E.Trips
					            where T.TripId == tripId
					            select T ).FirstOrDefault();

					if( Rec is not null )
					{
						Result.OriginalDriver = Rec.TripId;
						var Before = new TripLogger( Rec );
						Rec.DriverCode = driverCode;
						E.SaveChanges();
						var After = new TripLogger( Rec );
						LogTrip( programName, Before, After );
						Result.Trip = Rec;
					}
				}
				catch( DbUpdateConcurrencyException )
				{
					continue;
				}
				break;
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( programName, Exception );
		}
		return Result;
	}
}