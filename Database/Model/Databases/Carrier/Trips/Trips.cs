﻿#nullable enable

using Ids1WebService;
using StorageV2.Carrier;
using Trip = Database.Model.Databases.MasterTemplate.Trip;

// ReSharper disable InconsistentNaming

namespace Database.Model.Databases.MasterTemplate
{
	public partial class Trip
	{
		public Trip()
		{
		}

		public Trip( Trip t )
		{
			TripId                     = t.TripId;
			Location                   = t.Location;
			LastModified               = t.LastModified;
			Barcode                    = t.Barcode;
			AccountId                  = t.AccountId;
			Status1                    = t.Status1;
			Status2                    = t.Status2;
			Status3                    = t.Status3;
			Status4                    = t.Status4;
			Status5                    = t.Status5;
			Status6                    = t.Status6;
			Reference                  = t.Reference;
			CallTime                   = t.CallTime;
			CallerPhone                = t.CallerPhone;
			CallerEmail                = t.CallerEmail;
			ReadyTime                  = t.ReadyTime;
			DueTime                    = t.DueTime;
			DriverCode                 = t.DriverCode;
			Weight                     = t.Weight;
			Pieces                     = t.Pieces;
			Volume                     = t.Volume;
			Measurement                = t.Measurement;
			OriginalPieceCount         = t.OriginalPieceCount;
			MissingPieces              = t.MissingPieces;
			ReceivedByDevice           = t.ReceivedByDevice;
			ReadByDriver               = t.ReadByDriver;
			DangerousGoods             = t.DangerousGoods;
			HasItems                   = t.HasItems;
			UNClass                    = t.UNClass;
			HasDangerousGoodsDocuments = t.HasDangerousGoodsDocuments;
			Board                      = t.Board;
			PackageType                = t.PackageType;
			ServiceLevel               = t.ServiceLevel;
			POP                        = t.POP;
			POD                        = t.POD;

			PickupCompanyId            = t.PickupCompanyId;
			PickupCompanyName          = t.PickupCompanyName;
			PickupAddressBarcode       = t.PickupAddressBarcode;
			PickupAddressPostalBarcode = t.PickupAddressPostalBarcode;
			PickupAddressSuite         = t.PickupAddressSuite;
			PickupAddressAddressLine1  = t.PickupAddressAddressLine1;
			PickupAddressAddressLine2  = t.PickupAddressAddressLine2;
			PickupAddressVicinity      = t.PickupAddressVicinity;
			PickupAddressCity          = t.PickupAddressCity;
			PickupAddressRegion        = t.PickupAddressRegion;
			PickupAddressPostalCode    = t.PickupAddressPostalCode;
			PickupAddressCountry       = t.PickupAddressCountry;
			PickupAddressCountryCode   = t.PickupAddressCountryCode;
			PickupAddressPhone         = t.PickupAddressPhone;
			PickupAddressPhone1        = t.PickupAddressPhone1;
			PickupAddressMobile        = t.PickupAddressMobile;
			PickupAddressMobile1       = t.PickupAddressMobile1;
			PickupAddressFax           = t.PickupAddressFax;
			PickupAddressEmailAddress  = t.PickupAddressEmailAddress;
			PickupAddressEmailAddress1 = t.PickupAddressEmailAddress1;
			PickupAddressEmailAddress2 = t.PickupAddressEmailAddress2;
			PickupContact              = t.PickupContact;
			PickupAddressLatitude      = t.PickupAddressLatitude;
			PickupAddressLongitude     = t.PickupAddressLongitude;
			PickupAddressNotes         = t.PickupAddressNotes;
			PickupNotes                = t.PickupNotes;

			DeliveryCompanyId            = t.DeliveryCompanyId;
			DeliveryCompanyName          = t.DeliveryCompanyName;
			DeliveryAddressBarcode       = t.DeliveryAddressBarcode;
			DeliveryAddressPostalBarcode = t.DeliveryAddressPostalBarcode;
			DeliveryAddressSuite         = t.DeliveryAddressSuite;
			DeliveryAddressAddressLine1  = t.DeliveryAddressAddressLine1;
			DeliveryAddressAddressLine2  = t.DeliveryAddressAddressLine2;
			DeliveryAddressVicinity      = t.DeliveryAddressVicinity;
			DeliveryAddressCity          = t.DeliveryAddressCity;
			DeliveryAddressRegion        = t.DeliveryAddressRegion;
			DeliveryAddressPostalCode    = t.DeliveryAddressPostalCode;
			DeliveryAddressCountry       = t.DeliveryAddressCountry;
			DeliveryAddressCountryCode   = t.DeliveryAddressCountryCode;
			DeliveryAddressPhone         = t.DeliveryAddressPhone;
			DeliveryAddressPhone1        = t.DeliveryAddressPhone1;
			DeliveryAddressMobile        = t.DeliveryAddressMobile;
			DeliveryAddressMobile1       = t.DeliveryAddressMobile1;
			DeliveryAddressFax           = t.DeliveryAddressFax;
			DeliveryAddressEmailAddress  = t.DeliveryAddressEmailAddress;
			DeliveryAddressEmailAddress1 = t.DeliveryAddressEmailAddress1;
			DeliveryAddressEmailAddress2 = t.DeliveryAddressEmailAddress2;
			DeliveryContact              = t.DeliveryContact;
			DeliveryAddressLatitude      = t.DeliveryAddressLatitude;
			DeliveryAddressLongitude     = t.DeliveryAddressLongitude;
			DeliveryAddressNotes         = t.DeliveryAddressNotes;
			DeliveryNotes                = t.DeliveryNotes;

			BillingCompanyId            = t.BillingCompanyId;
			BillingCompanyName          = t.BillingCompanyName;
			BillingAddressBarcode       = t.BillingAddressBarcode;
			BillingAddressPostalBarcode = t.BillingAddressPostalBarcode;
			BillingAddressSuite         = t.BillingAddressSuite;
			BillingAddressAddressLine1  = t.BillingAddressAddressLine1;
			BillingAddressAddressLine2  = t.BillingAddressAddressLine2;
			BillingAddressVicinity      = t.BillingAddressVicinity;
			BillingAddressCity          = t.BillingAddressCity;
			BillingAddressRegion        = t.BillingAddressRegion;
			BillingAddressPostalCode    = t.BillingAddressPostalCode;
			BillingAddressCountry       = t.BillingAddressCountry;
			BillingAddressCountryCode   = t.BillingAddressCountryCode;
			BillingAddressPhone         = t.BillingAddressPhone;
			BillingAddressPhone1        = t.BillingAddressPhone1;
			BillingAddressMobile        = t.BillingAddressMobile;
			BillingAddressMobile1       = t.BillingAddressMobile1;
			BillingAddressFax           = t.BillingAddressFax;
			BillingAddressEmailAddress  = t.BillingAddressEmailAddress;
			BillingAddressEmailAddress1 = t.BillingAddressEmailAddress1;
			BillingAddressEmailAddress2 = t.BillingAddressEmailAddress2;
			BillingContact              = t.BillingContact;
			BillingAddressLatitude      = t.BillingAddressLatitude;
			BillingAddressLongitude     = t.BillingAddressLongitude;
			BillingAddressNotes         = t.BillingAddressNotes;
			BillingNotes                = t.BillingNotes;

			PickupCompanyAccountId   = t.PickupCompanyAccountId;
			DeliveryCompanyAccountId = t.DeliveryCompanyAccountId;
			BillingCompanyAccountId  = t.BillingCompanyAccountId;

			CurrentZone                = t.CurrentZone;
			PickupZone                 = t.PickupZone;
			DeliveryZone               = t.DeliveryZone;
			PickupTime                 = t.PickupTime;
			PickupLatitude             = t.PickupLatitude;
			PickupLongitude            = t.PickupLongitude;
			DeliveredTime              = t.DeliveredTime;
			DeliveredLatitude          = t.DeliveredLatitude;
			DeliveredLongitude         = t.DeliveredLongitude;
			VerifiedTime               = t.VerifiedTime;
			VerifiedLatitude           = t.VerifiedLatitude;
			VerifiedLongitude          = t.VerifiedLongitude;
			ClaimedTime                = t.ClaimedTime;
			ClaimedLatitude            = t.ClaimedLatitude;
			ClaimedLongitude           = t.ClaimedLongitude;
			TripChargesAsJson          = t.TripChargesAsJson;
			CallerName                 = t.CallerName;
			ExtensionType              = t.ExtensionType;
			ExtensionAsJson            = t.ExtensionAsJson;
			UndeliverableNotes         = t.UndeliverableNotes;
			TripItemsType              = t.TripItemsType;
			TripItemsAsJson            = t.TripItemsAsJson;
			FinalisedLatitude          = t.FinalisedLatitude;
			FinalisedLongitude         = t.FinalisedLongitude;
			FinalisedTime              = t.FinalisedTime;
			DriverCommissionCalculated = t.DriverCommissionCalculated;
			Group0                     = t.Group0;
			Group1                     = t.Group1;
			Group2                     = t.Group2;
			Group3                     = t.Group3;
			Group4                     = t.Group4;
			DeviceDeliverySortOrder    = t.DeviceDeliverySortOrder;
			DevicePickupSortOrder      = t.DevicePickupSortOrder;
			Route                      = t.Route;
			CallTakerId                = t.CallTakerId;
			DriverNotes                = t.DriverNotes;

			PickupWaitTimeStart               = t.PickupWaitTimeStart;
			PickupWaitTimeDurationInSeconds   = t.PickupWaitTimeDurationInSeconds;
			DeliveryWaitTimeStart             = t.DeliveryWaitTimeStart;
			DeliveryWaitTimeDurationInSeconds = t.DeliveryWaitTimeDurationInSeconds;

			PickupArriveTime   = t.PickupArriveTime;
			DeliveryArriveTime = t.DeliveryArriveTime;

			InvoiceNumber      = t.InvoiceNumber;
			TotalAmount        = t.TotalAmount;
			TotalTaxAmount     = t.TotalTaxAmount;
			DiscountAmount     = t.DiscountAmount;
			TotalFixedAmount   = t.TotalFixedAmount;
			TotalPayrollAmount = t.TotalPayrollAmount;
			DeliveryAmount     = t.DeliveryAmount;
			Pallets            = t.Pallets;
			Schedule           = t.Schedule;
			ImportReference    = t.ImportReference;
		}
	}
}

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		// Must map to Protocol.Data.TripItem for JsonConvert to work
		public class TripItem
		{
			public string  ItemCode     { get; set; } = "";
			public string  ItemCode1    { get; set; } = "";
			public string  Barcode      { get; set; } = "";
			public string  Barcode1     { get; set; } = "";
			public string  Description  { get; set; } = "";
			public string  Reference    { get; set; } = "";
			public decimal Pieces       { get; set; }
			public decimal Height       { get; set; }
			public decimal Width        { get; set; }
			public decimal Length       { get; set; }
			public decimal Volume       { get; set; }
			public decimal Weight       { get; set; }
			public decimal Value        { get; set; }
			public decimal Tax1         { get; set; }
			public decimal Tax2         { get; set; }
			public bool    HasDocuments { get; set; }
			public decimal Original     { get; set; }
			public decimal QH           { get; set; }
		}

		public TripList GetTripsForAccount( string accountId )
		{
			var Result = new TripList();

			try
			{
				accountId = accountId.Trim();

				Result.AddRange( from T1 in ( from T in Entity.Trips
				                              where T.AccountId == accountId
				                              select T ).ToList()
				                 select T1.ToTrip() );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}

		public TripIdList GetTripIdsByStatusAndLocationAndServiceLevel( StatusAndLocationAndServiceLevel sl )
		{
			var Result = new TripIdList();

			try
			{
				if( sl.Count > 0 )
				{
					var Statuses = ( from S in sl
					                 select (int)S ).ToList();

					var Location = sl.Location.Trim();

					var ServiceLevel = sl.ServiceLevel.Trim();

					if( Location.IsNotNullOrWhiteSpace() )
					{
						if( ServiceLevel.IsNotNullOrWhiteSpace() )
						{
							Result.AddRange( from T in Entity.Trips
							                 where Statuses.Contains( T.Status1 ) && ( T.Location == Location ) && ( T.ServiceLevel == ServiceLevel )
							                 select T.TripId );
						}
						else
						{
							Result.AddRange( from T in Entity.Trips
							                 where Statuses.Contains( T.Status1 ) && ( T.Location == Location )
							                 select T.TripId );
						}
					}
					else
					{
						if( ServiceLevel.IsNotNullOrWhiteSpace() )
						{
							Result.AddRange( from T in Entity.Trips
							                 where Statuses.Contains( T.Status1 ) && ( T.ServiceLevel == ServiceLevel )
							                 select T.TripId );
						}
						else
						{
							Result.AddRange( from T in Entity.Trips
							                 where Statuses.Contains( T.Status1 )
							                 select T.TripId );
						}
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
			return Result;
		}

		public bool IsTripIdValid( string tripId )
		{
			try
			{
				return !( from T in Entity.TripStorageIndexes
				          where T.TripId == tripId
				          select T ).Any()
				       && !( from T in Entity.Trips
				             where T.TripId == tripId
				             select T ).Any();
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
			return false;
		}

		public bool TripExists( string tripId ) => ( from T in Entity.Trips
		                                             where T.TripId == tripId
		                                             select T.TripId ).Any();

		public TripList GetTripsByStatus( StatusRequest requestObject )
		{
			var Temp = from S in requestObject
			           select (int)S;
			var Result = new TripList();

			try
			{
				var Recs = from T in Entity.Trips
				           where Temp.Contains( T.Status1 )
				           select T;

				if( Recs.Any() )
				{
					foreach( var Trip in Recs )
						Result.Add( Trip.ToTrip() );
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
			return Result;
		}

		public bool DeleteTrip( string program, Trip t, DateTime lastUpdated ) => DeleteFinaliseTrip( false, program, t, lastUpdated, DateTimeOffset.UtcNow, 0, 0 );
		public bool DeleteTrip( string program, Trip t, DateTime lastUpdated, DateTimeOffset deleteTime ) => DeleteFinaliseTrip( false, program, t, lastUpdated, deleteTime, 0, 0 );

		public bool DeleteTrip( string program, string tripId )
		{
			var Trip = ( from T in Entity.Trips
			             where T.TripId == tripId
			             select T ).FirstOrDefault();
			return Trip is not null && DeleteTrip( program, Trip, DateTime.UtcNow );
		}

		public void DeleteTrip( string program, List<string> tripIds )
		{
			foreach( var TripId in tripIds )
				DeleteTrip( program, TripId );
		}

		public bool FinaliseTrip( string program, Trip t, DateTime lastUpdated ) => DeleteFinaliseTrip( true, program, t, lastUpdated, DateTimeOffset.UtcNow, 0, 0 );

		public bool FinaliseTrip( string program, Trip t ) => DeleteFinaliseTrip( true, program, t, DateTime.UtcNow, DateTimeOffset.UtcNow, 0, 0 );

		public bool FinaliseTrip( string program, string tripId )
		{
			var Trip = ( from T in Entity.Trips
			             where T.TripId == tripId
			             select T ).FirstOrDefault();
			return Trip is not null && FinaliseTrip( program, Trip, DateTime.UtcNow );
		}

		public void FinaliseTrips( string program, List<string> tripIds )
		{
			foreach( var TripId in tripIds )
				FinaliseTrip( program, TripId );
		}

		public (Trip? RetryTrip, bool Ok) UpdateTrip( Trip t )
		{
			var Result = ( RetryTrip: (Trip?)null, Ok: true );

			try
			{
				t.LastModified = DateTime.UtcNow;
				Entity.SaveChanges();
			}
			catch( DbUpdateConcurrencyException Exception )
			{
				// Get the current entity values and the values in the database
				var Entry          = Exception.Entries.Single();
				var DatabaseValues = Entry.GetDatabaseValues();

				//Record deleted ??
				if( DatabaseValues is not null )
				{
					Result.RetryTrip = (Trip)DatabaseValues.ToObject();
					Result.Ok        = false;
				}
			}
			catch( Exception E )
			{
				Context.SystemLogException( E );
			}
			finally
			{
				try
				{
					if( Result.Ok && ( (STATUS)t.Status1 == STATUS.DISPATCHED ) )
					{
						var Prefs = GetEmailPreferences();

						if( Prefs.EmailDispatched )
						{
							var From = Prefs.SmtpFromAddress.Trim();

							if( ( From != "" )
							    && Prefs.SmtpServer.IsNotNullOrWhiteSpace()
							    && Prefs.SmtpPassword.IsNotNullOrWhiteSpace()
							    && Prefs.SmtpUserName.IsNotNullOrWhiteSpace() )
							{
								var Driver = t.DriverCode.Trim();
								var Drv    = GetStaffMember( Driver );

								if( Drv.Enabled )
								{
									var To = Drv.Address.EmailAddress.Trim();

									if( To != "" )
									{
										var TripId = t.TripId.Trim();

										string Combine( string suite, string addr )
										{
											suite = suite.Trim();

											if( suite != "" )
												suite = $"Suite {suite} / ";

											return $"{suite}{addr.Trim()}";
										}

										var Body = $"""
										                 Shipment Id: {TripId}
										                Package Type: {t.PackageType}
										               Service Level: {t.ServiceLevel}
										                      Pieces: {t.Pieces}
										                      Weight: {t.Weight}
										            
										              Pickup Company: {t.PickupCompanyName}
										                     Address: {Combine( t.PickupAddressSuite, t.PickupAddressAddressLine1 )}
										                              {t.PickupAddressAddressLine2.Trim()}
										                        City: {t.PickupAddressCity}
										                      Region: {t.PickupAddressRegion}
										                 Postal Code: {t.PickupAddressPostalCode}
										                     Country: {t.PickupAddressCountry} {t.PickupAddressCountryCode.Trim()}
										                            
										            Delivery Company: {t.DeliveryCompanyName}
										                     Address: {Combine( t.DeliveryAddressSuite, t.DeliveryAddressAddressLine1 )}
										                              {t.DeliveryAddressAddressLine2.Trim()}
										                        City: {t.DeliveryAddressCity}
										                      Region: {t.DeliveryAddressRegion}
										                 Postal Code: {t.DeliveryAddressPostalCode}
										                     Country: {t.DeliveryAddressCountry} {t.DeliveryAddressCountryCode.Trim()}

										            Pickup Notes:
										            {t.PickupNotes.Trim()}

										            Delivery Notes:
										            {t.DeliveryNotes.Trim()}
										            """;

										var Testing = Context.IsIds
										              || Context.CarrierId.StartsWith( "Test", StringComparison.OrdinalIgnoreCase )
										              || Context.CarrierId.EndsWith( "Test", StringComparison.OrdinalIgnoreCase )
											              ? "(!! Testing !!) " : "";

										Context.SendEmail( Context, From, To, $"{Testing}DISPATCHED Shipment: {TripId}", Body );
									}
								}
							}
						}
					}
				}
				catch( Exception Exception )
				{
					Context.SystemLogException( Exception );
				}
			}
			return Result;
		}

		public bool SetTripStatus( string program, Trip? trip, string driver, Action<Trip>? setStatus = null )
		{
			try
			{
				while( trip is not null )
				{
					var Before = new TripLogger( trip );
					setStatus?.Invoke( trip );
					var Status  = (STATUS)trip.Status1;
					var Status1 = (STATUS1)trip.Status2;
					var Status2 = (STATUS2)trip.Status3;

					switch( Status )
					{
					case STATUS.DISPATCHED when ( Status2 & STATUS2.WAS_UNDELIVERABLE ) != 0: // From dispatch board to driver & drivers board
						if( ( Status2 & STATUS2.WAS_PICKED_UP ) != 0 )
							Status = STATUS.PICKED_UP;
						goto case STATUS.DISPATCHED;

					case STATUS.DISPATCHED when ( Status1 & STATUS1.UNDELIVERED ) != 0: // Unpickupable from device
						Status2 |= STATUS2.WAS_UNDELIVERABLE;
						Status  =  STATUS.ACTIVE;
						goto case STATUS.ACTIVE;

					case STATUS.PICKED_UP when ( Status1 & STATUS1.UNDELIVERED ) != 0: // Undeliverable from device
						Status2 |= STATUS2.WAS_PICKED_UP | STATUS2.WAS_UNDELIVERABLE;
						Status1 &= ~STATUS1.UNDELIVERED;
						Status  =  STATUS.ACTIVE;
						goto case STATUS.ACTIVE;

					case STATUS.ACTIVE:
					case STATUS.DISPATCHED:
						Status2               &= ~STATUS2.DONT_SEND_TO_DRIVER;
						trip.ReadByDriver     =  false;
						trip.ReceivedByDevice =  false;
						break;
					}
					trip.Status1        = (int)Status;
					trip.Status2        = (int)Status1;
					trip.Status3        = (int)Status2;
					trip.DriverCode     = driver;
					var (RetryTrip, Ok) = UpdateTrip( trip );

					if( !Ok )
					{
						trip = RetryTrip;
						continue;
					}
					var After = new TripLogger( trip );
					LogTrip( program, Before, After );

					if( Context.SendToIds1 )
						IDSClient.Ids1.UpdateTrip( Context, false, trip.ToTransferRecord( program, Context.CarrierId ) );

					break;
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
			return true;
		}

		public bool SetTripStatus( string program, string driver, string tripId, Action<Trip> setStatus ) => SetTripStatus( program, ( from T in Entity.Trips
		                                                                                                                               where T.TripId == tripId
		                                                                                                                               select T ).FirstOrDefault(),
		                                                                                                                    driver, setStatus );

		public bool SetTripStatus( string program, STATUS status, STATUS1 status1, STATUS2 status2, string driver, string tripId )
		{
			return SetTripStatus( program, driver, tripId, trip =>
			                                               {
				                                               trip.Status1 = (int)status;
				                                               trip.Status2 = (int)status1;
				                                               trip.Status3 = (int)status2;
			                                               } );
		}

		public bool SetTripStatus( string program, STATUS status, STATUS1 status1, string driver, string tripId )
		{
			return SetTripStatus( program, driver, tripId, trip =>
			                                               {
				                                               trip.Status1 = (int)status;
				                                               trip.Status2 = (int)status1;
			                                               } );
		}

		public bool SetTripStatus( string program, STATUS status, string driver, string tripId )
		{
			return SetTripStatus( program, driver, tripId, trip =>
			                                               {
				                                               switch( status )
				                                               {
				                                               case STATUS.ACTIVE when (STATUS)trip.Status1 == STATUS.DISPATCHED:
					                                               trip.Status3 &= (int)~( STATUS2.WAS_UNDELIVERABLE | STATUS2.WAS_PICKED_UP );
					                                               break;

				                                               case STATUS.ACTIVE when ( trip.Status2 & (int)STATUS1.UNDELIVERED ) != 0:
					                                               trip.Status3 |= (int)( STATUS2.WAS_UNDELIVERABLE | STATUS2.WAS_PICKED_UP );
					                                               break;
				                                               }
				                                               trip.Status1 = (int)status;
			                                               } );
		}

		public void SetTripStatus( string program, STATUS status, string driver, List<string> tripIds )
		{
			foreach( var TripId in tripIds )
				SetTripStatus( program, status, driver, TripId );
		}

		public void SetTripStatus( string program, STATUS status, STATUS1 status1, STATUS2 status2, string driver, List<string> tripIds )
		{
			foreach( var TripId in tripIds )
				SetTripStatus( program, status, status1, status2, driver, TripId );
		}

		public void DispatchTrip( string program, string driver, STATUS1 status1, STATUS2 status2, List<string> tripIds )
		{
			SetTripStatus( program, STATUS.DISPATCHED, status1, status2, driver, tripIds );
		}

		public void PickupTrip( string program, string driver, List<string> tripIds )
		{
			SetTripStatus( program, STATUS.PICKED_UP, STATUS1.UNSET, STATUS2.UNSET, driver, tripIds );
		}

		public void BounceTrip( string program, string driver, List<string> tripIds )
		{
			SetTripStatus( program, STATUS.ACTIVE, driver, tripIds );
		}

		public Protocol.Data.Trip? GeTrip( GetTrip requestObject )
		{
			Protocol.Data.Trip? Result = null;

			try
			{
				var TripId = requestObject.TripId;

				var Rec = ( from T in Entity.Trips
				            where T.TripId == TripId
				            select T ).FirstOrDefault();

				if( Rec is null )
				{
					var StorageRec = ( from T in Entity.TripStorageIndexes
					                   where T.TripId == TripId
					                   select T ).FirstOrDefault();

					if( StorageRec is not null )
					{
						var Trips = TripsByDate.GetTrips( Context, StorageRec.CallTime, TripId );

						if( Trips is { Count: > 0 } )
							Result = Trips[ 0 ];
					}
				}
				else
					Result = Rec.ToTrip();

				if( Result is not null )
				{
					if( requestObject.Signatures )
						Result.Signatures = new Signatures( Context ).GetSignatures( requestObject.TripId );
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
			return Result;
		}

		public TripList ResponseGetTrips( TripIdList requestObject )
		{
			var Result = new TripList();

			try
			{
				var Trips = ( from T in Entity.Trips
				              where requestObject.Contains( T.TripId )
				              select T ).ToList();

				foreach( var T in Trips )
					Result.Add( T.ToTrip() );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
			return Result;
		}

		public void FinaliseTrip( string program, Trip t,
		                          DateTime lastUpdated, DateTimeOffset finalisedDateTime,
		                          double finalisedLatitude, double finalisedLongitude )
		{
			DeleteFinaliseTrip( true, program, t, lastUpdated, finalisedDateTime, finalisedLatitude, finalisedLongitude );
		}

		private static TripUpdateMessage BuildMessage( string program, TripUpdateMessage.ACTION action, Trip t ) =>
			new()
			{
				Program                 = program,
				Action                  = action,
				Board                   = t.Board,
				Status                  = (STATUS)t.Status1,
				Status1                 = (STATUS1)t.Status2,
				Status2                 = (STATUS2)t.Status3,
				ReadByDriver            = t.ReadByDriver,
				ReceivedByDevice        = t.ReceivedByDevice,
				BroadcastToDriversBoard = true,
				TripIdList              = [t.TripId]
			};

		private bool DeleteFinaliseTrip( bool finalise, string program, Trip? t,
		                                 DateTime lastUpdated, DateTimeOffset finalisedDateTime,
		                                 double finalisedLatitude, double finalisedLongitude )
		{
			if( t is not null )
			{
				try
				{
					var E     = Entity;
					var Trips = E.Trips;

					while( true )
					{
						try
						{
							Trips.Remove( t );
							E.SaveChanges();
							break;
						}
						catch( DbUpdateConcurrencyException )
						{
							t = ( from T in Trips
							      where T.TripId == t.TripId
							      select T ).FirstOrDefault();

							if( t is null ) // Deleted
								return false;
						}
					}
					t.FinalisedTime      = finalisedDateTime;
					t.FinalisedLatitude  = finalisedLatitude;
					t.FinalisedLongitude = finalisedLongitude;
					t.Status1            = finalise ? (int)STATUS.FINALISED : (int)STATUS.DELETED;
					StorageV2.Carrier.Trips.Add( Context, t.ToTrip() );

					var NdxRec = new TripStorageIndex
					             {
						             CallTime            = t.CallTime,
						             TripId              = t.TripId,
						             Reference           = t.Reference.MaxLength( 255 ),
						             Status1             = (byte)t.Status1,
						             Status2             = (byte)t.Status2,
						             Status3             = (byte)t.Status3,
						             AccountId           = t.AccountId,
						             DeliveryCompanyName = t.DeliveryCompanyName,
						             PickupCompanyName   = t.PickupCompanyName,
						             Lastupdated         = DateTimeOffset.UtcNow,
						             VerifiedTime        = t.VerifiedTime,
						             PickupedTime        = t.PickupTime
					             };
					E.TripStorageIndexes.Add( NdxRec );
					E.SaveChanges();
					t.LastModified = lastUpdated;

					if( finalise )
						LogFinalisedTrip( program, t );
					else
						LogDeletedTrip( program, t );
					return true;
				}
				catch( Exception Exception )
				{
					Context.SystemLogException( program, Exception );
				}
			}
			return false;
		}
	}
}