﻿#nullable enable

using StorageV2.Carrier;
using Constants = StorageV2.Constants;
using Trip = Database.Model.Databases.MasterTemplate.Trip;

// ReSharper disable InconsistentNaming

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public TripList GetTripsStartingWith( SearchTripsByStatus request )
	{
		var Result = new TripList();

		try
		{
			var FromStatus = (int)request.StartStatus;
			var ToStatus   = (int)request.EndStatus;

			var Tasks = new[]
			            {
				            Task.Run( () =>
				                      {
					                      try
					                      {
						                      using var Db = new CarrierDb( Context );

						                      var Trips = ( from T in Db.Entity.Trips
						                                    where T.TripId.StartsWith( request.TripId ) && ( T.Status1 >= FromStatus ) && ( T.Status1 <= ToStatus )
						                                    select T ).ToList();

						                      lock( Result )
						                      {
							                      Result.AddRange( from T in Trips
							                                       select T.ToTrip() );
						                      }
					                      }
					                      catch( Exception Exception )
					                      {
						                      Context.SystemLogException( Exception );
					                      }
				                      } ),

				            Task.Run( () =>
				                      {
					                      try
					                      {
						                      if( request.EndStatus >= STATUS.FINALISED )
						                      {
							                      using var Db = new CarrierDb( Context );

							                      var Trips = ( from T in Db.Entity.TripStorageIndexes
							                                    where T.TripId.StartsWith( request.TripId ) && ( T.Status1 >= FromStatus ) && ( T.Status1 <= ToStatus )
							                                    select T ).ToList();

							                      var OrigContext = Context.Debug ? OriginalCarrierContext() : null;

							                      foreach( var StorageIndex in Trips )
							                      {
								                      Task[] Tsks;

								                      void GetTrips( IRequestContext ctx )
								                      {
									                      var TripList = TripsByDate.GetTrips( ctx, StorageIndex.CallTime, StorageIndex.TripId );

									                      lock( Result )
										                      Result.AddRange( TripList );
								                      }

								                      if( OrigContext is not null )
								                      {
									                      Tsks = new Task[ 2 ];

									                      Tsks[ 1 ] = Task.Run( () =>
									                                            {
										                                            GetTrips( OrigContext );
									                                            } );
								                      }
								                      else
									                      Tsks = new Task[ 1 ];

								                      Tsks[ 0 ] = Task.Run( () =>
								                                            {
									                                            GetTrips( Context );
								                                            } );

								                      Task.WaitAll( Tsks );
							                      }
						                      }
					                      }
					                      catch( Exception Exception )
					                      {
						                      Context.SystemLogException( Exception );
					                      }
				                      } )
			            };

			Task.WaitAll( Tasks );

			var Temp = ( from T in Result
			             orderby T.TripId
			             select T ).ToList();
			Result.Clear();
			Result.AddRange( Temp );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}

	public TripList DbSearchTrips( SearchTrips requestObject )
	{
		var Result = new TripList();

		void AddTrips( IEnumerable<Trip> trips )
		{
			Result.AddRange( from T in trips
			                 select T.ToTrip() );
		}

		try
		{
			if( requestObject.ByReference )
			{
				AddTrips( ( from T in Entity.Trips
				            where T.Reference == requestObject.Reference
				            select T ).ToList() );
			}
			else if( requestObject.ByTripId )
			{
				AddTrips( ( from T in Entity.Trips
				            where T.TripId == requestObject.TripId
				            select T ).ToList() );
			}
			else
			{
				AddTrips( requestObject.CompanyCode.IsNotNullOrWhiteSpace()
					          ? FindTripsForCompany( requestObject )
					          : FindTripsForPackageTypeAndOrServiceLevel( requestObject )
				        );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}

	public TripList StorageSearchTrips( DateTime utcLastModifiedFrom, DateTime utcLastModifiedTo, STATUS fromStatus, STATUS toStatus )
	{
		var Result = new TripList();

		if( toStatus >= STATUS.FINALISED )
		{
			if( fromStatus < STATUS.FINALISED )
				fromStatus = STATUS.FINALISED;

			try
			{
				using var Db = new CarrierDb( Context );

				var FromStatus = (int)fromStatus;
				var ToStatus   = (int)toStatus;

				foreach( var StorageIndex in ( from T in Db.Entity.TripStorageIndexes
				                               where ( T.Status1 >= FromStatus ) && ( T.Status1 <= ToStatus )
				                                                                 && ( T.Lastupdated >= utcLastModifiedFrom ) && ( T.Lastupdated <= utcLastModifiedTo )
				                               select T ).ToList() )

					Result.AddRange( TripsByDate.GetTrips( Context, StorageIndex.CallTime, StorageIndex.TripId ) );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
		return Result;
	}

	public TripList StorageSearchTrips( SearchTrips requestObject )
	{
		var RetVal = new TripList();

		var               ToStatus = requestObject.EndStatus;
		TripStorageIndex? Rec      = null;

		if( requestObject.ByTripId || requestObject.ByReference )
		{
			using var Db                 = new CarrierDb( Context );
			var       TripStorageIndices = Db.Entity.TripStorageIndexes;

			if( requestObject.ByTripId )
			{
				Rec = ( from Ndx in TripStorageIndices
				        where Ndx.TripId == requestObject.TripId
				        select Ndx ).FirstOrDefault();
			}
			else if( requestObject.ByReference )
			{
				Rec = ( from Ndx in TripStorageIndices
				        where Ndx.Reference == requestObject.Reference
				        select Ndx ).FirstOrDefault();
			}

			if( Rec is not null )
				RetVal = TripsByDate.GetTrips( Context, Rec.CallTime, Rec.TripId );

			return RetVal;
		}

		switch( ToStatus )
		{
		case STATUS.FINALISED:
		case STATUS.DELETED:
			{
				using var Db = new CarrierDb( Context );

				var Ndx = Db.Entity.TripStorageIndexes;

				var CompanyCode                = requestObject.CompanyCode;
				var SelectedCompanyAddressName = requestObject.SelectedCompanyPUAddressName;

				var From = new DateTimeOffset( requestObject.FromDate.Date, requestObject.FromDate.Offset );
				var To   = new DateTimeOffset( requestObject.ToDate.Date.AddDays( 1 ).AddMilliseconds( -1 ), requestObject.ToDate.Offset );

				var FromStatus = requestObject.StartStatus;

				if( FromStatus < STATUS.FINALISED )
					FromStatus = STATUS.FINALISED;

				IList<TripStorageIndex> Recs;

				if( CompanyCode.IsNotNullOrWhiteSpace() )
				{
					if( SelectedCompanyAddressName.IsNotNullOrWhiteSpace() )
					{
						Recs = ( from T in Ndx
						         where ( T.CallTime >= From ) && ( T.CallTime <= To )
						                                      && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
						                                      && ( T.AccountId == CompanyCode )
						                                      && ( ( T.PickupCompanyName == SelectedCompanyAddressName ) || ( T.DeliveryCompanyName == SelectedCompanyAddressName ) )
						         select T ).ToList();
					}
					else
					{
						Recs = ( from T in Ndx
						         where ( T.CallTime >= From ) && ( T.CallTime <= To )
						                                      && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
						                                      && ( T.AccountId == CompanyCode )
						         select T ).ToList();
					}
				}
				else
				{
					// All companies
					Recs = ( from T in Ndx
					         where ( T.CallTime >= From ) && ( T.CallTime <= To )
					                                      && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
					         select T ).ToList();
				}

				foreach( var StorageIndex in Recs )
					RetVal.AddRange( TripsByDate.GetTrips( Context, StorageIndex.CallTime, StorageIndex.TripId ) );
			}
			break;
		}
		return RetVal;
	}

	public IRequestContext OriginalCarrierContext()
	{
		var Cid = Context.CarrierId.TrimStart( Constants.TEST, StringComparison.OrdinalIgnoreCase );

		return Context.Clone( Cid );
	}

	public TripList SearchTrips( SearchTrips requestObject )
	{
		var Result = new TripList();

		Task.WaitAll( Task.Run( () =>
		                        {
			                        var RetVal = DbSearchTrips( requestObject );

			                        lock( Result )
				                        Result.AddRange( RetVal );
		                        } ),

		              Task.Run( () =>
		                        {
			                        var Tasks = new Task[ 1 ];

			                        Tasks[ 0 ] = Task.Run( () =>
			                                               {
				                                               var RetVal = StorageSearchTrips( requestObject );

				                                               lock( Result )
					                                               Result.AddRange( RetVal );
			                                               } );

			                        Task.WaitAll( Tasks );
		                        } ) );

		Result.Sort( ( trip, trip1 ) =>
		             {
			             var CallTime  = trip.CallTime;
			             var CallTime1 = trip1.CallTime;

			             return CallTime switch
			                    {
				                    null when CallTime1 is null => 0,
				                    null when true              => -1,
				                    _                           => CallTime1 is null ? 1 : CallTime.Value.CompareTo( CallTime1.Value )
			                    };
		             } );

		return Result;
	}

	private IEnumerable<Trip> FindTripsForCompany( SearchTrips requestObject )
	{
		var Trips = new List<Trip>();

		if( requestObject.SelectedCompanyPUAddressName.IsNotNullOrWhiteSpace() && requestObject.SelectedCompanyDelAddressName.IsNotNullOrWhiteSpace() )
			Trips.AddRange( FindTripsForCompanyPUAddressAndDelAddress( requestObject ) );

		else if( requestObject.SelectedCompanyPUAddressName.IsNotNullOrWhiteSpace() )
			Trips.AddRange( FindTripsForCompanyPUAddress( requestObject ) );

		else if( requestObject.SelectedCompanyDelAddressName.IsNotNullOrWhiteSpace() )
			Trips.AddRange( FindTripsForCompanyDelAddress( requestObject ) );

		else if( requestObject.CompanyCode.IsNotNullOrWhiteSpace() )
			Trips.AddRange( FindTripsForCompanyAddress( requestObject ) );

		return Trips;
	}

	// ReSharper disable once InconsistentNaming
	private IEnumerable<Trip> FindTripsForCompanyPUAddressAndDelAddress( SearchTrips requestObject )
	{
		var Trips = new List<Trip>();

		var CompanyCode                   = requestObject.CompanyCode;
		var SelectedCompanyPuAddressName  = requestObject.SelectedCompanyPUAddressName;
		var SelectedCompanyDelAddressName = requestObject.SelectedCompanyDelAddressName;
		var FromStatus                    = requestObject.StartStatus;
		var ToStatus                      = requestObject.EndStatus;
		var PackageType                   = requestObject.PackageType;
		var ServiceLevel                  = requestObject.ServiceLevel;
		var UsePUTime                     = requestObject.ByPUTime;
		var UseDelTime                    = requestObject.ByDelTime;
		var UseVerTime                    = requestObject.ByVerTime;

		var From = new DateTimeOffset( requestObject.FromDate.Date, requestObject.FromDate.Offset );
		var To   = new DateTimeOffset( requestObject.ToDate.Date.AddDays( 1 ).AddMilliseconds( -1 ), requestObject.ToDate.Offset );

		if( requestObject.PackageType.IsNotNullOrWhiteSpace() && requestObject.ServiceLevel.IsNotNullOrWhiteSpace() )
		{
			if( UsePUTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                                                 && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                 && ( T.AccountId == CompanyCode )
				                                                 && ( T.PickupCompanyName == SelectedCompanyPuAddressName )
				                                                 && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                                 && ( T.PackageType == PackageType )
				                                                 && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                    && ( T.AccountId == CompanyCode )
				                                                    && ( T.PickupCompanyName == SelectedCompanyPuAddressName )
				                                                    && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                                    && ( T.PackageType == PackageType )
				                                                    && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseVerTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.VerifiedTime >= From ) && ( T.VerifiedTime <= To )
				                                                   && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                   && ( T.AccountId == CompanyCode )
				                                                   && ( T.PickupCompanyName == SelectedCompanyPuAddressName )
				                                                   && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                                   && ( T.PackageType == PackageType )
				                                                   && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                               && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                               && ( T.AccountId == CompanyCode )
				                                               && ( T.PickupCompanyName == SelectedCompanyPuAddressName )
				                                               && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                               && ( T.PackageType == PackageType )
				                                               && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
		}
		else if( requestObject.PackageType.IsNotNullOrWhiteSpace() )
		{
			if( UsePUTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                                                 && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                 && ( T.AccountId == CompanyCode )
				                                                 && ( T.PickupCompanyName == SelectedCompanyPuAddressName )
				                                                 && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                                 && ( T.PackageType == PackageType )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                    && ( T.AccountId == CompanyCode )
				                                                    && ( T.PickupCompanyName == SelectedCompanyPuAddressName )
				                                                    && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                                    && ( T.PackageType == PackageType )
				                  select T ).ToList() );
			}
			else if( UseVerTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.VerifiedTime >= From ) && ( T.VerifiedTime <= To )
				                                                   && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                   && ( T.AccountId == CompanyCode )
				                                                   && ( T.PickupCompanyName == SelectedCompanyPuAddressName )
				                                                   && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                                   && ( T.PackageType == PackageType )
				                  select T ).ToList() );
			}

			else
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                               && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                               && ( T.AccountId == CompanyCode )
				                                               && ( T.PickupCompanyName == SelectedCompanyPuAddressName )
				                                               && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                               && ( T.PackageType == PackageType )
				                  select T ).ToList() );
			}
		}
		else if( requestObject.ServiceLevel.IsNotNullOrWhiteSpace() )
		{
			if( UsePUTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                                                 && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                 && ( T.AccountId == CompanyCode )
				                                                 && ( T.PickupCompanyName == SelectedCompanyPuAddressName )
				                                                 && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                                 && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                    && ( T.AccountId == CompanyCode )
				                                                    && ( T.PickupCompanyName == SelectedCompanyPuAddressName )
				                                                    && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                                    && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseVerTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.VerifiedTime >= From ) && ( T.VerifiedTime <= To )
				                                                   && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                   && ( T.AccountId == CompanyCode )
				                                                   && ( T.PickupCompanyName == SelectedCompanyPuAddressName )
				                                                   && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                                   && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                               && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                               && ( T.AccountId == CompanyCode )
				                                               && ( T.PickupCompanyName == SelectedCompanyPuAddressName )
				                                               && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                               && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
		}
		else
		{
			if( UsePUTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                                                 && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                 && ( T.AccountId == CompanyCode )
				                                                 && ( T.PickupCompanyName == SelectedCompanyPuAddressName )
				                                                 && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                    && ( T.AccountId == CompanyCode )
				                                                    && ( T.PickupCompanyName == SelectedCompanyPuAddressName )
				                                                    && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                  select T ).ToList() );
			}
			else if( UseVerTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.VerifiedTime >= From ) && ( T.VerifiedTime <= To )
				                                                   && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                   && ( T.AccountId == CompanyCode )
				                                                   && ( T.PickupCompanyName == SelectedCompanyPuAddressName )
				                                                   && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                  select T ).ToList() );
			}
			else
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                               && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                               && ( T.AccountId == CompanyCode )
				                                               && ( T.PickupCompanyName == SelectedCompanyPuAddressName )
				                                               && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                  select T ).ToList() );
			}
		}

		return Trips;
	}

	// ReSharper disable once InconsistentNaming
	private IEnumerable<Trip> FindTripsForCompanyPUAddress( SearchTrips requestObject )
	{
		var Trips = new List<Trip>();

		var CompanyCode = requestObject.CompanyCode;

		// ReSharper disable once InconsistentNaming
		var SelectedCompanyPUAddressName = requestObject.SelectedCompanyPUAddressName;
		var FromStatus                   = requestObject.StartStatus;
		var ToStatus                     = requestObject.EndStatus;
		var PackageType                  = requestObject.PackageType;
		var ServiceLevel                 = requestObject.ServiceLevel;
		var UsePUTime                    = requestObject.ByPUTime;
		var UseDelTime                   = requestObject.ByDelTime;
		var UseVerTime                   = requestObject.ByVerTime;

		var From = new DateTimeOffset( requestObject.FromDate.Date, requestObject.FromDate.Offset );
		var To   = new DateTimeOffset( requestObject.ToDate.Date.AddDays( 1 ).AddMilliseconds( -1 ), requestObject.ToDate.Offset );

		if( requestObject.PackageType.IsNotNullOrWhiteSpace() && requestObject.ServiceLevel.IsNotNullOrWhiteSpace() )
		{
			if( UsePUTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                                                 && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                 && ( T.AccountId == CompanyCode )
				                                                 && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                                                 && ( T.PackageType == PackageType )
				                                                 && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                    && ( T.AccountId == CompanyCode )
				                                                    && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                                                    && ( T.PackageType == PackageType )
				                                                    && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                    && ( T.AccountId == CompanyCode )
				                                                    && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                                                    && ( T.PackageType == PackageType )
				                                                    && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseVerTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.VerifiedTime >= From ) && ( T.VerifiedTime <= To )
				                                                   && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                   && ( T.AccountId == CompanyCode )
				                                                   && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                                                   && ( T.PackageType == PackageType )
				                                                   && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                               && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                               && ( T.AccountId == CompanyCode )
				                                               && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                                               && ( T.PackageType == PackageType )
				                                               && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
		}
		else if( requestObject.PackageType.IsNotNullOrWhiteSpace() )
		{
			if( UsePUTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                                                 && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                 && ( T.AccountId == CompanyCode )
				                                                 && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                                                 && ( T.PackageType == PackageType )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                    && ( T.AccountId == CompanyCode )
				                                                    && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                                                    && ( T.PackageType == PackageType )
				                  select T ).ToList() );
			}
			else if( UseVerTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.VerifiedTime >= From ) && ( T.VerifiedTime <= To )
				                                                   && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                   && ( T.AccountId == CompanyCode )
				                                                   && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                                                   && ( T.PackageType == PackageType )
				                  select T ).ToList() );
			}
			else
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                               && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                               && ( T.AccountId == CompanyCode )
				                                               && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                                               && ( T.PackageType == PackageType )
				                  select T ).ToList() );
			}
		}
		else if( requestObject.ServiceLevel.IsNotNullOrWhiteSpace() )
		{
			if( UsePUTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                                                 && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                 && ( T.AccountId == CompanyCode )
				                                                 && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                                                 && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                    && ( T.AccountId == CompanyCode )
				                                                    && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                                                    && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseVerTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.VerifiedTime >= From ) && ( T.VerifiedTime <= To )
				                                                   && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                   && ( T.AccountId == CompanyCode )
				                                                   && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                                                   && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                               && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                               && ( T.AccountId == CompanyCode )
				                                               && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                                               && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
		}
		else
		{
			if( UsePUTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                                                 && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                 && ( T.AccountId == CompanyCode )
				                                                 && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                    && ( T.AccountId == CompanyCode )
				                                                    && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                  select T ).ToList() );
			}
			else if( UseVerTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.VerifiedTime >= From ) && ( T.VerifiedTime <= To )
				                                                   && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                   && ( T.AccountId == CompanyCode )
				                                                   && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                  select T ).ToList() );
			}
			else
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                               && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                               && ( T.AccountId == CompanyCode )
				                                               && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                  select T ).ToList() );
			}
		}

		return Trips;
	}

	private IEnumerable<Trip> FindTripsForCompanyDelAddress( SearchTrips requestObject )
	{
		var Trips = new List<Trip>();

		var CompanyCode                   = requestObject.CompanyCode;
		var SelectedCompanyDelAddressName = requestObject.SelectedCompanyDelAddressName;
		var FromStatus                    = requestObject.StartStatus;
		var ToStatus                      = requestObject.EndStatus;
		var PackageType                   = requestObject.PackageType;
		var ServiceLevel                  = requestObject.ServiceLevel;
		var UsePUTime                     = requestObject.ByPUTime;
		var UseDelTime                    = requestObject.ByDelTime;
		var UseVerTime                    = requestObject.ByVerTime;

		var From = new DateTimeOffset( requestObject.FromDate.Date, requestObject.FromDate.Offset );
		var To   = new DateTimeOffset( requestObject.ToDate.Date.AddDays( 1 ).AddMilliseconds( -1 ), requestObject.ToDate.Offset );

		if( requestObject.PackageType.IsNotNullOrWhiteSpace() && requestObject.ServiceLevel.IsNotNullOrWhiteSpace() )
		{
			if( UsePUTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                                                 && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                 && ( T.AccountId == CompanyCode )
				                                                 && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                                 && ( T.PackageType == PackageType )
				                                                 && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                    && ( T.AccountId == CompanyCode )
				                                                    && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                                    && ( T.PackageType == PackageType )
				                                                    && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseVerTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.VerifiedTime >= From ) && ( T.VerifiedTime <= To )
				                                                   && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                   && ( T.AccountId == CompanyCode )
				                                                   && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                                   && ( T.PackageType == PackageType )
				                                                   && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                               && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                               && ( T.AccountId == CompanyCode )
				                                               && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                               && ( T.PackageType == PackageType )
				                                               && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
		}
		else if( requestObject.PackageType.IsNotNullOrWhiteSpace() )
		{
			if( UsePUTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                                                 && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                 && ( T.AccountId == CompanyCode )
				                                                 && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                                 && ( T.PackageType == PackageType )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                    && ( T.AccountId == CompanyCode )
				                                                    && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                                    && ( T.PackageType == PackageType )
				                  select T ).ToList() );
			}
			else if( UseVerTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.VerifiedTime >= From ) && ( T.VerifiedTime <= To )
				                                                   && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                   && ( T.AccountId == CompanyCode )
				                                                   && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                                   && ( T.PackageType == PackageType )
				                  select T ).ToList() );
			}
			else
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                               && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                               && ( T.AccountId == CompanyCode )
				                                               && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                               && ( T.PackageType == PackageType )
				                  select T ).ToList() );
			}
		}
		else if( requestObject.ServiceLevel.IsNotNullOrWhiteSpace() )
		{
			if( UsePUTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                                                 && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                 && ( T.AccountId == CompanyCode )
				                                                 && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                                 && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                    && ( T.AccountId == CompanyCode )
				                                                    && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                                    && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseVerTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.VerifiedTime >= From ) && ( T.VerifiedTime <= To )
				                                                   && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                   && ( T.AccountId == CompanyCode )
				                                                   && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                                   && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                               && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                               && ( T.AccountId == CompanyCode )
				                                               && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                               && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
		}
		else
		{
			if( UsePUTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                                                 && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                 && ( T.AccountId == CompanyCode )
				                                                 && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                    && ( T.AccountId == CompanyCode )
				                                                    && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                  select T ).ToList() );
			}
			else if( UseVerTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.VerifiedTime >= From ) && ( T.VerifiedTime <= To )
				                                                   && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                   && ( T.AccountId == CompanyCode )
				                                                   && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                  select T ).ToList() );
			}
			else
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                               && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                               && ( T.AccountId == CompanyCode )
				                                               && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                  select T ).ToList() );
			}
		}

		return Trips;
	}

	/// <summary>
	///     Don't care about Pickup or Delivery addresses.
	/// </summary>
	/// <param
	///     name="requestObject">
	/// </param>
	/// <returns></returns>
	private IEnumerable<Trip> FindTripsForCompanyAddress( SearchTrips requestObject )
	{
		var Trips = new List<Trip>();

		var CompanyCode = requestObject.CompanyCode;

		//var SelectedCompanyPUAddressName = requestObject.SelectedCompanyPUAddressName;
		//var SelectedCompanyDelAddressName = requestObject.SelectedCompanyDelAddressName;
		var FromStatus   = requestObject.StartStatus;
		var ToStatus     = requestObject.EndStatus;
		var PackageType  = requestObject.PackageType;
		var ServiceLevel = requestObject.ServiceLevel;
		var UsePUTime    = requestObject.ByPUTime;
		var UseDelTime   = requestObject.ByDelTime;
		var UseVerTime   = requestObject.ByVerTime;

		var From = new DateTimeOffset( requestObject.FromDate.Date, requestObject.FromDate.Offset );
		var To   = new DateTimeOffset( requestObject.ToDate.Date.AddDays( 1 ).AddMilliseconds( -1 ), requestObject.ToDate.Offset );

		if( requestObject.PackageType.IsNotNullOrWhiteSpace() && requestObject.ServiceLevel.IsNotNullOrWhiteSpace() )
		{
			if( UsePUTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                                                 && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                 && ( T.AccountId == CompanyCode )
				                                                 && ( T.PackageType == PackageType )
				                                                 && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                    && ( T.AccountId == CompanyCode )
				                                                    && ( T.PackageType == PackageType )
				                                                    && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseVerTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.VerifiedTime >= From ) && ( T.VerifiedTime <= To )
				                                                   && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                   && ( T.AccountId == CompanyCode )
				                                                   && ( T.PackageType == PackageType )
				                                                   && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                               && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                               && ( T.AccountId == CompanyCode )
				                                               && ( T.PackageType == PackageType )
				                                               && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
		}
		else if( requestObject.PackageType.IsNotNullOrWhiteSpace() )
		{
			if( UsePUTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                                                 && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                 && ( T.AccountId == CompanyCode )
				                                                 && ( T.PackageType == PackageType )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                    && ( T.AccountId == CompanyCode )
				                                                    && ( T.PackageType == PackageType )
				                  select T ).ToList() );
			}
			else if( UseVerTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.VerifiedTime >= From ) && ( T.VerifiedTime <= To )
				                                                   && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                   && ( T.AccountId == CompanyCode )
				                                                   && ( T.PackageType == PackageType )
				                  select T ).ToList() );
			}
			else
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                               && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                               && ( T.AccountId == CompanyCode )
				                                               && ( T.PackageType == PackageType )
				                  select T ).ToList() );
			}
		}
		else if( requestObject.ServiceLevel.IsNotNullOrWhiteSpace() )
		{
			if( UsePUTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                                                 && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                 && ( T.AccountId == CompanyCode )
				                                                 && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                    && ( T.AccountId == CompanyCode )
				                                                    && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseVerTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.VerifiedTime >= From ) && ( T.VerifiedTime <= To )
				                                                   && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                   && ( T.AccountId == CompanyCode )
				                                                   && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                               && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                               && ( T.AccountId == CompanyCode )
				                                               && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
		}
		else
		{
			if( UsePUTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                                                 && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                 && ( T.AccountId == CompanyCode )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                    && ( T.AccountId == CompanyCode )
				                  select T ).ToList() );
			}
			else if( UseVerTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.VerifiedTime >= From ) && ( T.VerifiedTime <= To )
				                                                   && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                   && ( T.AccountId == CompanyCode )
				                  select T ).ToList() );
			}
			else
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                               && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                               && ( T.AccountId == CompanyCode )
				                  select T ).ToList() );
			}
		}

		return Trips;
	}

	private IEnumerable<Trip> FindTripsForPackageTypeAndOrServiceLevel( SearchTrips requestObject )
	{
		var Trips = new List<Trip>();

		var FromStatus   = requestObject.StartStatus;
		var ToStatus     = requestObject.EndStatus;
		var PackageType  = requestObject.PackageType;
		var ServiceLevel = requestObject.ServiceLevel;
		var UsePUTime    = requestObject.ByPUTime;
		var UseDelTime   = requestObject.ByDelTime;
		var UseVerTime   = requestObject.ByVerTime;

		var From = new DateTimeOffset( requestObject.FromDate.Date, requestObject.FromDate.Offset );
		var To   = new DateTimeOffset( requestObject.ToDate.Date.AddDays( 1 ).AddMilliseconds( -1 ), requestObject.ToDate.Offset );

		if( requestObject.PackageType.IsNotNullOrWhiteSpace() && requestObject.ServiceLevel.IsNotNullOrWhiteSpace() )
		{
			if( UsePUTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                                                 && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                 && ( T.PackageType == PackageType )
				                                                 && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                    && ( T.PackageType == PackageType )
				                                                    && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseVerTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.VerifiedTime >= From ) && ( T.VerifiedTime <= To )
				                                                   && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                   && ( T.PackageType == PackageType )
				                                                   && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                               && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                               && ( T.PackageType == PackageType )
				                                               && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
		}
		else if( requestObject.PackageType.IsNotNullOrWhiteSpace() )
		{
			if( UsePUTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                                                 && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                 && ( T.PackageType == PackageType )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                    && ( T.PackageType == PackageType )
				                  select T ).ToList() );
			}
			else if( UseVerTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.VerifiedTime >= From ) && ( T.VerifiedTime <= To )
				                                                   && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                   && ( T.PackageType == PackageType )
				                  select T ).ToList() );
			}
			else
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                               && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                               && ( T.PackageType == PackageType )
				                  select T ).ToList() );
			}
		}
		else if( requestObject.ServiceLevel.IsNotNullOrWhiteSpace() )
		{
			if( UsePUTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                                                 && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                 && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                    && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else if( UseVerTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.VerifiedTime >= From ) && ( T.VerifiedTime <= To )
				                                                   && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                                   && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
			else
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                               && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                               && ( T.ServiceLevel == ServiceLevel )
				                  select T ).ToList() );
			}
		}
		else
		{
			if( UsePUTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.PickupTime >= From ) && ( T.PickupTime <= To )
				                                                 && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                  select T ).ToList() );
			}
			else if( UseDelTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.DeliveredTime >= From ) && ( T.DeliveredTime <= To )
				                                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                  select T ).ToList() );
			}
			else if( UseVerTime )
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.VerifiedTime >= From ) && ( T.VerifiedTime <= To )
				                                                   && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                  select T ).ToList() );
			}
			else
			{
				Trips.AddRange( ( from T in Entity.Trips
				                  where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                               && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                  select T ).ToList() );
			}
		}

		return Trips;
	}
}