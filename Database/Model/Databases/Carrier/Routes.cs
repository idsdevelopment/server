﻿#nullable enable

using ResellerCustomer = Database.Model.Databases.MasterTemplate.ResellerCustomer;
using Route = Database.Model.Databases.MasterTemplate.Route;

// ReSharper disable StringCompareToIsCultureSpecific

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public class RouteLogger
	{
		public const string LOG_STORAGE = "Route";

		public string Name        { get; set; }
		public string CompanyCode { get; set; }

		public bool Monday    { get; set; }
		public bool Tuesday   { get; set; }
		public bool Wednesday { get; set; }
		public bool Thursday  { get; set; }
		public bool Friday    { get; set; }
		public bool Saturday  { get; set; }
		public bool Sunday    { get; set; }

		public TimeSpan StartTime { get; set; }
		public TimeSpan EndTime   { get; set; }

		public short   Count      { get; set; }
		public decimal Commission { get; set; }

		public string BillingCompany { get; set; }

		public bool ShowOnMobileApp { get; set; }

		public RouteLogger( Route route )
		{
			Name            = route.Name;
			Count           = route.StaticCount;
			BillingCompany  = route.BillingCompany;
			CompanyCode     = route.CustomerCode;
			Commission      = route.Commission;
			EndTime         = route.EndTime;
			Friday          = route.Friday;
			Monday          = route.Monday;
			Saturday        = route.Saturday;
			StartTime       = route.StartTime;
			Sunday          = route.Sunday;
			Thursday        = route.Thursday;
			Tuesday         = route.Tuesday;
			Wednesday       = route.Wednesday;
			ShowOnMobileApp = route.ShowOnMobileApp;
		}
	}

	public RouteLookupSummaryList RouteLookup( RouteLookup lookup )
	{
		var RetVal = new RouteLookupSummaryList();

		try
		{
			var Cust  = lookup.CustomerCode;
			var Key   = lookup.Key;
			var Count = lookup.PreFetchCount;

			IEnumerable<Route> Result;

			switch( lookup.Fetch )
			{
			case PreFetch.PREFETCH.FIRST:
				Result = ( from R in Entity.Routes
				           where R.CustomerCode == Cust
				           orderby R.Name
				           select R ).Take( Count );

				break;

			case PreFetch.PREFETCH.LAST:
				Result = ( from R in Entity.Routes
				           where R.CustomerCode == Cust
				           orderby R.Name descending
				           select R ).Take( Count );

				break;

			case PreFetch.PREFETCH.FIND_MATCH:
				Result = ( from R in Entity.Routes
				           where ( R.CustomerCode == Cust ) && ( R.Name.CompareTo( Key ) == 0 )
				           orderby R.Name descending
				           select R ).Take( Count );

				break;

			case PreFetch.PREFETCH.FIND_RANGE:
				if( Count >= 0 ) // Next
				{
					Result = ( from R in Entity.Routes
					           where ( R.CustomerCode == Cust ) && ( R.Name.CompareTo( Key ) >= 0 )
					           orderby R.Name
					           select R ).Take( Count );
				}
				else // Prior
				{
					Result = ( from R in Entity.Routes
					           where ( R.CustomerCode == Cust ) && ( R.Name.CompareTo( Key ) <= 0 )
					           orderby R.Name
					           select R ).Take( -Count );
				}

				break;

			default:
				Context.SystemLogException( nameof( RouteLookup ), new Exception( $"Unknown prefetch type: {lookup.Fetch}" ) );

				return RetVal;
			}

			RetVal.AddRange( from R in Result
			                 select new RouteSchedule
			                        {
				                        RouteName          = R.Name,
				                        BillingCompanyCode = R.BillingCompany ?? "",
				                        Commission         = R.Commission,
				                        EndTime            = R.EndTime,
				                        Friday             = R.Friday,
				                        IsStatic           = R.IsStatic,
				                        Monday             = R.Monday,
				                        Saturday           = R.Saturday,
				                        StartTime          = R.StartTime,
				                        StaticCount        = R.StaticCount,
				                        Sunday             = R.Sunday,
				                        Thursday           = R.Thursday,
				                        Tuesday            = R.Tuesday,
				                        Wednesday          = R.Wednesday,
				                        ShowOnMobileApp    = R.ShowOnMobileApp
			                        } );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( nameof( RouteLookup ), Exception );
		}

		return RetVal;
	}

	public void DeleteAllRoutes()
	{
		try
		{
			Entity.Database.ExecuteSqlCommand( "DELETE FROM dbo.Routes" );
		}
		catch( Exception E )
		{
			Context.SystemLogException( nameof( DeleteAllRoutes ), E );
		}
	}

	public ( long RoutetId, bool Ok ) AddUpdateRoute( Protocol.Data.Route route )
	{
		var RouteName = route.RouteName.NullTrim();

		if( RouteName != "" )
		{
			try
			{
				var E      = Entity;
				var Routes = E.Routes;

				var Rec = ( from R in Routes
				            where ( R.CustomerCode == route.CustomerCode ) && ( R.Name == RouteName )
				            select R ).FirstOrDefault();

				// Make sure timespans are safe
				route.StartTime = route.StartTime.DbTimeSpan();
				route.EndTime   = route.EndTime.DbTimeSpan();

				if( Rec is null )
				{
					Rec = new Route
					      {
						      Name            = RouteName,
						      StaticCount     = route.StaticCount,
						      IsStatic        = route.IsStatic,
						      BillingCompany  = route.BillingCompanyCode,
						      CustomerCode    = route.CustomerCode,
						      Commission      = route.Commission,
						      EndTime         = route.EndTime,
						      Friday          = route.Friday,
						      Monday          = route.Monday,
						      Saturday        = route.Saturday,
						      StartTime       = route.StartTime,
						      Sunday          = route.Sunday,
						      Thursday        = route.Thursday,
						      Tuesday         = route.Tuesday,
						      Wednesday       = route.Wednesday,
						      ShowOnMobileApp = route.ShowOnMobileApp
					      };

					Routes.Add( Rec );
					Context.LogDifferences( RouteLogger.LOG_STORAGE, route.ProgramName, nameof( AddUpdateRoute ), new RouteLogger( Rec ) );
				}
				else
				{
					var OldRec = new RouteLogger( Rec );
					Rec.BillingCompany  = route.BillingCompanyCode;
					Rec.Commission      = route.Commission;
					Rec.EndTime         = route.EndTime;
					Rec.Friday          = route.Friday;
					Rec.Monday          = route.Monday;
					Rec.Saturday        = route.Saturday;
					Rec.StartTime       = route.StartTime;
					Rec.Sunday          = route.Sunday;
					Rec.Thursday        = route.Thursday;
					Rec.Tuesday         = route.Tuesday;
					Rec.Wednesday       = route.Wednesday;
					Rec.ShowOnMobileApp = route.ShowOnMobileApp;

					Context.LogDifferences( RouteLogger.LOG_STORAGE, route.ProgramName, nameof( AddUpdateRoute ), OldRec, new RouteLogger( Rec ) );
				}

				E.SaveChanges();

				return ( Rec.RouteId, Ok: true );
			}
			catch( Exception E )
			{
				Context.SystemLogException( nameof( AddUpdateRoute ), E );
			}
		}

		return ( 0, Ok: false );
	}

	public (long RoutetId, bool Ok) AddUpdateRoute( string programName, string routeName ) =>
		AddUpdateRoute( new Protocol.Data.Route( programName )
		                {
			                RouteName    = routeName,
			                CustomerCode = ""
		                } );


	public (long RoutetId, bool Ok) AddUpdateRoute( string programName, string companyId, string routeName ) =>
		AddUpdateRoute( new Protocol.Data.Route( programName )
		                {
			                RouteName    = routeName,
			                CustomerCode = companyId
		                } );


	public void AddUpdateRoute( RouteAndCompanyAddresses route )
	{
		var (RouteId, Ok) = AddUpdateRoute( (Protocol.Data.Route)route );

		if( Ok )
		{
			try
			{
				foreach( var Company in route.CompaniesInRoute )
				{
					try
					{
						var RetVal = GetCompanyByName( Company.CompanyName );

						if( !RetVal.Ok )
						{
							var RetVal1 = AddCompany( Company, route.ProgramName );
							RetVal.Ok = RetVal1.Ok;
						}

						if( RetVal.Ok )
						{
							var E   = Entity;
							var Cid = RetVal.Company.CompanyId;

							var RaRec = ( from Ra in E.RouteAddresses
							              where ( Ra.CompanyId == Cid ) && ( Ra.RouteId == RouteId )
							              select Ra ).FirstOrDefault();

							if( RaRec == null )
							{
								var RAddr = new RouteAddress
								            {
									            CompanyId = Cid,
									            RouteId   = RouteId
								            };
								E.RouteAddresses.Add( RAddr );
								E.SaveChanges();
							}
						}
					}
					catch( Exception Exception )
					{
						Context.SystemLogException( $"{nameof( AddUpdateRoute )} - Task", Exception );
					}
				}
			}
			catch( Exception Ex )
			{
				Context.SystemLogException( nameof( AddUpdateRoute ), Ex );
			}
		}
	}

	public void UpdateRouteSchedule( RouteScheduleUpdate s )
	{
		AddUpdateRoute( new Protocol.Data.Route( s.ProgramName )
		                {
			                CustomerCode       = s.CustomerCode,
			                EndTime            = s.EndTime,
			                StartTime          = s.StartTime,
			                Monday             = s.Monday,
			                Tuesday            = s.Tuesday,
			                Wednesday          = s.Wednesday,
			                Thursday           = s.Thursday,
			                Friday             = s.Friday,
			                Saturday           = s.Saturday,
			                Sunday             = s.Sunday,
			                StaticCount        = s.StaticCount,
			                Commission         = s.Commission,
			                IsStatic           = s.IsStatic,
			                BillingCompanyCode = s.BillingCompanyCode,
			                RouteName          = s.RouteName,
			                ShowOnMobileApp    = s.ShowOnMobileApp
		                } );
	}

	public ( Route? Route, bool Ok) GetRoute( string customerCode, string routeName )
	{
		try
		{
			var Route = ( from R in Entity.Routes
			              where ( R.CustomerCode == customerCode ) && ( R.Name == routeName )
			              select R ).FirstOrDefault();

			return ( Route, Ok: Route is not null );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( nameof( GetRoute ), Exception );
		}

		return ( null, false );
	}

	public bool HasRoute( string customerCode, string routeName ) => GetRoute( customerCode, routeName ).Ok;

	public (long RoutetId, bool Ok) AddRoute( string programName, string customerCode, string routeName ) =>
		AddUpdateRoute( new Protocol.Data.Route( programName )
		                {
			                CustomerCode = customerCode,
			                RouteName    = routeName
		                } );

	public void DeleteRoute( string programName, string customerCode, string routeName )
	{
		try
		{
			var E   = Entity;
			var Rts = E.Routes;

			var Rec = ( from R in Rts
			            where ( R.CustomerCode == customerCode ) && ( R.Name == routeName )
			            select R ).FirstOrDefault();

			if( Rec is not null )
			{
				Rts.Remove( Rec );
				E.SaveChanges();
				Context.LogDeleted( RouteLogger.LOG_STORAGE, programName, "Delete route", new {customerCode, routeName} );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( nameof( DeleteRoute ), Exception );
		}
	}

	public void RenameRoute( string programName, string customerCode, string oldRouteName, string newRouteName )
	{
		try
		{
			var E = Entity;

			var Rec = ( from R in E.Routes
			            where ( R.CustomerCode == customerCode ) && ( R.Name == oldRouteName )
			            select R ).FirstOrDefault();

			if( Rec is not null )
			{
				var OldRec = new RouteLogger( Rec );
				Rec.Name = newRouteName;
				E.SaveChanges();
				Context.LogDifferences( RouteLogger.LOG_STORAGE, programName, "", OldRec, new RouteLogger( Rec ) );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( nameof( RenameRoute ), Exception );
		}
	}

	public void RemoveCompaniesFromRoute( RouteCompanyList clist )
	{
		var E = Entity;

		var (Route, Ok) = GetRoute( clist.CustomerCode, clist.RouteName );

		if( Ok )
		{
			foreach( var Options in clist.CompanyOptions )
			{
				var (Company, COk) = GetCompanyByName( Options.CompanyName );

				if( COk )
				{
					E.RouteAddresses.RemoveRange( from Ra in E.RouteAddresses
					                              where ( Ra.CompanyId == Company.CompanyId ) && ( Ra.RouteId == Route!.RouteId )
					                              select Ra );

					Context.LogDeleted( RouteLogger.LOG_STORAGE, clist.ProgramName, nameof( RemoveCompaniesFromRoute ), new
					                                                                                                    {
						                                                                                                    clist.CustomerCode,
						                                                                                                    clist.RouteName,
						                                                                                                    CompanyName = Options
					                                                                                                    } );
				}

				E.SaveChanges();
			}
		}
	}

	public void AddCompaniesToRoute( RouteCompanyList clist )
	{
		var E = Entity;

		var (Route, Ok) = GetRoute( clist.CustomerCode, clist.RouteName );

		if( Ok )
		{
			foreach( var Options in clist.CompanyOptions )
			{
				var (Company, COk) = GetCompanyByName( Options.CompanyName );

				if( COk )
				{
					E.RouteAddresses.Add( new RouteAddress
					                      {
						                      CompanyId = Company.CompanyId,
						                      RouteId   = Route!.RouteId,
						                      Option1   = Options.Option1,
						                      Option2   = Options.Option2,
						                      Option3   = Options.Option3,
						                      Option4   = Options.Option4,
						                      Option5   = Options.Option5,
						                      Option6   = Options.Option6,
						                      Option7   = Options.Option7,
						                      Option8   = Options.Option8
					                      } );

					Context.LogNew( RouteLogger.LOG_STORAGE, clist.ProgramName, "Added company to route", new
					                                                                                      {
						                                                                                      clist.CustomerCode,
						                                                                                      clist.RouteName,
						                                                                                      CompanyName = Options
					                                                                                      } );
				}

				E.SaveChanges();
			}
		}
	}

	public RouteCompanySummaryList GetCompanySummaryInRoute( string customerCode, string routeName )
	{
		var Result = new RouteCompanySummaryList();

		var (Route, Ok) = GetRoute( customerCode, routeName );

		if( Ok )
		{
			var E = Entity;

			Result.AddRange( from Ra in E.RouteAddresses
			                 where Ra.RouteId == Route!.RouteId
			                 let Company = ( from C in E.Companies
			                                 where C.CompanyId == Ra.CompanyId
			                                 select C ).FirstOrDefault()
			                 where Company != null
			                 let Address = ( from A in E.Addresses
			                                 where A.Id == Company.PrimaryAddressId
			                                 select A ).FirstOrDefault()
			                 where Address != null
			                 orderby Company.DisplayCompanyName
			                 select new RouteCompanySummary
			                        {
				                        CompanyName  = Company.DisplayCompanyName,
				                        CustomerCode = customerCode,
				                        Suite        = Address.Suite,
				                        AddressLine1 = Address.AddressLine1,
				                        AddressLine2 = Address.AddressLine2,
				                        City         = Address.City,
				                        Region       = Address.Region,
				                        PostalCode   = Address.PostalCode,
				                        CountryCode  = Address.CountryCode,
				                        Options = new RouteOptions
				                                  {
					                                  Option1 = Ra.Option1,
					                                  Option2 = Ra.Option2,
					                                  Option3 = Ra.Option3,
					                                  Option4 = Ra.Option4,
					                                  Option5 = Ra.Option5,
					                                  Option6 = Ra.Option6,
					                                  Option7 = Ra.Option7,
					                                  Option8 = Ra.Option8
				                                  }
			                        }
			               );
		}

		return Result;
	}

	public void UpdateOptionsInRoute( RouteUpdate upd )
	{
		var (Route, Ok) = GetRoute( upd.CustomerCode, upd.RouteName );

		if( Ok )
		{
			var (Company, COk) = GetCompanyByName( upd.CompanyName );

			if( COk )
			{
				var E = Entity;

				var Ra = ( from R in E.RouteAddresses
				           where ( R.RouteId == Route!.RouteId ) && ( R.CompanyId == Company.CompanyId )
				           select R ).FirstOrDefault();

				if( Ra is not null )
				{
					Ra.Option1 = upd.Options.Option1;
					Ra.Option2 = upd.Options.Option2;
					Ra.Option3 = upd.Options.Option3;
					Ra.Option4 = upd.Options.Option4;
					Ra.Option5 = upd.Options.Option5;
					Ra.Option6 = upd.Options.Option6;
					Ra.Option7 = upd.Options.Option7;
					Ra.Option8 = upd.Options.Option8;

					E.SaveChanges();
				}
			}
		}
	}

	public void AddUpdateRenameRouteBasic( AddUpdateRenameRouteBasic requestObject )
	{
		try
		{
			var CustomerCode = requestObject.CustomerCode;
			var OldName      = requestObject.OldName;
			var NewName      = requestObject.NewName;
			var E            = Entity;

			Route? Rec = null;

			if( OldName.IsNotNullOrWhiteSpace() )
			{
				Rec = ( from R in E.Routes
				        where ( R.CustomerCode == CustomerCode ) && ( R.Name == OldName )
				        select R ).FirstOrDefault();
			}

			// Add
			if( Rec is null )
			{
				Rec = new Route
				      {
					      Name            = NewName,
					      CustomerCode    = CustomerCode,
					      BillingCompany  = "",
					      ShowOnMobileApp = requestObject.Enabled,
					      EndTime         = DateTimeExtensions.END_OF_DAY,
					      StartTime       = DateTimeExtensions.START_OF_DAY
				      };

				E.Routes.Add( Rec );
			}
			else // Modify
			{
				// Rename
				if( NewName.IsNotNullOrWhiteSpace() && ( OldName != NewName ) )
					Rec.Name = NewName;

				Rec.ShowOnMobileApp = requestObject.Enabled;
			}

			E.SaveChanges();
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}


	public CustomersRoutesBasic GetCustomersRoutesBasic( CustomerCodeList clist )
	{
		var Result = new CustomersRoutesBasic();

		try
		{
			var                          E = Entity;
			IQueryable<ResellerCustomer> Recs;

			// All customers
			if( clist.IsNull() || ( clist.Count == 0 ) )
			{
				Recs = from C in E.ResellerCustomers
				       select C;
			}
			else
			{
				Recs = from C in E.ResellerCustomers
				       where clist.Contains( C.CustomerCode )
				       select C;
			}

			foreach( var Rec in Recs )
			{
				var Cb = new CustomerRoutesBasic
				         {
					         CustomerCode            = Rec.CustomerCode,
					         EnableRoutesForCustomer = Rec.BoolOption1
				         };

				var Routes = from R in E.Routes
				             where R.CustomerCode == Rec.CustomerCode
				             orderby R.Name
				             select R;

				foreach( var R in Routes )
				{
					Cb.Routes.Add( new CustomerRouteBasic
					               {
						               Enabled   = R.ShowOnMobileApp,
						               RouteName = R.Name
					               } );
				}

				Result.Add( Cb );
			}
		}
		catch( Exception E )
		{
			Context.SystemLogException( E );
		}

		return Result;
	}

	public CustomerRoutesScheduleList GetRoutesForCustomers( CustomerCodeList clist )
	{
		var Result = new CustomerRoutesScheduleList();

		try
		{
			var E = Entity;

			foreach( var CustCode in clist )
			{
				var EnableRoute = ( from C in E.ResellerCustomers
				                    where C.CustomerCode == CustCode
				                    select C.BoolOption1 ).FirstOrDefault();

				var Rs = from R in E.Routes
				         where R.CustomerCode == CustCode
				         let Options = from Ra in E.RouteAddresses
				                       where Ra.RouteId == R.RouteId
				                       select new
				                              {
					                              Ra.Option1,
					                              Ra.Option2,
					                              Ra.Option3,
					                              Ra.Option4,
					                              Ra.Option5,
					                              Ra.Option6,
					                              Ra.Option7,
					                              Ra.Option8,
					                              Address = ( from C in E.Companies
					                                          where C.CompanyId == Ra.CompanyId
					                                          let Addr = ( from A in E.Addresses
					                                                       where A.Id == C.PrimaryAddressId
					                                                       select A ).FirstOrDefault()
					                                          where Addr != null
					                                          select new
					                                                 {
						                                                 C.CompanyName,
						                                                 Addr.Suite,
						                                                 Addr.AddressLine1
					                                                 } ).FirstOrDefault()
				                              }
				         select new
				                {
					                Schedule = R,
					                Options
				                };

				var Rs1 = from R in Rs
				          let S = R.Schedule
				          select new RouteScheduleAndRoutes
				                 {
					                 RouteName          = S.Name,
					                 StartTime          = S.StartTime,
					                 EndTime            = S.EndTime,
					                 BillingCompanyCode = S.BillingCompany,
					                 Monday             = S.Monday,
					                 Tuesday            = S.Tuesday,
					                 Wednesday          = S.Wednesday,
					                 Thursday           = S.Thursday,
					                 Friday             = S.Friday,
					                 Saturday           = S.Saturday,
					                 Sunday             = S.Sunday,
					                 StaticCount        = S.StaticCount,
					                 Commission         = S.Commission,
					                 IsStatic           = S.IsStatic,
					                 ShowOnMobileApp    = S.ShowOnMobileApp,
					                 RouteSummary = ( from Opt in R.Options
					                                  let A = Opt.Address
					                                  select new RouteSummary
					                                         {
						                                         CompanyName  = A.CompanyName,
						                                         Suite        = A.Suite,
						                                         AddressLine1 = A.AddressLine1,
						                                         Option1      = Opt.Option1,
						                                         Option2      = Opt.Option2,
						                                         Option3      = Opt.Option3,
						                                         Option4      = Opt.Option4,
						                                         Option5      = Opt.Option5,
						                                         Option6      = Opt.Option6,
						                                         Option7      = Opt.Option7,
						                                         Option8      = Opt.Option8
					                                         } ).ToList()
				                 };

				if( Rs1.Any() )
				{
					var Crs = new CustomerRoutesSchedule
					          {
						          CustomerCode          = CustCode,
						          EnableRoutesForDevice = EnableRoute,
						          ScheduleAndRoutes     = Rs1.ToList()
					          };

					Result.Add( Crs );
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( nameof( GetRoutesForCustomers ), Exception );
		}

		return Result;
	}
}