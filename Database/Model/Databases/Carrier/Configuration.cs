﻿#nullable enable

using Configuration = Database.Model.Databases.MasterTemplate.Configuration;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public DateTime LastMessagingCleanup
	{
		get => ( from C in Entity.Configurations
			     select C.LastMessagingCleanup ).FirstOrDefault();

		set
		{
			var E = Entity;

			var Rec = ( from C in E.Configurations
					    select C ).FirstOrDefault();

			if( Rec is not null )
			{
				Rec.LastMessagingCleanup = value;
				E.SaveChanges();
			}
		}
	}

	public DateTime LastAddressCleanup
	{
		get => ( from C in Entity.Configurations
			     select C.LastDistanceCleanup ).FirstOrDefault();

		set
		{
			var E = Entity;

			var Rec = ( from C in E.Configurations
					    select C ).FirstOrDefault();

			if( Rec is not null )
			{
				Rec.LastDistanceCleanup = value;
				E.SaveChanges();
			}
		}
	}

	public Configuration GetConfiguration() => ( from C in Entity.Configurations
											     select C ).First();

	public void UpdateConfiguration( Configuration c )
	{
		while( true )
		{
			try
			{
				var Configurations = Entity.Configurations;

				var Cfg = ( from C in Configurations
						    select C ).FirstOrDefault();

				if( Cfg is not null )
				{
					Cfg.Id                   = c.Id;
					Cfg.CompanyAddressId     = c.CompanyAddressId;
					Cfg.StorageAccount       = c.StorageAccount;
					Cfg.StorageAccountKey    = c.StorageAccountKey;
					Cfg.StorageBlobEndpoint  = c.StorageBlobEndpoint;
					Cfg.StorageFileEndpoint  = c.StorageFileEndpoint;
					Cfg.StorageTableEndpoint = c.StorageTableEndpoint;
					Cfg.LastMessagingCleanup = c.LastMessagingCleanup;
				}
				else
					Configurations.Add( c );

				Entity.SaveChanges();
			}
			catch( DbUpdateConcurrencyException )
			{
				continue;
			}
			catch( Exception E )
			{
				Context.SystemLogException( E );
			}
			break;
		}
	}

#region Next Invoice Number
	private static readonly object NextInvoiceLockObject = new();

	public long GetAndUpdateNextInvoiceNumber()
	{
		lock( NextInvoiceLockObject )
		{
			var E = Entity;

			while( true )
			{
				try
				{
					var Rec = ( from C in E.Configurations
							    select C ).FirstOrDefault();

					if( Rec is not null )
					{
						var Result = Rec.NextInvoiceNumber++;
						E.SaveChanges();
						return Result;
					}
				}
				catch( DbUpdateConcurrencyException )
				{
					Thread.Sleep( 100 );
				}
				catch( Exception Exception )
				{
					Context.SystemLogException( Exception );
					break;
				}
			}
		}
		return -1;
	}

	public long NextInvoiceNumber
	{
		get
		{
			try
			{
				lock( NextInvoiceLockObject )
				{
					var E = Entity;

					var Rec = ( from C in E.Configurations
							    select C ).FirstOrDefault();

					if( Rec is not null )
						return Rec.NextInvoiceNumber;
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
			return -1;
		}

		set
		{
			lock( NextInvoiceLockObject )
			{
				while( true )
				{
					try
					{
						var E = Entity;

						var Rec = ( from C in E.Configurations
								    select C ).FirstOrDefault();

						if( Rec is not null )
						{
							Rec.NextInvoiceNumber = value;
							E.SaveChanges();
						}
						break;
					}
					catch( DbUpdateConcurrencyException )
					{
						Thread.Sleep( 100 );
					}
					catch( Exception Exception )
					{
						Context.SystemLogException( Exception );
						break;
					}
				}
			}
		}
	}
#endregion
}