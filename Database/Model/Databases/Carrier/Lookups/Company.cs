﻿#nullable enable

using Company = Protocol.Data.Company;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public int GetCompanyRecordCount() => ( from C in Entity.Companies
										    select C ).Count();

	public List<Company> GetCompanies( int offset, int count )
	{
		var Result = new List<Company>();

		try
		{
			var E = Entity;

			var Recs = ( from C in E.Companies
					     join A in E.Addresses on C.PrimaryAddressId equals A.Id
						 orderby C.CompanyName
					     select new {Company = C, Address = A} ).Skip( offset ).Take( count ).ToList();

			foreach( var Rec in Recs )
				Result.Add( ConvertCompany( Context, Rec.Company, Rec.Address ) );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}
}