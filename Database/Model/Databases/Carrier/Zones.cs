﻿#nullable enable

using Zone = Protocol.Data.Zone;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public const string ZONE_LOG_STORAGE = "Zone";

	public Zones GetZones()
	{
		var Result = new Zones();

		try
		{
			var Zones = ( from Z in Entity.Zones
						  orderby Z.ZoneName
						  select Z ).ToList();

			foreach( var Z in Zones )
				Result.Add( new Zone {Abbreviation = Z.Abbreviation, Name = Z.ZoneName, SortIndex = Z.SortIndex, ZoneId = Z.Id} );
		}
		catch( Exception E )
		{
			Context.SystemLogException( nameof( GetZones ), E );
		}

		return Result;
	}

	public void AddZones( List<Zone> zones, string programName )
	{
		try
		{
			var Zones = Entity.Zones;

			foreach( var Zone in zones )
			{
				var Zne = ( from Z in Zones
						    where Z.ZoneName == Zone.Name
						    select Z ).FirstOrDefault();

				if( Zne is null )
				{
					var Z = new MasterTemplate.Zone
							{
								ZoneName     = Zone.Name,
								Abbreviation = Zone.Abbreviation,
								SortIndex    = Zone.SortIndex
							};

					Zones.Add( Z );
					ZoneCacheById[ Zone.Id ]     = Z;
					ZoneCacheByName[ Zone.Name ] = Z;

					Context.LogDifferences( ZONE_LOG_STORAGE, programName, nameof( AddZones ), Z );
				}
			}

			Entity.SaveChanges();
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( nameof( AddZones ), Exception );
		}
	}

	public void DeleteZones( List<string> zoneNames, string programName )
	{
		try
		{
			using var Db    = new CarrierDb( Context );
			var       Zones = Entity.Zones;

			var DbZones = ( from Z in Zones
						    where zoneNames.Contains( Z.ZoneName )
						    select Z ).ToList();

			if( DbZones.Count > 0 )
			{
				// There are deletions
				foreach( var Z in DbZones )
				{
					Context.LogDeleted( ZONE_LOG_STORAGE, programName, nameof( DeleteZones ), Z );
					ZoneCacheByName.Remove( Z.ZoneName );
					ZoneCacheById.Remove( Z.Id );
				}

				Zones.RemoveRange( DbZones );
				Entity.SaveChanges();
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public void AddUpdateZones( List<Zone> zones, string programName )
	{
		try
		{
			var Zones = Entity.Zones;

			foreach( var Zone in zones )
			{
				var Zne = ( from Z in Zones
						    where Z.ZoneName == Zone.Name
						    select Z ).FirstOrDefault();

				void Modify()
				{
					Zne.SortIndex    = Zone!.SortIndex;
					Zne.ZoneName     = Zone.Name;
					Zne.Abbreviation = Zone.Abbreviation;

					Context.LogDifferences( ZONE_LOG_STORAGE, programName,
										    nameof( AddUpdateZones ),
										    new Zone {Name = Zne.ZoneName, SortIndex = Zne.SortIndex, Abbreviation = Zne.Abbreviation, ZoneId = Zne.Id},
										    Zone );
				}

				if( Zne is null )
				{
					// Could Be ReName
					Zne = ( from Z in Zones
						    where Z.Id == Zone.ZoneId
						    select Z ).FirstOrDefault();

					if( Zne is null )
					{
						Zne = new MasterTemplate.Zone
							  {
								  ZoneName     = Zone.Name,
								  Abbreviation = Zone.Abbreviation,
								  SortIndex    = Zone.SortIndex,
								  Id           = Zone.ZoneId
							  };

						Zones.Add( Zne );
						Context.LogDifferences( ZONE_LOG_STORAGE, programName, nameof( AddUpdateZones ), Zne );
					}
					else
						Modify();
				}
				else
					Modify();

				Entity.SaveChanges();

				ZoneCacheById[ Zne.Id ]         = Zne;
				ZoneCacheByName[ Zne.ZoneName ] = Zne;
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

#region Zone Cache
#region Cache Methods
	public string GetZoneNameById( long id )
	{
		var Zone = ZoneCacheById[ id ];

		if( Zone is null )
		{
			Zone = ( from Z in Entity.Zones
				     where Z.Id == id
				     select Z ).FirstOrDefault();

			if( Zone is not null )
				ZoneCacheById[ id ] = Zone;
			else
				return "";
		}
		return Zone.ZoneName;
	}

	public long GetIdByZoneName( string zoneName )
	{
		zoneName = zoneName.Trim();

		var Zone = ZoneCacheByName[ zoneName ];

		if( Zone is null )
		{
			Zone = ( from Z in Entity.Zones
				     where Z.ZoneName == zoneName
				     select Z ).FirstOrDefault();

			if( Zone is not null )
				ZoneCacheByName[ zoneName ] = Zone;
			else
				return 0;
		}
		return Zone.Id;
	}
#endregion

	private static readonly ZoneMemoryCacheById   ZoneCacheById   = new();
	private static readonly ZoneMemoryCacheByName ZoneCacheByName = new();

	internal class ZoneMemoryCacheById : CarrierMemoryCache<long, MasterTemplate.Zone>
	{
		protected override MasterTemplate.Zone? OnGetRecordForCache( CarrierDb db, long key )
		{
			try
			{
				return ( from Z in db.Entity.Zones
					     where Z.Id == key
					     select Z ).FirstOrDefault();
			}
			catch( Exception Exception )
			{
				db.Context.SystemLogException( Exception );
			}
			return null;
		}

		protected override CarrierMemoryCache<long, MasterTemplate.Zone> OnCreateInstance() => new ZoneMemoryCacheById();
	}

	internal class ZoneMemoryCacheByName : CarrierMemoryCache<string, MasterTemplate.Zone>
	{
		protected override MasterTemplate.Zone? OnGetRecordForCache( CarrierDb db, string key )
		{
			try
			{
				return ( from Z in db.Entity.Zones
					     where Z.ZoneName == key
					     select Z ).FirstOrDefault();
			}
			catch( Exception Exception )
			{
				db.Context.SystemLogException( Exception );
			}
			return null;
		}

		protected override CarrierMemoryCache<string, MasterTemplate.Zone> OnCreateInstance() => new ZoneMemoryCacheByName();
	}
#endregion
}