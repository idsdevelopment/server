﻿#nullable enable

using Role = Protocol.Data.Role;
using Staff = Protocol.Data.Staff;


namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public void AddDriver( string program, string driverCode, string password = "" )
	{
		if( password.IsNullOrWhiteSpace() )
			password = driverCode;

		var E     = Entity;
		var Staff = E.Staffs;

		void UpdateDriver()
		{
			var DriverRole = GetDriverRole();

			var StaffUpdate = new UpdateStaffMemberWithRolesAndZones( program, new Staff
			                                                                   {
				                                                                   StaffId  = driverCode,
				                                                                   Password = password
			                                                                   } );

			StaffUpdate.Roles.Add( new Role
			                       {
				                       Name            = DriverRole.RoleName,
				                       Mandatory       = DriverRole.Mandatory,
				                       IsDriver        = true,
				                       IsAdministrator = DriverRole.IsAdministrator
			                       } );

			AddUpdateStaffMember( StaffUpdate, false );
		}

		var Driver = ( from S in Staff
		               where S.StaffId == driverCode
		               select S ).FirstOrDefault();

		if( Driver is null )
			UpdateDriver();

		else if( !( from R in GetStaffMemberRoles( driverCode )
		            where R.IsDriver
		            select R ).Any() )
		{
			password = Driver.Password;
			UpdateDriver();
		}
	}


	public StaffLookupSummaryList GetDrivers()
	{
		var Result = new StaffLookupSummaryList();

		try
		{
			var E = Entity;

			Result.AddRange( from S in E.Staffs
			                 where S.Enabled && ( from Sr in E.StaffRoles
			                                      where ( from R in E.Roles
			                                              where R.IsDriver
			                                              select R.Id ).Contains( Sr.RoleId )
			                                      select Sr.StaffId ).Contains( S.StaffId )
			                 orderby S.StaffId
			                 select new StaffLookupSummary
			                        {
				                        FirstName = S.FirstName,
				                        LastName  = S.LastName,
				                        StaffId   = S.StaffId,

				                        AddressLine1 = ( from A in E.Addresses
				                                         where A.Id == S.AddressId
				                                         select A.AddressLine1 ).FirstOrDefault() ?? ""
			                        } );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}


	public StaffLookupSummary? GetDriver( string driverCode ) => ( from D in GetDrivers()
	                                                               where D.StaffId == driverCode
	                                                               select D ).FirstOrDefault();


	public void CreateDriver( string program, string driverCode )
	{
		Tasks.RunVoid( () =>
		               {
			               var Driver = driverCode.Trim();

			               using var Db = new CarrierDb( Context );

			               if( Db.GetDriver( Driver ) is null )
				               Db.AddDriver( program, Driver, Driver );
		               } );
	}

	public bool IsDriver( string driverCode )
	{
		var E = Entity;

		return ( from S in E.Staffs
		         where S.Enabled && ( S.StaffId == driverCode ) && ( from Sr in E.StaffRoles
		                                                             where ( from R in E.Roles
		                                                                     where R.IsDriver
		                                                                     select R.Id ).Contains( Sr.RoleId )
		                                                             select Sr.StaffId ).Contains( driverCode )
		         select S ).Any();
	}
}