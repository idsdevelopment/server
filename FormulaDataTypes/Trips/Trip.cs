﻿

// ReSharper disable InconsistentNaming

namespace FormulaDataTypes.Trips;

public class Trip : Measurement
{
	public string PACKAGE_TYPE  { get; set; }
	public string SERVICE_LEVEL { get; set; }
	public string PICKUP_ZONE   { get; set; }
	public string DELIVERY_ZONE { get; set; }

	public decimal PIECES { get; set; }
	public decimal WEIGHT { get; set; }

	public decimal PICKUP_WAIT_TIME_IN_SECONDS   { get; set; }
	public decimal DELIVERY_WAIT_TIME_IN_SECONDS { get; set; }


	public Trip()
	{
		PACKAGE_TYPE  = "";
		SERVICE_LEVEL = "";
		PICKUP_ZONE   = "";
		DELIVERY_ZONE = "";
	}

	public Trip( EvaluateFormula e ) : base( e )
	{
		PACKAGE_TYPE  = e.GetVariableAsString( nameof( PACKAGE_TYPE ) );
		SERVICE_LEVEL = e.GetVariableAsString( nameof( SERVICE_LEVEL ) );
		PICKUP_ZONE   = e.GetVariableAsString( nameof( PICKUP_ZONE ) );
		DELIVERY_ZONE = e.GetVariableAsString( nameof( DELIVERY_ZONE ) );

		var V = e.Variables;

		if( V.TryGetValue( nameof( PIECES ), out var Pieces ) && Pieces.IsNotNullOrWhiteSpace() )
			PIECES = e.GetVariableAsDecimal( nameof( PIECES ) );

		if( V.TryGetValue( nameof( WEIGHT ), out var Weight ) && Weight.IsNotNullOrWhiteSpace() )
			PIECES = e.GetVariableAsDecimal( nameof( WEIGHT ) );

		if( V.TryGetValue( nameof( PICKUP_WAIT_TIME_IN_SECONDS ), out var PickupWaitTime ) && PickupWaitTime.IsNotNullOrWhiteSpace() )
			PICKUP_WAIT_TIME_IN_SECONDS = e.GetVariableAsDecimal( nameof( PICKUP_WAIT_TIME_IN_SECONDS ) );

		if( V.TryGetValue( nameof( DELIVERY_WAIT_TIME_IN_SECONDS ), out var DeliveryWaitTime ) && DeliveryWaitTime.IsNotNullOrWhiteSpace() )
			DELIVERY_WAIT_TIME_IN_SECONDS = e.GetVariableAsDecimal( nameof( DELIVERY_WAIT_TIME_IN_SECONDS ) );
	}
}