﻿#nullable enable

using System.Collections.Generic;
using System.Linq;
using Formulas;

namespace FormulaDataTypes
{
	public abstract class FormulaBase
	{
		private readonly Formula F = new();

		public string Formula = "";

		public (List<string> Errors, decimal Value ) Evaluate()
		{
			var (ErrorList, Value) = F.Evaluate( Formula, this );
			return ( ErrorList, Value );
		}

		public List<string> Variables( object record ) => ( from V in F.GetPropertyMap( record )
		                                                    orderby V.Key
		                                                    select V.Key ).ToList();

		public List<string> Variables() => Variables( this );

		protected FormulaBase()
		{
		}

		protected FormulaBase( EvaluateFormula e )
		{
			Formula = e.Expression;
		}
	}
}