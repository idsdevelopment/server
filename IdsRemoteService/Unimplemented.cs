﻿using System;
using System.Collections.Generic;
using RemoteService;
using RemoteService.Model;

namespace IdsRemote
{
    public partial class IdsService : IRemoteService
    {
        public void SignOut()
        {
            throw new NotImplementedException();
        }

        public void ErrorLog( string errorMessage )
        {
        }

        public void AuditTrailNewOperation( string program, string text, string comment )
        {
        }

        public void AuditTrailModifyOperation( string program, string text, string comment )
        {
        }

        public void AuditTrailDeleteOperation( string program, string text, string comment )
        {
        }

        public void AuditTrailInformOperation( string program, string text, string comment )
        {
        }

        public List<Charge> GetCharges( RemoteService.Model.User user )
        {
            throw new NotImplementedException();
        }

        public List<Charge> GetCharges( RemoteService.Model.User user, string tripId )
        {
            throw new NotImplementedException();
        }

        public void UpdateCharges( RemoteService.Model.User user, string tripId, List<Charge> charges )
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, string> GetPreferences( RemoteService.Model.User user )
        {
            throw new NotImplementedException();
        }
    }
}