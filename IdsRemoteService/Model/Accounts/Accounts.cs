﻿using System;
using System.Collections.Generic;
using System.Linq;
using RemoteService.Model;
#if !ANDROID
#if ROUTE_CONSOLIDATION
using IdsRemoteService.IdsRemoteServiceReference;
#else
using IdsRemoteService.IdsServiceReference;

#endif
#else
using IdsRemoteService.org.internetdispatcher.jbtest;

#endif

namespace IdsRemote
{
    public partial class IdsService
    {
        private static Account ConvertAccount( remoteAccountValue account )
        {
            return new Account
                   {
                       OriginalData = account,
                       ResellerId = account.resellerId,
                       AccountId = account.accountId,
                       Name = account.companyName,
                       UserId = account.adminUserId,
                       AddressId = account.addressId
                   };
        }

        public List<Account> GetAllAccounts( RemoteService.Model.User user )
        {
            var Result = new List<Account>();
            using( var Client = new IdsClient() )
            {
                try
                {
                    var Request = new findAllAccountsForCarrier { authToken = AuthToken };
                    var Response = Client.findAllAccountsForCarrier( Request );
                    if( Response != null )
                        Result.AddRange( Response.Select( ConvertAccount ) );
                }
                catch( Exception E )
                {
                    Client.Log( E );
                }
            }
            return Result;
        }

        public Account GetAccount( RemoteService.Model.User user, string accountId )
        {
            using( var Client = new IdsClient() )
            {
                try
                {
                    var Request = new findAccount { authToken = AuthToken, accountId = accountId };
                    var Response = Client.findAccount( Request );
                    if( ( Response != null ) && ( Response.Length > 0 ) )
                        return ConvertAccount( Response[ 0 ] );
                }
                catch( Exception E )
                {
                    Client.Log( E );
                }
                return null;
            }
        }
    }
}