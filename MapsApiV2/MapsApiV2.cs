﻿using MapsApiV2.Xlate;
using static Protocol.Data.RouteBase;
using Leg = MapsApiV2.DataTypes.MapDataTypes.Leg;
using Point = MapsApiV2.DataTypes.MapDataTypes.Point;
using Route = MapsApiV2.DataTypes.MapDataTypes.Route;
using Step = MapsApiV2.DataTypes.MapDataTypes.Step;

namespace MapsApiV2;

using FOUND_LEGS = ( Point From, Point To, decimal DistanceInMeters, List<Leg> Legs, bool Found );

public class OptimiseEntry
{
	public PointPair Point { get; set; } = new();

	public string CompanyName = ""; // Mainly for debugging

	public decimal Distance,
	               DistanceFromStart;

	public bool IsPickupCompany;

	public object? Object;

	public OptimiseEntry()
	{
	}

	public OptimiseEntry( OptimiseEntry e )
	{
		Distance          = e.Distance;
		DistanceFromStart = e.DistanceFromStart;
		Point             = e.Point;
		Object            = e.Object;
		IsPickupCompany   = e.IsPickupCompany;
		CompanyName       = e.CompanyName;
	}
}

public abstract class OptimiseBase( IRequestContext context, IList<OptimiseEntry> points )
{
	protected const    decimal              DISTANCE_LIMIT = 0.0005M; // +- 50 meters
	protected readonly IRequestContext      Context        = context;
	protected          IList<OptimiseEntry> Points         = points;


	protected OptimiseBase( IRequestContext context, RouteOptimisationAddresses addresses ) : this( context, addresses.ToOptimiseEntries( context ) )
	{
	}

	protected FOUND_LEGS CachedDistance( UsersEntities db, Point frm, Point to )
	{
		var Ok = frm.IsValid && to.IsValid;

		if( Ok && !frm.IsEqual( to ) )
		{
			var FromLatUpper  = frm.Latitude + DISTANCE_LIMIT;
			var FromLatLower  = frm.Latitude - DISTANCE_LIMIT;
			var FromLongUpper = frm.Longitude + DISTANCE_LIMIT;
			var FromLongLower = frm.Longitude - DISTANCE_LIMIT;
			var ToLatUpper    = to.Latitude + DISTANCE_LIMIT;
			var ToLatLower    = to.Latitude - DISTANCE_LIMIT;
			var ToLongUpper   = to.Longitude + DISTANCE_LIMIT;
			var ToLongLower   = to.Longitude - DISTANCE_LIMIT;

			var Rec = ( from D in db.Entity.PointToPoints
			            where ( D.FromLatitude >= FromLatLower ) && ( D.FromLatitude <= FromLatUpper )
			                                                     && ( D.FromLongitude >= FromLongLower ) && ( D.FromLongitude <= FromLongUpper )
			                                                     && ( D.ToLatitude >= ToLatLower ) && ( D.ToLatitude <= ToLatUpper )
			                                                     && ( D.ToLongitude >= ToLongLower ) && ( D.ToLongitude <= ToLongUpper )
			            orderby D.DistanceInMeters
			            select D ).FirstOrDefault();

			if( Rec is not null )
			{
				try
				{
					var Legs = JsonConvert.DeserializeObject<List<Leg>>( Rec.LegsAsJson ) ?? [];
					return ( frm, to, Rec.DistanceInMeters, Legs, true );
				}
				catch // Probably an error in the JSON
				{
				}
			}
		}

		return ( frm, to, 0, [], false );
	}

	protected FOUND_LEGS CachedDistance( UsersEntities db, IList<OptimiseEntry> entries )
	{
		var Count = entries.Count;

		if( Count > 1 )
		{
			var From = entries[ 0 ].Point.From;
			var To   = entries[ Count - 1 ].Point.To;

			return CachedDistance( db, From, To );
		}

		return ( new Point(), new Point(), 0, [], false );
	}
}

public abstract class OptimiseRoute( IRequestContext context, IList<OptimiseEntry> points ) : OptimiseBase( context, points )
{
	protected OptimiseRoute( IRequestContext context, RouteOptimisationAddresses addresses ) : this( context, addresses.ToOptimiseEntries( context ) )
	{
	}


	public Task<Directions> Optimise( TRAVEL_MODE travelMode, int timezoneOffsetInMinutes ) => NormalOptimise( Points, travelMode, timezoneOffsetInMinutes );

#region Normal Optimise
	private async Task<Directions> NormalOptimise( IList<OptimiseEntry> entries, TRAVEL_MODE travelMode, int timezoneOffsetInMinutes )
	{
		var Count = entries.Count;

		if( Count >= 2 )
		{
			var Out  = new List<OptimiseEntry>();
			var Back = new List<OptimiseEntry>();

			// Round Entries
			foreach( var Entry in entries )
			{
				var P    = Entry.Point;
				var From = P.From;
				From.Latitude  = Maths.Round6( From.Latitude );
				From.Longitude = Maths.Round6( From.Longitude );

				var To = P.To;
				To.Latitude = Maths.Round6( To.Latitude );
				To.Latitude = Maths.Round6( To.Latitude );
			}

			var Start      = entries[ 0 ];
			var StartPoint = Start.Point.From;
			var End        = entries[ Count - 1 ];
			var EndPoint   = End.Point.From;

			// Distance from start
			for( var I = 1; I < Count; I++ )
			{
				var Way      = entries[ I ];
				var WayPoint = Way.Point.From;

				Way.DistanceFromStart = Measurement.SimpleGpsDistance( StartPoint.Latitude,
				                                                       StartPoint.Longitude,
				                                                       WayPoint.Latitude,
				                                                       WayPoint.Longitude ).Metres;
			}

			// Check if circular route
			var IsCircular = ( Count >= 3 ) && StartPoint.IsEqual( EndPoint );

			if( IsCircular )
			{
				// Find furthest point from start
				var Furthest      = Start;
				var FurthestPoint = Start.Point.From;

				var FurthestDistance = 0M;

				for( var I = 1; I < Count; I++ )
				{
					var WayPoint          = entries[ I ];
					var DistanceFromStart = WayPoint.DistanceFromStart;

					if( DistanceFromStart > FurthestDistance )
					{
						FurthestDistance = DistanceFromStart;
						Furthest         = WayPoint;
					}
				}

				Out.Add( Start );
				Back.Add( Furthest );

				var StartX = StartPoint.Longitude;
				var StartY = StartPoint.Latitude;

				var EndX = FurthestPoint.Longitude;
				var EndY = FurthestPoint.Latitude;

				for( var I = Count - 1; I < Count; I++ )
				{
					var WayPoint = entries[ I ];

					var Way = WayPoint.Point.From;

					if( !Way.IsEqual( FurthestPoint ) )
					{
						var Val = Maths.SideOfLine( StartX, StartY, EndX, EndY, Way.Longitude, Way.Latitude );

						if( Val <= 0 )
							Out.Add( WayPoint );
						else
							Back.Add( WayPoint );
					}
				}
			}
			else
				Out.AddRange( entries );

			try
			{
				// Fill in missing legs
				await using var Db = new UsersEntities();

				List<FOUND_LEGS> OutWayPoints  = [CachedDistance( Db, Out )],
				                 BackWayPoints = IsCircular ? [CachedDistance( Db, Back )] : [];

				async Task<Directions> AddNewLegs( IList<FOUND_LEGS> wayPoints )
				{
					Directions Result;

					if( wayPoints.Any( wp => !wp.Found ) )
					{
						var PointList = new List<Point>();

						foreach( var (From, To, _, _, _) in wayPoints )
						{
							PointList.Add( From );
							PointList.Add( To );
						}

						var Directions = await new MapsApiV2( Context ).GetDirections( travelMode, timezoneOffsetInMinutes, PointList );

						// Flatten the Steps
						var Flattened = new List<(Route Route, Leg Leg, Step Step)>();
						var Routes    = Directions.Routes;

						foreach( var Rte in Routes )
						{
							foreach( var Leg in Rte.Legs )
							{
								foreach( var Step in Leg.Steps )
								{
									var StartLoc = Step.StartLocation;

									if( StartLoc is not null )
									{
										StartLoc.Latitude  = Maths.Round6( StartLoc.Latitude );
										StartLoc.Longitude = Maths.Round6( StartLoc.Longitude );
									}

									var EndLoc = Step.EndLocation;

									if( EndLoc is not null )
									{
										EndLoc.Latitude  = Maths.Round6( EndLoc.Latitude );
										EndLoc.Longitude = Maths.Round6( EndLoc.Longitude );
									}

									Flattened.Add( ( Rte, Leg, Step ) );
								}
							}
						}

						// Remove contiguous duplicate points
						Point? Previous = null;

						for( var Ndx = Flattened.Count - 1; Ndx >= 0; Ndx-- )
						{
							var Current = Flattened[ Ndx ].Step.EndLocation;

							if( Previous is not null && ( Previous == Current ) )
								Flattened.RemoveAt( Ndx + 1 ); // Remove Previous

							Previous = Current;
						}

						Routes.Clear();

						// Rebuild Route
						foreach( var (R, L, S) in Flattened )
						{
							var Lgs = R.Legs;

							if( !Routes.Contains( R ) )
							{
								Routes.Add( R );
								Lgs.Clear();
							}

							var Steps = L.Steps;

							if( !Lgs.Contains( L ) )
							{
								Lgs.Add( L );
								Steps.Clear();
							}

							Steps.Add( S );
						}

						AddUpdateDatabasePoints( Directions, travelMode );
						Result = Directions;
					}
					else
					{
						Result = new Directions();
						var Route = new Route();
						Result.Routes.Add( Route );
						var Legs = Route.Legs;

						foreach( var WayPoint in wayPoints )
							Legs.AddRange( WayPoint.Legs );
					}

					// Convert wayPoints to Routes
					return Result;
				}

				var OutDirections = await AddNewLegs( OutWayPoints );

				if( IsCircular )
					OutDirections.Routes[ 0 ].Legs.AddRange( ( await AddNewLegs( BackWayPoints ) ).Routes[ 0 ].Legs );

				return OutDirections;
			}
			catch( Exception Exception )
			{
				// ReSharper disable once InconsistentlySynchronizedField
				Context.SystemLogException( Exception );
			}
		}
		return new Directions();
	}
#endregion


#region MyRegion
	private static void AddUpdateDatabasePoints( Directions directions, TRAVEL_MODE travelMode )
	{
		var Directions = new Directions( directions );

		Tasks.RunVoid( () =>
		               {
			               var Now = DateTime.Now;

			               using var Db            = new UsersEntities();
			               var       PointToPoints = Db.Entity.PointToPoints;

			               foreach( var Route in Directions.Routes )
			               {
				               var Legs  = Route.Legs;
				               var Count = Legs.Count;

				               if( Count > 0 )
				               {
					               var First = Legs[ 0 ];
					               var Last  = Legs[ Count - 1 ];

					               var From          = First.OriginalStartLocation;
					               var FromLatitude  = Maths.Round6( From!.Latitude );
					               var FromLongitude = Maths.Round6( From.Longitude );

					               var To          = Last.OriginalEndLocation;
					               var ToLatitude  = Maths.Round6( To!.Latitude );
					               var ToLongitude = Maths.Round6( To.Longitude );

					               var Distance = Legs.Sum( leg => (decimal)( leg.Distance?.Value ?? 0 ) ) * 1000; // Convert to meters;
					               var JLegs    = JsonConvert.SerializeObject( Legs );

					               var Rec = ( from P in PointToPoints
					                           where ( P.Mode == (byte)travelMode ) && ( P.FromLatitude == FromLatitude ) && ( P.FromLongitude == FromLongitude )
					                                 && ( P.ToLatitude == ToLatitude ) && ( P.ToLongitude == ToLongitude )
					                           select P ).FirstOrDefault();

					               if( Rec is null )
					               {
						               PointToPoints.Add( new PointToPoint
						                                  {
							                                  Mode = (byte)travelMode,

							                                  FromLatitude  = FromLatitude,
							                                  FromLongitude = FromLongitude,

							                                  ToLatitude  = ToLatitude,
							                                  ToLongitude = ToLongitude,

							                                  DistanceInMeters = Distance,
							                                  DateCreated      = Now,
							                                  LegsAsJson       = JLegs
						                                  } );
					               }
					               else
					               {
						               Rec.Mode = (byte)travelMode;

						               Rec.DistanceInMeters = Distance;
						               Rec.DateCreated      = Now;
						               Rec.LegsAsJson       = JLegs;
					               }
					               Db.SaveChanges();
				               }
			               }
		               } );
	}
#endregion
}

public class MapsApiV2( IRequestContext context )
{
	private AzureMaps AzureMaps => _AzureMaps ??= new AzureMaps( context );

	private static DateTime   Expires => DateTime.UtcNow.AddDays( Constants.ADDRESS_EXPIRES_IN_DAYS );
	private        AzureMaps? _AzureMaps;
	public static MapsApiV2 Create( IRequestContext context ) => new( context );

	public async Task<Directions> GetDirections( TRAVEL_MODE mode, int timezoneOffsetInMinutes, List<Point> points )
	{
		var List = new List<Point>( points );

		var Count = List.Count;

		if( Count >= 2 )
		{
			var SaveStart = List[ 0 ];
			var CountM1   = Count - 1;
			var SaveEnd   = List[ CountM1 ];

			var Circular = ( Count > 3 ) && ( SaveStart.Latitude == SaveEnd.Latitude ) && ( SaveStart.Longitude == SaveEnd.Longitude );

			if( Circular )
				List.RemoveAt( CountM1 );

			List.RemoveAt( 0 ); // Remove Start so not to reorder

			Count = List.Count;

			if( Count > 0 )
			{
				var NewList = new List<Point>();

				if( Count > 1 )
				{
					// Remove duplicates but leave start and end points
					NewList.AddRange( from Point in List
					                  group Point by new { Point.Latitude, Point.Longitude }
					                  into Points
					                  select Points.First() );
				}
				else
					NewList.AddRange( List );

				NewList.Insert( 0, SaveStart );
				NewList.Add( SaveEnd );

				return await AzureMaps.GetDirections( mode, timezoneOffsetInMinutes, NewList, Circular );
			}
		}
		return new Directions();
	}


	public async Task<(AddressList Address, bool RateLimited)> FindLocationByAddress( AddressData address )
	{
		(AddressList AddressList, bool RateLimited) Result = ( null, false )!;

		List<MapAddress> FoundAddresses;

		try
		{
			Regions.FixupCountryRegion( address );

			await using var Db           = new UsersEntities();
			var             Entity       = Db.Entity;
			var             MapAddresses = Entity.MapAddresses;

			var Addr1      = $"{address.StreetNumber.Trim()} {address.Street.Trim()}".TrimToUpperMax100();
			var City       = address.City.TrimToUpperMax50();
			var Region     = address.State.TrimToUpperMax100();
			var RegionCode = address.StateCode.TrimToUpperMax100();
			var Country    = address.Country.TrimToUpperMax50();

			FoundAddresses = ( from A in MapAddresses
			                   where ( A.AddressLine1 == Addr1 )
			                         && ( A.City == City )
			                         && ( A.Region == Region )
			                         && ( A.RegionCode == RegionCode )
			                         && ( A.Country == Country )
			                   select A ).ToList();

			if( FoundAddresses.Count > 0 )
			{
				UpdateMapAddressExpires( context, ( from R in FoundAddresses
				                                    select R.Id ).ToList() );
			}
			else
			{
				Result = await InternalFindLocationByAddress( address );

				if( Result is { RateLimited: false, AddressList.Count: > 0 } )
				{
					foreach( var Addr in Result.AddressList )
					{
						var MapAddress = Addr.ToMapAddress();
						MapAddress.Expires = DateOnly.FromDateTime( Expires.Date );
						MapAddresses.Add( MapAddress );
						FoundAddresses.Add( MapAddress );
					}
					await Entity.SaveChangesAsync();
				}
			}
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
			FoundAddresses = [];
		}

		var Co = address.CompanyName.TrimToUpperMax100();

		var AddressList = new AddressList();

		foreach( var Addr in FoundAddresses )
			AddressList.Add( new AddressData( Addr, Co ) );

		return Result with { AddressList = AddressList };
	}

	private static void UpdateMapAddressExpires( IRequestContext context, ICollection<long> updates )
	{
		if( updates.Count != 0 )
		{
			var Context = context.Clone();

			Tasks.RunVoid( () =>
			               {
				               try
				               {
					               using var Db           = new UsersEntities();
					               var       Entity       = Db.Entity;
					               var       MapAddresses = Entity.MapAddresses;
					               var       Exp          = DateOnly.FromDateTime( Expires );

					               // Had code generation problems using contains
					               foreach( var U in updates )
					               {
						               var Rec = ( from A in MapAddresses
						                           where A.Id == U
						                           select A ).FirstOrDefault();

						               if( Rec is not null )
						               {
							               Rec.Expires = Exp;
							               Entity.SaveChanges();
						               }
					               }
				               }
				               catch( Exception Exception )
				               {
					               Context.SystemLogException( Exception );
				               }
			               } );
		}
	}

	private async Task<(AddressList Address, bool RateLimited)> InternalFindLocationByAddress( AddressData address ) => await AzureMaps.FindLocationByAddress( address );
}