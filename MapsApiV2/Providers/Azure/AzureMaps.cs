﻿using MapsApiV2.Xlate;
using Leg = MapsApiV2.DataTypes.MapDataTypes.Leg;
using Point = MapsApiV2.DataTypes.MapDataTypes.Point;
using Route = MapsApiV2.DataTypes.MapDataTypes.Route;
using Step = MapsApiV2.DataTypes.MapDataTypes.Step;

namespace MapsApiV2.Providers.Azure;

public static class Extensions
{
	public static string ToSuite( this string freeformAddress )
	{
		const string SUITE = "SUITE ";
		var          Fa    = freeformAddress.Trim();
		var          Suite = "";

		if( Fa is { Length: > 0 } )
		{
			var Ndx = Fa.IndexOf( SUITE, StringComparison.OrdinalIgnoreCase );

			if( Ndx >= 0 )
			{
				Ndx += SUITE.Length;
				var Comma = Fa.IndexOf( ',', Ndx );

				if( Comma >= 0 )
					Suite = Fa.Substring( Ndx, Comma - Ndx ).Trim();
			}
		}
		return Suite;
	}


	public static AddressData ToAddressData( this Responses.SearchAddressResultBase address, string companyName )
	{
		var Pos  = address.position;
		var Addr = address.address;

		return new AddressData( Pos.lat, Pos.lon,
		                        companyName.Trim(),
		                        Addr.freeformAddress.ToSuite(),
		                        Addr.streetNumber.Trim(),
		                        Addr.streetName.Trim(),
		                        Addr.crossStreet.Trim(), // Vicinity
		                        Addr.municipalitySubdivision.Trim(),
		                        Addr.countrySubdivisionName.Trim(),
		                        Addr.countrySubdivision.Trim(), //StateCode
		                        Addr.postalCode.Trim(),
		                        Addr.country.Trim(),
		                        Addr.countryCode.Trim()
		                      );
	}

	public static AddressData ToAddressData( this Responses.SearchAddressResultBase address, string companyName, string stateCode, string stateName )
	{
		var Pos  = address.position;
		var Addr = address.address;

		return new AddressData( Pos.lat, Pos.lon,
		                        companyName.Trim(),
		                        Addr.freeformAddress.ToSuite(),
		                        Addr.streetNumber.Trim(),
		                        Addr.streetName.Trim(),
		                        Addr.crossStreet.Trim(), // Vicinity
		                        Addr.municipalitySubdivision.Trim(),
		                        stateName.Trim(),
		                        stateCode.Trim(), //StateCode
		                        Addr.postalCode.Trim(),
		                        Addr.country.Trim(),
		                        Addr.countryCode.Trim()
		                      );
	}
}

internal class AzureMaps( IRequestContext context ) : AMapsApi( context )
{
	private const int WAYPOINT_LIMIT = 148;

	public override async Task<(AddressList AddressList, bool RateLimited)> FindLocationByAddress( AddressData address )
	{
		var Result = address.CountryCode.IsNotNullOrWhiteSpace()
			             ? await FindLocationByStructuredAddress( address )
			             : [];

		if( Result.Count <= 0 )
			Result = await FindLocationByUnStructuredAddress( address );

		return ( Regions.FixupCountryRegion( Result ), false );
	}

	public override async Task<Directions> GetDirections( RouteBase.TRAVEL_MODE mode, int timezoneOffsetInMinutes, List<Point> points, bool circular )
	{
		var Result = new Directions();

		try
		{
			var Copy = new List<Point>( points );

			while( Copy.Count > 1 )
			{
				var Count  = Math.Min( Copy.Count, WAYPOINT_LIMIT ); // Must be greater than one
				var Points = Copy.GetRange( 0, Count );

				Copy.RemoveRange( 0, Count );

				if( Copy.Count == 1 )               // Leave last 2 points as start point for next
					Copy.Insert( 0, Points[ ^1 ] ); // Forced to have overlap

				var Args  = new StringBuilder();
				var First = true;

				var NewOffset = TimeSpan.FromMinutes( timezoneOffsetInMinutes );
				var TzNow     = DateTimeOffset.UtcNow.ToOffset( NewOffset );

				foreach( var Point in points )
				{
					if( First )
						First = false;
					else
						Args.Append( ':' );

					Args.Append( $"{Point.Latitude:F5},{Point.Longitude:F5}" );

					if( Point.DueTime is { } DueTime && ( DueTime >= TzNow ) )
						Args.Append( $",arriveAt={DueTime:u}" );

					if( Point.TimeAtStopInSeconds > 0 )
						Args.Append( $",travelTime={Point.TimeAtStopInSeconds}" );
				}

				var RouteType = ( mode & RouteBase.TRAVEL_MODE.FASTEST ) != 0 ? "fastest" : "shortest";
				var Avoids    = "";
				var BestOrder = ( mode & RouteBase.TRAVEL_MODE.DONT_OPTIMISE ) != 0 ? "" : "&computeBestOrder=True";

				void DoAvoid( RouteBase.TRAVEL_MODE mask, string avoid )
				{
					if( ( mode & mask ) != 0 )
						Avoids = $"{Avoids}&avoid={avoid}";
				}

				DoAvoid( RouteBase.TRAVEL_MODE.AVOID_TOLL_ROADS, "tollRoads" );
				DoAvoid( RouteBase.TRAVEL_MODE.AVOID_MOTORWAYS, "motorways" );
				DoAvoid( RouteBase.TRAVEL_MODE.AVOID_FERRIES, "ferries" );
				DoAvoid( RouteBase.TRAVEL_MODE.AVOID_BORDER_CROSSINGS, "borderCrossings" );
				DoAvoid( RouteBase.TRAVEL_MODE.AVOID_CAR_POOLS, "carpools" );
				DoAvoid( RouteBase.TRAVEL_MODE.AVOID_UNPAVED_ROADS, "unpavedRoads" );

				var RUri     = $"{Url.RouteUri}&instructionsType=tagged&routeType={RouteType}{Avoids}{BestOrder}&query={Args}";
				var Response = await GetResponse( RUri );

				if( Response.Value is not null )
				{
					var RouteResult = JsonConvert.DeserializeObject<Responses.RouteDirectionsResponse>( Response.Value );

					if( RouteResult is not null )
					{
						foreach( var Route in RouteResult.routes )
						{
							var Rte  = new Route();
							var Legs = Rte.Legs;
							var PNdx = 0;

							foreach( var Leg in Route.legs )
							{
								var S    = Leg.summary;
								var Ks   = S.lengthInMeters / 1000.0;
								var Mins = S.travelTimeInSeconds / 60.0;

								var Steps = new List<Step>();

								var InsPoints = ( from P in Leg.points
								                  select new RouteInstruction
								                         {
									                         Point = P
								                         } ).ToList();

								// Match up the instructions
								foreach( var Point in InsPoints )
								{
									var P1 = Point.Point;

									foreach( var I in Route.guidance.instructions )
									{
										var P2 = I.point;

										if( ( P1.latitude == P2.latitude ) && ( P1.longitude == P2.longitude ) )
										{
											const string SKIP = "~~SKIP~~";

											var Message = I.message.Trim();

											if( Message != SKIP ) // Don't reuse message
											{
												Point.Instruction = Message;
												I.message         = SKIP;
												break;
											}
										}
									}
								}

								RouteInstruction? Prev = null;

								foreach( var Point in InsPoints )
								{
									if( Prev is not null )
									{
										var Pp = Prev.Point;
										var P  = Point.Point;

										Steps.Add( new Step
										           {
											           StartLocation = new Point
											                           {
												                           Latitude  = Pp.latitude,
												                           Longitude = Pp.longitude
											                           },
											           EndLocation = new Point
											                         {
												                         Latitude  = P.latitude,
												                         Longitude = P.longitude
											                         },
											           Instructions = Prev.Instruction,
											           Distance     = new DistanceOrDuration(),
											           Duration     = new DistanceOrDuration()
										           } );
									}
									Prev = Point;
								}

								var Ndx = Steps.Count;

								if( ( Ndx > 0 ) && Prev is not null )
									Steps[ Ndx - 1 ].Instructions = Prev.Instruction;

								Legs.Add( new Leg
								          {
									          OriginalStartLocation = Points[ PNdx ],
									          OriginalEndLocation   = Points[ ++PNdx ],

									          Distance = new DistanceOrDuration
									                     {
										                     Value = Ks,
										                     Text  = $"{Ks:N} Kilometres"
									                     },
									          Duration = new DistanceOrDuration
									                     {
										                     Value = Mins,
										                     Text  = $"{Mins:N} Minutes and {60 / Mins.Frac():N0} Seconds"
									                     },
									          EndAddress    = "",
									          EndLocation   = new Point(),
									          StartAddress  = "",
									          StartLocation = new Point(),
									          Steps         = Steps
								          } );
							}
							Result.Routes.Add( Rte );
						}
					}
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return Result;
	}

	private async Task<AddressList> FindLocationByUnStructuredAddress( AddressData address )
	{
		var RetVal = new AddressList();

		try
		{
			var Args  = new StringBuilder();
			var First = true;

			void AppendArg( string arg )
			{
				arg = arg.Trim();

				if( arg.IsNotNullOrWhiteSpace() )
				{
					if( First )
						First = false;
					else
						Args.Append( ", " );

					Args.Append( arg );
				}
			}

			var CountryCode = address.Country;
			var RegionCode  = address.State.IsNotNullOrWhiteSpace() ? address.State : address.StateCode;

			var RegionType = Utils.Xlate.Region.TryGetVale( CountryCode, RegionCode, out var Region );

			string StateCode, State;

			switch( RegionType )
			{
			case Utils.Xlate.Region.RESULT_TYPE.ABBREVIATION:
				StateCode = Region!;
				State     = RegionCode;
				break;

			case Utils.Xlate.Region.RESULT_TYPE.NAME:
				StateCode = RegionCode;
				State     = Region!;
				break;

			default:
				StateCode = address.StateCode;
				State     = address.State;
				break;
			}

			Args.Append( $"{address.StreetNumber.Trim()} ".TrimStart() );
			AppendArg( address.Street );
			AppendArg( address.City );
			AppendArg( StateCode );
			AppendArg( State );
			AppendArg( address.PostalCode );
			AppendArg( address.CountryCode );
			AppendArg( address.Country );

			var Query = Uri.EscapeDataString( Args.ToString() );

			if( Query.IsNotNullOrWhiteSpace() )
			{
				var SearchUri = $"{Url.AddressSearchUri}&query={Query}";
				var Response  = await GetResponse( SearchUri );

				if( Response.Value is { } Value )
				{
					var Result = JsonConvert.DeserializeObject<Responses.SearchAddressResponse>( Value );

					if( Result is not null )
					{
						RetVal.AddRange( from AddressResult in Result.results
						                 let Address = AddressResult.address
						                 let City = Address.municipalitySubdivision.Trim()
						                 let LocalName = Address.localName.Trim()
						                 let St = Address.countrySubdivisionName.Trim()
						                 let Country = Address.country.Trim()
						                 where ( Country.Compare( address.Country, StringComparison.OrdinalIgnoreCase ) == 0 )
						                       && ( St.Compare( State, StringComparison.OrdinalIgnoreCase ) == 0 )
						                       && ( ( City.Compare( address.City, StringComparison.OrdinalIgnoreCase ) == 0 )
						                            || ( LocalName.Compare( address.City, StringComparison.OrdinalIgnoreCase ) == 0 ) )
						                 select AddressResult.ToAddressData( address.CompanyName ) );
					}
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return RetVal;
	}

// Requires Country Code
	private async Task<AddressList> FindLocationByStructuredAddress( AddressData address )
	{
		var RetVal = new AddressList();

		try
		{
			var Args = new StringBuilder();

			void AppendArg( string argName, string arg )
			{
				if( arg.IsNotNullOrWhiteSpace() )
					Args.Append( $"&{argName}={Uri.EscapeDataString( arg.Trim() )}" );
			}

			AppendArg( "streetNumber", address.StreetNumber );
			AppendArg( "streetName", address.Street );
			AppendArg( "municipality", address.City );
			AppendArg( "countrySubdivision", address.StateCode );

			var PCode = address.PostalCode.Trim();

			if( PCode.Length >= 4 )
				AppendArg( "postalCode", PCode );

			AppendArg( "countrySecondarySubdivision", address.Country );
			AppendArg( "countryCode", address.CountryCode );

			var Query = Args.ToString();

			if( Query.IsNotNullOrWhiteSpace() )
			{
				var SearchUri = $"{Url.StructuredAddressUri}{Query}";

				var Response = await GetResponse( SearchUri );

				if( Response.Value is { } Value )
				{
					var Result = JsonConvert.DeserializeObject<Responses.SearchAddressStructuredResponse>( Value );

					if( Result is not null )
					{
						RetVal.AddRange( from StructuredResult in Result.results
						                 select StructuredResult.ToAddressData( address.CompanyName ) );
					}
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return RetVal;
	}
}