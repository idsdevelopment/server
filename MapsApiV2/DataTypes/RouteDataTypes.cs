﻿namespace MapsApiV2.DataTypes.RouteDataTypes;

public sealed class RouteSegment
{
	public string TripId { get; set; } = "";

	public string AccountId { get; set; } = "";

	public AddressData? Account { get; set; }

	public AddressData? From { get; set; }

	public AddressData? To { get; set; }

	public string Reference { get; set; } = "";

	public decimal Pieces { get; set; }

	public decimal Weight { get; set; }

	public string Notes { get; set; } = "";
}