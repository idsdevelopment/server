﻿namespace MapsApiV2;

public partial class Routing : OptimiseRoute
{
	public Routing( IRequestContext context, RouteOptimisationAddresses addresses ) : base( context, addresses )
	{
	}

	public Routing( IRequestContext context, IList<OptimiseEntry> points ) : base( context, points )
	{
	}

	public static async Task<DistancesAndAddresses> Directions( IRequestContext context, RouteOptimisationAddresses addresses ) => ( await new Routing( context, addresses ).Optimise( addresses.TravelMode, addresses.TimeZoneOffsetInMinutes ) ).ToDistancesAndAddresses();
}