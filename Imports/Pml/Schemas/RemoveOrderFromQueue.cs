﻿// ReSharper disable InconsistentNaming

namespace Imports.Pml.Schemas.RemoveOrderFromQueue;

public class DeleteOrders
{
	public string orderId { get; set; }

	public DeleteOrders( string orderId )
	{
		this.orderId = orderId;
	}

	public DeleteOrders()
	{
		orderId = "";
	}

	public string ToXml() => Xml.ConvertObjectToXml( this );
}

public class DeleteOrdersResponse
{
}