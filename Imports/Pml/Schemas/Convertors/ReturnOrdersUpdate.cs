﻿// ReSharper disable InconsistentNaming

namespace Imports.Pml.Order.Update;

public partial class request
{
	public string ToXml() => Xml.ConvertObjectToXml( this );
}