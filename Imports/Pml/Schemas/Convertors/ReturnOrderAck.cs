// ReSharper disable InconsistentNaming

namespace Imports.Pml.ReturnOrderAck
{
	public partial class responsedata
	{
		public bool IsSuccess => string.Compare( "successful", status, StringComparison.OrdinalIgnoreCase ) == 0;
	}

	public static class Extensions
	{
		public static responsedata? FromXmlSuccessResponse( this string xmlString ) => Xml.ConvertXmlStringToObject<responsedata>( xmlString );
	}
}


namespace Imports.Pml.Order.Update
{
	public partial class responsedata
	{
		public bool IsSuccess => string.Compare("Your request has been accepted and will be processed soon", Response, StringComparison.OrdinalIgnoreCase) == 0;
	}

	public static class Extensions
	{
		public static responsedata? FromXmlOrderAcceptedResponse( this string xmlString ) => Xml.ConvertXmlStringToObject<responsedata>( xmlString );
	}
}