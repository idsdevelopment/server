﻿namespace Imports.Pml;

internal sealed class Constants
{
	public const string PML_EXPORT = "Pml Export",
	                    //
	                    //	Production
	                    //
	                    RETURN_ORDER_URL               = "https://alb-pmi-reev-prd.lb.anypointdns.net/prd-reev-mulesoft-dte-ccb2b-as/v1/api/v1/returnordersnotification",
	                    RETURN_ORDER_UPDATE_URL        = "https://alb-pmi-reev-prd.lb.anypointdns.net/prd-reev-mulesoft-dte-ccb2b-as/v1/api/v1/returnorderupdate",
	                    RETURN_ORDER_STATUS_UPDATE_URL = "https://alb-pmi-reev-prd.lb.anypointdns.net/prd-reev-mulesoft-dte-ccb2b-as/v1/api/v1/orderstatusupdate",
	                    CLIENT_ID_KEY                  = "client_id",
	                    CLIENT_ID                      = "4daa7d21615b4702819c566bb928efde",
	                    SECRET_CODE_KEY                = "client_secret",
	                    SECRET_CODE                    = "a798E60FeD494a6dAD6607C4ecab9eFe",
	                    //
	                    //	Testing
	                    //
	                    TESTING_RETURN_ORDER_URL               = "https://alb-pmi-reev-ppd.lb.anypointdns.net/ppd-reev-mulesoft-dte-ccb2b-as/v1/api/v1/returnordersnotification",
	                    TESTING_RETURN_ORDER_UPDATE_URL        = "https://alb-pmi-reev-ppd.lb.anypointdns.net/ppd-reev-mulesoft-dte-ccb2b-as/v1/api/v1/returnorderupdate",
	                    TESTING_RETURN_ORDER_STATUS_UPDATE_URL = "https://alb-pmi-reev-ppd.lb.anypointdns.net/ppd-reev-mulesoft-dte-ccb2b-as/v1/api/v1/orderstatusupdate",
	                    TESTING_CLIENT_ID                      = "4daa7d21615b4702819c566bb928efde",
	                    TESTING_SECRET_CODE                    = "a798E60FeD494a6dAD6607C4ecab9eFe",
	                    //
	                    //
	                    CHANNEL_KEY   = "X-Channel",
	                    CHANNEL_VALUE = "web",
	                    //
	                    // Status
	                    //
	                    RECONCILING_CONFIRMED = "Reconciling Confirmed",
	                    RECONCILING_ADJUSTED  = "Reconciling Adjusted",
	                    RECONCILING_REJECTED  = "Rejected",
	                    //
	                    //
	                    //
	                    SERVICE_LEVEL       = "Return",
	                    RETURN_ORDER_FEE    = "Return Order Fee",
	                    RETURN_ORDER_FEE_ID = "ReturnOrderFee";

	public const short REORDER_FEE_TYPE = 0;

	public enum RECONCILING
	{
		RECONCILING_CONFIRMED,
		RECONCILING_ADJUSTED,
		RECONCILING_REJECTED
	}

	public class Accounts
	{
		public const string PRIMARY_ACCOUNT = "PMLOz",
		                    CALLTAKER       = "Import";
	}

	public class Warehouse
	{
		public const string COMPANY_NAME  = "Melbourne Warehouse",
		                    LOCATION_CODE = "MelWare",
		                    STREET        = "1 Innovation Court",
		                    CITY          = "Derrimut",
		                    REGION_CODE   = "Vic",
		                    POSTAL_CODE   = "3030",
		                    COUNTRY       = "AUSTRALIA",
		                    COUNTRY_CODE  = "AU";
	}
}