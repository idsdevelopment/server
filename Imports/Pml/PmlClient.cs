﻿using DatabaseCore.Model.Carrier._Customers.Pml;
using Imports.Pml.Order.StatusUpdate;
using Imports.Pml.Order.Update;
using Imports.Pml.ReturnOrderAck;
using Imports.Pml.Schemas.RemoveOrderFromQueue;
using request = Imports.Pml.Order.Create.request;
using requestOrder = Imports.Pml.Order.Create.requestOrder;
using requestOrderProduct = Imports.Pml.Order.Create.requestOrderProduct;
using TripCharge = Protocol.Data.TripCharge;

// ReSharper disable IdentifierTypo

namespace Imports.Pml;

internal class PmlClient : AIClient
{
	private const string PML              = "pml",
	                     RETURN           = "Return",
	                     AUSTRALIAN_STORE = "AustraliaStore";

	private const ushort UPLIFT_SELLER_ID = 50146;


	public PmlClient() : base( PML )
	{
	}

	protected PmlClient( string carrierId ) : base( carrierId )
	{
	}

#region Virtuals
#region Urls
	public virtual string ReturnOrderUrl             => Constants.RETURN_ORDER_URL;
	public virtual string ReturnOrderUpdateUrl       => Constants.RETURN_ORDER_UPDATE_URL;
	public virtual string ReturnOrderStatusUpdateUrl => Constants.RETURN_ORDER_STATUS_UPDATE_URL;
#endregion

#region Property Overrides
	public override AUTHORISATION           Authorisation    => ( Constants.CLIENT_ID, Constants.SECRET_CODE );
	public override string                  Account          => PML;
	public override string                  AccountTimeZone  => TimeZones.AUSTRALIAN_EASTERN;
	public override Trip.AUTO_DISPATCH_ZONE AutoDispatchZone => Trip.AUTO_DISPATCH_ZONE.ZONE_4;
#endregion

#region Company
	public override bool UpdatePickupCompany  => true;
	public override bool UpdateBillingCompany => true;
#endregion

	private async Task<string> SendXmlAndGetResponse( HttpRequestMessage message, string xml )
	{
		try
		{
			message.Content                     = new StringContent( xml );
			message.Content.Headers.ContentType = new MediaTypeHeaderValue( "application/xml" );

			var Response = await Client.SendAsync( message );
			return await Response.Content.ReadAsStringAsync();
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return "";
	}


	private async Task<bool> SendXmlCheckSuccess( HttpRequestMessage message, string xml )
	{
		try
		{
			var Response = await SendXmlAndGetResponse( message, xml );
			return Response.FromXmlSuccessResponse() is { IsSuccess: true };
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return false;
	}

	private async Task<bool> SendXmlCheckAccepted( HttpRequestMessage message, string xml )
	{
		try
		{
			var Response = await SendXmlAndGetResponse( message, xml );
			return Response.FromXmlOrderAcceptedResponse() is { IsSuccess: true };
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return false;
	}


#region GetOrders
	public override Task<bool> AcknowledgeTripImported( Trip trip ) => SendXmlCheckSuccess( NewDeleteMessageBase( ReturnOrderUrl ), new DeleteOrders( trip.ReceiptHandle ).ToXml() );

	public override async Task<TripList> ImportTrips( IRequestContext context )
	{
		try
		{
			var Message  = NewGetMessageBase( ReturnOrderUrl );
			var Response = await Client.SendAsync( Message );
			var Content  = await Response.Content.ReadAsStringAsync();
			Content = WebUtility.HtmlDecode( Content ); // To Readable XML

			// Fix the XML
			const string XML_HEADER = "<?xml version='1.0' encoding='UTF-8'?>";

			Content = $"{XML_HEADER}{Content.Replace( XML_HEADER, "" )}";

			var Trips = Content.ToTrips();

			if( Trips.Count > 0 )
			{
				await using var Db = new CarrierDb( Context );

				var InventoryDictionary = Db.GetInventoryDictionary();

				foreach( var Trip in Trips )
				{
					SetTripId( Trip );

					var TripItems = Trip.Packages.SelectMany( p => p.Items ).ToList();

					foreach( var TripItem in TripItems )
					{
						// Fix the description
						if( InventoryDictionary.TryGetValue( TripItem.Barcode, out var Inventory ) )
							TripItem.Description = Inventory.Description;

						var Qty = Db.GetCartonQuantity( TripItem.Barcode );

						if( Qty is > 0 )
						{
							TripItem.IsCarton       = true;
							TripItem.CartonQuantity = Qty.Value;
						}
					}

					new CarrierDb.PmlSalesForce( Trip.TripId, Trip.StringData1, Trip.StringData2, Trip.StringData3, TripItems ).Save( Context );

					// Need to translate the pickup address location barcode to the actual location barcode.
					if( Trip.BoolData1 )
					{
						var Xref = Db.GetBarcodeFromAccountIdXref( Trip.PickupAddressBarcode );

						if( Xref is not null )
							Trip.PickupAddressBarcode = Xref.Id2;

						var (Ok, Company, _) = Db.GetCompanyByLocationBarcode( Trip.PickupAddressBarcode );

						if( Ok )
							Trip.PickupCompanyId = Company.CompanyId;
					}

					var Fee = Trip.ReturnOrderFee;

					if( Fee != 0 )
					{
						var Charge = Db.GetCharge( Constants.RETURN_ORDER_FEE );

						if( Charge is null )
						{
							Charge = new Charge
							         {
								         ChargeId   = Constants.RETURN_ORDER_FEE_ID,
								         Label      = Constants.RETURN_ORDER_FEE,
								         ChargeType = Constants.REORDER_FEE_TYPE,

								         DisplayOnDriversScreen = true,
								         DisplayOnEntry         = true,
								         DisplayOnInvoice       = true,
								         DisplayOnWaybill       = true
							         };

							Db.AddUpdateCharges( [Charge] );
						}

						Trip.TripCharges =
						[
							new TripCharge
							{
								ChargeId = Charge.ChargeId,
								Value    = Fee,
								Quantity = 1,
								Text     = Charge.Label
							}
						];
					}

					// If it has cartons, we need to add the packets.
					var Packages = Trip.Packages; // Deserialise the packages from JSON

					var ToAdd = new Dictionary<string, TripItem>();

					foreach( var Package in Packages )
					{
						var Items = Package.Items;

						void AddItem( TripItem item )
						{
							var Barcode = item.Barcode;

							if( ToAdd.TryGetValue( Barcode, out var Existing ) )
								Existing.Original += item.Original;
							else
								ToAdd.Add( Barcode, item );
						}

						foreach( var Item in Items )
						{
							var Barcode = Item.Barcode;
							var (Ok, CartonDescription, PacketBarcode, PacketDescription, Quantity) = Db.GetPacketFromCarton( Barcode );

							if( Ok )
							{
								Item.IsCarton       = true;
								Item.CartonQuantity = Quantity;
								Item.Description    = CartonDescription;

								AddItem( Item );

								AddItem( new TripItem
								         {
									         ItemCode    = Constants.SERVICE_LEVEL,
									         Barcode     = PacketBarcode,
									         Description = PacketDescription,
									         Reference   = Trip.Reference,
									         Original    = Quantity * Item.Original, // Number of packets in the carton
									         Weight      = 1
								         } );
							}
							else
							{
								if( InventoryDictionary.TryGetValue( Barcode, out var Inventory ) )
									Item.Description = Inventory.Description;

								AddItem( Item );
							}
						}
					}

					//Created the consolidated list of items
					var Itms = ( from I in ToAdd.Values
					             select I ).ToList();

					decimal TotalWeight = 0,
					        TotalPieces = 0;

					foreach( var Item in Itms )
					{
						TotalWeight += Item.Weight;
						TotalPieces += Item.Original;
					}

					Trip.Packages =
					[
						new TripPackage
						{
							PackageType = $"{Constants.SERVICE_LEVEL} ({Trip.Reference})",
							Items       = Itms,
							Weight      = TotalWeight,
							Pieces      = TotalPieces
						}
					];
				}
			}
			return Trips;
		}
		catch( Exception Exception )

		{
			Context.SystemLogException( Exception );
		}
		return [];
	}
#endregion


#region GetDetails
	private static string GetOrderNumber( string tripId ) => tripId.Replace( "IMP", "" ).TrimStart( '0' );

	private static (string SellerId, string Storefront, string OrderNumber, List<TripItem> Items) GetOrderDetails( CarrierDb.PmlSalesForce xref, TripUpdate trip )
	{
		var OrderId = GetOrderNumber( trip.TripId );
		var (_, SellerId, Storefront, _, Items) = xref;
		return ( SellerId, Storefront, OrderId, Items );
	}

	private static (string SellerId, string Storefront, string OrderNumber ) GetOrderDetails( TripExport export, TripUpdate trip )
	{
		var OrderId    = GetOrderNumber( trip.TripId );
		var SellerId   = export.String1;
		var Storefront = export.String2;
		return ( SellerId, Storefront, OrderId );
	}
#endregion

#region Update Orders
	private async Task<(bool Ok, List<string> LogData, string LogDataReference)> FinaliseTrip( CarrierDb.PmlSalesForce xref, TripUpdate trip )
	{
		var ResultXml = new List<string>();

		var (SellerId, Storefront, OrderId, OriginalItems) = GetOrderDetails( xref, trip );

		await using var Db = new CarrierDb( Context );

		var AggregatedPackets = new Dictionary<string, TripItem>();

		var Packages   = trip.Packages;
		var HasUplifts = false;

		// Sum the items and convert cartons to packets
		foreach( var Package in Packages )
		{
			foreach( var Item in Package.Items )
			{
				HasUplifts |= Item.IsUplift();
				var Barcode = Item.Barcode;

				// If it's a carton convert it to packets
				var (Ok, _, Packet, PacketDescription, CartonQuantity) = Db.GetPacketFromCarton( Barcode );

				if( Ok )
				{
					Barcode          = Packet;
					Item.Barcode     = Packet;
					Item.Description = PacketDescription;
					Item.Pieces      = CartonQuantity * Item.Pieces; // Number of packets in the carton
				}

				if( AggregatedPackets.TryGetValue( Barcode, out var Existing ) )
					Existing.Pieces += Item.Pieces;
				else
					AggregatedPackets[ Barcode ] = Item;
			}
		}

		var OriginalValues = ( from O in OriginalItems
		                       select O ).ToDictionary( o => o.Barcode, o => o.Original );

		var AggregatedItems = new List<TripItem>();

		// Rebuild the cartons and packets
		foreach( var (PacketCode, Item) in AggregatedPackets )
		{
			var (Ok, CartonCode, CartonDescription, CartonQuantity) = Db.GetFromCartonPacket( PacketCode );

			if( Ok )
			{
				var (Pieces, Remainder) = Item.Pieces.Divide( CartonQuantity );
				Item.Pieces             = Remainder; // Carton remainder

				var OriginalValue = OriginalValues.GetValueOrDefault( CartonCode, Pieces );

				// Add the carton
				AggregatedItems.Add( new TripItem
				                     {
					                     Barcode     = CartonCode,
					                     ItemCode    = RETURN,
					                     Description = CartonDescription,
					                     Original    = OriginalValue,
					                     Pieces      = Pieces,
					                     Weight      = 1
				                     } );
			}

			if( Item.Pieces > 0 )
			{
				Item.Original = OriginalValues.GetValueOrDefault( PacketCode, Item.Pieces );
				AggregatedItems.Add( Item ); // Add the packets if any
			}
		}

		// Find the items in the original order that are not in the new order
		var MissingItems = new List<TripItem>();

		foreach( var OriginalItem in OriginalItems )
		{
			var Barcode = OriginalItem.Barcode;
			var Found   = false;

			foreach( var AggregatedItem in AggregatedItems )
			{
				if( AggregatedItem.Barcode == Barcode )
				{
					Found = true;
					break;
				}
			}

			if( !Found )
			{
				MissingItems.Add( new TripItem
				                  {
					                  Barcode     = OriginalItem.Barcode,
					                  ItemCode    = RETURN,
					                  Description = OriginalItem.Description,
					                  Original    = OriginalItem.Original,
					                  Pieces      = 0,
					                  Weight      = 1
				                  } );
			}
		}

		// Add the missing items
		AggregatedItems.AddRange( MissingItems );

		var PackageType = $"{Constants.SERVICE_LEVEL} ({OrderId})";

		Packages =
		[
			new TripPackage
			{
				PackageType = PackageType,
				Items       = AggregatedItems,
				Weight      = AggregatedItems.Sum( i => i.Weight ),
				Pieces      = AggregatedItems.Sum( i => i.Pieces )
			}
		];

		trip.Packages = Packages;

		string Xml;

		if( HasUplifts )
		{
			var Gln = Db.GetPmlGln( trip.PickupAddressBarcode );

			var Items = ( from I in AggregatedItems
			              select new requestOrderProduct
			                     {
				                     sku      = $"{SellerId}_{I.Barcode}",
				                     quantity = (ushort)I.Pieces
			                     } ).ToArray();

			var Update = new request
			             {
				             order = new requestOrder
				                     {
					                     sellerId              = UPLIFT_SELLER_ID,
					                     storefront            = AUSTRALIAN_STORE,
					                     customerDistributorId = $"{UPLIFT_SELLER_ID}_{Gln}",
					                     extOrderId            = $"{trip.TripId}",
					                     orderReason           = "Uplift",
					                     products              = Items
				                     }
			             };

			Xml = Update.ToXml();
		}
		else
		{
			var Items = ( from I in AggregatedItems
			              select new Order.Update.requestOrderProduct
			                     {
				                     sku      = $"{SellerId}_{I.Barcode}",
				                     quantity = (ushort)I.Pieces
			                     } ).ToArray();

			var Update = new Order.Update.request
			             {
				             order = new Order.Update.requestOrder
				                     {
					                     sellerId   = ushort.Parse( SellerId ),
					                     storefront = Storefront,
					                     extOrderId = $"{OrderId}",
					                     products   = Items
				                     }
			             };
			Xml = Update.ToXml();
		}

		var Result = await SendXmlCheckAccepted( NewPostMessageBase( ReturnOrderUpdateUrl ), Xml );

		if( Result )
		{
			ResultXml.Add( Xml );

			if( !HasUplifts )
			{
				static bool QuantitiesDifferent( Dictionary<string, decimal> originalQuantities, List<TripItem> agreatedItems )
				{
					foreach( var Item in agreatedItems )
					{
						if( originalQuantities.TryGetValue( Item.Barcode, out var Original ) && ( Original != Item.Pieces ) )
							return true;
					}
					return false;
				}

				static bool HasExtra( Dictionary<string, decimal> originalQuantities, List<TripItem> agreatedItems )
				{
					foreach( var Item in agreatedItems )
					{
						if( !originalQuantities.ContainsKey( Item.Barcode ) )
							return true;
					}
					return false;
				}

				bool Adjusted()
				{
					return QuantitiesDifferent( OriginalValues, AggregatedItems ) || HasExtra( OriginalValues, AggregatedItems );
				}

				if( Adjusted() )
				{
					( Result, Xml ) = await OrderAdjusted( SellerId, Storefront, OrderId );

					if( Result )
						ResultXml.Add( Xml );
				}
				else
				{
					var Confirmed = await OrderConfirmed( SellerId, Storefront, OrderId );

					if( Confirmed.Ok )
					{
						ResultXml.Add( Confirmed.Xml );
						Result = true;
					}
				}

				if( Result )
				{
					( _, Result ) = Db.AddUpdateTrip( trip ); // Update the package details

					if( Result )
						Db.FinaliseTrip( Constants.PML_EXPORT, trip.TripId );

					CarrierDb.PmlSalesForce.Delete( Context, trip );
				}
			}
		}
		return ( Result, ResultXml, OrderId );
	}

	private static bool IsImportedTrip( Trip? trip ) => trip is { Status4: STATUS3.IMPORTED };

	public override async Task<(bool Ok, List<string> LogData, string LogDataReference)> ExportTripStatus( IRequestContext context, TripExport export, Trip? trip )
	{
		if( IsImportedTrip( trip ) )
		{
			var TripId = trip!.TripId;

			string SellerId, Storefront, OrderId;

			if( export.Status is STATUS.DELETED )
				( SellerId, Storefront, OrderId ) = GetOrderDetails( export, trip );
			else
			{
				var Xref = CarrierDb.PmlSalesForce.Get( context, TripId );

				if( Xref is null )
					return ( true, [], "" );

				( SellerId, Storefront, OrderId, _ ) = GetOrderDetails( Xref, trip );
			}

			var PmlTrip = new PmlTripExport( export );

			if( PmlTrip.IsFutile || trip.Status1 is STATUS.DELETED or STATUS.LIMBO )
			{
				var Result = await OrderRejected( SellerId, Storefront, OrderId );

				if( Result.Ok )
					return ( true, [Result.Xml], OrderId );
			}
		}
		return ( true, [], "" );
	}

	public override Task<(bool Ok, List<string> LogData, string LogDataReference)> ExportTrip( IRequestContext context, TripExport export, Trip? trip )
	{
		if( IsImportedTrip( trip ) )
		{
			var Xref = CarrierDb.PmlSalesForce.Get( context, trip!.TripId );

			if( Xref is not null )
			{
				switch( export.SubType )
				{
				case TripExport.TRIP_DATA_SUB_TYPES.UPDATE when trip.Status1 is STATUS.PICKED_UP or STATUS.LIMBO:
					return FinaliseTrip( Xref, trip );
				}
			}
		}
		return Task.FromResult( ( true, new List<string>(), "" ) );
	}
#endregion

#region Status Updates
	private async Task<(bool Ok, string Xml)> UpdateStatus( ushort sellerId, string storefront, string orderNumber, string status )
	{
		var Update = new OrderUpdates
		             {
			             OrderStatus = new OrderUpdatesOrderStatus
			                           {
				                           SellerId    = sellerId,
				                           Storefront  = storefront,
				                           OrderNumber = orderNumber,
				                           Status      = status
			                           }
		             };
		var Xml = Update.ToXml();
		var Ok  = await SendXmlCheckAccepted( NewPostMessageBase( ReturnOrderStatusUpdateUrl ), Xml );
		return ( Ok, Xml );
	}

	private Task<(bool Ok, string Xml)> UpdateStatus( string sellerId, string storefront, string orderNumber, string status ) => UpdateStatus( ushort.Parse( sellerId ), storefront, orderNumber, status );

	private Task<(bool Ok, string Xml)> Reconcile( string sellerId, string storefront, string orderNumber, Constants.RECONCILING status )
	{
		try
		{
			return UpdateStatus( sellerId, storefront, orderNumber, status switch
			                                                        {
				                                                        Constants.RECONCILING.RECONCILING_CONFIRMED => Constants.RECONCILING_CONFIRMED,
				                                                        Constants.RECONCILING.RECONCILING_ADJUSTED  => Constants.RECONCILING_ADJUSTED,
				                                                        Constants.RECONCILING.RECONCILING_REJECTED  => Constants.RECONCILING_REJECTED,
				                                                        _                                           => throw new ArgumentOutOfRangeException( nameof( status ), status, null )
			                                                        } );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return Task.FromResult( ( false, "" ) );
	}

	private Task<(bool Ok, string Xml)> OrderRejected( string sellerId, string storefront, string orderNumber ) => Reconcile( sellerId, storefront, orderNumber, Constants.RECONCILING.RECONCILING_REJECTED );
	private Task<(bool Ok, string Xml)> OrderConfirmed( string sellerId, string storefront, string orderNumber ) => Reconcile( sellerId, storefront, orderNumber, Constants.RECONCILING.RECONCILING_CONFIRMED );
	private Task<(bool Ok, string Xml)> OrderAdjusted( string sellerId, string storefront, string orderNumber ) => Reconcile( sellerId, storefront, orderNumber, Constants.RECONCILING.RECONCILING_ADJUSTED );
#endregion

#region Messages
	private HttpRequestMessage NewMessage( HttpMethod method, string uri )
	{
		var Auth = Authorisation;

		return new HttpRequestMessage
		       {
			       RequestUri = new Uri( uri ),
			       Method     = method,
			       Headers =
			       {
				       { Constants.CLIENT_ID_KEY, Auth.ClientId },
				       { Constants.SECRET_CODE_KEY, Auth.SecretCode },
				       { Constants.CHANNEL_KEY, Constants.CHANNEL_VALUE }
			       }
		       };
	}

	public HttpRequestMessage NewMessageBase( HttpMethod method, string uri ) => NewMessage( method, uri );
	public HttpRequestMessage NewGetMessageBase( string uri ) => NewMessageBase( HttpMethod.Get, uri );
	public HttpRequestMessage NewDeleteMessageBase( string uri ) => NewMessageBase( HttpMethod.Delete, uri );
	public HttpRequestMessage NewPostMessageBase( string uri ) => NewMessageBase( HttpMethod.Post, uri );
#endregion
#endregion
}