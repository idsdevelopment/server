﻿namespace Imports.Pml;

internal class PmlPlayClient : PmlTestClient
{
	private const   string PML_PLAY = "PmlPlay";
	public override string Account => PML_PLAY;

#region Urls
	public override string ReturnOrderUrl             => Constants.TESTING_RETURN_ORDER_URL;
	public override string ReturnOrderUpdateUrl       => Constants.TESTING_RETURN_ORDER_UPDATE_URL;
	public override string ReturnOrderStatusUpdateUrl => Constants.TESTING_RETURN_ORDER_STATUS_UPDATE_URL;
#endregion

	public override AUTHORISATION Authorisation => ( Constants.TESTING_CLIENT_ID, Constants.TESTING_SECRET_CODE );

	protected override HttpClient Client => SslNoCheckClient;

	public PmlPlayClient() : base( PML_PLAY )
	{
	}
}