﻿global using Database.Model.Databases.Carrier;
global using Interfaces.Interfaces;
global using Protocol.Data;
global using Utils;
global using System.Net;
global using Imports.Pml.Order;
global using StorageV2.Logs;
global using System.Xml.Serialization;
global using System.Net.Http.Headers;

// Global Types
global using AUTHORISATION = (string ClientId, string SecretCode);