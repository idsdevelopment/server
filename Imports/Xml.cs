﻿using System.Diagnostics;
using System.Text;
using System.Xml;

public class Xml
{
	public static T? ConvertXmlStringToObject<T>( string xmlString ) where T : class
	{
		try
		{
			var       Serializer   = new XmlSerializer( typeof( T ) );
			using var StringReader = new StringReader( xmlString );
			return (T)Serializer.Deserialize( StringReader )!;
		}
		catch( InvalidOperationException E )
		{
			// Log or print the exception message
			Debug.WriteLine( E.Message );
		}
		return default;
	}

	public static string ConvertObjectToXml<T>( T obj ) where T : notnull
	{
		try
		{
			var Serializer = new XmlSerializer( typeof( T ) );
			var Namespaces = new XmlSerializerNamespaces();
			Namespaces.Add( string.Empty, string.Empty ); // Disable the xmlns:xsi and xmlns:xsd lines.

			var Settings = new XmlWriterSettings
			               {
				               Encoding           = Encoding.UTF8,
				               Indent             = true,
				               OmitXmlDeclaration = false
			               };

			using var MemoryStream = new MemoryStream();

			using( var Writer = XmlWriter.Create( MemoryStream, Settings ) )
				Serializer.Serialize( Writer, obj, Namespaces );

			var Result = Encoding.UTF8.GetString( MemoryStream.ToArray() );
			return Result;
		}
		catch( InvalidOperationException E )
		{
			Debug.WriteLine( E.Message );
		}
		return "";
	}
}