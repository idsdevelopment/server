﻿using Database.Model.Database;
using Address = Protocol.Data.Address;
using Company = Protocol.Data.Company;

// ReSharper disable InconsistentNaming

namespace Imports;

// Only one HttpClient
internal class InternalClient
{
	protected static HttpClient Client
	{
		get { return _Client ??= new HttpClient(); }
	}

	protected static HttpClient SslNoCheckClient
	{
		get { return _SslNoCheckClient ??= new HttpClient( new CustomHttpMessageHandler() ); }
	}

	private static HttpClient? _Client, _SslNoCheckClient;

	public class CustomHttpMessageHandler : DelegatingHandler
	{
		public CustomHttpMessageHandler()
		{
			InnerHandler = new HttpClientHandler
			               {
				               ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
			               };
		}
	}
}

internal abstract class AIClient : InternalClient, IClient
{
	public const int    DEFAULT_RUN_INTERVAL = 2;
	public const string IMPORT_PREFIX        = "IMP";

	protected new virtual HttpClient Client => InternalClient.Client;

	internal IRequestContext Context { get; private set; } = null!;

	protected readonly string        CarrierId;
	private readonly   SemaphoreSlim Semaphore = new( 1 );

	internal bool ImportEnabled,
	              ExportEnabled;

	protected AIClient( string carrierId )
	{
		CarrierId                 = carrierId;
		( _LastRun, _NextRunUtc ) = Users.ImportExport.GetRunTimes( carrierId );
	}

	public virtual bool NeedToBroadcastImport( Trip trip ) => true;
	public virtual bool NeedToBroadcastExport( Trip trip ) => true;

	public async Task Lock()
	{
		await Semaphore.WaitAsync();
	}

	public void Release()
	{
		Semaphore.Release();
	}

	public async Task Export()
	{
		BuildContext();

		if( Context.CarrierServicePreferences.Export )
		{
			await using var Db = new CarrierDb( Context );
			var             E  = Db.Entity;

			var Exports = ( from Ex in E.Exports
			                where Ex.DataType == (int)Database.Model.Databases.MasterTemplate.Export.DATA_TYPES.TRIP_UPDATE
			                select Ex ).ToList()
			                           .Select( e => new TripExport( e ) )
			                           .ToList();

			foreach( var Export in Exports )
			{
				var DbTrips = Db.SearchTrips( new SearchTrips
				                              {
					                              ByTripId = true,
					                              TripId   = Export.Code
				                              } ).FirstOrDefault();

				var Trip = DbTrips is not null ? new Trip( DbTrips ) : null;

				var Result = await ( Export is { SubType: TripExport.TRIP_DATA_SUB_TYPES.STATUS }
					                     ? ExportTripStatus( Context, Export, Trip )
					                     : ExportTrip( Context, Export, Trip ) );

				if( Result.Ok )
				{
					if( Result.LogData.Count > 0 )
					{
						var Flattened = string.Join( "\r\n\r\n", Result.LogData ).Trim();

						if( Flattened != "" )
						{
							var LogData = Result.LogDataReference.IsNotNullOrWhiteSpace()
								              ? $"""
								                 Reference: {Result.LogDataReference}
								                 --------------------------------------
								                 {Flattened}

								                 """
								              : Flattened;

							ExportLog.Append( Program, Context.CarrierId, DateTimeOffset.UtcNow, LogData );
						}
					}

					var Rec = ( from Ex in E.Exports
					            where ( Ex.DataType == (int)Database.Model.Databases.MasterTemplate.Export.DATA_TYPES.TRIP_UPDATE )
					                  && ( Ex.Code == Export.Code )
					            select Ex ).FirstOrDefault();

					if( Rec is not null )
					{
						E.Exports.Remove( Rec );
						await E.SaveChangesAsync();

						if( Trip is not null && NeedToBroadcastExport( Trip ) )
						{
							Trip.BroadcastToDriver        = true;
							Trip.BroadcastToDriverBoard   = true;
							Trip.BroadcastToDispatchBoard = true;

							Trip.Broadcast( Context );
						}
					}
				}
			}
		}
	}

	public static void SetTripId( Trip trip )
	{
		if( trip.TripId.IsNullOrWhiteSpace() )
		{
			trip.TripId = trip.TripIdBase.IsNullOrWhiteSpace()
				              ? Database.Model.Databases.MasterTemplate.Trip.NextTripId( IMPORT_PREFIX )
				              : $"{IMPORT_PREFIX}{trip.TripIdBase}";
		}
	}

	public async Task Import()
	{
		var Offset = BuildContext();

		if( Context.CarrierServicePreferences.Import )
		{
			var Trips = await ImportTrips( Context );

			if( Trips.Count > 0 )
			{
				var DefaultTime = Offset.DateTimeATimeOffset();

				// Make Sure the quantities balance
				foreach( var Trip in Trips )
				{
					decimal TotalTripOriginalPieces = 0,
					        TotalTripWeight         = 0,
					        TotalTripValue          = 0;

					if( Trip.ReadyTime == DateTimeOffset.MinValue )
					{
						Trip.ReadyTime          = DefaultTime;
						Trip.ReadyTimeSpecified = true;
					}

					if( Trip.DueTime == DateTimeOffset.MinValue )
					{
						Trip.DueTime          = DefaultTime;
						Trip.DueTimeSpecified = true;
					}

					var Packages = Trip.Packages; // Deserialize the packages from JSON

					foreach( var Package in Packages )
					{
						decimal TotalItemOriginalPieces = 0,
						        TotalItemWeight         = 0,
						        TotalItemValue          = 0;

						foreach( var Item in Package.Items )
						{
							Item.Pieces             =  Item.Original;
							TotalItemOriginalPieces += Item.Original;
							TotalItemWeight         += Item.Weight;
							TotalItemValue          += Item.Value;
						}

						Package.Pieces   = TotalItemOriginalPieces;
						Package.Original = TotalItemOriginalPieces;
						Package.Weight   = TotalItemWeight;
						Package.Value    = TotalItemValue;

						TotalTripOriginalPieces += TotalItemOriginalPieces;
						TotalTripWeight         += TotalItemWeight;
						TotalTripValue          += TotalItemValue;
					}

					Trip.Packages = Packages; // Serialize the packages to JSON

					Trip.Pieces             = TotalTripOriginalPieces;
					Trip.OriginalPieceCount = TotalTripOriginalPieces;
					Trip.Weight             = TotalTripWeight;
					Trip.TotalAmount        = TotalTripValue;
				}

				if( Trips.LogData.IsNotNullOrWhiteSpace() )
				{
					var LogData = Trips.LogDataReference.IsNotNullOrWhiteSpace()
						              ? $"""
						                 Reference: {Trips.LogDataReference}
						                 --------------------------------------
						                 {Trips.LogData}

						                 """
						              : Trips.LogData;

					ImportLog.Append( Program, Context.CarrierId, DefaultTime, LogData );
				}

				await using var Db = new CarrierDb( Context );

				foreach( var Trip in Trips )
				{
					Trip.Status4 = STATUS3.IMPORTED;

					SetTripId( Trip );

					Trip.AutoDispatch( Db, AutoDispatchZone, Trip.DoNotSendToDriver );
					Trip.Status3 &= ~STATUS2.DONT_SEND_TO_DRIVER;

					if( UpdatePickupCompany )
					{
						UpdateAccount( Db,
						               Trip.PickupAccountId, Trip.PickupAddressBarcode, Trip.PickupCompanyName,
						               Trip.PickupAddressSuite, Trip.PickupAddressAddressLine1, Trip.PickupAddressAddressLine2,
						               Trip.PickupAddressCity, Trip.PickupAddressRegion, Trip.PickupAddressPostalCode,
						               Trip.PickupAddressCountry, Trip.PickupAddressCountryCode, Trip.PickupAddressNotes );
					}

					if( UpdateDeliveryCompany )
					{
						UpdateAccount( Db,
						               Trip.DeliveryAccountId, Trip.DeliveryAddressBarcode, Trip.DeliveryCompanyName,
						               Trip.DeliveryAddressSuite, Trip.DeliveryAddressAddressLine1, Trip.DeliveryAddressAddressLine2,
						               Trip.DeliveryAddressCity, Trip.DeliveryAddressRegion, Trip.DeliveryAddressPostalCode,
						               Trip.DeliveryAddressCountry, Trip.DeliveryAddressCountryCode, Trip.DeliveryAddressNotes );
					}

					if( UpdateBillingCompany )
					{
						UpdateAccount( Db,
						               Trip.BillingAccountId, Trip.BillingAddressBarcode, Trip.BillingCompanyName,
						               Trip.BillingAddressSuite, Trip.BillingAddressAddressLine1, Trip.BillingAddressAddressLine2,
						               Trip.BillingAddressCity, Trip.BillingAddressRegion, Trip.BillingAddressPostalCode,
						               Trip.BillingAddressCountry, Trip.BillingAddressCountryCode, Trip.BillingAddressNotes );
					}

					await AddUpdateTrip( Db, Trip );

					if( NeedToBroadcastImport( Trip ) )
					{
						if( Trip.Status1 == STATUS.DISPATCHED )
						{
							Trip.BroadcastToDriver      = true;
							Trip.BroadcastToDriverBoard = true;
						}
						else
							Trip.BroadcastToDispatchBoard = true;

						Trip.Broadcast( Context );
					}
				}
			}
		}
	}

	private int BuildContext()
	{
		var Offset = TimeZoneOffset;

		// ReSharper disable once SuggestVarOrType_SimpleTypes
		IRequestContext? Ctx = Context;

		// ReSharper disable once ConditionIsAlwaysTrueOrFalseAccordingToNullableAPIContract
		if( Ctx is null )
		{
			var Debug = Account.StartsWith( "TEST", StringComparison.OrdinalIgnoreCase )
			            || Account.EndsWith( "TEST", StringComparison.InvariantCultureIgnoreCase );

			Context = RequestContext.RequestContext.ToRequestContext( Account, UserName, Password, Offset, Debug );
		}

		return Offset;
	}

#region Timezone Offset
	private int _TimeZoneOffset = int.MinValue;

	protected int TimeZoneOffset
	{
		get
		{
			if( _TimeZoneOffset == int.MinValue )
				_TimeZoneOffset = AccountTimeZone.TimeZoneToHoursOffset();

			return _TimeZoneOffset;
		}
	}
#endregion

#region Program
	private string? _Program;

	public string Program
	{
		get { return _Program ??= $"{Account} Import"; }
	}
#endregion

#region Virtuals / Abstracts
	public virtual AUTHORISATION Authorisation        => ( UserName, Password );
	public virtual uint          RunIntervalInMinutes => DEFAULT_RUN_INTERVAL;

#region Next Run
	private DateTime _NextRunUtc;

	public DateTime NextRunUtc
	{
		get => _NextRunUtc;
		set
		{
			_NextRunUtc = value;
			Users.ImportExport.UpdateRunTimes( Account, value, _LastRun );
		}
	}
#endregion

#region Last Run
	private DateTime _LastRun;

	public DateTime LastRun
	{
		get => _LastRun;
		set
		{
			_LastRun = value;
			Users.ImportExport.UpdateRunTimes( Account, _NextRunUtc, value );
		}
	}
#endregion

	public virtual async Task<bool> AcknowledgeTripImported( Trip trip ) => await new ValueTask<bool>( true );

	public virtual async Task AddUpdateTrip( CarrierDb db, Trip trip )
	{
		TripUpdate? Update = null;

		try
		{
			try
			{
				Update = new TripUpdate( trip )
				         {
					         Program = Program
				         };

				db.AddUpdateTrip( Update );

				if( !await AcknowledgeTripImported( trip ) )
				{
					Context.SystemLogExceptionProgram( Program, new Exception( $"""
					                                                            Failed to acknowledge imported trip.
					                                                               TripId : {Update.TripId}
					                                                            Reference : {( trip.TrackingReference.IsNotNullOrWhiteSpace() ? trip.TrackingReference : trip.Reference )}
					                                                            """ )
					                                 );
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogExceptionProgram( Program, Update is not null
					                                            ? new Exception( $"TripId: {Update.TripId}", Exception )
					                                            : Exception );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	private void UpdateAccount( CarrierDb db,
	                            string customerCode, string barcode,
	                            string companyName, string suite, string addressLine1, string addressLine2, string city, string region, string postalCode, string country, string countryCode,
	                            string notes )
	{
		try
		{
			var Co = new AddUpdatePrimaryCompany( Program )
			         {
				         CustomerCode       = customerCode,
				         UserName           = customerCode,
				         SuggestedLoginCode = customerCode,
				         Password           = "",
				         Company = new Company( new Address
				                                {
					                                Barcode = barcode,

					                                Suite        = suite,
					                                AddressLine1 = addressLine1,
					                                AddressLine2 = addressLine2,

					                                City        = city,
					                                Region      = region,
					                                Country     = country,
					                                CountryCode = countryCode,
					                                PostalCode  = postalCode,

					                                Notes = notes
				                                } )
				                   {
					                   CompanyName     = companyName,
					                   LocationBarcode = barcode
				                   }
			         };
			db.AddUpdatePrimaryCompany( Co );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}


	public virtual Trip.AUTO_DISPATCH_ZONE AutoDispatchZone => Trip.AUTO_DISPATCH_ZONE.NONE;

	public virtual Task<TripList> ImportTrips( IRequestContext context ) => Task.FromResult<TripList>( [] );

	public virtual Task<(bool Ok, List<string> LogData, string LogDataReference)> ExportTrip( IRequestContext context, TripExport export, Trip? trip ) => Task.FromResult( ( true, new List<string>(), "" ) );
	public virtual Task<(bool Ok, List<string> LogData, string LogDataReference)> ExportTripStatus( IRequestContext context, TripExport export, Trip? trip ) => Task.FromResult( ( true, new List<string>(), "" ) );

#region Ids
	public abstract string AccountTimeZone { get; }

#region Companies
	public virtual bool UpdatePickupCompany   => false;
	public virtual bool UpdateDeliveryCompany => false;
	public virtual bool UpdateBillingCompany  => false;
#endregion

	public virtual string Account  => CarrierId;
	public virtual string UserName => RequestContext.RequestContext.IDS_USER;
	public virtual string Password => RequestContext.RequestContext.WEB_SERVICE_PASSWORD;
#endregion
#endregion
}