﻿namespace Imports;

internal interface IClient
{
	AUTHORISATION Authorisation { get; }

	Task<TripList> ImportTrips( IRequestContext context );

#region Ids
	string Account  { get; }
	string Password { get; }
	string UserName { get; }

	string AccountTimeZone { get; }
#endregion

#region Companies
	bool UpdatePickupCompany   { get; }
	bool UpdateDeliveryCompany { get; }
	bool UpdateBillingCompany  { get; }
#endregion

#region Trips
	Task<bool> AcknowledgeTripImported( Trip trip );
	Task AddUpdateTrip( CarrierDb db, Trip trip );

	Trip.AUTO_DISPATCH_ZONE AutoDispatchZone { get; }
#endregion

#region Run Interval
	uint     RunIntervalInMinutes { get; }
	DateTime NextRunUtc           { get; set; }
#endregion
}