﻿namespace Imports;

public class TripList : List<Trip>
{
	public string LogData          = "",
	              LogDataReference = "";

	public TripList( IEnumerable<Trip> trips ) : base( trips )
	{
	}

	public TripList()
	{
	}

	public TripList( IEnumerable<Protocol.Data.Trip> trips )
	{
		AddRange( trips.Select( t => new Trip( t ) ) );
	}
}

public class Trip : Protocol.Data.Trip
{
	public bool BoolData1,
	            BoolData2,
	            BoolData3;

	public string MessageId         = "",
	              ReceiptHandle     = "",
	              TrackingReference = "",
	              TripIdBase        = "", // Suffix for TripId
	              //
	              StringData1 = "",
	              StringData2 = "",
	              StringData3 = "";

	public decimal ReturnOrderFee = 0;

	public Trip( Protocol.Data.Trip t ) : base( t )
	{
	}

	public Trip()
	{
	}

	public enum AUTO_DISPATCH_ZONE
	{
		NONE,
		ZONE_1,
		ZONE_2,
		ZONE_3,
		ZONE_4
	}

#if !SERVER_TESTER
	public void Broadcast( IRequestContext context )
	{
		if( Status1 == STATUS.DISPATCHED )
			Status2 = STATUS1.UNSET;

		context.Broadcast( this );
	}

	public void AutoDispatch( CarrierDb db, AUTO_DISPATCH_ZONE zone, bool dontDispatch )
	{
		var E = db.Entity;

		// Auto dispatch
		var PostalRec = ( from P in E.PostalCodeToRoutes
		                  where P.PostalCode == PickupAddressPostalCode
		                  select P ).FirstOrDefault();

		if( PostalRec is not null )
		{
			if( !dontDispatch )
			{
				var DriverName = ( from R in E.Routes
				                   where R.RouteId == PostalRec.RouteId
				                   select R.Name ).FirstOrDefault();

				if( DriverName is not null )
				{
					Status1 = STATUS.DISPATCHED;
					Driver  = DriverName;

					BroadcastToDispatchBoard = false;
					BroadcastToDriver        = true;
					BroadcastToDriverBoard   = true;
				}
			}

			PickupZone = zone switch
			             {
				             AUTO_DISPATCH_ZONE.ZONE_1 => PostalRec.Zone1,
				             AUTO_DISPATCH_ZONE.ZONE_2 => PostalRec.Zone2,
				             AUTO_DISPATCH_ZONE.ZONE_3 => PostalRec.Zone3,
				             AUTO_DISPATCH_ZONE.ZONE_4 => PostalRec.Zone4,
				             _                         => PickupZone
			             };
		}

		if( PickupZone.Trim() == "" )
			PickupZone = "UNKNOWN";
	}
#endif
}