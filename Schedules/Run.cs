﻿using Database.Model.Databases.MasterTemplate;
using Interfaces.Abstracts;
using Logs;
using Schedule = Protocol.Data.Schedule;
using Trip = Database.Model.Databases.MasterTemplate.Trip;
#if NET5_0_OR_GREATER
using Database.Model.Database;
#endif

namespace Schedules;

public static partial class Users
{
	private static bool InSchedule;

	private static readonly object LockObject = new();

	public static void Run()
	{
		Monitor.Enter( LockObject );

		if( !InSchedule )
		{
			InSchedule = true;

			Monitor.Exit( LockObject );

			try
			{
				var Now = DateTimeOffset.UtcNow;

				using var Db    = new UsersEntities();
				var       Sheds = Db.Entity.Schedules;

				var Recs = ( from S in Sheds
				             where ( S.TriggerType != (byte)Schedule.TRIGGER_TYPE.DISABLED ) && ( S.NextRun <= Now )
				             select S ).ToList();

				if( Recs.Count > 0 )
				{
					var Schedules = ( from R in Recs
					                  group R by R.CarrierId
					                  into G
					                  select G ).ToDictionary( g => g.Key, g => g.ToList() );

					foreach( var CarrierId in Schedules.Keys )
					{
						var CarrierContext = AIRequestInterface.NewRequestContext( CarrierId );

						if( CarrierContext is not null )
						{
							using var CarrierDb = new CarrierDb( CarrierContext );
							var       E         = CarrierDb.Entity;

							var Drivers = new HashSet<string>( from D in CarrierDb.GetDrivers()
							                                   select D.StaffId );

							foreach( var Schedule in Schedules[ CarrierId ] )
							{
								var TripId = Schedule.TripId;

								var Rec = ( from T in E.Trips
								            where T.TripId == TripId
								            select T ).FirstOrDefault();

								if( Rec is not null )
								{
									var NewRec = Rec.Clone();

									var NewTripId = Trip.NextTripId( CarrierContext );
									NewRec.TripId = NewTripId;

									var CallTime = Now.UtcToClient( Schedule.ClientTimeZoneId );
									NewRec.CallTime = CallTime;

									var TimeString = $"{CallTime:F}";
									NewRec.Schedule = $"Created from template id: {TripId} on the {TimeString}";

									var Driver = Schedule.Driver;

									if( Driver.IsNotNullOrWhiteSpace() )
									{
										if( Driver.Contains( " - " ) )
										{
											var Parts = Driver.Split( " - " );
											Driver = Parts[ 0 ].Trim();
										}

										if( !Drivers.Contains( Driver, StringComparison.OrdinalIgnoreCase ) )
											Driver = "";
									}
									else
										Driver = "";

									NewRec.DriverCode = Driver;
									NewRec.Status1    = (int)( Driver.IsNotNullOrWhiteSpace() ? STATUS.DISPATCHED : STATUS.ACTIVE );

									void UpdateTrip( Trip trip )
									{
										while( true )
										{
											var (RetryTrip, Ok) = CarrierDb.UpdateTrip( trip );

											if( !Ok && RetryTrip is not null )
											{
												trip = RetryTrip;
												continue;
											}
											return;
										}
									}

									E.Trips.Add( NewRec );
									UpdateTrip( NewRec );

									const string SCHEDULER = "Scheduler";

									if( NewRec.Status1 == (int)STATUS.DISPATCHED )
									{
										CarrierContext.Broadcast( new TripUpdateMessage
										                          {
											                          Program    = SCHEDULER,
											                          Driver     = Driver,
											                          Status1    = (STATUS1)NewRec.Status1,
											                          Status2    = (STATUS2)NewRec.Status2,
											                          TripIdList = [NewTripId],
											                          Action     = TripUpdateMessage.ACTION.UPDATE_TRIP
										                          } );
									}

									CarrierDb.LogTrip( SCHEDULER, NewRec );

									var Before = new CarrierDb.TripLogger( Rec );
									Rec.Schedule = $"New shipment create : {NewTripId} on the {TimeString}";
									UpdateTrip( Rec );
									var After = new CarrierDb.TripLogger( Rec );
									CarrierDb.LogTrip( SCHEDULER, Before, After );

									switch( (Schedule.TRIGGER_TYPE)Schedule.TriggerType )
									{
									case Protocol.Data.Schedule.TRIGGER_TYPE.ALWAYS:
										Schedule.NextRun = Schedule.NextRun.AddDays( 1 );
										Schedule.NextRun = Schedule.NextRun( true );
										Db.SaveChanges();
										break;

									case Protocol.Data.Schedule.TRIGGER_TYPE.ON_DAY_AND_DELETE_SCHEDULE_AND_TRIP:
									case Protocol.Data.Schedule.TRIGGER_TYPE.ONCE_AND_DELETE_SCHEDULE_AND_TRIP:
										Sheds.Remove( Schedule );
										Db.SaveChanges();
										DeleteTrip( CarrierContext, TripId );
										break;

									case Protocol.Data.Schedule.TRIGGER_TYPE.ON_DAY_AND_DELETE_SCHEDULE:
									case Protocol.Data.Schedule.TRIGGER_TYPE.ONCE_AND_DELETE_SCHEDULE:
										Sheds.Remove( Schedule );
										Db.SaveChanges();
										break;
									}
								}
								else // Not Trip Available
								{
									Sheds.Remove( Schedule );
									Db.SaveChanges();
								}
							}
						}
					}
				}
			}
			catch( Exception Exception )
			{
				new SystemLog( "Schedule Error", Exception.Message );
			}
			finally
			{
				lock( LockObject )
					InSchedule = false;
			}
		}
		else
			Monitor.Exit( LockObject );
	}
}