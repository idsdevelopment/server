﻿using Database.Model.Database;
using Schedule = Database.Model.Databases.Users.Schedule;
using Trip = Database.Model.Databases.MasterTemplate.Trip;

namespace Schedules;

public static class Extensions
{
	public static DateTimeOffset UtcMidnight( this DateTimeOffset dateTime ) => new( dateTime.Year, dateTime.Month, dateTime.Day, 23, 59, 59, 999, TimeSpan.Zero );

	public static Schedule ToDbSchedule( this Protocol.Data.Schedule schedule ) => new()
	                                                                               {
		                                                                               TriggerType      = (byte)schedule.TriggerType,
		                                                                               ClientTimeZoneId = schedule.ClientTimeZoneId,
		                                                                               Driver           = schedule.Driver,
		                                                                               TripId           = schedule.TripId,
		                                                                               Hour             = schedule.Hour,
		                                                                               Minute           = schedule.Minute,
		                                                                               January          = schedule.January,
		                                                                               February         = schedule.February,
		                                                                               March            = schedule.March,
		                                                                               April            = schedule.April,
		                                                                               May              = schedule.May,
		                                                                               June             = schedule.June,
		                                                                               July             = schedule.July,
		                                                                               August           = schedule.August,
		                                                                               September        = schedule.September,
		                                                                               October          = schedule.October,
		                                                                               November         = schedule.November,
		                                                                               December         = schedule.December,
		                                                                               Monday1          = schedule.Monday1,
		                                                                               Monday2          = schedule.Monday2,
		                                                                               Monday3          = schedule.Monday3,
		                                                                               Monday4          = schedule.Monday4,
		                                                                               Monday5          = schedule.Monday5,
		                                                                               Tuesday1         = schedule.Tuesday1,
		                                                                               Tuesday2         = schedule.Tuesday2,
		                                                                               Tuesday3         = schedule.Tuesday3,
		                                                                               Tuesday4         = schedule.Tuesday4,
		                                                                               Tuesday5         = schedule.Tuesday5,
		                                                                               Wednesday1       = schedule.Wednesday1,
		                                                                               Wednesday2       = schedule.Wednesday2,
		                                                                               Wednesday3       = schedule.Wednesday3,
		                                                                               Wednesday4       = schedule.Wednesday4,
		                                                                               Wednesday5       = schedule.Wednesday5,
		                                                                               Thursday1        = schedule.Thursday1,
		                                                                               Thursday2        = schedule.Thursday2,
		                                                                               Thursday3        = schedule.Thursday3,
		                                                                               Thursday4        = schedule.Thursday4,
		                                                                               Thursday5        = schedule.Thursday5,
		                                                                               Friday1          = schedule.Friday1,
		                                                                               Friday2          = schedule.Friday2,
		                                                                               Friday3          = schedule.Friday3,
		                                                                               Friday4          = schedule.Friday4,
		                                                                               Friday5          = schedule.Friday5,
		                                                                               Saturday1        = schedule.Saturday1,
		                                                                               Saturday2        = schedule.Saturday2,
		                                                                               Saturday3        = schedule.Saturday3,
		                                                                               Saturday4        = schedule.Saturday4,
		                                                                               Saturday5        = schedule.Saturday5,
		                                                                               Sunday1          = schedule.Sunday1,
		                                                                               Sunday2          = schedule.Sunday2,
		                                                                               Sunday3          = schedule.Sunday3,
		                                                                               Sunday4          = schedule.Sunday4,
		                                                                               Sunday5          = schedule.Sunday5
	                                                                               };

	public static Schedule ToDbSchedule( this Protocol.Data.Schedule schedule, string carrierId )
	{
		var S = schedule.ToDbSchedule();
		S.CarrierId = carrierId;
		return S;
	}

	public static Protocol.Data.Schedule ToSchedule( this Schedule schedule ) => new()
	                                                                             {
		                                                                             Id               = schedule.Id,
		                                                                             TriggerType      = (Protocol.Data.Schedule.TRIGGER_TYPE)schedule.TriggerType,
		                                                                             ClientTimeZoneId = schedule.ClientTimeZoneId.Trim(),
		                                                                             Date             = schedule.NextRun,
		                                                                             Driver           = schedule.Driver,
		                                                                             TripId           = schedule.TripId,
		                                                                             Hour             = schedule.Hour,
		                                                                             January          = schedule.January,
		                                                                             February         = schedule.February,
		                                                                             March            = schedule.March,
		                                                                             April            = schedule.April,
		                                                                             May              = schedule.May,
		                                                                             June             = schedule.June,
		                                                                             July             = schedule.July,
		                                                                             August           = schedule.August,
		                                                                             September        = schedule.September,
		                                                                             October          = schedule.October,
		                                                                             November         = schedule.November,
		                                                                             December         = schedule.December,
		                                                                             Minute           = schedule.Minute,
		                                                                             Monday1          = schedule.Monday1,
		                                                                             Monday2          = schedule.Monday2,
		                                                                             Monday3          = schedule.Monday3,
		                                                                             Monday4          = schedule.Monday4,
		                                                                             Monday5          = schedule.Monday5,
		                                                                             Tuesday1         = schedule.Tuesday1,
		                                                                             Tuesday2         = schedule.Tuesday2,
		                                                                             Tuesday3         = schedule.Tuesday3,
		                                                                             Tuesday4         = schedule.Tuesday4,
		                                                                             Tuesday5         = schedule.Tuesday5,
		                                                                             Wednesday1       = schedule.Wednesday1,
		                                                                             Wednesday2       = schedule.Wednesday2,
		                                                                             Wednesday3       = schedule.Wednesday3,
		                                                                             Wednesday4       = schedule.Wednesday4,
		                                                                             Wednesday5       = schedule.Wednesday5,
		                                                                             Thursday1        = schedule.Thursday1,
		                                                                             Thursday2        = schedule.Thursday2,
		                                                                             Thursday3        = schedule.Thursday3,
		                                                                             Thursday4        = schedule.Thursday4,
		                                                                             Thursday5        = schedule.Thursday5,
		                                                                             Friday1          = schedule.Friday1,
		                                                                             Friday2          = schedule.Friday2,
		                                                                             Friday3          = schedule.Friday3,
		                                                                             Friday4          = schedule.Friday4,
		                                                                             Friday5          = schedule.Friday5,
		                                                                             Saturday1        = schedule.Saturday1,
		                                                                             Saturday2        = schedule.Saturday2,
		                                                                             Saturday3        = schedule.Saturday3,
		                                                                             Saturday4        = schedule.Saturday4,
		                                                                             Saturday5        = schedule.Saturday5,
		                                                                             Sunday1          = schedule.Sunday1,
		                                                                             Sunday2          = schedule.Sunday2,
		                                                                             Sunday3          = schedule.Sunday3,
		                                                                             Sunday4          = schedule.Sunday4,
		                                                                             Sunday5          = schedule.Sunday5
	                                                                             };

	public static Protocol.Data.Schedules ToSchedules( this IEnumerable<Schedule>? schedules ) => schedules is not null ? new Protocol.Data.Schedules( schedules.Select( ToSchedule ) ) : [];

	public static DateTimeOffset UtcToClient( this DateTimeOffset dateTimeUtc, string clientTimeZoneId )
	{
		var Tz = TimeZoneInfo.FindSystemTimeZoneById( clientTimeZoneId );
		return TimeZoneInfo.ConvertTime( dateTimeUtc, Tz );
	}

	public static DateTimeOffset NextRun( this Schedule schedule, bool isUtc ) => schedule.ToSchedule().NextRun( isUtc );

	public static DateTimeOffset NextRun( this Protocol.Data.Schedule schedule, bool isUtc )
	{
		var Tz = TimeZoneInfo.FindSystemTimeZoneById( schedule.ClientTimeZoneId );

		var CurrentDate = schedule.Date;

		var Found = false;

		if( schedule.TriggerType is Protocol.Data.Schedule.TRIGGER_TYPE.ON_DAY_AND_DELETE_SCHEDULE
		                            or Protocol.Data.Schedule.TRIGGER_TYPE.ON_DAY_AND_DELETE_SCHEDULE_AND_TRIP )
			Found = true;
		else
		{
			if( !isUtc )
				CurrentDate = TimeZoneInfo.ConvertTime( CurrentDate, Tz );

			var EndDate = CurrentDate.AddYears( 1 ).AddDays( 1 );

			var DidMonthAdjust = false;

			while( !Found && ( CurrentDate <= EndDate ) )
			{
				switch( CurrentDate.Month )
				{
				case 1 when schedule.January:
				case 2 when schedule.February:
				case 3 when schedule.March:
				case 4 when schedule.April:
				case 5 when schedule.May:
				case 6 when schedule.June:
				case 7 when schedule.July:
				case 8 when schedule.August:
				case 9 when schedule.September:
				case 10 when schedule.October:
				case 11 when schedule.November:
				case 12 when schedule.December:
					break;

				default:
					CurrentDate = CurrentDate.AddMonths( 1 );

					if( !DidMonthAdjust )
					{
						DidMonthAdjust = true;
						CurrentDate    = new DateTimeOffset( CurrentDate.Year, CurrentDate.Month, 1, 0, 0, 0, CurrentDate.Offset );
					}
					continue;
				}

				var (Day, Ordinal) = CurrentDate.GetDayOfWeekAndOrdinal();

				switch( Ordinal )
				{
				case 1:
					switch( Day )
					{
					case DayOfWeek.Monday when schedule.Monday1:
					case DayOfWeek.Tuesday when schedule.Tuesday1:
					case DayOfWeek.Wednesday when schedule.Wednesday1:
					case DayOfWeek.Thursday when schedule.Thursday1:
					case DayOfWeek.Friday when schedule.Friday1:
					case DayOfWeek.Saturday when schedule.Saturday1:
					case DayOfWeek.Sunday when schedule.Sunday1:
						Found = true;
						continue;
					}
					break;

				case 2:
					switch( Day )
					{
					case DayOfWeek.Monday when schedule.Monday2:
					case DayOfWeek.Tuesday when schedule.Tuesday2:
					case DayOfWeek.Wednesday when schedule.Wednesday2:
					case DayOfWeek.Thursday when schedule.Thursday2:
					case DayOfWeek.Friday when schedule.Friday2:
					case DayOfWeek.Saturday when schedule.Saturday2:
					case DayOfWeek.Sunday when schedule.Sunday2:
						Found = true;
						continue;
					}
					break;

				case 3:
					switch( Day )
					{
					case DayOfWeek.Monday when schedule.Monday3:
					case DayOfWeek.Tuesday when schedule.Tuesday3:
					case DayOfWeek.Wednesday when schedule.Wednesday3:
					case DayOfWeek.Thursday when schedule.Thursday3:
					case DayOfWeek.Friday when schedule.Friday3:
					case DayOfWeek.Saturday when schedule.Saturday3:
					case DayOfWeek.Sunday when schedule.Sunday3:
						Found = true;
						continue;
					}
					break;

				case 4:
					switch( Day )
					{
					case DayOfWeek.Monday when schedule.Monday4:
					case DayOfWeek.Tuesday when schedule.Tuesday4:
					case DayOfWeek.Wednesday when schedule.Wednesday4:
					case DayOfWeek.Thursday when schedule.Thursday4:
					case DayOfWeek.Friday when schedule.Friday4:
					case DayOfWeek.Saturday when schedule.Saturday4:
					case DayOfWeek.Sunday when schedule.Sunday4:
						Found = true;
						continue;
					}
					break;

				case 5:
					switch( Day )
					{
					case DayOfWeek.Monday when schedule.Monday5:
					case DayOfWeek.Tuesday when schedule.Tuesday5:
					case DayOfWeek.Wednesday when schedule.Wednesday5:
					case DayOfWeek.Thursday when schedule.Thursday5:
					case DayOfWeek.Friday when schedule.Friday5:
					case DayOfWeek.Saturday when schedule.Saturday5:
					case DayOfWeek.Sunday when schedule.Sunday5:
						Found = true;
						continue;
					}
					break;
				}
				CurrentDate = CurrentDate.AddDays( 1 );
			}
		}

		if( Found )
		{
			var ClientTimeSpan = new TimeSpan( 0, schedule.Hour, schedule.Minute, 0 );
			var ClientDateTime = new DateTimeOffset( CurrentDate.Date.Ticks + ClientTimeSpan.Ticks, CurrentDate.Offset ).UtcDateTime;
			return ClientDateTime;
		}
		return DateTimeOffset.MaxValue;
	}
}

public static partial class Users
{
	public static Protocol.Data.Schedules GetSchedules( IRequestContext context )
	{
		try
		{
			using var Db        = new UsersEntities();
			var       CarrierId = context.CarrierId;

		#if NET5_0_OR_GREATER
			var Schedules = Db.Entity.Schedules;
		#else
			var Schedules = Db.Schedules;
		#endif

			var Recs = ( from S in Schedules
			             where S.CarrierId == CarrierId
			             select S ).ToList();

			return Recs.ToSchedules();
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
		return [];
	}

	public static void DeleteSchedule( IRequestContext context, long requestObject )
	{
		try
		{
			var CarrierId = context.CarrierId;

			using var Db = new UsersEntities();

		#if NET5_0_OR_GREATER
			var Schedules = Db.Entity.Schedules;
		#else
			var Schedules = Db.Schedules;
		#endif

			var Rec = ( from S in Schedules
			            where ( S.Id == requestObject ) && ( S.CarrierId == CarrierId )
			            select S ).FirstOrDefault();

			if( Rec is not null )
			{
				Schedules.Remove( Rec );
				Db.SaveChanges();
				DeleteTrip( context, Rec.TripId );
			}
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
	}

	public static long AddUpdateSchedule( IRequestContext context, Protocol.Data.Schedule requestObject )
	{
		try
		{
			var CarrierId = context.CarrierId;
			var Id        = requestObject.Id;

			using var Db = new UsersEntities();

		#if NET5_0_OR_GREATER
			var Schedules = Db.Entity.Schedules;
		#else
			var Schedules = Db.Schedules;
		#endif

			if( Id <= 0 )
			{
				var S = requestObject.ToDbSchedule( CarrierId );
				S.NextRun = requestObject.NextRun( false );
				Schedules.Add( S );
				Db.SaveChanges();
				return S.Id;
			}

			var Rec = ( from S in Schedules
			            where ( S.Id == Id ) && ( S.CarrierId == CarrierId )
			            select S ).FirstOrDefault();

			if( Rec is not null )
			{
				var TripId   = Rec.TripId;
				var DoDelete = TripId != requestObject.TripId;

				Rec.TriggerType = (byte)requestObject.TriggerType;
				Rec.Driver      = requestObject.Driver;
				Rec.TripId      = requestObject.TripId;

				Rec.ClientTimeZoneId = requestObject.ClientTimeZoneId.Trim();
				Rec.Hour             = requestObject.Hour;
				Rec.Minute           = requestObject.Minute;
				Rec.January          = requestObject.January;
				Rec.February         = requestObject.February;
				Rec.March            = requestObject.March;
				Rec.April            = requestObject.April;
				Rec.May              = requestObject.May;
				Rec.June             = requestObject.June;
				Rec.July             = requestObject.July;
				Rec.August           = requestObject.August;
				Rec.September        = requestObject.September;
				Rec.October          = requestObject.October;
				Rec.November         = requestObject.November;
				Rec.December         = requestObject.December;
				Rec.Monday1          = requestObject.Monday1;
				Rec.Monday2          = requestObject.Monday2;
				Rec.Monday3          = requestObject.Monday3;
				Rec.Monday4          = requestObject.Monday4;
				Rec.Monday5          = requestObject.Monday5;
				Rec.Tuesday1         = requestObject.Tuesday1;
				Rec.Tuesday2         = requestObject.Tuesday2;
				Rec.Tuesday3         = requestObject.Tuesday3;
				Rec.Tuesday4         = requestObject.Tuesday4;
				Rec.Tuesday5         = requestObject.Tuesday5;
				Rec.Wednesday1       = requestObject.Wednesday1;
				Rec.Wednesday2       = requestObject.Wednesday2;
				Rec.Wednesday3       = requestObject.Wednesday3;
				Rec.Wednesday4       = requestObject.Wednesday4;
				Rec.Wednesday5       = requestObject.Wednesday5;
				Rec.Thursday1        = requestObject.Thursday1;
				Rec.Thursday2        = requestObject.Thursday2;
				Rec.Thursday3        = requestObject.Thursday3;
				Rec.Thursday4        = requestObject.Thursday4;
				Rec.Thursday5        = requestObject.Thursday5;
				Rec.Friday1          = requestObject.Friday1;
				Rec.Friday2          = requestObject.Friday2;
				Rec.Friday3          = requestObject.Friday3;
				Rec.Friday4          = requestObject.Friday4;
				Rec.Friday5          = requestObject.Friday5;
				Rec.Saturday1        = requestObject.Saturday1;
				Rec.Saturday2        = requestObject.Saturday2;
				Rec.Saturday3        = requestObject.Saturday3;
				Rec.Saturday4        = requestObject.Saturday4;
				Rec.Saturday5        = requestObject.Saturday5;
				Rec.Sunday1          = requestObject.Sunday1;
				Rec.Sunday2          = requestObject.Sunday2;
				Rec.Sunday3          = requestObject.Sunday3;
				Rec.Sunday4          = requestObject.Sunday4;
				Rec.Sunday5          = requestObject.Sunday5;

				Rec.NextRun = requestObject.NextRun( false );

				Db.SaveChanges();

				if( DoDelete )
					DeleteTrip( context, TripId );
				else
					ScheduleTrip( context, TripId );

				return Rec.Id;
			}
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
		return -1;
	}

	// Must dispose Db
	private static ( CarrierDb Db, Trip? Trip ) GeTrip( IRequestContext context, string tripId )
	{
		var Db = new CarrierDb( context );

		var Rec = ( from T in Db.Entity.Trips
		            where T.TripId == tripId
		            select T ).FirstOrDefault();

		return ( Db, Rec );
	}

	private static void ScheduleTrip( IRequestContext context, string tripId )
	{
		try
		{
			var (Db, Rec) = GeTrip( context, tripId );

			try
			{
				if( Rec is not null && ( Rec.Status1 != (int)STATUS.SCHEDULED ) )
				{
					Rec.Status1 = (int)STATUS.SCHEDULED;
					Db.Entity.SaveChanges();
				}
			}
			finally
			{
				Db.Dispose();
			}
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
	}

	private static void DeleteTrip( IRequestContext context, string tripId )
	{
		try
		{
			// First make sure the trip is not used in another scheduled
			var UserEntity = new UsersEntities();

		#if NET5_0_OR_GREATER
			var Schedules = UserEntity.Entity.Schedules;
		#else
			var Schedules = UserEntity.Schedules;
		#endif

			var Sch = ( from S in Schedules
			            where S.TripId == tripId
			            select S ).FirstOrDefault();

			if( Sch is null )
			{
				var (Db, Rec) = GeTrip( context, tripId );

				try
				{
					if( Rec is not null )
					{
						var E = Db.Entity;
						E.Trips.Remove( Rec );
						E.SaveChanges();
					}
				}
				finally
				{
					Db.Dispose();
				}
			}
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
	}
}