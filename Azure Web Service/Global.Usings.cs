﻿global using System;
global using System.Collections.Generic;
global using System.Linq;
global using Database.Model.Databases.Carrier;
global using Protocol.Data;
global using System.Text;
global using System.Threading.Tasks;
global using Database.Model.Database;
global using Database.Model.Databases.MasterTemplate;
global using StorageV2.Logs;
global using Utils;

#if NET5_0
global using Microsoft.AspNetCore.Http;
#else
global using System.Web;
#endif