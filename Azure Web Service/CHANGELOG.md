# Changelog for Azure Web Service

Based on https://keepachangelog.com/en/1.0.0/

Types of changes within each release:

- **Added** for new features.
- **Changed** for changes in existing functionality.
- **Deprecated** for soon-to-be removed features.
- **Removed** for now removed features.
- **Fixed** for any bug fixes.
- **Security** in case of vulnerabilities.


## [Unreleased]

## 2020-09-14
### Changed
- Carrier/Company.cs - Added 'Enabled' flag to 

private static CompanyAddress ConvertCompanyAddress( Company co, Address addr )

## 2020-04-28
### Changed
- CarrierDb.Staff.cs - Modified GetStaffMember to load the address.