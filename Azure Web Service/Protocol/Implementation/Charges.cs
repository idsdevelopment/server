﻿namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override Charges ResponseGetAllCharges()
		{
			using var Db = new CarrierDb( Context );

			return Db.GetAllCharges();
		}

		public override void ResponseAddUpdateCharges( Charges requestObject )
		{
			using var Db = new CarrierDb( Context );

			Db.AddUpdateCharges( requestObject );
		}
	}
}