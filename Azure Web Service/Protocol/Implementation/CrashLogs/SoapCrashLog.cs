﻿namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override void ResponseSoapError( CrashReport requestObject )
		{
			// Make sure request context has been initialised
			new RequestContext.RequestContext();

			SoapCrashLog.Add( requestObject );
		}
	}
}