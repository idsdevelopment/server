﻿namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override void ResponseAppCrashLog( CrashReport requestObject )
		{
			// Make sure request context has been initialised
			new RequestContext.RequestContext();

			AppCrashLog.Add( requestObject );
		}
	}
}