﻿// ReSharper disable UseDeconstruction

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		private const string TIME_ERROR = "TimeError";

		private const int SECONDS_THRESHOLD             = 5 * 60,
		                  AUTH_TOKEN_EXPIRES_IN_SECONDS = 7 * 24 * 60 * 60; // 7 days

		private static readonly long SecondsThresholdTicks = new TimeSpan( 0, 0, SECONDS_THRESHOLD ).Ticks;

		private RequestContext.RequestContext Context = null!;

		public string ResponseLogin( string pgm, string carrierId, string userName, string password, string timeZoneOffset, string version, string isDebugServer, Func<string, string, bool>? validate = null )
		{
			if( !sbyte.TryParse( timeZoneOffset, out var TimeZoneOffset ) )
				throw new AbortConnectionException();

			var Debug     = isDebugServer.ToBool();
			var TimeError = true;

			var UserName = Encryption.FromTimeLimitedToken( userName );

			if( UserName.Ok )
			{
				var Password = Encryption.FromTimeLimitedToken( password );

				if( Password.Ok )
				{
					var CarrierId = Encryption.FromTimeLimitedToken( carrierId );

					if( CarrierId.Ok )
					{
						TimeError = false;

						var P = Password.Value;
						var U = UserName.Value.Trim();
						var C = CarrierId.Value.Trim();

						var CidUsr = C.ToLower().Split( '/', '\\' );

						// Special IDS Login
						if( CidUsr is ["ids", _] )
						{
							Context = new RequestContext.RequestContext
							          {
								          CarrierId      = CidUsr[ 1 ],
								          Password       = P,
								          UserName       = U,
								          TimeZoneOffset = TimeZoneOffset,
								          Debug          = Debug
							          };

							if( IdsSpecialLogin( U, P, out var IsNewCarrier ) )
							{
								Context.IsIds        = true;
								Context.IsNewCarrier = IsNewCarrier;

								if( !IsNewCarrier )
								{
									using var Db = new CarrierDb( Context );

									try
									{
										Db.GetConfiguration(); // Make sure the configuration is created
									}
									catch( Exception E )
									{
										Context.SystemLogException( E );
										return "";
									}
								}
								return Encryption.ToTimeLimitedToken( Context.ToAuthToken() );
							}
						}

					#if NET5_0_OR_GREATER
						var Ip = HttpContext.IpAddress();
					#else
						var Ip = HttpContext.Request.UserHostAddress ?? "Unknown";
					#endif

						Context = RequestContext.RequestContext.ToRequestContext( C, U, P, TimeZoneOffset, Debug, Ip );

						if( CarrierDb.HasCarrierId( Context ) )
						{
							var (UserId, Ok) = ( U.Compare( RequestContext.RequestContext.AZURE_USER, StringComparison.OrdinalIgnoreCase ) == 0 )
							                   || ( U.Compare( RequestContext.RequestContext.IDS_USER, StringComparison.OrdinalIgnoreCase ) == 0 )
								                   ? ( -1, P == RequestContext.RequestContext.WEB_SERVICE_PASSWORD )
								                   : UserLogin( Context );

							if( Ok && ( validate is null || validate( C, U ) ) )
							{
								Context.UserId = UserId;

								using var Db = new CarrierDb( Context );

								try
								{
									Db.GetConfiguration(); // Make sure the configuration is created

									var Ctx = Context.Clone();

									Tasks.RunVoid( () =>
									               {
										               UserAuditTrail.Add( Ctx.GetStorageId( LOG.LOGIN ), new UserAuditTrailEntryEntry( Ctx, AuditTrailEntryBase.OPERATION.SIGN_IN, $"{pgm} - {version}" ) );
									               } );

									return RequestContext.RequestContext.ToAuthToken( C, U, P, TimeZoneOffset, Debug, UserId, Ip, AUTH_TOKEN_EXPIRES_IN_SECONDS );
								}
								catch( Exception E )
								{
									Context.SystemLogException( E );
									return "";
								}
							}
						}
					}
				}
			}

			return TimeError ? TIME_ERROR : "";
		}

		public override bool ValidateAuthorisation( string authToken )
		{
			try
			{
				Context = RequestContext.RequestContext.ToRequestContext( authToken )!;

				if( Context is null )
					throw new QuietException();

				return true;
			}
			catch( Exception E ) when( E is not QuietException )
			{
				throw new AbortConnectionException( E.Message );
			}
		}


		public override string ResponseLogin( string pgm, string c, string u, string p, string t, string v, string d, string ut ) => CheckUserTime( ut ) ? ResponseLogin( pgm, c, u, p, t, v, d ) : TIME_ERROR;

		public override string ResponseLoginAsDriver( string pgm, string c, string u, string p, string t, string v, string d, string ut )
		{
			var (Value, Ok) = Encryption.FromTimeLimitedToken( u );

			if( Ok )
			{
				var Temp = ResponseLogin( pgm, c, u, p, t, v, d, ( _, u1 ) =>
				                                                 {
					                                                 using var Db = new CarrierDb( Context );

					                                                 return Db.IsStaffMemberADriverOrAdmin( u1 );
				                                                 } );

				if( Temp.IsNotNullOrWhiteSpace() && ( Temp != TIME_ERROR ) )
					Messaging<TripUpdateMessage>.Clear( Context, Protocol.Data.Messaging.DRIVERS, Value );

				return Temp;
			}
			return TIME_ERROR;
		}

		public override string ResponseReLogin() => RequestContext.RequestContext.ToAuthToken( Context, AUTH_TOKEN_EXPIRES_IN_SECONDS );

		public override string ResponseResellerCustomerLogin( string pgm, string c, string u, string p, string t, string v, string d, string ut ) => CheckUserTime( ut ) ? ResponseResellerCustomerLogin( pgm, c, u, p, t, v, d ) : TIME_ERROR;

		public override void ResponseSignOut( SignOut requestObject )
		{
			var Ctx = Context.Clone();

			Tasks.RunVoid( () =>
			               {
				               var Data = $"{requestObject.Hours:D2}:{requestObject.Minutes:D2}:{requestObject.Seconds:D2}";
				               UserAuditTrail.Add( Ctx.GetStorageId( LOG.LOGIN ), new UserAuditTrailEntryEntry( Ctx, AuditTrailEntryBase.OPERATION.SIGN_OUT, Data ) );
			               } );
		}

		private bool IdsSpecialLogin( string employee, string password, out bool isNewCarrier )
		{
			isNewCarrier = false;

			var Emp = Users.Employee( Context, employee );

			if( Emp is not null && ( Emp.Password == password ) )
			{
				var EPoints = Users.GetEndPoints( Context );

				if( EPoints is null )
				{
					isNewCarrier = true;

					return Emp.AllowCreation;
				}

				return true;
			}

			return false;
		}

		private (int UserId, bool Ok) UserLogin( IRequestContext context )
		{
			using var Db = new CarrierDb( Context );

			var (_, _, UserId, Ok) = Db.LoginOk( context.UserName, Context.Password );
			return ( UserId, Ok );
		}

		private static bool CheckUserTime( string userTime )
		{
			var (Value, Ok) = Encryption.FromTimeLimitedToken( userTime );

			if( Ok )
			{
				if( long.TryParse( Value, out var UserTicks ) )
				{
					var NowTicks = DateTime.UtcNow.Ticks;
					var Diff     = Math.Abs( UserTicks - NowTicks );

					return Diff <= SecondsThresholdTicks;
				}
			}

			return false;
		}
	}
}