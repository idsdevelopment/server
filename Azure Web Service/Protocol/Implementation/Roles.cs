﻿using Role = Protocol.Data.Role;

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override Roles ResponseRoles()
		{
			using var Db = new CarrierDb( Context );
			return Db.GetRoles();
		}

		public override Roles ResponseStaffMemberRoles( string requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.GetStaffMemberRoles( requestObject );
		}

		public override void ResponseAddUpdateRole( Role requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.AddUpdateRole( requestObject );
		}

		public override void ResponseRenameRole( RoleRename requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.RenameRole( requestObject );
		}

		public override void ResponseDeleteRoles( DeleteRoles requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.DeleteRoles( requestObject );
		}
	}
}