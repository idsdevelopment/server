﻿using RoutingModule.Optimisation;

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override DistancesAndAddresses ResponseDirections( RouteOptimisationAddresses requestObject ) => throw new NotImplementedException();

		public override MapApiKeys ResponseGetMapsApiKeys()
		{
			var Config = Context.Configuration;

			return new MapApiKeys
				   {
					   BingMapsKey   = Config.BingMapsApiKey,
					   GoogleMapsKey = Config.GoogleMapsApiKey,
					   AzureMapKey   = Config.AzureMapsApiKey
				   };
		}

		public override AddressValidList ResponseValidateAddresses( AddressValidationList requestObject )
		{
			try
			{
				return new Routing( Context ).ValidateAddresses( Context, requestObject );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
			return new AddressValidList();
		}
	}
}