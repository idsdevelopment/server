﻿namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override string ResponseGetProcessingResponse( long requestObject ) => Users.GetProcessingResponse( Context, requestObject );

		public override bool ResponseIsProcessRunning( long requestObject ) => Users.IsProcessRunning( Context, requestObject );
	}
}