﻿#nullable enable

using Interfaces.Interfaces;
using Logs;
using StorageV2;

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public IRequestContext ExceptionContext
		{
			get
			{
				var Ctx = (IRequestContext?)Context ?? new RequestContext.RequestContext
				                                       {
					                                       CarrierId = RequestType,
					                                       UserName  = UserHostName,
					                                       IpAddress = UserHostAddress
				                                       };

				return Ctx;
			}
		}

		protected override void InitialiseExceptions()
		{
		#if DISABLE_TELEMETRY
			DisableApplicationInsightsOnDebug();
		#endif
			OnAbortConnectionException = exception =>
			                             {
				                             var Request = HttpContext.Request;
				                             BadRequestLog.Add( ExceptionContext, Request.Url.OriginalString, DoNoKeyException( exception ) );
			                             };

			OnException = exception =>
			              {
				              SystemLog.Add( ExceptionContext, HttpContext.Request.Url.OriginalString, DoNoKeyException( exception ) );
			              };
		}

	#if DISABLE_TELEMETRY
		private static void DisableApplicationInsightsOnDebug()
		{
//				TelemetryConfiguration.Active.DisableTelemetry = true;
		}
	#endif

		private Exception DoNoKeyException( Exception exception )
		{
			if( exception is IdsServerExtensions.KeyNotPresentException )
			{
				var Temp = new StringBuilder( $"{exception.Message}\r\n" );

				var Request = HttpContext.Request;
				var Keys    = Request.Form.AllKeys;

				var Form = Request.Form;

				foreach( var Key in Keys )
					Temp.Append( $"Key: {Key} Post Data: {Form[ Key ]}\r\n" );

				exception = new Exception( Temp.ToString().Trim(), exception );
			}

			return exception;
		}
	}
}