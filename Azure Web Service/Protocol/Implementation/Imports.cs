﻿using Imports;

namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override void ResponseCheckForImportsAndExports()
		{
			Tasks.RunVoid( async () =>
			               {
				               await RunImportsAndExports.Execute();
			               } );
		}
	}
}