﻿using Export = Exports.Export;

namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override string ResponseExportCsv( ExportArgs requestObject ) => new Export( Context, requestObject ).Run();
	}
}