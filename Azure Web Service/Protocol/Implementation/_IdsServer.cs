﻿namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation : AServerImplementation
	{
		public IdsServerImplementation( HttpContext context ) : base( context )
		{
		}
	}

	protected override AServerImplementation OnGetImplementation( HttpContext context ) => new IdsServerImplementation( context );


	/* Useful for debugging */

	/*
	public Dictionary<string, string> GetQueryArgs( HttpContext context )
	{
	#if DEBUG
		Debug.WriteLine( $"Request: {context.Request.GetDisplayUrl()}" );
	#endif
		var QueryString = context.Request.QueryString.Value;

		// Remove the leading '?' if it exists
		if( !string.IsNullOrEmpty( QueryString ) && ( QueryString[ 0 ] == '?' ) )
			QueryString = QueryString[ 1.. ];

		// Split the query string into individual parameters
		var QueryParameters = QueryString?.Split( '&' );

		var Result = new Dictionary<string, string>();

		// Parse each parameter into a key and a value
		if( QueryParameters is not null )
		{
			foreach( var Parameter in QueryParameters )
			{
				var Parts = Parameter.Split( '=' );

				if( Parts.Length == 2 )
				{
					var Key   = Parts[ 0 ];
					var Value = Uri.UnescapeDataString( Parts[ 1 ] );

					// Add the key-value pair to the result dictionary
					Result[ Key ] = Value;
				}
			}
		}

		return Result;
	}

	protected override string GetAuthorisationString( HttpContext context, string authTokenId ) => GetQueryArgs( context ).GetValueOrDefault( authTokenId, "" );
	*/
}