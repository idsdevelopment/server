﻿using Company = Database.Utils.Company;

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override bool ResponseIsIds() => Context.IsIds;

		public override bool ResponseIsNewCarrier() => Context.IsNewCarrier;

		public override long ResponseCreateCarrier( string carrierId, string adminId, string adminPassword ) => Company.CreateCompany( Context, carrierId, adminId, adminPassword );
	}
}