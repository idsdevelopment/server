﻿using Emails;

namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override void ResponseProcessEmails()
		{
			Client.ProcessEmails();
		}

		public override void ResponseProcessLimbo()
		{
			CarrierDb.ProcessLimbo();
		}
	}
}