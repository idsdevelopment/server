﻿namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override byte[] Robots_Txt_Forwarder( string path, HttpRequest request, HttpResponse response ) => "User-agent: *\r\nDisallow: /".ToUTF8Bytes();
	}
}