﻿namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override byte[] Php_Default_Forwarder( string path, HttpRequest request, HttpResponse response ) => "\r\nNothing To See Here\r\n".ToUTF8Bytes();
	}
}