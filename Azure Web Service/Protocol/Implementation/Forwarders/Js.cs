﻿using AzureWebService.Protocol.Implementation.Forwarders;

namespace Ids;

public partial class IdsServer
{
	private const string JQUERY         = "jquery.js";
	private const string CURRENT_JQUERY = "jquery-3.4.1.min.js";

	public partial class IdsServerImplementation
	{
		public override byte[] Js_Default_Forwarder( string path, HttpRequest request, HttpResponse response )
		{
			if( path.IndexOf( JQUERY, StringComparison.OrdinalIgnoreCase ) >= 0 )
			{
				var Ndx = path.LastIndexOf( '/' );

				if( Ndx >= 0 )
				{
					var Pth = path.Substring( 0, Ndx + 1 );
					path = $"{Pth}{CURRENT_JQUERY}";
				}
			}

			return Cache.Read( path );
		}
	}
}