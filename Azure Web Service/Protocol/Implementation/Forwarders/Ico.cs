﻿using AzureWebService.Protocol.Implementation.Forwarders;

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override byte[] Ico_Default_Forwarder( string path, HttpRequest request, HttpResponse response ) => Cache.Read( path );
	}
}