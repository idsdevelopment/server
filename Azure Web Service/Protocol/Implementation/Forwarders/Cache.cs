﻿#if !NET5_0_OR_GREATER
	using System.Web.Hosting;
#endif
using File = System.IO.File;
using Timer = System.Timers.Timer;

namespace AzureWebService.Protocol.Implementation.Forwarders;

public static class FileExtensions
{
#if NET5_0_OR_GREATER
	public static string ContentRoot = "";
#endif

	public static byte[] ReadAllBytes( this string path )
	{
		try
		{
		#if NET5_0_OR_GREATER
			path = path.Replace( '/', '\\' );
			var Pth = $"{ContentRoot}{path}";
		#else
			var Pth = HostingEnvironment.MapPath( path ) ?? "";
		#endif
			return File.ReadAllBytes( Pth );
		}
		catch
		{
		}

		return Array.Empty<byte>();
	}

	public static byte[] CacheRead( this string path )
	{
		return Cache.Read( path );
	}
}

public class CacheEntry
{
	public byte[]   Page      = null!;
	public DateTime TimeStamp = DateTime.UtcNow;
}

public class Cache : Dictionary<string, CacheEntry>
{
	private static readonly Cache StaticCache = new();

#if DEBUG

	// ReSharper disable once InconsistentNaming
	private static int _TimeoutInMinutes = 1;
#else
		private static int _TimeoutInMinutes = 10;
#endif

	// ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
	private static Timer CleanUpTimer = null!;

	public static int TimeoutInMinutes
	{
		get
		{
			lock( StaticCache )
				return _TimeoutInMinutes;
		}

		set
		{
			lock( StaticCache )
				_TimeoutInMinutes = value;
		}
	}

	private Cache()
	{
		CleanUpTimer = new Timer( 60 * 1000 ) { AutoReset = true };

		CleanUpTimer.Elapsed += ( _, _ ) =>
		                        {
			                        var Now = DateTime.UtcNow;

			                        lock( StaticCache )
			                        {
				                        foreach( var Key in ( from Kv in StaticCache
				                                              where ( Now - Kv.Value.TimeStamp ).Minutes >= _TimeoutInMinutes
				                                              select Kv.Key ).ToList() )
					                        StaticCache.Remove( Key );
			                        }
		                        };
		CleanUpTimer.Start();
	}


	private static bool TryGetValue( string pageId, out byte[] page )
	{
		lock( StaticCache )
		{
			if( ( (Dictionary<string, CacheEntry>)StaticCache ).TryGetValue( pageId, out var Entry ) )
			{
			#if !DEBUG
					Entry.TimeStamp = DateTime.UtcNow;
			#endif
				page = Entry.Page;

				return true;
			}

			page = null!;

			return false;
		}
	}

	private static void Add( string pageId, byte[] page )
	{
		lock( StaticCache )
		{
			StaticCache[ pageId ] = new CacheEntry
			                        {
				                        Page = page
			                        };
		}
	}

	public static byte[] Read( string fileName )
	{
		lock( StaticCache )
		{
			if( TryGetValue( fileName, out var Entry ) )
				return Entry;
		}

		var Bytes = fileName.ReadAllBytes();

		if( Bytes.Length > 0 )
			Add( fileName, Bytes );

		return Bytes;
	}
}