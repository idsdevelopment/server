﻿namespace Ids;

public static class Extensions
{
	// ReSharper disable once InconsistentNaming
	public static byte[] ToUTF8Bytes( this string txt ) => Encoding.UTF8.GetBytes( txt );
}

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override byte[] Ads_Txt_Forwarder( string path, HttpRequest request, HttpResponse response ) => "#ads.txt\r\n\r\n#Nothing to see here.".ToUTF8Bytes();
	}
}