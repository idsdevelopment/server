﻿namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override string ResponsePing() => "OK";

		public override long ResponsePingTime() => DateTime.UtcNow.Ticks;

		public override string ResponseStartUpSite()
		{
			// ReSharper disable once UnusedVariable
			var _ = Users.Configuration;

			return ResponsePing();
		}
	}
}