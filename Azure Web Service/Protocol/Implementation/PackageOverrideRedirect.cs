﻿namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override void ResponseAddUpdatePackageOverride( OverridePackage requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.AddUpdatePackageOverride( requestObject );
		}

		public override void ResponseAddUpdatePackageRedirect( RedirectPackage requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.AddUpdatePackageRedirect( requestObject );
		}

		public override void ResponseDeletePackageOverride( int requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.DeletePackageOverride( requestObject );
		}

		public override void ResponseDeletePackageRedirect( int requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.DeletePackageRedirect( requestObject );
		}

		public override RedirectPackageList ResponseGetPackageRedirectsByCompanyId( long requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.GetPackageRedirectsByCompanyId( requestObject );
		}

		public override OverridePackageList ResponseGetPackageOverridesByCompanyId( long requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.GetPackageOverridesByCompanyId( requestObject );
		}
	}
}