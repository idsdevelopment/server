﻿namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override RateMatrixItems ResponseGetAllRateMatrixItems()
		{
			using var Db = new CarrierDb( Context );
			return Db.GetAllRateMatrixItems();
		}

		public override void ResponseAddUpdateRateMatrixItems( RateMatrixItems requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.AddUpdateRateMatrixItems( requestObject );
		}
	}
}