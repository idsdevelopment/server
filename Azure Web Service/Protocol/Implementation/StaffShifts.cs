﻿namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override StaffShifts ResponseGetCompanyShifts( string companyName )
		{
			using var Db = new CarrierDb( Context );
			return Db.GetCompanyShifts( companyName );
		}

		public override StaffShifts ResponseGetStaffShifts()
		{
			try
			{
				using var Db = new CarrierDb( Context );
				return Db.GetStaffShifts();
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
			return new StaffShifts();
		}

		public override void ResponseUpdateCompanyShifts( UpdateCompanyShifts requestObject )
		{
			try
			{
				using var Db = new CarrierDb( Context );
				Db.UpdateCompanyShifts( requestObject );
			}
			catch( Exception Exception )
			{
				Context.SystemLogExceptionProgram( requestObject.ProgramName, Exception );
			}
		}

		public override void ResponseUpdateStaffShifts( StaffShiftsUpdate requestObject )
		{
			try
			{
				using var Db = new CarrierDb( Context );
				Db.UpdateStaffShifts( requestObject );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
	}
}