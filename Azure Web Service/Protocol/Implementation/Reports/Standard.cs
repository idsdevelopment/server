﻿namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override ReportReply ResponseReport( Report requestObject )
		{
			return Reports.Standard.Execute( Context, requestObject );
		}
	}
}