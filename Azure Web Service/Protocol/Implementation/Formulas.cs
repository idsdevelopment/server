﻿using Shift = FormulaDataTypes.Employee.Shift;
using Trip = FormulaDataTypes.Trips.Trip;

namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		private const string SHIFT = "SHIFT",
		                     TRIP  = "TRIP";

		public override FormulaResult ResponseEvaluateFormula( EvaluateFormula requestObject )
		{
			var Retval = new FormulaResult();
			var Type   = requestObject.DataType.TrimToUpper();

			switch( Type )
			{
			case SHIFT:
				var (Errors, Value) = new Shift( requestObject ).Evaluate();
				Retval.ErrorList    = Errors;
				Retval.Value        = Value;
				break;

			case TRIP:
				( Errors, Value ) = new Trip( requestObject ).Evaluate();
				Retval.ErrorList  = Errors;
				Retval.Value      = Value;
				break;

			default:
				Retval.ErrorList.Add( $"Unknown Type: {Type}" );
				break;
			}
			return Retval;
		}

		public override FormulaVariables ResponseGetFormulaVariables( string requestObject )
		{
			var RetVal = new FormulaVariables();

			try
			{
				return requestObject.ToUpper().Trim() switch
				       {
					       SHIFT => RetVal.AddRange( new Shift().Variables() ),
					       TRIP  => RetVal.AddRange( new Trip().Variables() ),
					       _     => RetVal
				       };
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
			return RetVal;
		}
	}
}