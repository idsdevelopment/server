﻿namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override bool ResponseAddUpdateCarrierChargeTokens( ChargeTokens requestObject ) => CarrierDb.AddUpdateCarrierChargeTokens( Context, requestObject );

		public override ChargeTokens ResponseGetCarrierChargeTokens( GetChargeTokens requestObject ) => CarrierDb.GetCarrierChargeTokens( Context, requestObject );
	}
}