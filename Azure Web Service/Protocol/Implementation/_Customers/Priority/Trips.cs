﻿using Protocol.Data.Customers.Priority;

namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override void ResponsePriorityUpdatePickupAddress( UpdatePickupAddress requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.PriorityUpdatePickupAddress( requestObject );
		}
	}
}