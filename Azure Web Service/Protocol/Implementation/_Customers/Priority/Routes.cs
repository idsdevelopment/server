﻿using Protocol.Data.Customers.Priority;

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override PriorityDeviceRoutes ResponsePriorityGetRouteNamesForDevice()
		{
			using var Db = new CarrierDb( Context );

			return Db.PriorityGetRouteNamesForDevice();
		}
	}
}