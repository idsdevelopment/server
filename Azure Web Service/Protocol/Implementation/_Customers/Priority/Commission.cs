﻿using Protocol.Data.Customers.Priority;

namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override PriorityDriverCommissions ResponsePriorityGetDriverCommission( PriorityDriverCommissionRange requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.PriorityGetDriverCommission( requestObject );
		}

		public override TripList ResponsePriorityGetUnCalculatedDriverCommission( DateTimeOffset requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.PriorityGetUnCalculatedDriverCommission( requestObject );
		}

		public override TripList ResponsePriorityGetUnCalculatedDriverCommissionForDriver( PriorityDriverCommissionRange requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.PriorityGetUnCalculatedDriverCommissionForDriver( requestObject );
		}


		public override bool ResponsePriorityUpdateDriverCommission( UpdateDriverCommission requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.PriorityUpdateDriverCommission( requestObject );
			return true;
		}
	}
}