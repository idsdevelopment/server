﻿namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override long ResponseFixPmlXref() => CarrierDb.FixPmlXref( Context );
	}
}