﻿using Protocol.Data._Customers.Pml;
using Address = Protocol.Data.Address;
using Company = Protocol.Data.Company;

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		// Remove Current Trip From Drivers Board and Driver
		public void PmlRemoveTripFromDevice( string programName, string driver, string tripId )
		{
			// Remove Current Trip From Drivers Board and Driver
			Broadcast( BuildMessage( TripUpdateMessage.ACTION.UPDATE_STATUS, new TripUpdateStatus
			                                                                 {
				                                                                 Driver           = driver,
				                                                                 Program          = programName,
				                                                                 Broadcast        = TripUpdateStatus.BROADCAST.DISPATCH_BOARD | TripUpdateStatus.BROADCAST.DRIVERS_BOARD | TripUpdateStatus.BROADCAST.DRIVER,
				                                                                 ReadByDriver     = true,
				                                                                 ReceivedByDevice = true,
				                                                                 Status           = STATUS.DELETED,
				                                                                 Status1          = STATUS1.UNSET,
				                                                                 Status2          = STATUS2.UNSET,
				                                                                 TripIdList       = [tripId]
			                                                                 } ) );
		}

		public override void ResponsePML_ClaimTrips( ClaimTrips requestObject )
		{
			using var Db = new CarrierDb( Context );

			foreach( var (OriginalDriver, Trip) in Db.PML_ClaimTrips( requestObject ) )
			{
				var UpdateTrip = Trip.ToTripUpdate( requestObject.ProgramName, false, true, false );
				PmlRemoveTripFromDevice( requestObject.ProgramName, OriginalDriver, UpdateTrip.TripId );
				Broadcast( UpdateTrip );
			}
		}

		public override TripList ResponsePML_FindTripsDetailedUpdatedSince( PmlFindSince requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.PML_FindTripsDetailedUpdatedSince( requestObject );
		}

		public override TripUpdate ResponsePML_updateTripDetailedQuick( TripUpdate r )
		{
			try
			{
				using var Db = new CarrierDb( Context );

				void UpdateAccount( string customerCode, string barcode,
				                    string companyName, string suite, string addressLine1, string addressLine2, string city, string region, string postalCode, string country, string countryCode,
				                    string notes )
				{
					try
					{
						var Co = new AddUpdatePrimaryCompany( r.Program )
						         {
							         CustomerCode       = customerCode,
							         UserName           = customerCode,
							         SuggestedLoginCode = customerCode,
							         Password           = "",
							         Company = new Company( new Address
							                                {
								                                Barcode = barcode,

								                                Suite        = suite,
								                                AddressLine1 = addressLine1,
								                                AddressLine2 = addressLine2,

								                                City        = city,
								                                Region      = region,
								                                Country     = country,
								                                CountryCode = countryCode,
								                                PostalCode  = postalCode,

								                                Notes = notes
							                                } )
							                   {
								                   CompanyName     = companyName,
								                   LocationBarcode = barcode
							                   }
						         };
						Db.AddUpdatePrimaryCompany( Co );
					}
					catch( Exception Exception )
					{
						Context.SystemLogException( Exception );
					}
				}

				UpdateAccount( r.BillingAccountId, r.BillingAddressBarcode,
				               r.BillingCompanyName, r.BillingAddressSuite, r.BillingAddressAddressLine1, r.BillingAddressAddressLine2, r.BillingAddressCity, r.BillingAddressRegion, r.BillingAddressPostalCode, r.BillingAddressCountry,
				               r.BillingAddressCountryCode,
				               r.BillingAddressNotes );

				UpdateAccount( r.PickupAccountId, r.PickupAddressBarcode,
				               r.PickupCompanyName, r.PickupAddressSuite, r.PickupAddressAddressLine1, r.BillingAddressAddressLine2, r.PickupAddressCity, r.PickupAddressRegion, r.PickupAddressPostalCode, r.PickupAddressCountry,
				               r.PickupAddressCountryCode,
				               r.PickupAddressNotes );

				UpdateAccount( r.DeliveryAccountId, r.DeliveryAddressBarcode,
				               r.DeliveryCompanyName, r.DeliveryAddressSuite, r.DeliveryAddressAddressLine1, r.BillingAddressAddressLine2, r.DeliveryAddressCity, r.DeliveryAddressRegion, r.DeliveryAddressPostalCode, r.DeliveryAddressCountry,
				               r.DeliveryAddressCountryCode,
				               r.DeliveryAddressNotes );

				Broadcast( Db.PML_updateTripDetailedQuick( r ) );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
			return r;
		}
	}
}