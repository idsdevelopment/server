﻿using Protocol.Data._Customers.Pml;

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override void ResponsePML_AddUpdateRoutes( PmlRoutes requestObject )
		{
			try
			{
				var Csv = new StringBuilder();

				foreach( var PmlRoute in requestObject )
					Csv.Append( $"{PmlRoute.PostalCode},{PmlRoute.Operation},{PmlRoute.Region},{PmlRoute.IdsRoute}\r\n" );

				ResponsePML_PostCodeToRouteImport( Csv.ToString() );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}

		public override void ResponsePML_DeletePostcodes( PmlDeletePostCodes requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.PML_DeletePostcodes( requestObject );
		}

		public override void ResponsePML_DeleteRoutes( PmlDeleteRoutes requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.PML_DeleteRoutes( requestObject );
		}

		public override PmlRoutes ResponsePML_GetRoutes()
		{
			using var Db = new CarrierDb( Context );
			return Db.PML_GetRoutes();
		}

		public override long ResponsePML_PostCodeToRouteImport( string requestObject )
		{
			var Retval = Users.StartProcess( Context );

			var Ctx = Context.Clone();

			Task.Run( () =>
			          {
				          try
				          {
					          using var Db = new CarrierDb( Ctx );
					          Db.PML_PostCodeImport( requestObject );
				          }
				          finally
				          {
					          Users.EndProcess( Ctx, Retval );
				          }
			          } );

			return Retval;
		}
	}
}