﻿using Protocol.Data._Customers.Pml;

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override void ResponsePML_AddUpdateInventory( PmlInventoryList requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.PML_AddUpdateInventory( requestObject );
		}

		public override void ResponsePML_DeleteInventory( PmlInventoryList requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.PML_DeleteInventory( requestObject );
		}

		public override PmlInventoryList ResponsePML_GetInventory()
		{
			using var Db = new CarrierDb( Context );

			return Db.PML_GetInventoryList();
		}
	}
}