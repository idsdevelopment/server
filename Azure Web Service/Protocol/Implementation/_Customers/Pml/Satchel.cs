﻿using Protocol.Data._Customers.Pml;
using Reports.Pml;

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override void ResponsePML_ClaimSatchels( ClaimSatchels requestObject )
		{
			DriverLog.Append( Context, requestObject.ProgramName, requestObject );

			using var Db       = new CarrierDb( Context );
			var       Satchels = Db.PML_ClaimSatchels( requestObject );

			foreach( var (OriginalTrip, OriginalDriver) in Satchels )
			{
				PmlRemoveTripFromDevice( requestObject.ProgramName, OriginalDriver, OriginalTrip.TripId );

				var Trip = OriginalTrip.ToTripUpdate( requestObject.ProgramName, false, true, true );

				// Remove the old trip
				var Save = Trip.Status1;
				Trip.Status1 = STATUS.NEW;
				Broadcast( Trip );

				// Move the trip
				Trip.Status1 = Save;
				Broadcast( Trip );
			}
		}

		public override void ResponsePML_UpdateSatchel( Satchel requestObject )
		{
			DriverLog.Append( Context, requestObject.ProgramName, requestObject );

			using var Db = new CarrierDb( Context );
			var (Finalised, Satchel) = Db.PML_UpdateSatchel( requestObject );

			if( Finalised.Count > 0 )
			{
				var Msg = BuildMessage( TripUpdateMessage.ACTION.UPDATE_STATUS, new TripUpdateStatus
				                                                                {
					                                                                Program      = requestObject.ProgramName,
					                                                                Driver       = requestObject.Driver,
					                                                                DeviceStatus = TripUpdateStatusBase.DEVICE_STATUS.READ_BY_DRIVER | TripUpdateStatusBase.DEVICE_STATUS.RECEIVED_BY_DEVICE,
					                                                                Status       = STATUS.FINALISED,
					                                                                Status1      = STATUS1.UNSET,
					                                                                Status2      = STATUS2.UNSET,
					                                                                TripIdList   = Finalised,
					                                                                Broadcast    = TripUpdateStatus.BROADCAST.DRIVERS_BOARD | TripUpdateStatus.BROADCAST.DRIVER
				                                                                } );

				Broadcast( Msg );

				if( Satchel is not null )
				{
					Broadcast( Satchel.ToTripUpdate( requestObject.ProgramName, false, true, true ) );
					eBolReport.PML_eBOL( Context, Satchel.TripId );
				}
			}
		}


		public override void ResponsePML_VerifySatchel( VerifySatchel requestObject )
		{
			DriverLog.Append( Context, requestObject.ProgramName, requestObject );

			using var Db = new CarrierDb( Context );
			var (Satchel, OriginalDriver) = Db.PML_VerifySatchel( requestObject );

			if( Satchel is not null )
			{
				PmlRemoveTripFromDevice( requestObject.ProgramName, OriginalDriver, Satchel.TripId );
				Broadcast( Satchel.ToTripUpdate( requestObject.ProgramName, false, true, true ) );
			}
		}
	}
}