﻿using Trip = Database.Model.Databases.MasterTemplate.Trip;

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		private void FixStatusBroadCast( TripUpdate requestObject )
		{
			FixStatus( requestObject );
			Broadcast( requestObject );
		}

		private static void FixStatus( TripUpdate t )
		{
			if( t.Status1 == STATUS.DISPATCHED )
				t.Status2 = STATUS1.UNSET;
		}

		private static TripUpdateMessage BuildMessage( TripUpdateMessage.ACTION action, TripUpdateStatus s, string board = "" ) => new( action, board, s );
		public override string ResponseGetNextTripId() => Trip.NextTripId( Context );

		public override bool ResponseTripExits( string requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.TripExists( requestObject );
		}

		public override void ResponseAddUpdateTrips( TripUpdateList requestObject )
		{
			using var Db = new CarrierDb( Context );

			foreach( var Update in requestObject.Trips )
			{
				Update.Program = requestObject.Program;
				FixStatusBroadCast( Update );

				Db.AddUpdateTrip( Update );
			}
		}

		public override TripList ResponseSearchTrips( SearchTrips requestObject )
		{
			try
			{
				using var Db = new CarrierDb( Context );
				return Db.SearchTrips( requestObject );
			}
			finally
			{
				ResponseClearPendingSearchTripMessages( requestObject.UniqueUserId );
			}
		}

		public override Protocol.Data.Trip ResponseGetTrip( GetTrip requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.GeTrip( requestObject ) ?? new Protocol.Data.Trip();
		}

		public override TripList ResponseGetTrips( TripIdList requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.ResponseGetTrips( requestObject );
		}

		public override TripList ResponseGetTripsByBillingPeriod( GetTripsByBillingPeriod requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.GetTripsByBillingPeriod( requestObject );
		}

		public override TripIdList ResponseGetTripIdsByStatusAndLocationAndServiceLevel( StatusAndLocationAndServiceLevel requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.GetTripIdsByStatusAndLocationAndServiceLevel( requestObject );
		}

		public override TripList ResponseGetTripsByStatus( StatusRequest requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.GetTripsByStatus( requestObject );
		}

		public override TripList ResponseGetTripsForAccount( string accountId )
		{
			using var Db = new CarrierDb( Context );
			return Db.GetTripsForAccount( accountId );
		}

		public override TripList ResponseGetTripsStartingWith( SearchTripsByStatus requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.GetTripsStartingWith( requestObject );
		}

		public override bool ResponseIsTripIdValid( string requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.IsTripIdValid( requestObject );
		}

		public override void ResponseUpdateTripServiceLevel( TripServiceLevelUpdate requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.UpdateTripServiceLevel( requestObject );
		}

		public override void ResponseUpdateTripStatus( TripUpdateStatus requestObject )
		{
			using var Db = new CarrierDb( Context );

			TripUpdateMessage? Msg = null;

			if( Db.CarrierPreUpdateTripStatusHook( requestObject ) ) // Returns true if no more to do
				return;

			switch( requestObject.Status )
			{
			case STATUS.FINALISED:
				Db.FinaliseTrips( requestObject.Program, requestObject.TripIdList );
				Msg = BuildMessage( TripUpdateMessage.ACTION.UPDATE_STATUS, requestObject );
				break;

			case STATUS.DELETED:
				Db.DeleteTrip( requestObject.Program, requestObject.TripIdList );
				Msg = BuildMessage( TripUpdateMessage.ACTION.UPDATE_STATUS, requestObject );
				break;

			case STATUS.DISPATCHED:
				Db.DispatchTrip( requestObject.Program, requestObject.Driver, requestObject.Status1, requestObject.Status2, requestObject.TripIdList );
				Msg = BuildMessage( TripUpdateMessage.ACTION.UPDATE_TRIP, requestObject );
				break;

			case STATUS.PICKED_UP:
				Db.PickupTrip( requestObject.Program, requestObject.Driver, requestObject.TripIdList );
				Msg = BuildMessage( TripUpdateMessage.ACTION.UPDATE_TRIP, requestObject );
				break;

			case STATUS.ACTIVE:
				Db.BounceTrip( requestObject.Program, requestObject.Driver, requestObject.TripIdList );
				Msg = BuildMessage( TripUpdateMessage.ACTION.UPDATE_TRIP, requestObject );
				break;
			}

			if( Msg is not null )
				Broadcast( Msg );
		}

	#region Driver
		public override void ResponseTripReadByDriver( string program, string tripId )
		{
			using var Db = new CarrierDb( Context );
			Broadcast( Db.TripReadByDriver( program, tripId ) );
		}

		public override void ResponseTripReceivedByDevice( string program, string tripId )
		{
			using var Db = new CarrierDb( Context );
			Broadcast( Db.TripReceivedByDevice( program, tripId ) );
		}

		public override void ResponsePushTripsToDriver( string requestObject )
		{
			using var Db    = new CarrierDb( Context );
			var       Trips = Db.GetTripsForDriver( ref requestObject );

			Broadcast( new TripUpdateMessage
			           {
				           Action    = TripUpdateMessage.ACTION.UPDATE_TRIP,
				           Broadcast = TripUpdateStatus.BROADCAST.DRIVER,
				           TripIdList = ( from T in Trips
				                          select T.TripId ).ToList()
			           } );
		}

		public override TripList ResponseGetTripsForDriver( string requestObject )
		{
			var Result = new TripList();

			using var Db = new CarrierDb( Context );
			Result.AddRange( Db.GetTripsForDriver( ref requestObject ) );

			return Result;
		}

		public override void ResponseUpdateTripFromDevice( DeviceTripUpdate requestObject )
		{
			DriverLog.Append( Context, requestObject.Program, requestObject );

			using var Db = new CarrierDb( Context );
			Broadcast( Db.UpdateTripFromDevice( requestObject ) );
		}
	#endregion

	#region Ids1 Updates
		private void DoIds1( Action<CarrierDb, TripUpdate> updateAction, TripUpdate requestObject )
		{
			using var Db = new CarrierDb( Context );

			var OldTrip = Db.GeTrip( new GetTrip
			                         {
				                         TripId = requestObject.TripId
			                         } );

			// Remove the old trip from the device
			var Removed = OldTrip is not null && ( OldTrip.Driver != requestObject.Driver );

			if( Removed )
			{
				Broadcast( new TripUpdateMessage
				           {
					           Status    = STATUS.DELETED,
					           Action    = TripUpdateMessage.ACTION.UPDATE_STATUS,
					           Broadcast = TripUpdateStatus.BROADCAST.DRIVER,
					           Driver    = OldTrip!.Driver,

					           TripIdList = [OldTrip.TripId]
				           } );
			}

			var SendToIds1 = Db.SendToIds1Preference();

			if( !Removed && !SendToIds1 )
				FixStatusBroadCast( requestObject );

			updateAction( Db, requestObject );

			if( Removed || SendToIds1 )
				FixStatusBroadCast( requestObject );
		}

		public override void ResponseAddUpdateTrip( TripUpdate requestObject )
		{
			DoIds1( ( db, update ) =>
			        {
				        db.AddUpdateTrip( update );
			        },
			        requestObject );
		}

		public override bool ResponseAddUpdateTripAutoCreate( TripUpdate requestObject )
		{
			DoIds1( ( db, update ) =>
			        {
				        db.AddUpdateTripAutoCreate( update, true );
			        },
			        requestObject );

			return true; // Slowdown SOAP
		}

		public override bool ResponseAddUpdateTripAutoCreateDontUpdateAddresses( TripUpdate requestObject )
		{
			DoIds1( ( db, update ) =>
			        {
				        db.AddUpdateTripAutoCreateDontUpdateAddresses( update );
			        },
			        requestObject );

			return true; // Slowdown SOAP
		}
	#endregion

	#region Trip Messaging
		private void Broadcast( TripUpdateMessage msg )
		{
			Context.Broadcast( msg );
		}

		private bool Broadcast( IEnumerable<TripUpdateMessage> msg ) => Context.Broadcast( msg );

		private void Broadcast( TripUpdate t )
		{
			Context.Broadcast( t );
		}

		private void Broadcast( IEnumerable<TripUpdate> trips )
		{
			foreach( var TripUpdate in trips )
				Context.Broadcast( TripUpdate );
		}

		public override void ResponseClaimTrip( string programName, string driverCode, string tripId )
		{
			throw new NotImplementedException();
		}

		public override void ResponseClearPendingSearchTripMessages( string queue )
		{
			Messaging<TripUpdateMessage>.Clear( Context, Protocol.Data.Messaging.SEARCH_TRIPS, queue );
		}
	#endregion
	}
}