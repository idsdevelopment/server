﻿namespace Ids;

public static class LogExtensions
{
	public static WebLogList ToWebLogList( this IEnumerable<string> list )
	{
		var Result = new WebLogList();
		Result.AddRange( list );
		return Result;
	}

	public static WebLogList ToWebLogListFiltered( this IEnumerable<string> list, string filter ) => ( from Log in list
	                                                                                                   where Log.StartsWith( filter )
	                                                                                                   select Log.Replace( filter, "" ) ).ToWebLogList();

	public static WebLogCarrierList ToWebLogCarrierList( this IEnumerable<string> list )
	{
		var Result = new WebLogCarrierList();
		Result.AddRange( list );
		return Result;
	}

	public static WebLogList ToWebLogListDistinct( this IEnumerable<string> list ) => ( from Log in list
	                                                                                    let P = Log.LastIndexOf( '/' )
	                                                                                    select Log.Substring( 0, P ) ).Distinct().ToWebLogList();
}

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
	#region Web Jobs
		public override WebLogCarrierList ResponseGetWebJobCarrierList() => WebJobsLog.GetWebJobCarrierList().ToWebLogCarrierList();

		public override string ResponseGetWebJobContents( string carrierId, string jobType, string jobName ) => WebJobsLog.GetWebJobContents( carrierId, jobType, jobName );

		public override WebLogList ResponseGetWebJobList( string carrierId ) => WebJobsLog.GetWebJobList( carrierId ).ToWebLogListDistinct();
		public override WebLogList ResponseGetWebJobs( string carrierId, string jobType ) => WebJobsLog.GetWebJobList( carrierId ).ToWebLogListFiltered( $"{jobType}/".Pack() );


		public override bool ResponseLogWebJob( WebLog requestObject )
		{
			WebJobsLog.Append( requestObject );
			return true;
		}

		public override WebLogList ResponseSearchWebJobLogs( WebLogSearch requestObject ) => WebJobsLog.SearchLogs( requestObject.CarrierId, requestObject.LogType,
		                                                                                                            requestObject.FromDateTime, requestObject.ToDateTime,
		                                                                                                            requestObject.SearchString ).ToWebLogList();
	#endregion

	#region SOAP
		public override WebLogCarrierList ResponseGetSOAPCarrierList() => SOAPLog.GetSOAPCarrierList().ToWebLogCarrierList();

		public override string ResponseGetSOAPLogContents( string carrierId, string logType, string logName ) => SOAPLog.GetSOAPLogContents( carrierId, logType, logName );

		public override WebLogList ResponseGetSOAPLogList( string carrierId ) => SOAPLog.GetSOAPLogsList( carrierId ).ToWebLogListDistinct();

		public override WebLogList ResponseGetSOAPLogs( string carrierId, string logType ) => SOAPLog.GetSOAPLogsList( carrierId ).ToWebLogListFiltered( $"{logType}/".Pack() );

		public override bool ResponseLogSOAP( WebLog requestObject )
		{
			SOAPLog.Append( Context.CarrierId, requestObject );
			return true;
		}

		public override WebLogList ResponseSearchSOAPLogs( WebLogSearch requestObject ) => SOAPLog.SearchLogs( requestObject.CarrierId, requestObject.LogType,
		                                                                                                       requestObject.FromDateTime, requestObject.ToDateTime,
		                                                                                                       requestObject.SearchString ).ToWebLogList();
	#endregion
	}
}