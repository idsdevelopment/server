﻿using CompanyAddress = Protocol.Data.CompanyAddress;

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override long ResponseBeginCustomerImport( string requestObject )
		{
			var Retval = Users.StartProcess( Context );

			var DbContext = Context.Clone();

			Task.Run( () =>
			          {
				          try
				          {
					          var LogContext = DbContext.Clone();

					          Task.Run( () =>
					                    {
						                    LogContext.LogProgramStarted( requestObject );
					                    } );

					          using var Db = new CarrierDb( DbContext );

					          Db.DeleteAllResellerCustomers();
				          }
				          finally
				          {
					          Users.EndProcess( DbContext, Retval );
				          }
			          } );

			return Retval;
		}

		public override long ResponseImportCustomers( AddUpdateCustomer requestObject )
		{
			var Retval = Users.StartProcess( Context );

			var DbContext = Context.Clone();

			Task.Run( () =>
			          {
				          try
				          {
					          ResponseAddUpdateCustomer( requestObject );
				          }
				          finally
				          {
					          Users.EndProcess( DbContext, Retval );
				          }
			          } );

			return Retval;
		}


		public override ResellersCustomerCompanyLookup ResponseGetCustomerCompanyAddress( string customerCode, string companyName )
		{
			using var Db = new CarrierDb( Context );

			return Db.GetCustomerCompanyAddress( customerCode, companyName );
		}

		public override string ResponseAddUpdateCustomer( AddUpdateCustomer requestObject )
		{
			using var Db = new CarrierDb( Context );

			return Db.AddUpdateResellerCustomer( requestObject );
		}

		public override CustomerLookupSummaryList ResponseCustomerLookup( CustomerLookup requestObject )
		{
			using var Db = new CarrierDb( Context );

			return Db.CustomersLookup( requestObject );
		}

		public override CustomerLookupSummary ResponseCustomerSummaryLookup( string customerAccountCode )
		{
			using var Db = new CarrierDb( Context );

			return Db.CustomerSummaryLookup( customerAccountCode );
		}

		public override CompanyAddress ResponseGetResellerCustomerCompany( string requestObject )
		{
			using var Db = new CarrierDb( Context );

			return Db.GetResellerCustomerCompany( requestObject );
		}

		public override CompanyByAccountAndCompanyNameList ResponseCheckMissingCompanies( CompanyByAccountAndCompanyNameList requestObject )
		{
			using var Db = new CarrierDb( Context );

			return Db.FindMissingCompanies( requestObject );
		}

		public override bool ResponseCheckCompanyByCompanyAndAddressLine1( CompanyByAccountSummary requestObject )
		{
			using var Db = new CarrierDb( Context );

			return Db.CheckCustomerCompanyAndAddressLine1( requestObject );
		}

		public override ResellersCustomerCompanyLookup ResponseGetCustomerCompanyBySecondaryId( string requestObject )
		{
			using var Db = new CarrierDb( Context );

			return Db.GetCustomerCompanySecondaryId( requestObject );
		}

		public override CustomerCodeCompanyNameList ResponseGetCustomerCodeCompanyNameList()
		{
			using var Db = new CarrierDb( Context );

			return Db.GetCustomerCodeCompanyNameList();
		}

		public override CustomerCodeList ResponseGetCustomerCodeList()
		{
			using var Db = new CarrierDb( Context );

			return Db.GetCustomerList();
		}
	}
}