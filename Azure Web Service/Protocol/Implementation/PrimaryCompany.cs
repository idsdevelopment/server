﻿namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override string ResponseAddUpdatePrimaryCompany( AddUpdatePrimaryCompany requestObject )
		{
			using var Db = new CarrierDb( Context );

			return Db.AddUpdatePrimaryCompany( requestObject );
		}

		public override PrimaryCompany ResponseGetPrimaryCompanyByCompanyName( string requestObject )
		{
			using var Db = new CarrierDb( Context );

			return Db.GetPrimaryCompanyByCompanyName( requestObject );
		}

		public override PrimaryCompany ResponseGetPrimaryCompanyByCustomerCode( string requestObject )
		{
			using var Db = new CarrierDb( Context );

			return Db.GetPrimaryCompanyByCustomerCode( requestObject );
		}

		public override PrimaryCompany ResponseGetPrimaryCompanyByLoginCode( string requestObject )
		{
			using var Db = new CarrierDb( Context );

			return Db.GetPrimaryCompanyByLoginCode( requestObject );
		}

		public override CustomerCodeList ResponseGetPrimaryCompanyList()
		{
			using var Db = new CarrierDb( Context );

			return Db.GetCustomerList();
		}
	}
}