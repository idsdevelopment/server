﻿using Image = Imaging.Image;

namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override Images ResponseDownloadImage( string baseFolder, string imageName ) => StorageV2.Carrier.Images.GetImages( Context, baseFolder, imageName );

		public override Images ResponseDownloadImageByType( GetImages requestObject )
		{
			var Images = requestObject.ImageType switch
			             {
				             GetImages.IMAGE_TYPE.TRIP => ResponseDownloadImage( "Trips", requestObject.ImageNameOrId ),
				             _                         => new Images()
			             };
			new Image().Resize( Images, requestObject.Resolution.Height, requestObject.Resolution.Width );
			return Images;
		}

		public override void ResponseUploadImage( Images images )
		{
			foreach( var (DateTime, Nme, Status, Bytes, MetaData) in images )
			{
				var Name = Nme.Trim();

				var Folder = System.IO.Path.GetDirectoryName( Name );

				Folder = Folder is null ? "" : Folder.Trim();

				if( Folder != "" )
					Folder += '\\';

				var ImageName = System.IO.Path.GetFileNameWithoutExtension( Name ).Trim();
				var Path      = $"{Folder}{ImageName}\\{Status.AsString()}";

				var ImageType = System.IO.Path.GetExtension( Name ).Trim();
				ImageName = $"{DateTime:O}{ImageType}";

				StorageV2.Carrier.Images.Add( Context, Path, ImageName, Bytes, MetaData );
			}
		}
	}
}