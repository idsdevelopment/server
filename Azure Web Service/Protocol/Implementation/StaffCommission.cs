﻿namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override void ResponseSetCommissionCalculated( string tripId )
		{
			using var Db = new CarrierDb( Context );
			Db.SetCommissionCalculated( tripId );
		}
	}
}