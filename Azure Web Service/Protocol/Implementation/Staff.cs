﻿using Staff = Protocol.Data.Staff;

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override StaffLookupSummaryList ResponseStaffLookup( StaffLookup requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.StaffLookup( requestObject );
		}

		public override bool ResponseStaffMemberExists( string requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.StaffMemberExists( requestObject );
		}

		public override StaffList ResponseGetStaff()
		{
			using var Db = new CarrierDb( Context );
			return Db.GetStaffAsProtocolStaff();
		}

		public override StaffAndRolesList ResponseGetStaffAndRoles()
		{
			using var Db = new CarrierDb( Context );
			return Db.GetStaffAndRoles();
		}

		public override Staff ResponseGetStaffMember( string staffId )
		{
			using var Db = new CarrierDb( Context );
			return Db.GetStaffMember( staffId );
		}

		public override void ResponseUpdateStaffMember( UpdateStaffMemberWithRolesAndZones requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.AddUpdateStaffMember( requestObject );
		}

		public override void ResponseDeleteStaffMember( string programName, string staffId )
		{
			using var Db = new CarrierDb( Context );
			Db.DeleteStaffMember( programName, staffId );
		}

		public override Notes ResponseGetStaffNotes( string staffId )
		{
			using var Db = new CarrierDb( Context );
			return Db.GetStaffNotes( staffId );
		}

		public override bool ResponseIsDriver( string requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.IsDriver( requestObject );
		}

		public override void ResponseRenameStaffNote( string programName, string staffId, string oldNoteName, string newNoteName )
		{
			using var Db = new CarrierDb( Context );
			Db.RenameStaffNote( programName, staffId, oldNoteName, newNoteName );
		}

		public override void ResponseAddUpdateStaffNote( string programName, string staffId, string noteName, string note )
		{
			using var Db = new CarrierDb( Context );
			Db.AddStaffUpdateNote( programName, staffId, noteName, note );
		}

		public override void ResponseDeleteStaffNote( string programName, string staffId, string noteName )
		{
			using var Db = new CarrierDb( Context );
			Db.DeleteStaffNote( programName, staffId, noteName );
		}

		public override StaffLookupSummaryList ResponseGetAdministrators()
		{
			using var Db = new CarrierDb( Context );
			return Db.GetAdministrators();
		}

		public override StaffLookupSummaryList ResponseGetDrivers()
		{
			using var Db = new CarrierDb( Context );
			return Db.GetDrivers();
		}
	}
}