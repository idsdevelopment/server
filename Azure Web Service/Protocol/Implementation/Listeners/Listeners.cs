﻿namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override List<TripUpdatePacket> ResponseListenTripUpdate( string t, string q, string th )
		{
			var (Ok, DataAndIds) = Messaging<TripUpdateMessage>.Listen( Context, t, q, th );

			var Result = new List<TripUpdatePacket>();

			if( Ok )
			{
				foreach( var DataAndId in DataAndIds )
				{
					Result.Add( new TripUpdatePacket
					            {
						            Id   = DataAndId.Id,
						            Data = DataAndId.Data
					            } );
				}
			}
			return Result;
		}

		public override bool ResponseListenTripUpdateAck( List<long> idList ) => Messaging<TripUpdateMessage>.ListenAck( Context, idList );

		public override bool ResponseTripUpdateReceiveTripUpdateMessage( string t, string q, string th, List<TripUpdateMessage> receivedObject ) => Broadcast( receivedObject );
	}
}