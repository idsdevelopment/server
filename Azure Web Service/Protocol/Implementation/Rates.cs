﻿namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override RateResult ResponseCalculateRate( Rate requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.CalculateRate( requestObject );
		}
	}
}