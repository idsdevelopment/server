﻿namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override RecordLookupResponse ResponseGetLookupRecords( RecordLookup requestObject )
		{
			using var Db = new CarrierDb( Context );

			var Response = new RecordLookupResponse
						   {
							   RecordType = requestObject.RecordType
						   };

			if( requestObject.Count < 0 )
			{
				Response.Count = requestObject.RecordType switch
								 {
									 RecordLookupType.RECORD_TYPE.COMPANY => Db.GetCompanyRecordCount(),
									 _                                    => 0
								 };
			}
			else
			{
				switch( requestObject.RecordType )
				{
				case RecordLookupType.RECORD_TYPE.COMPANY:
					Response.CompanyList = Db.GetCompanies( requestObject.Offset, requestObject.Count );
					break;
				}
			}

			return Response;
		}
	}
}