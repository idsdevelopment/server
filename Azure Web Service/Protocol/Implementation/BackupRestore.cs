﻿namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override BackupList ResponseBackupList()
		{
			using var Db = new CarrierDb( Context );

			return Db.BackupList();
		}

		public override long ResponseBackupRestore( BackupRestore requestObject )
		{
			using var Db = new CarrierDb( Context );

			return Db.BackupRestore( requestObject );
		}

		public override bool ResponseDeleteBackups( BackupList requestObject )
		{
			using var Db = new CarrierDb( Context );

			return Db.DeleteBackups( requestObject );
		}

		public override long ResponseDeleteTestCarrier( string requestObject )
		{
			using var Db = new CarrierDb( Context );

			return Db.DeleteTestCarrier( requestObject );
		}
	}
}