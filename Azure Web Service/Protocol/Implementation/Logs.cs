﻿namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		private const string LOG_STORAGE = "Program";

		public override void ResponseAuditTrailNew( string program, string text, string comment )
		{
			AuditTrailOp( program, text, comment, AuditTrailEntryBase.OPERATION.NEW );
		}

		public override void ResponseAuditTrailModify( string program, string text, string comment )
		{
			AuditTrailOp( program, text, comment, AuditTrailEntryBase.OPERATION.MODIFY );
		}

		public override void ResponseAuditTrailDelete( string program, string text, string comment )
		{
			AuditTrailOp( program, text, comment, AuditTrailEntryBase.OPERATION.DELETE );
		}

		public override void ResponseAuditTrailInform( string program, string text, string comment )
		{
			AuditTrailOp( program, text, comment, AuditTrailEntryBase.OPERATION.INFORM );
		}

		public override Log ResponseGetLog( LogLookup requestObject )
		{
			return requestObject.Log switch
			       {
				       LOG.TRIP => TripAuditTrail.GetLog( Context, requestObject ),
				       _        => UserAuditTrail.GetLog( Context, requestObject )
			       };
		}

		private void AuditTrailOp( string program, string text, string comment, AuditTrailEntryBase.OPERATION op )
		{
			UserAuditTrail.Add( LOG_STORAGE, new UserAuditTrailEntryEntry( Context, op, program, text, comment ) );
		}
	}
}