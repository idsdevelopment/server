﻿namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override Settings ResponseImportSettings() => new CarrierDb( Context ).GetImportSettings();

		public override void ResponseUpdateImportSettings( Settings requestObject )
		{
			new CarrierDb( Context ).UpdateImportSettings( requestObject );
		}
	}
}