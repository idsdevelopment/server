﻿namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override void ResponseLocationDeliveries( LocationDeliveries requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.LocationDeliveries( requestObject );
		}
	}
}