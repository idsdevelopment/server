﻿using System.IO;

namespace StorageV2;

public abstract class BlobStorageBase<TClient> : StorageBase where TClient : BlobBaseClient
{
	public (List<string> Directories, List<string> Blobs) BlobList
	{
		get
		{
			var Blobs       = new List<string>();
			var Directories = new List<string>();

			foreach( var BlobItem in from Item in ContainerClient.GetBlobsByHierarchy( delimiter: "/" )
			                         select Item )
			{
				if( BlobItem.IsBlob )
					Blobs.Add( BlobItem.Blob.Name );
				else if( BlobItem.IsPrefix )
					Directories.Add( BlobItem.Prefix ); // Leave '/' symbol.  Indicates has sub directories
			}
			return ( Directories, Blobs );
		}
	}

	protected readonly string BlobName;

	protected readonly BlobContainerClient ContainerClient;

	public IList<BlobItem> FlatBlobListDetailed( string path = "", string? delimiter = "/" )
	{
		IList<BlobItem> ListBlobs( string basePath )
		{
			var Result = new List<BlobItem>();

			foreach( var Hierarchy in ContainerClient.GetBlobsByHierarchy( prefix: basePath, delimiter: delimiter ) )
			{
				if( Hierarchy.IsPrefix )
					Result.AddRange( ListBlobs( Hierarchy.Prefix ) );
				else if( Hierarchy.IsBlob )
					Result.Add( Hierarchy.Blob );
			}
			return Result;
		}

		return ListBlobs( path );
	}

	public List<string> FlatBlobList( string prefix ) => ( from B in FlatBlobListDetailed( prefix )
	                                                       select B.Name ).ToList();

	public byte[] DownloadBlobAsByteArray()
	{
		var Content = BlobClient.DownloadContent();
		return Content.Value.Content.ToArray();
	}

	public string DownloadBlobAsString()
	{
		var Content = BlobClient.DownloadContent();
		return Content.Value.Content.ToString();
	}

	public Response<BlobContentInfo>? ClientUploadBlob( BinaryData content, CancellationToken cancellationToken = default ) => ContainerClient.UploadBlob( BlobName, content, cancellationToken );


	public Response<BlobContentInfo>? ClientUploadBlob( Stream content, CancellationToken cancellationToken = default )
	{
		content.Position = 0;
		return ContainerClient.UploadBlob( BlobName, content, cancellationToken );
	}

	public void UploadBlob( BinaryData content, CancellationToken cancellationToken = default )
	{
		Retry( () => ClientUploadBlob( content, cancellationToken ) );
	}

	public void UploadBlob( Stream content, CancellationToken cancellationToken = default )
	{
		Retry( () => ClientUploadBlob( content, cancellationToken ) );
	}

	public void UploadBlob( byte[] content, CancellationToken cancellationToken = default )
	{
		var Content = new BinaryData( content );
		Retry( () => ClientUploadBlob( Content, cancellationToken ) );
	}

	public void UploadBlob( string content, CancellationToken cancellationToken = default )
	{
		var Content = new BinaryData( content );
		Retry( () => ClientUploadBlob( Content, cancellationToken ) );
	}


#region Delete
	public void DeleteBlob()
	{
		BlobClient.DeleteIfExists( DeleteSnapshotsOption.IncludeSnapshots );
	}
#endregion

	internal BlobStorageBase( IRequestContext context, string containerName, string blobName, bool toIdsStorage = false )
		: base( context, containerName, blobName, toIdsStorage )
	{
		BlobName        = blobName;
		ContainerClient = new BlobContainerClient( ConnectionString, ContainerName.TrimToLower(), BlobOptions );
	}

#region Overrides
	protected override void OnDeleteContainer( string containerName )
	{
		ContainerClient.Delete();
	}

	protected override void OnDeleteObject( string objectName )
	{
		BlobClient.Delete( DeleteSnapshotsOption.IncludeSnapshots );
	}

	protected override void OnCreateContainer( string containerName )
	{
		ContainerClient.Create();
	}

	protected override bool OnContainerExists( string containerName ) => ContainerClient.Exists();

	protected override IDictionary<string, string> OnGetMetaData() => BlobClient.GetProperties().Value.Metadata;

	protected override void OnSetMetaData( IDictionary<string, string> metaData )
	{
		BlobClient.SetMetadata( metaData );
	}
#endregion

#region Client
	private TClient? _BlobClient;

	protected TClient BlobClient
	{
		get { return _BlobClient ??= GetBlobClient(); }
	}

	protected abstract TClient GetBlobClient();
#endregion
}