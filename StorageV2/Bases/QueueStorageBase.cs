﻿using System.Text;
using Azure.Storage.Queues.Models;

namespace StorageV2;

public static class QueueExtensions
{
	public static string FixQueueName( this string name )
	{
		var Result = new StringBuilder();

		foreach( var C in name )
		{
			Result.Append( C switch
			               {
				               >= 'a' and <= 'z' or >= '0' and <= '9' => C,
				               >= 'A' and <= 'Z'                      => (char)( ( C - 'A' ) + 'a' ),
				               _                                      => '-'
			               } );
		}
		return Result.ToString();
	}
}

public class QueueStorageBase : StorageBase
{
	public           string             QueueName => ContainerName;
	private readonly QueueServiceClient QueueServiceClient;
	private readonly QueueClient?       QueueClient;

	public IList<string> QueueList() => ( from Q in QueueServiceClient.GetQueues()
	                                      select Q.Name ).ToList();

	protected override void OnDeleteObject( string objectName )
	{
	}

	protected override void OnDeleteContainer( string containerName )
	{
		QueueClient?.Delete();
	}

	// Create the Queue
	protected override void OnCreateContainer( string containerName )
	{
		QueueClient?.Create();
	}

	// Queue Exists
	protected override bool OnContainerExists( string containerName ) => QueueClient?.Exists() ?? false;

	protected override IDictionary<string, string> OnGetMetaData() => QueueClient?.GetProperties().Value.Metadata ?? DefaultDictionary;

	protected override void OnSetMetaData( IDictionary<string, string> metaData )
	{
		QueueClient?.SetMetadata( metaData );
	}

	public void Delete()
	{
		try
		{
			QueueClient?.Delete();
		}
		catch( RequestFailedException E ) when( E.ErrorCode == QueueErrorCode.QueueNotFound )
		{
		}
	}

	public void Clear()
	{
		try
		{
			QueueClient?.ClearMessages();
		}
		catch( RequestFailedException E ) when( E.ErrorCode == QueueErrorCode.QueueNotFound )
		{
		}
	}

	public void SendMessage( string message, TimeSpan? visibilityTimeout = null )
	{
		if( QueueClient is not null )
		{
			Retry( () =>
			       {
				       QueueClient.SendMessage( message, visibilityTimeout );
			       } );
		}
	}

	public IList<string> ReceiveMessages( int? maxCount = null )
	{
		List<string> Result = new();

		if( QueueClient is not null )
		{
			Retry( () =>
			       {
				       try
				       {
					       var Message = QueueClient.ReceiveMessages( maxCount );
					       var Value   = Message.Value;

					       foreach( var QueueMessage in Value )
					       {
						       Result.Add( QueueMessage.MessageText );
						       QueueClient.DeleteMessage( QueueMessage.MessageId, QueueMessage.PopReceipt );
					       }
				       }
				       catch( RequestFailedException E ) when( E.ErrorCode == QueueErrorCode.QueueNotFound )
				       {
				       }
			       } );
		}
		return Result;
	}

	public string? ReceiveMessage()
	{
		var Result = ReceiveMessages( 1 );
		return Result.Count > 0 ? Result[ 0 ] : null;
	}

	public QueueStorageBase( IRequestContext context, string queueName, bool toIdsStorage = false ) : base( context, queueName.FixQueueName(), queueName.FixQueueName(), toIdsStorage )
	{
		QueueServiceClient = new QueueServiceClient( ConnectionString );
		QueueClient        = new QueueClient( ConnectionString, ContainerName, QueueOptions );
	}

	public QueueStorageBase( IRequestContext context, bool toIdsStorage = false ) : base( context, "", "", toIdsStorage )
	{
		QueueServiceClient = new QueueServiceClient( ConnectionString );
	}
}