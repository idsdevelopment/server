﻿using System.Diagnostics;
using System.Linq.Expressions;
using Azure.Data.Tables;
using Azure.Data.Tables.Models;

namespace StorageV2;

public class TableEntity : ITableEntity
{
	public string          PartitionKey { get; set; } = "";
	public string          RowKey       { get; set; } = "";
	public DateTimeOffset? Timestamp    { get; set; } = DateTimeOffset.MinValue;
	public ETag            ETag         { get; set; } = new( "*" ); // Don't care about overwrite

	public TableEntity()
	{
	}

	public TableEntity( string partitionKey, string rowKey )
	{
		PartitionKey = partitionKey;
		RowKey       = rowKey;
	}

	public TableEntity( string partitionKey, string rowKey, DateTimeOffset timestamp ) : this( partitionKey, rowKey )
	{
		Timestamp = timestamp;
	}
}

public class TableStorageBase : StorageBase
{
	private readonly TableClient        TableClient;
	private readonly TableServiceClient TableServiceClient;

	public TableStorageBase( IRequestContext context, string tableName, bool toIdsStorage = false ) : base( context, tableName, tableName, toIdsStorage )
	{
		TableServiceClient = new TableServiceClient( ConnectionString );
		TableClient        = new TableClient( ConnectionString, ContainerName );
	}

	protected override void OnDeleteObject( string objectName )
	{
	}

	protected override void OnDeleteContainer( string tableName )
	{
		TableClient.Delete();
	}

	// Create Table
	protected override void OnCreateContainer( string tableName )
	{
		TableClient.Create();
	}

	protected override bool OnContainerExists( string tableName )
	{
		var Table = TableServiceClient.Query( $"TableName eq '{tableName}'" );
		return Table.Count() == 1;
	}

	protected override IDictionary<string, string> OnGetMetaData() => DefaultDictionary;

	protected override void OnSetMetaData( IDictionary<string, string> metaData )
	{
	}

	public void Delete()
	{
		try
		{
			TableClient.Delete();
		}
		catch( RequestFailedException E ) when( E.ErrorCode == TableErrorCode.TableNotFound )
		{
		}
	}

	public void AddEntity<T>( T entity ) where T : ITableEntity
	{
		Retry( () =>
		       {
		       #if DEBUG
			       try
			       {
				       TableClient.AddEntity( entity );
			       }
			       catch( Exception Exception )
			       {
				       Debug.WriteLine( $"""
				                         ----------------------------------------
				                         Partition Key = {entity.PartitionKey}
				                         Row Key = {entity.RowKey}
				                         Exception = {Exception.Message}
				                         Stack Trace = {Exception.StackTrace}
				                         ----------------------------------------
				                         """
				                      );
				       throw;
			       }
		       #else
				       TableClient.AddEntity( entity );
		       #endif
		       } );
	}

	public void AddOrUpdateEntity<T>( T entity ) where T : ITableEntity
	{
		Retry( () =>
		       {
			       try
			       {
				       TableClient.AddEntity( entity );
			       }
			       catch( RequestFailedException E ) when( E.ErrorCode == TableErrorCode.EntityAlreadyExists )
			       {
				       TableClient.UpdateEntity( entity, new ETag( "*" ), TableUpdateMode.Replace );
			       }
		       } );
	}

	public Pageable<T> Query<T>( Expression<Func<T, bool>> filter,
	                             int? maxPerPage = null,
	                             IEnumerable<string>? select = null,
	                             CancellationToken cancellationToken = default ) where T : class, ITableEntity, new()
	{
		var Result = Pageable<T>.FromPages( new List<Page<T>>() );

		Retry( () =>
		       {
			       Result = TableClient.Query( filter, maxPerPage, select, cancellationToken );
		       } );

		return Result;
	}

	public T? GetEntity<T>( string partitionKey, string rowKey ) where T : class, ITableEntity, new()
	{
		T? Result = null;

		Retry( () =>
		       {
			       Result = QueryNoRetry<T>( ent => ( ent.PartitionKey == partitionKey ) && ( ent.RowKey == rowKey ) ).FirstOrDefault();
		       } );

		return Result;
	}


	public IList<T> GetRowEntities<T>( string rowKey ) where T : class, ITableEntity, new()
	{
		var Result = new List<T>();

		Retry( () =>
		       {
			       Result = QueryNoRetry<T>( ent => ent.RowKey == rowKey ).ToList();
		       } );

		return Result;
	}


	public void DeleteEntity( string partitionKey, string rowKey )
	{
		Retry( () =>
		       {
			       TableClient.DeleteEntity( partitionKey, rowKey );
		       } );
	}

	public void DeleteEntity( TableEntity entity )
	{
		DeleteEntity( entity.PartitionKey, entity.RowKey );
	}

#region GetPartitionEntities
	public IList<T> GetPartitionEntities<T>( string partitionKey ) where T : class, ITableEntity, new()
	{
		var Result = new List<T>();

		Retry( () =>
		       {
			       Result = QueryNoRetry<T>( ent => ent.PartitionKey == partitionKey ).ToList();
		       } );
		return Result;
	}

	public IList<T> GetTimeStampEntities<T>( DateTimeOffset fromDateTime, DateTimeOffset toDateTime ) where T : class, ITableEntity, new()
	{
		return Query<T>( ent => ( ent.Timestamp >= fromDateTime ) && ( ent.Timestamp <= toDateTime ) ).ToList();
	}
#endregion

#region Date Time Entity
	private static readonly DateTime MinUteDateTime = new( 1601, 1, 1, 0, 0, 0, DateTimeKind.Utc );
	public IList<T> GetEntities<T>() where T : class, ITableEntity, new() => GetTimeStampEntities<T>( MinUteDateTime, DateTimeOffset.MaxValue );
#endregion

#region No Retry
	public Pageable<T> QueryNoRetry<T>( Expression<Func<T, bool>> filter,
	                                    int? maxPerPage = null,
	                                    IEnumerable<string>? select = null,
	                                    CancellationToken cancellationToken = default ) where T : class, ITableEntity, new() => TableClient.Query( filter, maxPerPage, select, cancellationToken );

	public T? GetEntityNoRetry<T>( string partitionKey, string rowKey ) where T : class, ITableEntity, new()
	{
		return QueryNoRetry<T>( ent => ( ent.PartitionKey == partitionKey ) && ( ent.RowKey == rowKey ) ).FirstOrDefault();
	}

	public bool Exists<T>( string partitionKey, string rowKey ) where T : class, ITableEntity, new()
	{
		try
		{
			return GetEntityNoRetry<T>( partitionKey, rowKey ) is not null;
		}
		catch( RequestFailedException E ) when( E.ErrorCode == TableErrorCode.TableNotFound )
		{
		}
		catch( Exception Exception )
		{
			LogException( Exception );
		}
		return false;
	}
#endregion
}