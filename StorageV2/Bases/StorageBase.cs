﻿using System.Diagnostics;
using Azure.Data.Tables;
using Azure.Data.Tables.Models;
using Azure.Storage.Queues.Models;
using Interfaces.Abstracts;

// ReSharper disable ExplicitCallerInfoArgument

namespace StorageV2;

public abstract class StorageBase
{
#region MetatData
	public IDictionary<string, string> MetaData
	{
		get
		{
			var Result = DefaultDictionary;

			Retry( () =>
			       {
				       Result = OnGetMetaData();
			       } );

			return Result;
		}

		set
		{
			Retry( () =>
			       {
				       OnSetMetaData( value );
			       } );
		}
	}
#endregion

	public bool   Err          { get; private set; }
	public string ErrorMessage { get; private set; } = "";

	protected static readonly BlobClientOptions BlobOptions = new()
	                                                          {
		                                                          Diagnostics =
		                                                          {
			                                                          IsLoggingEnabled            = false,
			                                                          IsTelemetryEnabled          = false,
			                                                          IsDistributedTracingEnabled = false
		                                                          },
		                                                          Retry =
		                                                          {
			                                                          Mode       = RetryMode.Exponential,
			                                                          MaxRetries = 10,
			                                                          Delay      = TimeSpan.FromSeconds( 1 )
		                                                          }
	                                                          };

	protected static readonly QueueClientOptions QueueOptions = new()
	                                                            {
		                                                            Diagnostics =
		                                                            {
			                                                            IsLoggingEnabled            = false,
			                                                            IsTelemetryEnabled          = false,
			                                                            IsDistributedTracingEnabled = false
		                                                            },
		                                                            Retry =
		                                                            {
			                                                            Mode       = RetryMode.Exponential,
			                                                            MaxRetries = 10,
			                                                            Delay      = TimeSpan.FromSeconds( 1 )
		                                                            }
	                                                            };

	protected static readonly TableClientOptions TableOptions = new()
	                                                            {
		                                                            Diagnostics =
		                                                            {
			                                                            IsLoggingEnabled            = false,
			                                                            IsTelemetryEnabled          = false,
			                                                            IsDistributedTracingEnabled = false
		                                                            },
		                                                            Retry =
		                                                            {
			                                                            Mode       = RetryMode.Exponential,
			                                                            MaxRetries = 10,
			                                                            Delay      = TimeSpan.FromSeconds( 1 )
		                                                            }
	                                                            };


	protected static readonly IDictionary<string, string> DefaultDictionary = new ReadOnlyDictionary<string, string>( new Dictionary<string, string>() );
	protected                 string                      ConnectionString => Account.ConnectionString;

	private readonly StorageAccount Account;

	protected readonly string ContainerName,
	                          ObjectName;

	protected readonly IRequestContext Context;

	private bool InDeleteContainer;

	internal StorageBase( IRequestContext context, string containerName, string objectName, bool toIdsStorage = false )
	{
		Context       = context;
		ContainerName = containerName.Trim();
		ObjectName    = objectName.Trim();
		Account       = new StorageAccount( context, toIdsStorage );
	}

	public static string MakeCarrierStorageName( IRequestContext context, string storageName, bool allowDebug = true )
	{
		var Debug = allowDebug && context.Debug ? Constants.TEST : "";

		return $"{Debug}{storageName}";
	}

	public static IRequestContext? NewContext() => AIRequestInterface.NewContext?.Invoke( "idsroute", "" );
	public static string ToDateTime( DateTimeOffset t, int offset ) => $"{t:yyyy-MM-dd_hh-mm-ss}_Tz_{offset:D2}";
	public static string ToDateTime( DateTimeOffset t ) => ToDateTime( t, t.Offset.Hours );

	public static string ToDateTimeHours( DateTimeOffset t, int offset ) => $"{t:yyyy-MM-dd}_Tz_{offset:D2}_{t:HH}";
	public static string ToDateTimeHours( DateTimeOffset t ) => ToDateTimeHours( t, t.Offset.Hours );

	public static string ToDateTimePath( DateTimeOffset t, int offset ) => $"{t:yyyy/MM/dd}/Tz_{offset:D2}/{t:HH}";
	public static string ToDateTimePath( DateTimeOffset t ) => ToDateTimePath( t, t.Offset.Hours );

	public static string ToDate( DateTimeOffset t, int offset ) => $"{t:yyyy-MM-dd}_Tz_{offset:D2}";
	public static string ToDate( DateTimeOffset t ) => ToDate( t, t.Offset.Hours );


#region Retry
	protected void Retry( Action action )
	{
		for( var I = 60; I-- > 0; )
		{
			try
			{
				action();
				return;
			}
			catch( RequestFailedException E ) when( ( E.ErrorCode == BlobErrorCode.BlobAlreadyExists )
			                                        || ( E.ErrorCode == QueueErrorCode.QueueAlreadyExists )
			                                        || ( E.ErrorCode == TableErrorCode.TableAlreadyExists ) )
			{
				DeleteObject( ObjectName );
			}

			catch( RequestFailedException E ) when( ( E.ErrorCode == BlobErrorCode.ContainerNotFound )
			                                        || ( E.ErrorCode == QueueErrorCode.QueueNotFound )
			                                        || ( E.ErrorCode == TableErrorCode.TableNotFound ) )
			{
				if( !InDeleteContainer )
				{
					for( var K = 20; K-- > 0; )
					{
						try
						{
							CreateContainer( ContainerName );
							break;
						}
						catch( RequestFailedException Ex ) when( ( Ex.ErrorCode == BlobErrorCode.ContainerAlreadyExists )
						                                         || ( Ex.ErrorCode == BlobErrorCode.BlobAlreadyExists )
						                                         || ( Ex.ErrorCode == QueueErrorCode.QueueAlreadyExists )
						                                         || ( Ex.ErrorCode == TableErrorCode.TableAlreadyExists ) )
						{
							return;
						}
						catch( RequestFailedException Ex ) when( ( Ex.ErrorCode == BlobErrorCode.ContainerBeingDeleted )
						                                         || ( Ex.ErrorCode == QueueErrorCode.QueueBeingDeleted )
						                                         || ( Ex.ErrorCode == TableErrorCode.TableBeingDeleted ) )
						{
							Thread.Sleep( 3_000 );
						}
						catch( Exception Exception )
						{
							LogException( Exception );
							return;
						}
					}
					var J = 100;

					while( J-- > 0 )
					{
						if( !ContainerExists )
							Thread.Sleep( 10 );
						else
							break;
					}

					if( J <= 0 )
					{
						LogException( new Exception( $"Cannot create container\"{ContainerName}\"" ) );
						return;
					}
				}
				else
					break;
			}
			catch( RequestFailedException E ) when( E.ErrorCode == BlobErrorCode.BlobNotFound )
			{
				ObjectNotFound( ObjectName );
			}
			catch( RequestFailedException E ) when( ( E.ErrorCode == BlobErrorCode.AccountBeingCreated )
			                                        || ( E.ErrorCode == BlobErrorCode.ContainerBeingDeleted )
			                                        || ( E.ErrorCode == BlobErrorCode.ServerBusy )
			                                        || ( E.ErrorCode == BlobErrorCode.SystemInUse )
			                                        || ( E.ErrorCode == QueueErrorCode.AccountBeingCreated )
			                                        || ( E.ErrorCode == QueueErrorCode.QueueBeingDeleted )
			                                        || ( E.ErrorCode == QueueErrorCode.ServerBusy )
			                                        || ( E.ErrorCode == TableErrorCode.TableBeingDeleted ) )
			{
				Thread.Sleep( 1_000 );
			}
			catch( Exception Exception )
			{
				Debug.WriteLine( $"Storage 1: {Exception.Message}" );
				LogException( Exception );
				return;
			}
		}
	}
#endregion

#region Log Error
	private static          bool   InErrorLog;
	private static readonly object LockObject = new();

	protected void LogException( Exception ex, [CallerMemberName] string caller = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNumber = 0 )
	{
		Err          = true;
		ErrorMessage = ex is RequestFailedException Rex ? $"{Rex.ErrorCode ?? ""}\r\n{Rex.Message}" : ex.Message;

		lock( LockObject )
		{
			if( !InErrorLog )
			{
				InErrorLog = true;

				try
				{
					Debug.WriteLine( $"Storage 2: {ex.Message}" );
					Context.SystemLogException( ex, caller, fileName, lineNumber );
				}
				finally
				{
					InErrorLog = false;
				}
			}
		}
	}
#endregion

#region Abstracts & Virtuals
#region Abstracts
	protected abstract void OnDeleteObject( string objectName );
	protected abstract void OnDeleteContainer( string containerName );
	protected abstract void OnCreateContainer( string containerName );
	protected abstract bool OnContainerExists( string containerName );

	protected abstract IDictionary<string, string> OnGetMetaData();
	protected abstract void OnSetMetaData( IDictionary<string, string> metaData );
#endregion

#region Virtuals
	protected virtual void ObjectNotFound( string objectName ) // Append Blob
	{
	}
#endregion
#endregion

#region Methods
	protected void DeleteObject( string objectName )
	{
		OnDeleteObject( objectName );
	}

	public void DeleteContainer( string objectName )
	{
		if( !InDeleteContainer )
		{
			InDeleteContainer = true;

			try
			{
				OnDeleteContainer( ContainerName );
			}
			finally
			{
				InDeleteContainer = false;
			}
		}
	}

	protected void CreateContainer( string containerName )
	{
		OnCreateContainer( containerName );
	}

	protected bool ContainerExists => OnContainerExists( ContainerName );
#endregion
}