﻿namespace StorageV2.Reports;

public static class Extensions
{
	public static IEnumerable<string> Chunks( this string txt, int chunkSize )
	{
		var Result = new List<string>();

		for( var I = 0; I < txt.Length; I += chunkSize )
		{
			var Length = Math.Min( chunkSize, txt.Length - I );
			Result.Add( txt.Substring( I, Length ) );
		}
		return Result;
	}
}

public class Report
{
	private const string HTML_CONTAINER = "htmlreports";

	private class HtmlReportStorage : AppendBlobStorage
	{
		internal HtmlReportStorage( IRequestContext context, string path, string blobName ) : base( context, HTML_CONTAINER, path, blobName )
		{
		}
	}

	private static HtmlReportStorage? GetStorage( IRequestContext context, string category, string subCategory, string subCategory1, string name )
	{
		try
		{
			static string Append( string txt )
			{
				txt = txt.Trim();
				return txt != "" ? $"{txt.Replace( '\\', '^' )}\\" : txt;
			}

			return new HtmlReportStorage( context, $"{Append( category )}{Append( subCategory )}{Append( subCategory1 )}", name.Trim() );
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
		return null;
	}

	public static void Save( IRequestContext context, string category, string subCategory, string subCategory1, string name, string htmlData )
	{
		var Context = context.Clone();

		Tasks.RunVoid( () =>
		               {
			               try
			               {
				               if( GetStorage( Context, category, subCategory, subCategory1, name ) is { } St )
				               {
					               foreach( var Chunk in htmlData.Chunks( 4_000_000 ) )
						               St.Append( Chunk );
				               }
			               }
			               catch( Exception Exception )
			               {
				               Context.SystemLogException( Exception );
			               }
		               } );
	}

	public static string Get( IRequestContext context, string category, string subCategory, string subCategory1, string name )
	{
		try
		{
			if( GetStorage( context, category, subCategory, subCategory1, name ) is { } St )
				return St.DownloadBlobAsString();
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}

		return "";
	}
}