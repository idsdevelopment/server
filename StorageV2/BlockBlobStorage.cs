﻿namespace StorageV2;

public class BlockBlobStorage : BlobStorageBase<BlockBlobClient>
{
	protected override BlockBlobClient GetBlobClient() => ContainerClient.GetBlockBlobClient( BlobName );

	public void Upload( string message )
	{
		UploadBlob( message );
	}

	public void Upload( byte[] content )
	{
		UploadBlob( new BinaryData( content ) );
	}

	public void Upload( BinaryData content )
	{
		UploadBlob( content.ToStream() );
	}

	public BlockBlobStorage( IRequestContext context, string containerName, string blobName, bool toIdsStorage = false )
		: base( context, containerName, blobName, toIdsStorage )
	{
	}

	protected BlockBlobStorage( IRequestContext context, string containerName, string path, string blobName, bool toIdsStorage = false )
		: this( context, containerName, StorageAccount.CombinePathAndName( path, blobName ), toIdsStorage )
	{
	}
}