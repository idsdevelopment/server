﻿using Azure_Web_Service;
using Microsoft.Azure.Management.Storage.Fluent.Models;

namespace StorageV2;

public class StorageAccount
{
	internal readonly string ConnectionString;

	protected readonly IRequestContext Context;


	public static (string AccountName, string AccountKey, Endpoints? EndPoint, bool Ok) CreateBlobStorageAccount( IRequestContext context )
	{
		try
		{
			var (GroupName, Ok) = Resource.CreateResourceGroup( context );

			if( Ok )
			{
				var Acc = Subscription.AzureAuthenticate( context ).StorageAccounts
				                      .Define( MakeStorageAccountName( context ) )
				                      .WithRegion( Subscription.DEFAULT_REGION )
				                      .WithExistingResourceGroup( GroupName )
				                      .Create();

				var StorageAccountKeys = Acc.GetKeys();
				var PrimaryKey         = StorageAccountKeys[ 0 ].Value;

				return ( AccountName: Acc.Name, AccountKey: PrimaryKey, EndPoint: Acc.EndPoints.Primary, Ok: true );
			}
		}
		catch( Exception E )
		{
			context.SystemLogException( nameof( CreateBlobStorageAccount ), E );
		}

		return ( AccountName: "", AccountKey: "", EndPoint: null, Ok: false );
	}

	public static string CombinePathAndName( string path, string name )
	{
		if( ( path != "" ) && !path.EndsWith( "/" ) )
			path += "/";

		return path + Uri.EscapeUriString( name );
	}

	public StorageAccount( IRequestContext context, bool toIdsStorage = false )
	{
		try
		{
			Context = context;

			string StorageAccount,
			       StorageKey;

			if( toIdsStorage )
			{
				var (Acnt, Key) = context.BlobStorageAccount;
				StorageAccount  = Acnt;
				StorageKey      = Key;
			}
			else
			{
				StorageAccount = context.StorageAccount;
				StorageKey     = context.StorageAccountKey;
			}

			ConnectionString = $"DefaultEndpointsProtocol=https;AccountName={StorageAccount};AccountKey={StorageKey}";
		}
		catch( Exception E )
		{
			Console.WriteLine( E );

			throw;
		}
	}

	private static string MakeStorageAccountName( IRequestContext context )
	{
		// Space in middle for  capitalisation
		var RetVal = $"{Subscription.SITE_NAME} {context.CarrierId}".ToLower().ToAlphaNumeric();

		if( RetVal.Length > 24 )
			RetVal = RetVal.MaxLength( 24 );

		return RetVal;
	}
}