﻿global using System;
global using Interfaces.Interfaces;
global using ResourceGroups;
global using Utils;
global using Azure.Storage.Blobs;
global using Azure.Storage.Blobs.Specialized;
global using System.Collections.Generic;
global using System.Collections.ObjectModel;
global using System.Runtime.CompilerServices;
global using System.Threading;
global using Azure.Storage.Blobs.Models;
global using Azure.Storage.Queues;
global using Newtonsoft.Json;
global using Protocol.Data;
global using System.Threading.Tasks;
global using Azure;
global using Azure.Core;
global using System.Linq;

#if NET5_0_OR_GREATER
global using Azure.ResourceManager;
global using Azure.ResourceManager.Storage;
global using Azure.ResourceManager.Storage.Models;
global using Microsoft.EntityFrameworkCore;

#else
global using System.Data.Entity.Infrastructure;
global using System.Data.Entity.Validation;

#endif