﻿namespace StorageV2;

public class QueueStorage : QueueStorageBase
{
	public QueueStorage( IRequestContext context, string queueName, bool toIdsStorage = false ) : base( context, queueName, toIdsStorage )
	{
	}

	public QueueStorage( IRequestContext context, bool toIdsStorage = false ) : base( context, toIdsStorage )
	{
	}
}