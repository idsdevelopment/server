﻿namespace StorageV2.Logs;

public class DriverLog : ALogBase
{
	private const string DRIVER_LOG = "Driver Logs";

	public static void Append( string program, string carrierId, string driver, object obj, [CallerMemberName] string caller = "" )
	{
		Tasks.RunVoid( () =>
		               {
			               carrierId = carrierId.TrimToLower();
			               driver    = driver.Trim().Capitalise();

			               var Now  = DateTime.UtcNow;
			               var Type = obj.GetType().Name;

			               var Json = JsonConvert.SerializeObject( obj, Formatting.Indented );

			               var Message = $"""
			                              ------- {Now:yyyy-MM-dd hh:mm:ss.fff zz} -------
			                              Packet Type: {Type}
			                              Caller: {caller}

			                              {Json.Trim()}

			                              """;

			               var Log = new WebLog
			                         {
				                         CarrierId   = carrierId,
				                         Program     = program,
				                         Message     = Message,
				                         BasePath    = "",
				                         LogDateTime = Now
			                         };

			               Append( Log, dateTime => $"{carrierId}/{DRIVER_LOG}/{driver}/{program}/{StorageBase.ToDateTimeHours( dateTime )}", false );
		               } );
	}

	public static void Append( IRequestContext context, string program, object obj, [CallerMemberName] string caller = "" )
	{
		Append( program, context.CarrierId, context.UserName, obj, caller );
	}
}