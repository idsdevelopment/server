﻿using System.Globalization;

namespace StorageV2.Logs;

public class WebJobsLog : ALogBase
{
	private const string CONTAINER = "WebJobs";

	public static string GetWebJobContents( string carrierId, string jobType, string jobName ) => GetContents( CONTAINER, carrierId, jobType, jobName );

	public static void Append( WebLog log )
	{
		Append( log, null );
	}

	public static List<string> GetWebJobCarrierList() => GetCarrierList( CONTAINER );

	public static List<string> GetWebJobList( string carrierId ) => GetLogList( CONTAINER, carrierId );

	public static List<string> SearchLogs( string carrierId, string logType, DateTimeOffset fromDate, DateTimeOffset toDate, string searchString )
	{
		return SearchLogs( CONTAINER, carrierId, logType, fromDate, toDate, searchString, logName =>
		                                                                                  {
			                                                                                  const string FORMAT = @"yyyy-MM-dd_T\z_zz";

			                                                                                  if( !DateTimeOffset.TryParseExact( logName, FORMAT, EnUs, DateTimeStyles.None, out var DateTime ) )
				                                                                                  DateTime = DateTimeOffset.MaxValue;

			                                                                                  return DateTime;
		                                                                                  } );
	}
}