﻿namespace StorageV2;

public class CrashLog : AppendBlobStorage
{
	private CrashLog( string logName, IRequestContext context, string carrierId, string userName, sbyte tz ) : base( context, logName, carrierId.Capitalise(), MakeBlobName( userName, tz ), true )
	{
	}

	internal static void Add( string logName, CrashReport log )
	{
		var Context = NewContext();

		if( Context is not null )
		{
			try
			{
				new CrashLog( logName, Context, log.Account, log.User, log.TSbyte ).InternalAdd( log );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
	}

	private void InternalAdd( CrashReport log )
	{
		Append( $"Account: {log.Account}\r\nUser: {log.User}\r\n\r\n{log.CrashData}" );
	}

	private static string MakeBlobName( string userName, sbyte tz ) => $"{userName.Trim()}_{ToDateTime( DateTimeOffset.UtcNow, tz )}.log";
}

public class ClientCrashLog
{
	public static void Add( CrashReport log )
	{
		CrashLog.Add( "CrashLogClient", log );
	}
}

public class AppCrashLog
{
	public static void Add( CrashReport log )
	{
		CrashLog.Add( "CrashLogApp", log );
	}
}

public class SoapCrashLog
{
	public static void Add( CrashReport log )
	{
		CrashLog.Add( "CrashLogSOAP", log );
	}
}