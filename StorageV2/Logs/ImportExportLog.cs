﻿namespace StorageV2.Logs;

public class ImportExportLog : ALogBase
{
	protected static void Append( string program, string carrierId, string logBase, DateTimeOffset importDateTime, string importedData, [CallerMemberName] string caller = "" )
	{
		Tasks.RunVoid( () =>
		               {
			               carrierId = carrierId.TrimToLower();

			               var Message = $"""
			                              ------- {importDateTime:yyyy-MM-dd hh:mm:ss.fff zz} -------
			                              {importedData}

			                              """;

			               var Log = new WebLog
			                         {
				                         CarrierId   = carrierId,
				                         Program     = program,
				                         Message     = Message,
				                         BasePath    = "",
				                         LogDateTime = importDateTime
			                         };

			               Append( Log, dateTime => $"{carrierId}/{logBase}/{StorageBase.ToDateTimePath( dateTime )}", false );
		               } );
	}
}

public class ImportLog : ImportExportLog
{
	public static void Append( string program, string carrierId, DateTimeOffset importDateTime, string importedData, [CallerMemberName] string caller = "" )
	{
		Append( program, carrierId, "Import Logs", importDateTime, importedData, caller );
	}
}

public class ExportLog : ImportExportLog
{
	public static void Append( string program, string carrierId, DateTimeOffset importDateTime, string importedData, [CallerMemberName] string caller = "" )
	{
		Append( program, carrierId, "Export Logs", importDateTime, importedData, caller );
	}
}