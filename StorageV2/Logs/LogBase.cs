﻿using System.Globalization;

namespace StorageV2.Logs;

public class ALogBase
{
	protected static readonly CultureInfo EnUs = new( "en-US" );
	protected static readonly string      LOGS = "LogsV2";

	protected static string GetContents( string container, string carrierId, string logType, string logName )
	{
		var Context = StorageBase.NewContext();

		if( Context is not null )
		{
			try
			{
				var LogType = logType.Pack();

				return CreateBlob( Context, container, $"{LogType}/{logName}/{carrierId}" ).DownloadBlobAsString();
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
		return "";
	}

	protected static void Append( WebLog log, Func<DateTimeOffset, string>? makeLogName = null, bool trim = true )
	{
		var Context = StorageBase.NewContext();

		if( Context is not null )
		{
			try
			{
				var Prog = log.Program.Pack();
				var Date = log.LogDateTime != DateTimeOffset.MinValue ? log.LogDateTime : DateTimeOffset.UtcNow;

				if( trim )
					log.Message = log.Message.Trim();

				makeLogName ??= dateTime => $"{log.CarrierId}/{Prog}/{StorageBase.ToDateTimeHours( dateTime )}";

				CreateBlob( Context, LOGS, $"{makeLogName( Date )}.log" ).Append( $"{log.Message}\r\n" );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
	}

	protected static void AppendAsPath( WebLog log, bool trim = true )
	{
		Append( log, dateTime => $"{log.CarrierId}/{log.Program.Pack()}/{StorageBase.ToDateTimePath( dateTime )}", trim );
	}

	protected static List<string> GetCarrierList( string container )
	{
		var Result  = new List<string>();
		var Context = StorageBase.NewContext();

		if( Context is not null )
		{
			try
			{
				return ( from D in CreateBlob( Context, container ).BlobList.Directories
				         select D.TrimEnd( '/' ) ).ToList();
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
		return Result;
	}

	protected static List<string> GetLogList( string container, string carrierId )
	{
		var Context = StorageBase.NewContext();

		if( Context is not null )
		{
			try
			{
				var Filter = $"{carrierId}/";
				var Result = CreateBlob( Context, container ).FlatBlobList( Filter );

				var Count = Result.Count;

				for( var I = 0; I < Count; I++ )
				{
					var Log = Uri.UnescapeDataString( Result[ I ].Replace( Filter, "" ) );
					Result[ I ] = Log;
				}
				return Result;
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
		return [];
	}

#region Search
	protected static List<string> SearchLogs( string container, string carrierId, string logType, DateTimeOffset fromDate, DateTimeOffset toDate, string searchString, Func<string, DateTimeOffset> logNameToDateTimeOffset )
	{
		fromDate = fromDate.UtcDateTime;
		toDate   = toDate.UtcDateTime;

		var Logs = GetLogList( container, carrierId );

		var Result = new List<string>();

		foreach( var L in Logs )
		{
			var Parts = L.ToLower().Replace( ".log", "" ).Split( '/' );

			if( Parts.Length == 2 )
			{
				var LogType = Parts[ 0 ];
				var LogName = Parts[ 1 ];

				if( LogType.Compare( logType, StringComparison.OrdinalIgnoreCase ) == 0 )
				{
					var Date = logNameToDateTimeOffset( LogName ).UtcDateTime;

					if( ( Date >= fromDate ) && ( Date <= toDate ) )
					{
						Parts   = L.Split( new[] { '/' }, StringSplitOptions.RemoveEmptyEntries );
						LogName = Parts[ 1 ];

						if( searchString != "" )
						{
							var Contents = GetContents( container, carrierId, LogType, LogName );

							if( Contents.Contains( searchString, StringComparison.OrdinalIgnoreCase ) )
								Result.Add( LogName );
						}
						else
							Result.Add( LogName );
					}
				}
			}
		}
		return Result;
	}
#endregion

#region Construction
	protected ALogBase()
	{
	}


	private static AppendBlobStorage CreateBlob( IRequestContext context, string container, string carrierId = "" ) => new( context, container, carrierId, true );
#endregion
}