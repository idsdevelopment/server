﻿namespace StorageV2.Logs;

public class ImportExportLog : ALogBase
{
	private const string IMPORT_LOG = "Import Logs";

	public static void Append( string program, string carrierId, DateTimeOffset importDateTime, string importedData, [CallerMemberName] string caller = "" )
	{
		Tasks.RunVoid( () =>
		               {
			               carrierId = carrierId.TrimToLower();

			               var Message = $"""
			                              ------- {importDateTime:yyyy-MM-dd hh:mm:ss.fff zz} -------
			                              {importedData}

			                              """;

			               var Log = new WebLog
			                         {
				                         CarrierId   = carrierId,
				                         Program     = program,
				                         Message     = Message,
				                         BasePath    = "",
				                         LogDateTime = importDateTime
			                         };

			               Append( Log, dateTime => $"{carrierId}/{IMPORT_LOG}/{StorageBase.ToDateTimePath( dateTime )}", false );
		               } );
	}
}