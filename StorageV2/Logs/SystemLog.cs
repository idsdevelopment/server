﻿using StorageV2;

// ReSharper disable ObjectCreationAsStatement

namespace Logs;

public class SystemLog : TableStorage
{
	private static readonly object LockObject = new();

	private static bool InAddToLog;

	public SystemLog( IRequestContext? context )
		: this( "SystemLog", context )
	{
	}

	public SystemLog( IRequestContext? context, Exception e )
		: this( context )
	{
		AddToLog( context, e );
	}

	public SystemLog( IRequestContext context, string message )
		: this( context )
	{
		AddToLog( context, message );
	}

	public SystemLog( IRequestContext context, string message, string details )
		: this( context )
	{
		AddToLog( context, message, details );
	}

	public SystemLog( string message, string details )
		: this( null )
	{
		AddToLog( null, message, details );
	}

	public SystemLog( IRequestContext context, string message, Exception e )
		: this( context )
	{
		AddToLog( context, message, e );
	}

	protected SystemLog( string logName, IRequestContext? context )
		: base( context!, logName, true )
	{
	}

	public class SystemLogEntry : TableEntry
	{
		public string Reason { get; set; } = "";

		public string Details { get; set; } = "";

		public string SubDetails { get; set; } = "";

		public string CallStack { get; set; } = Environment.StackTrace;

		public string IpAddress { get; set; }

		public SystemLogEntry( IRequestContext? context, string tableType, string ipAddress ) : base( context )
		{
			IpAddress = ipAddress;

			if( context is not null )
			{
				PartitionKey = $"{context.CarrierId}__{tableType}";
				RowKey       = $"{context.UserName}_{RowKey}";
			}
		}

		public SystemLogEntry( IRequestContext? context, string tableType ) : this( context, tableType, context?.IpAddress ?? "Unknown" )
		{
		}
	}

	protected void AddToLog( IRequestContext? context, Exception e )
	{
		lock( LockObject )
		{
			if( !InAddToLog )
			{
				InAddToLog = true;

				try
				{
					AddEntity( new SystemLogEntry( context, "Exception" )
					           {
						           Reason     = $"Exception: {e.GetType().Name}",
						           Details    = e.Message,
						           SubDetails = e.InnerException?.Message ?? ""
					           } );
					AddEntityFramework( context, e );
				}
				finally
				{
					InAddToLog = false;
				}
			}
		}
	}


	protected void AddToLog( IRequestContext context, string message )
	{
		lock( LockObject )
		{
			if( !InAddToLog )
			{
				InAddToLog = true;

				try
				{
					AddEntity( new SystemLogEntry( context, "Message" )
					           {
						           Reason     = message,
						           Details    = "",
						           SubDetails = ""
					           } );
				}
				finally
				{
					InAddToLog = false;
				}
			}
		}
	}

	protected void AddToLog( IRequestContext? context, string message, string details )
	{
		lock( LockObject )
		{
			if( !InAddToLog )
			{
				InAddToLog = true;

				try
				{
					AddEntity( new SystemLogEntry( context, "Message" )
					           {
						           Reason     = message,
						           Details    = details,
						           SubDetails = ""
					           } );
				}
				finally
				{
					InAddToLog = false;
				}
			}
		}
	}


	protected void AddToLog( IRequestContext context, string message, Exception e )
	{
		lock( LockObject )
		{
			if( !InAddToLog )
			{
				InAddToLog = true;

				try
				{
					AddEntity( new SystemLogEntry( context, "Exception" )
					           {
						           Reason     = message,
						           Details    = e.Message,
						           SubDetails = e.InnerException?.Message ?? ""
					           } );
					AddEntityFramework( context, e );
				}
				finally
				{
					InAddToLog = false;
				}
			}
		}
	}

	public static void Add( IRequestContext context )
	{
		new SystemLog( context );
	}

	public static void Add( IRequestContext context, Exception e )
	{
		new SystemLog( context, e );
	}

	public static void Add( IRequestContext context, string message )
	{
		new SystemLog( context, message );
	}

	public static void Add( IRequestContext context, string message, string details )
	{
		new SystemLog( context, message, details );
	}

	public static void Add( string message, string details )
	{
		new SystemLog( message, details );
	}

	public static void Add( Exception e )
	{
		Add( "No Context", e.Message );
	}

	public static void Add( IRequestContext context, string message, Exception e )
	{
		new SystemLog( context, message, e );
	}

	private void AddEntityFramework( IRequestContext? context, Exception e )
	{
		switch( e )
		{
		case DbUpdateException _:
			var Details    = "";
			var SubDetails = "";

			if( e.InnerException is not null )
			{
				Details = e.InnerException.Message;

				if( e.InnerException.InnerException is not null )
					SubDetails = e.InnerException.InnerException.Message;
			}

			AddEntity( new SystemLogEntry( context, "Entity Update Error" )
			           {
				           Reason     = e.Message,
				           Details    = Details,
				           SubDetails = SubDetails
			           } );
			break;
		}
	}
}