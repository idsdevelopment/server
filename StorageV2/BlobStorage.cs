﻿namespace StorageV2;

public class BlobStorage : BlobStorageBase<BlobClient>
{
	protected override BlobClient GetBlobClient() => ContainerClient.GetBlobClient( BlobName );

	public BlobStorage( IRequestContext context, string containerName, string blobName, bool toIdsStorage = false )
		: base( context, containerName, blobName, toIdsStorage )
	{
	}

	public BlobStorage( IRequestContext context, string containerName, string path, string blobName, bool toIdsStorage = false )
		: this( context, containerName, StorageAccount.CombinePathAndName( path, blobName ), toIdsStorage )
	{
	}
}