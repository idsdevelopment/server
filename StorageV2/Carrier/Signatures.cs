﻿using System.Runtime.Serialization;

namespace StorageV2.Carrier;

public class Signatures : TableStorage
{
	public class SignatureEntry : TableEntity
	{
		[IgnoreDataMember]
		public List<Signature> Signatures { get; set; } = [];


		public string SignaturesAsJson
		{
			get => JsonConvert.SerializeObject( Signatures );
			set => Signatures = JsonConvert.DeserializeObject<List<Signature>>( value ) ?? [];
		}

		[IgnoreDataMember]
		public string TripId
		{
			get => PartitionKey;
			set => PartitionKey = value;
		}

		public SignatureEntry()
		{
		}


		public SignatureEntry( IRequestContext context, string tripId )
		{
			RowKey       = context.CarrierId;
			PartitionKey = tripId;
		}
	}


	public Signatures( IRequestContext context, bool toIdsStorage = false ) : base( context, "Signatures", toIdsStorage )
	{
	}

	public void AddUpdate( string tripId, Signature sig )
	{
		try
		{
			if( sig is { Height: > 0, Width: > 0 } && sig.Points.IsNotNullOrWhiteSpace() )
			{
				for( var I = 0; I++ < 10; )
				{
					try
					{
						var Recs = GetPartitionEntities<SignatureEntry>( tripId );
						var Rec  = Recs.Count > 0 ? Recs[ 0 ] : new SignatureEntry( Context, tripId );
						Rec.Signatures.Add( sig );
						AddOrUpdateEntity( Rec );

						break;
					}
					catch( Exception E )
					{
						Console.WriteLine( E.Message );
						Thread.Sleep( 100 );
					}
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}


	public List<Signature> GetSignatures( string tripId )
	{
		try
		{
			var Rec = GetPartitionEntities<SignatureEntry>( tripId );

			if( Rec is {Count: > 0} )
				return Rec[ 0 ].Signatures;
		}
		catch( Exception Exception )
		{
			Console.WriteLine( Exception );
		}

		return [];
	}
}