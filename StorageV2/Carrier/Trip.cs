﻿using System.Runtime.Serialization;

// ReSharper disable StringCompareToIsCultureSpecific

namespace StorageV2.Carrier;

public class TripDate : TableEntry
{
	public string? TripAsJson { get; set; } = ""; // For Old Data


	public string? Data

	{
		get => _Data;
		set => _Data = value ?? "";
	}

	public string? Data1

	{
		get => _Data1;
		set => _Data1 = value ?? "";
	}

	public string? Data2

	{
		get => _Data2;
		set => _Data2 = value ?? "";
	}

	public string? Data3

	{
		get => _Data3;
		set => _Data3 = value ?? "";
	}

	public string? Data4

	{
		get => _Data4;
		set => _Data4 = value ?? "";
	}

	public string? Data5

	{
		get => _Data5;
		set => _Data5 = value ?? "";
	}

	public string? Data6

	{
		get => _Data6;
		set => _Data6 = value ?? "";
	}

	public string? Data7

	{
		get => _Data7;
		set => _Data7 = value ?? "";
	}

	public string? Data8

	{
		get => _Data8;
		set => _Data8 = value ?? "";
	}

	public string? Data9

	{
		get => _Data9;
		set => _Data9 = value ?? "";
	}

	public string? Data10

	{
		get => _Data10;
		set => _Data10 = value ?? "";
	}

	public string? Data11

	{
		get => _Data11;
		set => _Data11 = value ?? "";
	}

	public string? Data12

	{
		get => _Data12;
		set => _Data12 = value ?? "";
	}

	public string? Data13

	{
		get => _Data13;
		set => _Data13 = value ?? "";
	}

	public string? Data14

	{
		get => _Data14;
		set => _Data14 = value ?? "";
	}

	[IgnoreDataMember]
	public string CombinedData

	{
		get => TripAsJson + Data + Data1 + Data2 + Data3 + Data4 + Data5 + Data6 + Data7 + Data8 + Data9
		       + Data10 + Data11 + Data12 + Data13 + Data14;
		set

		{
			var Ndx = 0;

			foreach( var Chunk in value.Chunks( 31_000, 15 ) ) // 2 bytes / char
			{
				switch( Ndx++ )
				{
				case 0:
					Data = Chunk;
					break;

				case 1:
					Data1 = Chunk;
					break;

				case 2:
					Data2 = Chunk;
					break;

				case 3:
					Data3 = Chunk;
					break;

				case 4:
					Data4 = Chunk;
					break;

				case 5:
					Data5 = Chunk;
					break;

				case 6:
					Data6 = Chunk;
					break;

				case 7:
					Data7 = Chunk;
					break;

				case 8:
					Data8 = Chunk;
					break;

				case 9:
					Data9 = Chunk;
					break;

				case 10:
					Data10 = Chunk;
					break;

				case 11:
					Data11 = Chunk;
					break;

				case 12:
					Data12 = Chunk;
					break;

				case 13:
					Data13 = Chunk;
					break;

				case 14:
					Data14 = Chunk;
					break;
				}
			}
		}
	}

	public TripDate() : base( null )
	{
	}

	public TripDate( IRequestContext context, DateTimeOffset date, string tripId, string tripAsJson ) : base( context )
	{
		PartitionKey = date.ToPartitionKey();
		RowKey       = tripId;
		CombinedData = tripAsJson;
	}

	private string? _Data   = "",
	                _Data1  = "",
	                _Data2  = "",
	                _Data3  = "",
	                _Data4  = "",
	                _Data5  = "",
	                _Data6  = "",
	                _Data7  = "",
	                _Data8  = "",
	                _Data9  = "",
	                _Data10 = "",
	                _Data11 = "",
	                _Data12 = "",
	                _Data13 = "",
	                _Data14 = "";
}

public class TripsByDate : TableStorage
{
	public TripsByDate( IRequestContext context ) : base( context, MakeCarrierStorageName( context, "TripsByDate" ) )
	{
	}

	public static void Add( IRequestContext context, string tripId, DateTimeOffset callDate, string tripAsJson )
	{
		var Ctx = context.Clone();

		Tasks.RunVoid( () =>
		               {
			               try
			               {
				               var TripStorage = new TripsByDate( Ctx );
				               var Trip        = new TripDate( Ctx, callDate, tripId, tripAsJson );
				               TripStorage.AddEntity( Trip );
			               }
			               catch( Exception Exception )
			               {
				               Ctx.SystemLogException( Exception );
			               }
		               } );
	}

	public static void Add( IRequestContext context, Trip trip ) => Add( context, trip.TripId, trip.CallTime ?? DateTimeOffset.UtcNow, JsonConvert.SerializeObject( trip ) );

	public static List<TripDate> Get( IRequestContext context, string fromDate, string toDate )
	{
		var Table = new TripsByDate( context );
		return Table.Query<TripDate>( T => ( T.PartitionKey.CompareTo( fromDate ) >= 0 ) && ( T.PartitionKey.CompareTo( toDate ) <= 0 ) ).ToList();
	}

	public static List<TripDate> Get( IRequestContext context, string fromDate, string toDate, string tripId )
	{
		var Table = new TripsByDate( context );

		return Table.Query<TripDate>( T => ( string.Compare( T.PartitionKey, fromDate, StringComparison.Ordinal ) >= 0 )
		                                   && ( T.PartitionKey.CompareTo( toDate ) <= 0 ) && ( T.RowKey == tripId ) ).ToList();
	}

	public static TripList GetTrips( IRequestContext context, string fromDate, string toDate )
	{
		var Result = new TripList();

		foreach( var T in Get( context, fromDate, toDate ) )
		{
			var Json = JsonConvert.DeserializeObject<Trip>( T.CombinedData );

			if( Json is not null )
				Result.Add( Json );
		}
		return Result;
	}

	public static TripList GetTrips( IRequestContext context, string fromDate, string toDate, string tripId )
	{
		var Result = new TripList();

		foreach( var T in Get( context, fromDate, toDate, tripId ) )
		{
			var Json = JsonConvert.DeserializeObject<Trip>( T.CombinedData );

			if( Json is not null )
				Result.Add( Json );
		}
		return Result;
	}


	public static List<TripDate> Get( IRequestContext context, DateTimeOffset fromDate, DateTimeOffset toDate )
	{
		var FromDate = fromDate.ToPartitionKey();
		var ToDate   = toDate.ToPartitionKey();
		return Get( context, FromDate, ToDate );
	}

	public static List<TripDate> Get( IRequestContext context, DateTimeOffset fromDate, DateTimeOffset toDate, string tripId )
	{
		var FromDate = fromDate.ToPartitionKey();
		var ToDate   = toDate.ToPartitionKey();
		return Get( context, FromDate, ToDate, tripId );
	}


	public static TripList GetTrips( IRequestContext context, DateTimeOffset fromDate, DateTimeOffset toDate )
	{
		var FromDate = fromDate.ToPartitionKey();
		var ToDate   = toDate.ToPartitionKey();
		return GetTrips( context, FromDate, ToDate );
	}


	public static TripList GetTrips( IRequestContext context, DateTimeOffset fromDate, DateTimeOffset toDate, string tripId )
	{
		var FromDate = fromDate.ToPartitionKey();
		var ToDate   = toDate.ToPartitionKey();
		return GetTrips( context, FromDate, ToDate, tripId );
	}


	public static List<TripDate> Get( IRequestContext context, DateTimeOffset date )
	{
		var Date = date.ToUniversalTime().Date;
		return Get( context, Date, Date.ToEndOfDay() );
	}

	public static List<TripDate> Get( IRequestContext context, DateTimeOffset date, string tripId )
	{
		var Date = date.ToUniversalTime().Date;
		return Get( context, Date, Date.ToEndOfDay(), tripId );
	}


	public static TripList GetTrips( IRequestContext context, DateTimeOffset date )
	{
		var Date = date.ToUniversalTime().Date;
		return GetTrips( context, Date, Date.ToEndOfDay());
	}

	public static TripList GetTrips( IRequestContext context, DateTimeOffset date, string tripId )
	{
		var Date = date.ToUniversalTime().Date;
		return GetTrips( context, Date, Date.ToEndOfDay(), tripId );
	}
}

public class Trips
{
	public static void Add( IRequestContext context, Trip trip )
	{
		TripsByDate.Add( context, trip );
	}
}