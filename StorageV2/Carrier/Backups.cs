﻿namespace StorageV2.Carrier;

public class Backups : BlockBlobStorage
{
	public Backups( IRequestContext context, string backupName = "" ) : base( context, MakeCarrierStorageName( context, "Backups", false ), "", backupName )
	{
	}
}