﻿namespace StorageV2.Carrier;

public class RouteManifest : BlockBlobStorage
{
	public void WriteDirections( string jsonDirections )
	{
		try
		{
			Upload( jsonDirections );
		}
		catch( Exception E )
		{
			Context.SystemLogException( E );
		}
	}

	public string ReadDirections()
	{
		try
		{
			return DownloadBlobAsString();
		}
		catch( Exception E )
		{
			Context.SystemLogException( E );
		}
		return "";
	}

	public RouteManifest( IRequestContext context, string routeName, int legNumber ) : base( context, MakeCarrierStorageName( context, "RouteManifest" ), routeName.ToLower(), $"{legNumber:D5}.json" )
	{
	}
}