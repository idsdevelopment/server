﻿using System.IO;

namespace StorageV2.Carrier;

public sealed class Images : BlockBlobStorage
{
	private Images( IRequestContext context, string imageCategory, string imageName ) :
		base( context, MakeCarrierStorageName( context, "Images", false ), imageCategory, imageName )
	{
	}

	public static Images? Add( IRequestContext context, string path, string imageName, byte[] image )
	{
		try
		{
			var Storage = new Images( context, path, imageName );
			Storage.AddImage( image );
			return Storage;
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
		return null;
	}

	public static Protocol.Data.Images GetImages( IRequestContext context, string baseFolder, string imageName )
	{
		var Result = new Protocol.Data.Images();

		try
		{
			var Storage = new Images( context, "", "" );
			var Name    = Path.GetFileNameWithoutExtension( imageName );
			var Ext     = Path.GetExtension( imageName );

			foreach( var BlobItem in Storage.FlatBlobListDetailed( $"{baseFolder}/{Name}" ) )
			{
				var Parts = BlobItem.Name.Split( new[] { '/' }, StringSplitOptions.RemoveEmptyEntries );

				var Len = Parts.Length;

				if( Len-- > 0 )
				{
					var FileName = Parts[ Len ];

					var BExt = Path.GetExtension( FileName );

					if( ( Ext == "" ) || ( BExt.Compare( Ext, StringComparison.OrdinalIgnoreCase ) == 0 ) )
					{
						var Folder = string.Join( "/", Parts, 0, Len );

						Storage = new Images( context, Folder, FileName );

						var Bytes = Storage.DownloadBlobAsByteArray();

						if( Bytes.Length > 0 )
						{
							var TimeString = FileName.ReplaceLastOccurrence( BExt, "" );

							if( !DateTimeOffset.TryParse( TimeString, out var DateTime ) )
								DateTime = DateTimeOffset.MinValue;

							var Md = Storage.MetaData;

							var Image = new Image
							            {
								            Name     = $"{Name}{BExt}",
								            DateTime = DateTime,
								            Bytes    = Bytes,
								            Status   = Parts[ Len - 1 ].AsStatus(),
								            MetaData = new Dictionary<string, string>( Md )
							            };

							Result.Add( Image );
						}
					}
				}
			}
		}
		catch( RequestFailedException E ) when( ( E.ErrorCode == BlobErrorCode.BlobNotFound )
		                                        || ( E.ErrorCode == BlobErrorCode.ContainerNotFound ) )
		{
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}

		return Result;
	}

	public static void Add( IRequestContext context, string path, string imageName, byte[] image, Dictionary<string, string> metaData )
	{
		if( Add( context, path, imageName, image ) is { } Image && ( metaData.Count > 0 ) )
		{
			try
			{
				var Md = new Dictionary<string, string>( Image.MetaData );

				foreach( var Pair in metaData )
					Md[ Pair.Key ] = Pair.Value;

				Image.MetaData = Md;
			}
			catch( Exception Exception )
			{
				context.SystemLogException( Exception );
			}
		}
	}

	private void AddImage( byte[] image )
	{
		try
		{
			UploadBlob( image );
		}
		catch( Exception E )
		{
			Context.SystemLogException( E );
		}
	}
}