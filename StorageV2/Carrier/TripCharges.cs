﻿using System.Runtime.Serialization;

namespace StorageV2.Carrier;

public class TripCharge : TableEntry
{
	[IgnoreDataMember]
	public string AccountCode
	{
		get => PartitionKey;
		set => PartitionKey = value;
	}

	[IgnoreDataMember]
	public string TripId
	{
		get => RowKey;
		set => RowKey = value;
	}

	public string ChargeId { get; set; } = "";

	public double DValue { get; set; }

	[IgnoreDataMember]
	public decimal Value
	{
		get => (decimal)DValue;
		set => DValue = (double)value;
	}

	public bool   ChargeApplied { get; set; }
	public string PackageType   { get; set; } = "";

	public double DResult { get; set; }

	[IgnoreDataMember]
	public decimal Result
	{
		get => (decimal)DResult;
		set => DResult = (double)value;
	}

	public TripCharge( IRequestContext context ) : base( context )
	{
	}
}

public class TripCharges : TableStorage
{
	public static void Add( IRequestContext context, TripCharge charge )
	{
		var Ctx = context.Clone();

		Tasks.RunVoid( () =>
		               {
			               charge.Timestamp = DateTime.UtcNow;
			               new TripCharges( Ctx ).AddEntity( charge );
		               } );
	}

	public TripCharges( IRequestContext context ) : base( context, MakeCarrierStorageName( context, "TripCharges" ) )
	{
	}
}