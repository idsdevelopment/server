﻿using System.IO;
using System.Text;

namespace StorageV2.Carrier;

public class GpsLog : AppendBlobStorage
{
	public class GpsPoint
	{
		public DateTimeOffset DateTime;

		public double Latitude,
		              Longitude;

		public override string ToString() => $";{DateTime:O},{Latitude},{Longitude}";

		public GpsPoint()
		{
		}

		public GpsPoint( string point )
		{
			var Temp = point.Split( ',' );

			if( Temp.Length == 3 )
			{
				DateTime  = DateTimeOffset.Parse( Temp[ 0 ] );
				Latitude  = double.Parse( Temp[ 1 ] );
				Longitude = double.Parse( Temp[ 2 ] );
			}
		}
	}

	public void Append( IList<GpsPoint> points )
	{
		try
		{
			if( points.Count > 0 )
			{
				var AppendData = new StringBuilder();

				foreach( var Point in points )
					AppendData.Append( Point );

				// Don't use Blob.AppendText
				using var Stream = new MemoryStream( Encoding.UTF8.GetBytes( AppendData.ToString() ) );
				Stream.Position = 0;

				Append( Stream );
			}
		}
		catch( Exception E )
		{
			Context.SystemLogException( E );
		}
	}

	public static GpsPoints GetPoints( IRequestContext context, GpsDriverDateRange range )
	{
		var Result = new GpsPoints();

		try
		{
			var FromUtc = range.FromDateTime.UtcDateTime;
			var ToUtc   = range.ToDateTime.UtcDateTime;

			var StartDate = FromUtc.Date;
			var StartTime = FromUtc.TimeOfDay;

			var EndDate = ToUtc.Date;
			var EndTime = ToUtc.TimeOfDay;

			var CurrentDate = FromUtc.Date;

			while( CurrentDate <= EndDate )
			{
				try
				{
					var Log    = new GpsLog( context, range.Driver, CurrentDate );
					var Points = Log.DownloadBlobAsString();

					if( Points.IsNotNullOrWhiteSpace() )
					{
						var IsStartDate = CurrentDate == StartDate;
						var IsEndDate   = !IsStartDate && ( CurrentDate == EndDate );

						var PSplit = Points.Split( new[] { ";" }, StringSplitOptions.RemoveEmptyEntries );

						foreach( var Point in PSplit )
						{
							var  GpsPoint = new GpsPoint( Point );
							bool Ok;

							if( IsStartDate )
								Ok = GpsPoint.DateTime.TimeOfDay >= StartTime;

							else if( IsEndDate )
								Ok = GpsPoint.DateTime.TimeOfDay <= EndTime;

							else
								Ok = true;

							if( Ok )
							{
								Result.Add( new Protocol.Data.GpsPoint
								            {
									            Latitude      = GpsPoint.Latitude,
									            Longitude     = GpsPoint.Longitude,
									            LocalDateTime = GpsPoint.DateTime
								            } );
							}
						}
					}
				}
				catch( RequestFailedException E ) when( E.ErrorCode == BlobErrorCode.BlobNotFound )
				{
				}
				CurrentDate = CurrentDate.AddDays( 1 );
			}
		}
		catch( RequestFailedException E ) when( ( E.ErrorCode == BlobErrorCode.BlobNotFound ) || ( E.ErrorCode == BlobErrorCode.ContainerNotFound ) )
		{
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}

		return Result;
	}

	public GpsLog( IRequestContext context, string driver, DateTime utcDateTime ) : base( context, MakeCarrierStorageName( context, "GpsLogs" ), driver.ToLower(), $"{utcDateTime:yyyy-MM-dd}.log" )
	{
	}

	public GpsLog( IRequestContext context ) : this( context, context.UserName, DateTime.UtcNow )
	{
	}
}