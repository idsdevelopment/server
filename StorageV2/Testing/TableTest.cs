﻿namespace StorageV2;

public class TestEntity : TableEntity
{
	public string Data       { get; set; } = "";
	public int    TestNumber { get; set; } = 9999;
}

internal class TableStorageTest
{
	private readonly IRequestContext Context;

	public void Execute( StorageTest test )
	{
		var Table = new TableStorage( Context, test.Name );

		try
		{
			switch( test.Action )
			{
			case StorageTest.ACTION.PUT:
				var Data = test.Data;

				if( Data.Count == 3 )
				{
					var TestRec = new TestEntity
					              {
						              PartitionKey = Data[ 0 ],
						              RowKey       = Data[ 1 ],
						              Data         = Data[ 2 ]
					              };

					Table.AddEntity( TestRec );
				}
				break;

			case StorageTest.ACTION.GET:
				Data      = test.Data;
				test.Data = new List<string>();

				if( Data.Count == 2 )
				{
					var Rec = Table.GetEntity<TestEntity>( Data[ 0 ], Data[ 1 ] );

					if( Rec is not null )
						test.Data.Add( Rec.Data );
				}

				break;

			case StorageTest.ACTION.DELETE:
				Table.Delete();
				break;

			case StorageTest.ACTION.PUT_METADATA:
			case StorageTest.ACTION.GET_METADATA:
				throw new Exception( "Table does not support Metadata" );
			}
		}
		finally
		{
			test.Ok           = !Table.Err;
			test.ErrorMessage = Table.ErrorMessage;
		}
	}

	public TableStorageTest( IRequestContext context )
	{
		Context = context;
	}
}