﻿namespace StorageV2;

public class Testing
{
	internal const string TEST_CONTAINER = "testing-container";

	public StorageTest Execute( IRequestContext context, StorageTest test )
	{
		test.Ok = false;
		var SaveMessage = test.ErrorMessage;
		test.ErrorMessage = "";

		try
		{
			switch( test.Storage )
			{
			case StorageTest.STORAGE.BLOB:
				new BlobTest( context ).Execute( test );
				break;

			case StorageTest.STORAGE.APPEND_BLOB:
				new AppendBlobTest( context ).Execute( test );
				break;

			case StorageTest.STORAGE.QUEUE:
				new QueueStorageTest( context ).Execute( test );
				break;

			case StorageTest.STORAGE.TABLE:
				new TableStorageTest( context ).Execute( test );
				break;

			case StorageTest.STORAGE.SYSTEM_LOG:
				context.SystemLogExceptionProgram( test.Name, new Exception( SaveMessage ) );
				test.Ok = true;
				break;
			}
		}
		catch( Exception Exception )
		{
			test.Ok           = false;
			test.ErrorMessage = Exception.Message;
		}
		return test;
	}
}