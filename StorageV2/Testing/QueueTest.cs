﻿namespace StorageV2;

internal class QueueStorageTest
{
	private readonly IRequestContext Context;

	public void Execute( StorageTest test )
	{
		var Queue = new QueueStorage( Context, test.Name );

		try
		{
			switch( test.Action )
			{
			case StorageTest.ACTION.PUT:
				foreach( var Data in test.Data )
					Queue.SendMessage( Data );
				break;

			case StorageTest.ACTION.GET:
				var StringData = Queue.ReceiveMessage() ?? "";
				test.Data = new List<string> {StringData};
				break;

			case StorageTest.ACTION.DELETE:
				Queue.Delete();
				break;

			case StorageTest.ACTION.PUT_METADATA:
				Queue.MetaData = test.MetaData;
				break;

			case StorageTest.ACTION.GET_METADATA:
				test.MetaData = new Dictionary<string, string>( Queue.MetaData );
				break;
			}
		}
		finally
		{
			test.Ok           = !Queue.Err;
			test.ErrorMessage = Queue.ErrorMessage;
		}
	}

	public QueueStorageTest( IRequestContext context )
	{
		Context = context;
	}
}