﻿using System.Runtime.Serialization;

namespace StorageV2;

public static class TableEntryExtensions
{
	private static readonly object LockObject = new();
	private static          ulong  Cntr;

	public static string ToRowKey( this DateTime time )
	{
		ulong C;

		lock( LockObject )
			C = Cntr++;

		return $"{time:yyyy-MM-dd HH:mm:ss:FFFFFFF} [[{C}]]";
	}

	public static string ToMinRowKey( this DateTimeOffset time ) => time.UtcDateTime.ToRowKey();

	public static string ToMaxRowKey( this DateTimeOffset time ) => time.Add( new TimeSpan( 0, 23, 59, 59, 999 ) ).UtcDateTime.ToRowKey();

	public static string ToPartitionKey( this DateTimeOffset date ) => date.ToUniversalTime().Date.ToString( "s" );
}

public class TableEntry : TableEntity
{
    [IgnoreDataMember]
	public IRequestContext? Context { get; }

	public TableEntry( IRequestContext? context, string partitionKey, string rowKey )
	{
		Context      = context;
		PartitionKey = partitionKey;
		RowKey       = rowKey;
	}

	public TableEntry( IRequestContext? context ) : this( context, context is null ? "TableEntry NULL Context" : context.UserName, DateTime.UtcNow.ToRowKey() )
	{
	}
}

public class TableStorage : TableStorageBase
{
	public TableStorage( IRequestContext context, string tableName, bool toIdsStorage = false ) : base( context, tableName, toIdsStorage )
	{
	}
}