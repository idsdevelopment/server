﻿#nullable enable

using System.Net;

// ReSharper disable InconsistentNaming

namespace Maps.BingMapsApi;

internal class BingMaps<T> : BingAMaps
{
	public class CommonResponse
	{
		public class ResourceSet
		{
			public long estimatedTotal = -1;
			public T[]? resources      = null;
		}

		public string[]? errorDetails = null;

		public ResourceSet[]? resourceSets = null;
		public int            statusCode   = -1;

		public string? statusDescription        = null,
		               authenticationResultCode = null,
		               traceId                  = null,
		               copyright                = null,
		               brandLogoUri             = null;
	}

	public override string ShowMapLink => throw new NotImplementedException();

	internal override string BaseUrl => Owner.BaseUrl;

	internal override string Version => Owner.Version;

	private readonly BingAMaps Owner;

	internal CommonResponse? GetResponse( string queryType, string queryParameters, out bool rateLimited, bool errorDetail = false ) => GetResponse( Url( queryType, queryParameters, errorDetail ), out rateLimited );

	internal CommonResponse? GetResponse( string queryType, Dictionary<string, string> queryParameters, out bool rateLimited, bool errorDetail = false ) => GetResponse( Url( queryType, MapsApi.DictionaryToQueryParameters( queryParameters ), errorDetail ),
	                                                                                                                                                                     out rateLimited );

	public CommonResponse.ResourceSet[]? GetResources( string queryType, string queryParameters, out bool rateLimited, bool errorDetail = false ) => GetResources( Url( queryType, queryParameters, errorDetail ), out rateLimited );

	public CommonResponse.ResourceSet[]? GetResources( string queryType, Dictionary<string, string> queryParameters, out bool rateLimited, bool errorDetail = false ) => GetResources( Url( queryType, queryParameters, errorDetail ), out rateLimited );

	public override List<MapsApi.AddressCoordinate> FindLocationByAddress( MapsApi.AddressData address, out bool rateLimited ) => throw new NotImplementedException();

	public override void ShowDirections( MapsApi.Directions directions )
	{
		throw new NotImplementedException();
	}

	public BingMaps( BingAMaps owner )
		: base( owner.Context )
	{
		Owner = owner;
	}

	private static CommonResponse? GetResponse( string url, out bool rateLimited )
	{
		const string RATE_LIMIT = "X-MS-BM-WS-INFO";

		try
		{
			var ResponseString = GetResponse( url, out WebHeaderCollection? Headers );

			if( Headers is { } )
			{
				var Limit = Headers[ RATE_LIMIT ];
				rateLimited = Limit is { } && ( Limit.Trim() == "1" );
			}
			else
				rateLimited = true;

			if( ResponseString is { } )
				return JsonConvert.DeserializeObject<CommonResponse>( ResponseString );
		}
		catch( Exception )
		{
			rateLimited = false;
		}
		return null;
	}

	private static CommonResponse.ResourceSet[]? GetResources( string url, out bool rateLimited )
	{
		var Data = GetResponse( url, out rateLimited );

		return Data?.resourceSets?.Length > 0 ? Data.resourceSets : null;
	}
}

public abstract class BingAMaps : AMapsApi
{
	public class Address
	{
		public string? addressLine      = null,
		               locality         = null,
		               neighborhood     = null,
		               adminDistrict    = null,
		               adminDistrict2   = null,
		               formattedAddress = null,
		               postalCode       = null,
		               countryRegion    = null;

		public MapsApi.AddressData ToAddressData() => new()
		                                              {
			                                              PostalCode       = postalCode ?? "",
			                                              StateCode        = adminDistrict ?? "",
			                                              City             = locality ?? "",
			                                              FormattedAddress = formattedAddress ?? "",
			                                              CountryCode      = countryRegion ?? "",
			                                              Vicinity         = neighborhood ?? "",
			                                              Country          = countryRegion ?? ""
		                                              };
	}

	public class BingPoint
	{
		public double[]? coordinates = null;
		public string?   type        = null;
	}

	public class GeocodeBingPoints : BingPoint
	{
		public string?   calculationMethod = null;
		public string[]? usageTypes        = null;
	}

	public class LocationResource
	{
		public string?              __type        = null;
		public Address?             address       = null;
		public double[]?            bbox          = null;
		public GeocodeBingPoints[]? geocodePoints = null;
		public string[]?            matchCodes    = null;

		public string? name       = null,
		               entityType = null,
		               confidence = null;
	}

	protected override string Key => Context.Configuration.BingMapsApiKey;

	internal abstract string BaseUrl { get; }

	internal abstract string Version { get; }

	public string Url( string queryType, string queryParameters, bool errorDetail = false )
	{
		var Result = new StringBuilder( BaseUrl ).Append( Version ).Append( '/' ).Append( queryType )
		                                         .Append( '?' ).Append( queryParameters ).Append( "&key=" ).Append( Key );

		if( errorDetail )
			Result.Append( "&ed=true" );

		return Result.ToString();
	}

	public string Url( string queryType, Dictionary<string, string> queryParameters, bool errorDetail = false ) => Url( queryType, MapsApi.DictionaryToQueryParameters( queryParameters ), errorDetail );

	public override MapsApi.Directions GetDirections( RouteBase.TRAVEL_MODE mode, List<Point> points, bool circular ) => throw new NotImplementedException();

	internal BingAMaps( RequestContext.RequestContext context ) : base( context )
	{
	}
}