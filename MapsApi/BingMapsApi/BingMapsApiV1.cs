﻿#nullable enable

namespace Maps.BingMapsApi;

public class BingMapsV1 : BingAMaps
{
	private const string
		BASE_URL = "http://dev.virtualearth.net/REST/",
		VERSION  = "v1";

	public const string
		ADMIN_DISTRICT       = "adminDistrict",
		ADMIN_DISTRICT2      = "adminDistrict2",
		LOCALITY             = "locality",
		POSTAL_CODE          = "postalCode",
		ADDRESS_LINE         = "addressLine",
		COUNTRY_REGION       = "countryRegion",
		COUNTRY_REGION_ISO2  = "countryRegionIso2",
		INCLUDE_NEIGHBORHOOD = "includeNeighborhood",
		LOCATION_BY_ADDRESS  = "Locations",
		GEOCODE_POINTS       = "GeocodePoints",
		ADDRESS              = "address",
		NEIGHBORHOOD         = "neighborhood";

	public override string ShowMapLink => throw new NotImplementedException();


	protected override string Key => throw new NotImplementedException();

	internal override string BaseUrl => BASE_URL;

	internal override string Version => VERSION;

	public override List<MapsApi.AddressCoordinate> FindLocationByAddress( MapsApi.AddressData address, out bool rateLimited )
	{
		address.Trim();
		var Params = new Dictionary<string, string>();

		if( address.StateCode != "" )
			Params.Add( ADMIN_DISTRICT, address.StateCode );

		if( address.State != "" )
			Params.Add( ADMIN_DISTRICT2, address.State );

		if( address.City != "" )
			Params.Add( LOCALITY, address.City );

		if( ( address.PostalCode != "" ) && ( address.PostalCode.Length >= 4 ) )
			Params.Add( POSTAL_CODE, address.PostalCode );

		var Address = new StringBuilder();

		if( address.StreetNumber != "" )
			Address.Append( address.StreetNumber ).Append( " " );

		if( address.Street != "" )
			Address.Append( address.Street ).Append( " " );

		var Addr = Address.ToString().Trim();

		if( Addr != "" )
			Params.Add( ADDRESS_LINE, Addr );

		if( address.Country != "" )
			Params.Add( COUNTRY_REGION, address.Country );

		if( address.CountryCode != "" )
			Params.Add( COUNTRY_REGION_ISO2, address.CountryCode );

		Params.Add( INCLUDE_NEIGHBORHOOD, "1" );

		var Response = new BingMaps<LocationResource>( this ).GetResources( LOCATION_BY_ADDRESS, Params,
		                                                                    out rateLimited );

		var RetVal    = new List<MapsApi.AddressCoordinate>();
		var Resources = Response?[ 0 ].resources;

		if( Resources?.Length > 0 )
		{
			var Res = Resources.Where( locationResource => locationResource.confidence == "high" ).ToList();

			foreach( var LocationResource in Res )
			{
				try
				{
					if( LocationResource.geocodePoints is { } )
					{
						var Cords = LocationResource.geocodePoints[ 0 ].coordinates;

						if( LocationResource.address is { } )
						{
							if( Cords is { } )
							{
								var Result = new MapsApi.AddressCoordinate
								             {
									             Address   = LocationResource.address.ToAddressData(),
									             Longitude = (decimal)Cords[ 1 ],
									             Latitude  = (decimal)Cords[ 0 ]
								             };
								RetVal.Add( Result );
							}
						}
					}
				}
				catch
				{
				}
			}
		}

		return RetVal;
	}

	public override void ShowDirections( MapsApi.Directions directions )
	{
		throw new NotImplementedException();
	}

	public BingMapsV1( RequestContext.RequestContext context ) : base( context )
	{
	}
}