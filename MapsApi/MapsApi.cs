﻿using System.Diagnostics;
using System.Net;
using Database.Model.Databases.Users;
using Maps.BingMapsApi;
using Maps.GoogleMapsApi;
using MapsApi.AzureMapsApi;
using Point = MapsApi.GeoLocation.Point;

// ReSharper disable CompareOfFloatsByEqualityOperator

namespace Maps;

public abstract class AMapsApi
{
	public abstract string ShowMapLink // Required because needed IFrame to show Google Map
	{
		get;
	}

	protected abstract string Key { get; }

	public readonly RequestContext.RequestContext Context;

	protected AMapsApi( RequestContext.RequestContext context )
	{
		Context = context;
	}

	public abstract List<MapsApi.AddressCoordinate> FindLocationByAddress( MapsApi.AddressData address, out bool rateLimited );
	public abstract MapsApi.Directions GetDirections( RouteBase.TRAVEL_MODE mode, List<Point> points, bool circular );

	public abstract void ShowDirections( MapsApi.Directions directions );

	protected static string? GetResponse( string url, out WebHeaderCollection? headers )
	{
		try
		{
 #pragma warning disable SYSLIB0014
			var Wc = new WebClient();
 #pragma warning restore SYSLIB0014
			var Result = Encoding.ASCII.GetString( Wc.DownloadData( url ) );
			headers = Wc.ResponseHeaders;
			return Result;
		}
		catch( Exception E )
		{
			Console.WriteLine( E.Message );
			headers = null;
			return null;
		}
	}

	protected static string? GetResponse( string url )
	{
		try
		{
			return GetResponse( url, out _ );
		}
		catch
		{
			return null;
		}
	}
}

public class MapsApi : AMapsApi
{
	public override string ShowMapLink => new GoogleMapsApiV3( Context ).ShowMapLink;

	protected override string Key => throw new NotImplementedException();

	public MapsApi( RequestContext.RequestContext context ) : base( context )
	{
	}

	public enum OPTIMISATION_METHOD
	{
		FURTHEST,
		CIRCULAR,
		TO_END_POINT
	}


	public class OptimiseEntry
	{
		public Point FromPoint { get; init; } = new();
		public Point Point     { get; init; } = new();

		public string CompanyName = ""; // Mainly for debugging

		public decimal Distance,
		               DistanceFromStart;

		public bool IsPickupCompany;

		public object Object = null!;

		public OptimiseEntry()
		{
		}

		public OptimiseEntry( OptimiseEntry e )
		{
			Distance          = e.Distance;
			DistanceFromStart = e.DistanceFromStart;
			Point             = e.Point;
			Object            = e.Object;
			IsPickupCompany   = e.IsPickupCompany;
			FromPoint         = e.FromPoint;
			CompanyName       = e.CompanyName;
		}
	}


	public abstract class OptimiseBase( RequestContext.RequestContext context, List<OptimiseEntry> points )
	{
		protected readonly RequestContext.RequestContext Context = context;
		protected          List<OptimiseEntry>           Points  = points;

		public abstract decimal CalculateDistance( Point from, Point to );
	}

	public abstract class OptimiseFurthest : OptimiseBase
	{
		private static readonly object LockObject = new();

		protected OptimiseFurthest( RequestContext.RequestContext context, List<OptimiseEntry> points ) : base( context, points )
		{
		}

		public Directions Optimise( RouteBase.TRAVEL_MODE travelMode ) => ( travelMode & RouteBase.TRAVEL_MODE.PICKUP_AND_DELIVERIES ) != 0
			                                                                  ? PickupDeliveryOptimise( Points, travelMode )
			                                                                  : NormalOptimise( Points, travelMode );

		public Directions PickupDeliveryOptimise( List<OptimiseEntry> points, RouteBase.TRAVEL_MODE travelMode )
		{
			var MapDirections = new Directions();

			try
			{
				List<OptimiseEntry> RebuildOptimiseEntryList( Directions d )
				{
					var Entries = new List<OptimiseEntry>();

					void AddOptimisedPoint( Point? point )
					{
						if( point is { } P )
						{
							var Entry = ( from P1 in points
							              where P1.Point.Equals( P )
							              select P1 ).FirstOrDefault();

							if( Entry is not null )
								Entries.Add( Entry );
						}
					}

					var First = true;

					// Rebuild The OptimiseEntries Ordered
					foreach( var Route in d.Routes )
					{
						foreach( var Leg in Route.Legs )
						{
							if( First )
							{
								First = false;
								AddOptimisedPoint( Leg.OriginalStartLocation );
							}
							AddOptimisedPoint( Leg.OriginalEndLocation );
						}
					}
					return Entries;
				}

				var RemainingPoints        = new List<OptimiseEntry>( points );
				var OptimisedEntriesPoints = new List<OptimiseEntry>();
				var Pickups                = new HashSet<Point>();

				while( true )
				{
					var PickupsDeliveries = NormalOptimise( RemainingPoints, travelMode ); // Optimise remaining points

					var OrderedEntries = RebuildOptimiseEntryList( PickupsDeliveries );
					RemainingPoints = new List<OptimiseEntry>( OrderedEntries );

					var DidPoint = false;

					var ToRemove = new List<OptimiseEntry>();

					foreach( var Entry in OrderedEntries )
					{
						void AddRemoveEntry()
						{
							OptimisedEntriesPoints.Add( Entry );
							ToRemove.Add( Entry );
							DidPoint = true;
						}

						var PickupPoint = Entry.FromPoint;

						if( Entry.IsPickupCompany )
						{
							Pickups.Add( PickupPoint );
							AddRemoveEntry();
						}
						else
						{
							if( Pickups.Contains( PickupPoint ) ) // Delivery After Pickup. OK
								AddRemoveEntry();
							else
								break; // Delivery Before Pickup
						}
					}

					if( !DidPoint ) // Must be all deliveries (Shouldn't Happen, but just in case)
					{
						OptimisedEntriesPoints.AddRange( OrderedEntries );
						break;
					}

					foreach( var Entry in ToRemove )
						RemainingPoints.Remove( Entry );

					if( RemainingPoints.Count > 0 )
					{
						var Ndx = OptimisedEntriesPoints.Count - 1;

						if( Ndx >= 0 )
						{
							RemainingPoints.Insert( 0, OptimisedEntriesPoints[ Ndx ] ); // New Starting Point
							OptimisedEntriesPoints.RemoveAt( Ndx );
							continue;
						}
					}
					break;
				}
				/*
									var OkEntries = new List<OptimiseEntry>();

									foreach( var Entry in OrderedEntries )
									{
										var Id = Entry.FromPoint;

										if( Entry.IsPickupCompany )
										{
											Pickups.Add( Id );
											OkEntries.Add( Entry );
										}
										else
										{
											if( Pickups.Contains( Id ) ) // Delivery After Pickup. OK
												OkEntries.Add( Entry );
											else
												break;
										}
									}

									var OkCount = OkEntries.Count;

									OptimiseEntry? FirstPickupEntry()
									{
										return ( from O in OrderedEntries
												 where O.IsPickupCompany
												 select O ).FirstOrDefault();
									}

									if( OkCount == 0 ) // All deliveries before first pickup
									{
										var FirstPickup = FirstPickupEntry();

										if( FirstPickup is null )
											break; //Something really wrong

										OkEntries.Add( FirstPickup );
										OkCount = 1;
									}

									foreach( var Entry in OkEntries )
									{
										OrderedEntries.Remove( Entry );

										// Could be start point fom second pass
										var E = ( from OptimiseEntry in OptimisedEntriesPoints
												  where OptimiseEntry.Point.Equals( Entry.Point )
												  select OptimiseEntry ).FirstOrDefault();

										if( E is null )
											OptimisedEntriesPoints.Add( Entry );
									}

									if( OkCount == OrderedEntries.Count )
										break;

									RemainingPoints = new List<OptimiseEntry>();
									var FirstP = FirstPickupEntry();

									if( FirstP is not null )
									{
										RemainingPoints.Add( FirstP ); // Good starting point
										OrderedEntries.Remove( FirstP );
										RemainingPoints.AddRange( OrderedEntries );
									}
									else // No more pickups
									{
										var Current = ( from O in OptimisedEntriesPoints
														where O.IsPickupCompany
														select O ).LastOrDefault();

										if( Current is not null )
										{
											while( true )
											{
												var Count = OrderedEntries.Count;

												if( Count == 0 )
													break;

												if( Count == 1 )
												{
													OptimisedEntriesPoints.Add( OrderedEntries[ 0 ] );
													break;
												}

												var            MinDistance = decimal.MaxValue;
												OptimiseEntry? Closest     = null;

												var P = Current!.Point;

												foreach( var Entry in OrderedEntries )
												{
													var Ep   = Entry.Point;
													var Dist = Math.Abs( Measurement.SimpleGpsDistance( P.Latitude, P.Longitude, Ep.Latitude, Ep.Longitude ).Metres );

													if( Dist < MinDistance )
													{
														MinDistance = Dist;
														Closest     = Entry;
													}
												}
												OptimisedEntriesPoints.Add( Closest! );
												OrderedEntries.Remove( Closest! );
												Current = Closest;
											}
										}
										else // Shouldn't happen
											OptimisedEntriesPoints.AddRange( OrderedEntries );
										break;
									}
								}
				*/
				// Try to put deliveries before pickups
				var OpList = new LinkedList<OptimiseEntry>( OptimisedEntriesPoints );

				for( var Current = OpList.Last; Current is not null; Current = Current.Previous )
				{
					var Entry = Current.Value;

					var PickupPoint = Entry.FromPoint;

					if( Entry.IsPickupCompany )
					{
						var PPoint = Entry.Point;

						var Delivery = Current.Next;
						var Previous = Current.Previous;

						if( Delivery?.Next is not null && Previous is not null ) // Don't do last Delivery
						{
							var DeliveryEntry = Delivery.Value;

							if( !DeliveryEntry.IsPickupCompany && DeliveryEntry.FromPoint.Equals( PickupPoint ) ) // Delivery and not same pickup/delivery
							{
								var DPoint       = DeliveryEntry.Point;
								var DeliveryDist = Math.Abs( Measurement.SimpleGpsDistance( PPoint.Latitude, PPoint.Longitude, DPoint.Latitude, DPoint.Longitude ).Metres );

								var PrevPoint = Previous.Value.Point;
								var PrevDist  = Math.Abs( Measurement.SimpleGpsDistance( PPoint.Latitude, PPoint.Longitude, PrevPoint.Latitude, PrevPoint.Longitude ).Metres );

								if( DeliveryDist < PrevDist )
								{
									OpList.Remove( DeliveryEntry );
									OpList.AddBefore( Current, Delivery );
								}
							}
						}
					}
				}

				OptimisedEntriesPoints = new List<OptimiseEntry>( OpList );

				var MapPointsIsPickup = new List<(Point Point, bool IsPickupCompany )>();
				var MapPoints         = new List<Point>();

				foreach( var O in OptimisedEntriesPoints )
				{
					MapPointsIsPickup.Add( ( O.Point, O.IsPickupCompany ) );
					MapPoints.Add( O.Point );
				}

				MapDirections = new MapsApi( Context ).GetDirections( travelMode, MapPoints, false );

				var FirstLeg = true;

				Leg PrevLeg = null!;

				void AddPoint( Point? p, bool pickupOnly )
				{
					/*
					if( p is { } P )
					{
						foreach( var (Point, IsPickupCompany) in MapPointsIsPickup )
						{
							if( !pickupOnly || ( pickupOnly && IsPickupCompany ) )
							{
								if( P.Equals( Point ) )
								{
									var PickupDeliveryPoints = IsPickupCompany ? CurrentPoints.PickupPoints : CurrentPoints.DeliveryPoints;

									if( ( from Pt in PickupDeliveryPoints
									      where ( Pt.Latitude == P.Latitude ) && ( Pt.Longitude == P.Longitude )
									      select P ).FirstOrDefault() is null )
									{
										PickupDeliveryPoints.Add( new Protocol.Data.Maps.Route.Point
										                          {
											                          Latitude  = P.Latitude,
											                          Longitude = P.Longitude
										                          } );
									}
								}
							}
						}
					}
					*/
				}

				foreach( var Route in MapDirections.Routes )
				{
					foreach( var Leg in Route.Legs )
					{
						if( FirstLeg ) // Only Pickups
						{
							FirstLeg = false;
//							CurrentPoints = Leg.StartPickupDeliveryPoints;
							AddPoint( Leg.OriginalStartLocation, true );
						}
						else
//							Leg.StartPickupDeliveryPoints = PrevLeg.EndPickupDeliveryPoints;

							PrevLeg = Leg;
						//CurrentPoints = Leg.EndPickupDeliveryPoints;
						AddPoint( Leg.OriginalEndLocation, false );
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
			return MapDirections;
		}

		public Directions NormalOptimise( List<OptimiseEntry> points, RouteBase.TRAVEL_MODE travelMode )
		{
			if( points.Count > 1 )
			{
				const decimal LIMIT = 0.0005M;

				var Now = DateTime.Now;

				try
				{
					var Circular = false;

					using var Db = new UsersEntities();

					decimal DoDistance( Point frm, Point to )
					{
						var FromLat  = frm.Latitude;
						var FromLong = frm.Longitude;
						var ToLat    = to.Latitude;
						var ToLong   = to.Longitude;

						List<PointToPoint>? Recs;

						lock( LockObject )
						{
						#if NET7_0_OR_GREATER
							var ToPoints = Db.Entity.PointToPoints;
						#else
							var ToPoints = Db.PointToPoints;
						#endif

							Recs = ( from D in ToPoints
							         where ( Math.Abs( D.FromLatitude - FromLat ) <= LIMIT )
							               && ( Math.Abs( D.FromLongitude - FromLong ) <= LIMIT )
							               && ( Math.Abs( D.ToLatitude - ToLat ) <= LIMIT )
							               && ( Math.Abs( D.ToLongitude - ToLong ) <= LIMIT )
							         select D ).ToList();
						}

						PointToPoint? FindClosest()
						{
							var           Dist    = decimal.MaxValue;
							PointToPoint? Closest = null;

							foreach( var R in Recs )
							{
								if( ( R.FromLatitude == frm.Latitude )
								    && ( R.FromLongitude == frm.Longitude )
								    && ( R.ToLatitude == to.Latitude )
								    && ( R.ToLongitude == to.Longitude ) )
									return R;

								var D = R.DistanceInMeters;

								if( D < Dist )
								{
									Dist    = D;
									Closest = R;
								}
							}
							return Closest;
						}

						var Rec = Recs.Count switch
						          {
							          0 => null,
							          1 => Recs[ 0 ],
							          _ => FindClosest()
						          };

						return ( Rec?.DistanceInMeters / 1000 ) ?? CalculateDistance( frm, to );
					}

					OptimiseEntry? Previous = null,
					               Start    = null;

					foreach( var Point in points )
					{
						if( Previous is not null )
						{
							Point.DistanceFromStart = DoDistance( Start!.Point, Point.Point );
							Point.Distance          = DoDistance( Previous.Point, Point.Point );
						}
						else
							Start = Point;

						Previous = Point;
					}

					var Count = points.Count;

					if( Count >= 3 ) // Need at least 3 points for a circular route
					{
						var FirstPoint = points[ 0 ].Point;
						var PLast      = points[ --Count ];
						var LastPoint  = PLast.Point;

						Circular = ( FirstPoint.Latitude == LastPoint.Latitude ) && ( FirstPoint.Longitude == LastPoint.Longitude ); // Circular Route

						if( Circular )
							points.RemoveAt( Count ); // So not added to sort
					}

					// Google give better results if already approximately in order
					points = OrderByDistanceFromStart( points );

					if( Circular )
					{
						var P = points[ 0 ].Point; // Initial start

						var LineStartX = P.Longitude;
						var LineStartY = P.Latitude;

						var Ndx = points.Count - 1;

						var LineEndSave = points[ Ndx ];
						points.RemoveAt( Ndx ); // Don't include end in line compare

						P = LineEndSave.Point;
						var LineEndX = P.Longitude;
						var LineEndY = P.Latitude;

						List<OptimiseEntry> Left  = new(),
						                    Right = new();

						foreach( var Point in points )
						{
							P = Point.Point;

							var Val = Maths.SideOfLine( LineStartX, LineStartY, LineEndX, LineEndY, P.Longitude, P.Latitude );

							if( Val <= 0 )
								Left.Add( Point );
							else
								Right.Add( Point );
						}

						Left.Add( LineEndSave ); // Put the end point back
						Right.Reverse();
						Left.AddRange( Right ); // Coming back
						Left.Add( Left[ 0 ] );  // Back To Start
						points = Left;
					}

					Count = points.Count;

					// Can't have a pickup at the end must be a delivery
					if( ( Count-- > 0 ) && points[ Count-- ].IsPickupCompany )
					{
						for( ; Count >= 0; Count-- )
						{
							var Entry = points[ Count ];

							if( !Entry.IsPickupCompany ) // Delivery
							{
								points.RemoveAt( Count );
								points.Add( Entry ); // Put at the end
								break;
							}
						}
					}

				#if DEBUG
					Debug.WriteLine( "-----------------------Pickup/Deliveries-------------------------" );

					foreach( var Entry in points )
						Debug.WriteLine( $"({Entry.Point.Latitude:N6},{Entry.Point.Longitude:N6}) - {( Entry.IsPickupCompany ? "Pickup" : "Delivery" )} - {Entry.CompanyName}" );

					Debug.WriteLine( "-----------------------------------------------------------------" );
				#endif

					var MapPoints = new List<Point>();

					foreach( var P in points )
					{
						MapPoints.Add( new Point
						               {
							               Latitude    = Maths.Round6( P.Point.Latitude ),
							               Longitude   = Maths.Round6( P.Point.Longitude ),
							               Distance    = P.DistanceFromStart,
							               CompanyName = P.CompanyName
						               } );
					}

					// ReSharper disable once InconsistentlySynchronizedField
					var MapDirections = new MapsApi( Context ).GetDirections( travelMode, MapPoints, Circular );
					var Directions    = new Directions( MapDirections );

					Tasks.RunVoid( () =>
					               {
						               using var Db1          = new UsersEntities();
						               var       NeedToUpdate = new List<(Point From, Point To)>();

						               foreach( var Route in Directions.Routes )
						               {
							               foreach( var Leg in Route.Legs )
							               {
								               var From = Leg.OriginalStartLocation;
								               var To   = Leg.OriginalEndLocation;

							               #if NET7_0_OR_GREATER
								               var ToPoints = Db1.Entity.PointToPoints;
							               #else
								               var ToPoints = Db.PointToPoints;
							               #endif

								               var Rec = ( from P in ToPoints
								                           where ( P.Mode == (byte)travelMode ) && ( P.FromLatitude == From!.Latitude ) && ( P.FromLongitude == From.Longitude )
								                                 && ( P.ToLatitude == To!.Latitude ) && ( P.ToLongitude == To.Longitude )
								                           select P ).FirstOrDefault();

								               if( Rec is null )
									               NeedToUpdate.Add( ( From!, To! ) );
							               }
						               }

						               if( NeedToUpdate.Count > 0 )
						               {
							               lock( LockObject )
							               {
								               try
								               {
									               void DoUpdate( decimal fromLat, decimal fromLong, decimal toLat, decimal toLong, decimal distanceInKs, string jLeg )
									               {
										               try
										               {
										               #if NET7_0_OR_GREATER
											               var ToPoints = Db1.Entity.PointToPoints;
										               #else
											               var ToPoints = Db1.PointToPoints;
										               #endif

											               var Rec = ( from D in ToPoints
											                           where ( D.FromLatitude == fromLat )
											                                 && ( D.FromLongitude == fromLong )
											                                 && ( D.ToLatitude == toLat )
											                                 && ( D.ToLongitude == toLong )
											                           select D ).FirstOrDefault();

											               distanceInKs *= 1000;

											               if( Rec is null )
											               {
												               ToPoints.Add( new PointToPoint
												                             {
													                             Mode = (byte)travelMode,

													                             FromLatitude  = fromLat,
													                             FromLongitude = fromLong,

													                             ToLatitude  = toLat,
													                             ToLongitude = toLong,

													                             DistanceInMeters = distanceInKs,
													                             DateCreated      = Now,
													                             LegsAsJson       = jLeg
												                             } );
											               }
											               else
											               {
												               Rec.DistanceInMeters = distanceInKs;
												               Rec.DateCreated      = Now;
												               Rec.LegsAsJson       = jLeg;
											               }

											               Db1.SaveChanges();
										               }
										               catch( Exception Exception )
										               {
											               Context.SystemLogException( Exception );
										               }
									               }

									               foreach( var (From, To) in NeedToUpdate )
									               {
										               var UFromLat  = Maths.Round6( From.Latitude );
										               var UFromLong = Maths.Round6( From.Longitude );
										               var UToLat    = Maths.Round6( To.Latitude );
										               var UToLong   = Maths.Round6( To.Longitude );

										               if( ( UFromLat == UToLat ) && ( UFromLong == UToLong ) )
											               continue;

										               foreach( var M in Directions.Routes )
										               {
											               foreach( var Leg in M.Legs )
											               {
												               var MFromLat  = Maths.Round6( Leg.OriginalStartLocation!.Latitude );
												               var MFromLong = Maths.Round6( Leg.OriginalStartLocation!.Longitude );
												               var MToLat    = Maths.Round6( Leg.OriginalEndLocation!.Latitude );
												               var MToLong   = Maths.Round6( Leg.OriginalEndLocation!.Longitude );

												               // Find the matching Map record
												               if( ( UFromLat == MFromLat ) && ( UFromLong == MFromLong )
												                                            && ( UToLat == MToLat ) && ( UToLong == MToLong ) )
												               {
													               var JLeg         = JsonConvert.SerializeObject( Leg );
													               var DistanceInKs = (decimal)Leg.Distance!.Value;

													               DoUpdate( UFromLat, UFromLong, UToLat, UToLong, DistanceInKs, JLeg );
												               }
											               }
										               }
									               }
								               }
								               catch( Exception Exception )
								               {
									               Context.SystemLogException( Exception );
								               }
							               }
						               }
					               } );
					return MapDirections;
				}
				catch( Exception Exception )
				{
					// ReSharper disable once InconsistentlySynchronizedField
					Context.SystemLogException( Exception );
				}
			}
			return new Directions();
		}
	}

	public class DistanceOrDuration
	{
		public string Text = "";
		public double Value;
	}

	public class Step
	{
		public DistanceOrDuration? Distance,
		                           Duration;

		public Point? EndLocation;
		public string Instructions = "";
		public Point? StartLocation;
	}


	public class Leg
	{
		public DistanceOrDuration? Distance,
		                           Duration;

		public string EndAddress = "";
		public Point? EndLocation;

		public int Order; // Used to reorder route

		public Point? OriginalStartLocation,
		              OriginalEndLocation;

		public string StartAddress = "";
		public Point? StartLocation;

		public List<Step> Steps = new();

		public static string EmptyJsonLeg( Point from, Point to ) => JsonConvert.SerializeObject( new Leg
		                                                                                          {
			                                                                                          StartLocation         = from,
			                                                                                          OriginalStartLocation = from,

			                                                                                          EndLocation         = from,
			                                                                                          OriginalEndLocation = to
		                                                                                          } );

		public static string EmptyJsonLeg( Point point ) => EmptyJsonLeg( point, point );
	}

	public class Route
	{
		public List<Leg> Legs = new();
	}

	public class Directions
	{
		public List<string> Json   = new();
		public List<Route>  Routes = new();

		public Directions()
		{
		}

		public Directions( Directions d )
		{
			Json   = new List<string>( d.Json );
			Routes = new List<Route>( d.Routes );
		}
	}

	public class AddressData
	{
		public Company AsCompany
		{
			get
			{
				var StreetAndNumber = StreetNumber.Trim();

				if( StreetAndNumber != "" )
					StreetAndNumber += ' ';

				StreetAndNumber = $"{StreetAndNumber} {Street}".Pack();

				Company Co = new()
				             {
					             CompanyName  = CompanyName,
					             Suite        = Suite,
					             AddressLine1 = StreetAndNumber,
					             City         = City,
					             Vicinity     = Vicinity,
					             Region       = StateCode,
					             PostalCode   = PostalCode,
					             Country      = Country,
					             CountryCode  = CountryCode,

					             Latitude  = Latitude,
					             Longitude = Longitude
				             };
				return Co;
			}
		}

		public decimal Latitude
		{
			get => Point.Latitude;
			set => Point.Latitude = value;
		}

		public decimal Longitude
		{
			get => Point.Longitude;
			set => Point.Longitude = value;
		}

		public string FormattedAddress { get; set; } = null!;

		public string MultiLineAddress => DisplayAddress.Replace( ",", "\r\n" );

		public string HtmlMultiLineAddress => DisplayAddress.Replace( ",", "<br>" );

		public string DbLookupKey { get; set; } = null!;

		public string DisplayAddress
		{
			get
			{
				Trim();

				var Result = new StringBuilder();

				var HasSuite = Suite != "";

				Result.Append( Suite );

				if( HasSuite )
					Result.Append( '/' );

				Result.Append( StreetNumber );

				if( Street != "" )
					Result.Append( ' ' ).Append( Street );

				if( City != "" )
					Result.Append( ' ' ).Append( City );

				Result.Append( ',' );

				var HaveState = State != "";

				if( HaveState )
					Result.Append( State );

				if( StateCode != "" )
				{
					if( HaveState )
						Result.Append( '(' ).Append( StateCode ).Append( ')' );
					else
						Result.Append( StateCode );
				}

				if( PostalCode != "" )
					Result.Append( ' ' ).Append( PostalCode );

				if( Country != "" )
					Result.Append( ',' ).Append( Country );

				return Result.ToString().Trim().Pack().Capitalise();
			}
		}

		public bool NotFound { get; set; }

		// Rate limited by maps
		public bool RateLimited { get; set; }

		public string CompanyName,
		              Suite,
		              StreetNumber,
		              Street,
		              Vicinity,
		              City,
		              State,
		              StateCode,
		              PostalCode,
		              Country,
		              CountryCode;

		public bool IsPickupCompany;

		public Point PickupPoint = new();
		public Point Point       = new();

		private bool Trimmed;

		public AddressData()
		{
			CompanyName = Suite = StreetNumber = Street = Vicinity = City = State = StateCode = PostalCode = Country = CountryCode = "";
		}

		public AddressData Trim()
		{
			if( !Trimmed )
			{
				Trimmed = true;

				CompanyName  = CompanyName.TrimToUpper();
				Suite        = Suite.TrimToUpper();
				StreetNumber = StreetNumber.TrimToUpper();
				Street       = Street.TrimToUpper();
				Vicinity     = Vicinity.TrimToUpper();
				City         = City.TrimToUpper();
				State        = State.TrimToUpper();
				StateCode    = StateCode.TrimToUpper();
				PostalCode   = PostalCode.TrimToUpper();
				Country      = Country.TrimToUpper();
				CountryCode  = CountryCode.TrimToUpper();

				// Has only 1 space  Spaces
				DbLookupKey = string.Join( "", new StringBuilder().Append( CompanyName )
				                                                  .Append( Suite ).Append( StreetNumber )
				                                                  .Append( Street ).Append( Vicinity ).Append( City )
				                                                  .Append( State ).Append( StateCode ).Append( PostalCode )
				                                                  .Append( Country )
				                                                  .Append( CountryCode )
				                                                  .ToString()
				                                                  .ToLower()
				                                                  .Split( new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries ) ).Pack();
			}
			return this;
		}

		public AddressData ReTrim()
		{
			Trimmed = false;
			return Trim();
		}
	}

	public class AddressCoordinate
	{
		public AddressData? Address;

		public decimal Latitude,
		               Longitude;
	}

	protected static List<OptimiseEntry> OrderByDistanceFromStart( List<OptimiseEntry> points ) => ( from P in points
	                                                                                                 orderby P.DistanceFromStart
	                                                                                                 select P ).ToList();

	// To 11 metres
	public static int IntCoordinate( decimal c ) => (int)( ( ( c * 100000 ) + 5 ) / 10 );

	public static bool IsCoordinateEqual( decimal v1, decimal v2 ) => IntCoordinate( v1 ) == IntCoordinate( v2 );

	public static bool IsCoordinateEqual( Point p1, Point p2 ) => IsCoordinateEqual( p1.Latitude, p2.Latitude ) && IsCoordinateEqual( p1.Longitude, p2.Longitude );

	public override List<AddressCoordinate> FindLocationByAddress( AddressData address, out bool rateLimited )
	{
		var Result = new AzureMaps( Context ).FindLocationByAddress( address, out rateLimited );

		if( Result.Count == 0 )
		{
			Result = new GoogleMapsApiV3( Context ).FindLocationByAddress( address, out rateLimited );

			if( Result.Count == 0 )
				Result = new BingMapsV1( Context ).FindLocationByAddress( address, out rateLimited );
		}
		return Result;
	}

	public override Directions GetDirections( RouteBase.TRAVEL_MODE mode, List<Point> points, bool circular )
	{
		var List = new List<Point>( points );

		var Count = List.Count;

		if( Count >= 2 )
		{
			var SaveStart = List[ 0 ];
			var SaveEnd   = List[ --Count ];

			if( circular && ( SaveStart.Latitude == SaveEnd.Latitude ) && ( SaveStart.Longitude == SaveEnd.Longitude ) )
				List.RemoveAt( Count );

			List.RemoveAt( 0 ); // Remove Start so not to reorder

			Count = List.Count;

			if( Count > 0 )
			{
				var NewList = new List<Point>();

				if( Count > 1 )
				{
					// Remove duplicates but leave start and end points
					NewList.AddRange( from Point in List
					                  group Point by new { Point.Latitude, Point.Longitude }
					                  into Points
					                  select Points.First() );

					var Ndx   = NewList.Count - 1;
					var Entry = NewList[ Ndx-- ];

					// Make Sure The Last Stays The Last
					if( !Entry.Equals( SaveEnd ) )
					{
						for( ; Ndx >= 0; Ndx-- )
						{
							Entry = NewList[ Ndx ];

							if( Entry.Equals( SaveEnd ) )
							{
								NewList.RemoveAt( Ndx );
								NewList.Add( Entry );
								break;
							}
						}
					}
				}
				else
					NewList.AddRange( List );

				NewList.Insert( 0, SaveStart );

				if( circular )
					NewList.Add( SaveStart );
/*
					var Pc = new PointCache();
					var D  = Pc.FindRoute( mode, NewList );

					if( D is not null ) // Route In Cache
						return D;
*/
				var Directions = new AzureMaps( Context ).GetDirections( mode, NewList, circular );

				return Directions.Routes.Count > 0 ? Directions : new GoogleMapsApiV3( Context ).GetDirections( mode, NewList, circular );
			}
		}
		return new Directions();
	}

	public override void ShowDirections( Directions directions )
	{
		new GoogleMapsApiV3( Context ).ShowDirections( directions );
	}

	public static string DictionaryToQueryParameters( Dictionary<string, string> queryParameters )
	{
		var Params = new StringBuilder();
		var First  = true;

		foreach( var Parameter in queryParameters )
		{
			if( !First )
				Params.Append( '&' );
			else
				First = false;

			Params.Append( Uri.EscapeDataString( Parameter.Key ) )
			      .Append( '=' )
			      .Append( Uri.EscapeDataString( Parameter.Value ) );
		}

		return Params.ToString();
	}
}