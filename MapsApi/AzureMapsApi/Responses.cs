﻿#nullable enable

#pragma warning disable IDE1006 // Name violations

// ReSharper disable InconsistentNaming

namespace MapsApi.AzureMapsApi;

public partial class AzureMaps : AMapsApi
{
#region Datasources
	public class DataSourcesGeometry
	{
		public string id { get; set; } = "";
	}

	public class DataSources
	{
		public DataSourcesGeometry geometry { get; set; } = new();
	}
#endregion

#region Addresses
	public class SearchResultAddress
	{
		public string buildingNumber              { get; set; } = "";
		public string country                     { get; set; } = "";
		public string countryCode                 { get; set; } = "";
		public string countryCodeISO3             { get; set; } = "";
		public string countrySecondarySubdivision { get; set; } = "";
		public string countrySubdivision          { get; set; } = "";
		public string countrySubdivisionName      { get; set; } = "";
		public string countryTertiarySubdivision  { get; set; } = "";
		public string crossStreet                 { get; set; } = "";
		public string extendedPostalCode          { get; set; } = "";
		public string freeformAddress             { get; set; } = "";
		public string localName                   { get; set; } = "";
		public string municipality                { get; set; } = "";
		public string municipalitySubdivision     { get; set; } = "";
		public string postalCode                  { get; set; } = "";
		public int[]  routeNumbers                { get; set; } = Array.Empty<int>();
		public string street                      { get; set; } = "";
		public string streetName                  { get; set; } = "";
		public string streetNameAndNumber         { get; set; } = "";
		public string streetNumber                { get; set; } = "";
	}

#region Position
	public class CoordinateAbbreviated
	{
		public decimal lat { get; set; }
		public decimal lon { get; set; }
	}

	public class SearchSummaryGeoBias : CoordinateAbbreviated
	{
	}
#endregion

	public class SearchResultViewport
	{
		public CoordinateAbbreviated btmRightPoint { get; set; } = new();
		public CoordinateAbbreviated topLeftPoint  { get; set; } = new();
	}

#region Search Results
	public class SearchResultEntryPoint
	{
		public CoordinateAbbreviated position { get; set; } = new();
		public string                type     { get; set; } = "";
	}

	public class SearchAddressResultBase : SearchResultEntryPoint
	{
		public SearchResultAddress      address     { get; set; } = new();
		public SearchResultEntryPoint[] entryPoints { get; set; } = Array.Empty<SearchResultEntryPoint>();
		public string                   id          { get; set; } = "";
		public decimal                  score       { get; set; }
		public SearchResultViewport     viewport    { get; set; } = new();
	}

	public class SearchAddressResult : SearchAddressResultBase
	{
		public DataSources dataSources { get; set; } = new();
	}

	public class SearchAddressStructuredResult : SearchAddressResultBase
	{
		public SearchResultAddressRanges addressRanges { get; set; } = new();
		public decimal                   dist          { get; set; }
	}
#endregion

#region Summary
	public class SearchAddressSummary
	{
		public int                  fuzzyLevel   { get; set; }
		public SearchSummaryGeoBias geoBias      { get; set; } = new();
		public int                  limit        { get; set; }
		public int                  numResults   { get; set; }
		public int                  offset       { get; set; }
		public string               query        { get; set; } = "";
		public int                  queryTime    { get; set; }
		public string               queryType    { get; set; } = "";
		public int                  totalResults { get; set; }
	}

	public class SearchAddressStructuredSummary : SearchAddressSummary
	{
	}
#endregion


	public class SearchAddressResponse
	{
		public SearchAddressResult[] results { get; set; } = Array.Empty<SearchAddressResult>();
		public SearchAddressSummary  summary { get; set; } = new();
	}

	public class SearchResultAddressRanges
	{
		public CoordinateAbbreviated from      { get; set; } = new();
		public string                rangeLeft { get; set; } = "";
	}

	public class SearchAddressStructuredResponse
	{
		public SearchAddressStructuredResult[] results { get; set; } = Array.Empty<SearchAddressStructuredResult>();
		public SearchAddressStructuredSummary  summary { get; set; } = new();
	}
#endregion

#region Route
	public class RouteOptimizedWaypoint
	{
		public int optimizedIndex { get; set; }
		public int providedIndex  { get; set; }
	}

	public class Coordinate
	{
		public decimal latitude  { get; set; }
		public decimal longitude { get; set; }
	}

	public class RouteResultInstruction
	{
		public string     combinedMessage           { get; set; } = "";
		public string     countryCode               { get; set; } = "";
		public string     drivingSide               { get; set; } = "";
		public string     exitNumber                { get; set; } = "";
		public string     instructionType           { get; set; } = "";
		public string     junctionType              { get; set; } = "";
		public string     maneuver                  { get; set; } = "";
		public string     message                   { get; set; } = "";
		public Coordinate point                     { get; set; } = new();
		public int        pointIndex                { get; set; }
		public bool       possibleCombineWithNext   { get; set; }
		public string[]   roadNumbers               { get; set; } = Array.Empty<string>();
		public string     roundaboutExitNumber      { get; set; } = "";
		public int        routeOffsetInMeters       { get; set; }
		public string     signpostText              { get; set; } = "";
		public string     stateCode                 { get; set; } = "";
		public string     street                    { get; set; } = "";
		public int        travelTimeInSeconds       { get; set; }
		public int        turnAngleInDecimalDegrees { get; set; }
	}

	public class RouteResultGuidance
	{
		public RouteResultInstructionGroup[] instructionGroups { get; set; } = Array.Empty<RouteResultInstructionGroup>();
		public RouteResultInstruction[]      instructions      { get; set; } = Array.Empty<RouteResultInstruction>();
	}

	public class RouteResultInstructionGroup
	{
		public int    firstInstructionIndex { get; set; }
		public int    groupLengthInMeters   { get; set; }
		public string groupMessage          { get; set; } = "";
		public int    lastInstructionIndex  { get; set; }
	}

	public class RouteResultLegSummary
	{
		public string  arrivalTime                             { get; set; } = "";
		public decimal batteryConsumptionInkWh                 { get; set; }
		public string  departureTime                           { get; set; } = "";
		public decimal fuelConsumptionInLiters                 { get; set; }
		public int     historicTrafficTravelTimeInSeconds      { get; set; }
		public int     lengthInMeters                          { get; set; }
		public int     liveTrafficIncidentsTravelTimeInSeconds { get; set; }
		public int     noTrafficTravelTimeInSeconds            { get; set; }
		public int     trafficDelayInSeconds                   { get; set; }
		public int     travelTimeInSeconds                     { get; set; }
	}

	public class RouteResultLeg
	{
		public Coordinate[]          points  { get; set; } = Array.Empty<Coordinate>();
		public RouteResultLegSummary summary { get; set; } = new();
	}

	public class RouteResultSectionTecCause
	{
		public int mainCauseCode { get; set; }
		public int subCauseCode  { get; set; }
	}

	public class RouteResultSectionTec
	{
		public RouteResultSectionTecCause[] causes     { get; set; } = Array.Empty<RouteResultSectionTecCause>();
		public int                          effectCode { get; set; }
	}

	public class RouteResultSection
	{
		public int                   delayInSeconds      { get; set; }
		public int                   effectiveSpeedInKmh { get; set; }
		public int                   endPointIndex       { get; set; }
		public string                magnitudeOfDelay    { get; set; } = "";
		public string                sectionType         { get; set; } = "";
		public string                simpleCategory      { get; set; } = "";
		public int                   startPointIndex     { get; set; }
		public RouteResultSectionTec tec                 { get; set; } = new();
		public string                travelMode          { get; set; } = "";
	}

	public class RouteDirectionsSummary
	{
		public string arrivalTime           { get; set; } = "";
		public string departureTime         { get; set; } = "";
		public int    lengthInMeters        { get; set; }
		public int    trafficDelayInSeconds { get; set; }
		public int    travelTimeInSeconds   { get; set; }
	}

	public class RouteDirectionsResult
	{
		public RouteResultGuidance    guidance { get; set; } = new();
		public RouteResultLeg[]       legs     { get; set; } = Array.Empty<RouteResultLeg>();
		public RouteResultSection[]   sections { get; set; } = Array.Empty<RouteResultSection>();
		public RouteDirectionsSummary summary  { get; set; } = new();
	}

	public class RouteResponseReportEffectiveSetting
	{
		public string key   { get; set; } = "";
		public string value { get; set; } = "";
	}

	public class RouteResponseReport
	{
		public RouteResponseReportEffectiveSetting effectiveSettings { get; set; } = new();
	}

	public class RouteDirectionsResponse
	{
		public string                   formatVersion      { get; set; } = "";
		public RouteOptimizedWaypoint[] optimizedWaypoints { get; set; } = Array.Empty<RouteOptimizedWaypoint>();

		public RouteResponseReport report { get; set; } = new();

		public RouteDirectionsResult[] routes { get; set; } = Array.Empty<RouteDirectionsResult>();
	}
#endregion
}