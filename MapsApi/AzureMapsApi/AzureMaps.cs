﻿#nullable enable

namespace MapsApi.AzureMapsApi;

public partial class AzureMaps : AMapsApi
{
	private const int WAYPOINT_LIMIT = 148;

	// Temp Class
	internal class RouteInstruction
	{
		public string     Instruction = "";
		public Coordinate Point       = null!;
	}

	public override string ShowMapLink => throw new NotImplementedException();

	protected override string Key => Context.Configuration.AzureMapsApiKey;

	public override List<Maps.MapsApi.AddressCoordinate> FindLocationByAddress( Maps.MapsApi.AddressData address, out bool rateLimited )
	{
		if( address.CountryCode.IsNotNullOrWhiteSpace() )
		{
			var RetVal = FindLocationByStructuredAddress( address, out rateLimited );

			if( RetVal.Count > 0 )
				return RetVal;
		}
		return FindLocationByUnStructuredAddress( address, out rateLimited );
	}

	public override Maps.MapsApi.Directions GetDirections( RouteBase.TRAVEL_MODE mode, List<Point> points, bool circular )
	{
		var Result = new Maps.MapsApi.Directions();

		try
		{
			var Copy = new List<Point>( points );

			while( Copy.Count > 1 )
			{
				var Count  = Math.Min( Copy.Count, WAYPOINT_LIMIT ); // Must be greater than one
				var Points = Copy.GetRange( 0, Count );

				Copy.RemoveRange( 0, Count );

				if( Copy.Count == 1 )                             // Leave last 2 points as start point for next
					Copy.Insert( 0, Points[ Points.Count - 1 ] ); // Forced to have overlap

				var Args  = new StringBuilder();
				var First = true;

				foreach( var Point in points )
				{
					if( First )
						First = false;
					else
						Args.Append( ':' );
					Args.Append( $"{Point.Latitude:F5},{Point.Longitude:F5}" );
				}

				var RouteType = ( mode & RouteBase.TRAVEL_MODE.FASTEST ) != 0 ? "fastest" : "shortest";
				var Avoids    = "";
				var BestOrder = ( mode & RouteBase.TRAVEL_MODE.DONT_OPTIMISE ) != 0 ? "" : "&computeBestOrder=True";

				void DoAvoid( RouteBase.TRAVEL_MODE mask, string avoid )
				{
					if( ( mode & mask ) != 0 )
						Avoids = $"{Avoids}&avoid={avoid}";
				}

				DoAvoid( RouteBase.TRAVEL_MODE.AVOID_TOLL_ROADS, "tollRoads" );
				DoAvoid( RouteBase.TRAVEL_MODE.AVOID_MOTORWAYS, "motorways" );
				DoAvoid( RouteBase.TRAVEL_MODE.AVOID_FERRIES, "ferries" );
				DoAvoid( RouteBase.TRAVEL_MODE.AVOID_BORDER_CROSSINGS, "borderCrossings" );
				DoAvoid( RouteBase.TRAVEL_MODE.AVOID_CAR_POOLS, "carpools" );
				DoAvoid( RouteBase.TRAVEL_MODE.AVOID_UNPAVED_ROADS, "unpavedRoads" );

				var RUri     = $"{RouteUri}&instructionsType=tagged&routeType={RouteType}{Avoids}{BestOrder}&query={Args}";
				var Response = GetResponse( RUri );

				if( Response is not null )
				{
					var RouteResult = JsonConvert.DeserializeObject<RouteDirectionsResponse>( Response );

					if( RouteResult is not null )
					{
						foreach( var Route in RouteResult.routes )
						{
							var Rte  = new Maps.MapsApi.Route();
							var Legs = Rte.Legs;
							var PNdx = 0;

							foreach( var Leg in Route.legs )
							{
								var S    = Leg.summary;
								var Ks   = S.lengthInMeters / 1000.0;
								var Mins = S.travelTimeInSeconds / 60.0;

								var Steps = new List<Maps.MapsApi.Step>();

								var InsPoints = ( from P in Leg.points
								                  select new RouteInstruction
								                         {
									                         Point = P
								                         } ).ToList();

								// Match up the instructions
								foreach( var Point in InsPoints )
								{
									var P1 = Point.Point;

									foreach( var I in Route.guidance.instructions )
									{
										var P2 = I.point;

										if( ( P1.latitude == P2.latitude ) && ( P1.longitude == P2.longitude ) )
										{
											Point.Instruction = I.message.Trim();
											break;
										}
									}
								}

								RouteInstruction? Prev = null;

								foreach( var Point in InsPoints )
								{
									if( Prev is not null )
									{
										var Pp = Prev.Point;
										var P  = Point.Point;

										Steps.Add( new Maps.MapsApi.Step
										           {
											           StartLocation = new Point
											                           {
												                           Latitude  = Pp.latitude,
												                           Longitude = Pp.longitude
											                           },
											           EndLocation = new Point
											                         {
												                         Latitude  = P.latitude,
												                         Longitude = P.longitude
											                         },
											           Instructions = Prev.Instruction,
											           Distance     = new Maps.MapsApi.DistanceOrDuration(),
											           Duration     = new Maps.MapsApi.DistanceOrDuration()
										           } );
									}
									Prev = Point;
								}

								var Ndx = Steps.Count;

								if( ( Ndx > 0 ) && Prev is not null )
									Steps[ Ndx - 1 ].Instructions = Prev.Instruction;

								Legs.Add( new Maps.MapsApi.Leg
								          {
									          OriginalStartLocation = Points[ PNdx ],
									          OriginalEndLocation   = Points[ ++PNdx ],

									          Distance = new Maps.MapsApi.DistanceOrDuration
									                     {
										                     Value = Ks,
										                     Text  = $"{Ks:N} Kilometres"
									                     },
									          Duration = new Maps.MapsApi.DistanceOrDuration
									                     {
										                     Value = Mins,
										                     Text  = $"{Mins:N} Minutes and {60 / Mins.Frac():N0} Seconds"
									                     },
									          EndAddress    = "",
									          EndLocation   = new Point(),
									          StartAddress  = "",
									          StartLocation = new Point(),
									          Steps         = Steps
								          } );
							}
							Result.Routes.Add( Rte );
						}
					}
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}

	public override void ShowDirections( Maps.MapsApi.Directions directions )
	{
		throw new NotImplementedException();
	}

	public AzureMaps( RequestContext.RequestContext context ) :
		base( context )

	{
	}

// Requires Country Code
	private List<Maps.MapsApi.AddressCoordinate> FindLocationByStructuredAddress( Maps.MapsApi.AddressData address, out bool rateLimited )
	{
		rateLimited = false;
		var RetVal = new List<Maps.MapsApi.AddressCoordinate>();

		try
		{
			var Args = new StringBuilder();

			void AppendArg( string argName, string arg )
			{
				if( arg.IsNotNullOrWhiteSpace() )
					Args.Append( $"&{argName}={Uri.EscapeDataString( arg.Trim() )}" );
			}

			AppendArg( "streetNumber", address.StreetNumber );
			AppendArg( "streetName", address.Street );
			AppendArg( "municipality", address.City );
			AppendArg( "countrySubdivision", address.StateCode );

			var PCode = address.PostalCode.Trim();

			if( PCode.Length >= 4 )
				AppendArg( "postalCode", PCode );

			AppendArg( "countrySecondarySubdivision", address.Country );
			AppendArg( "countryCode", address.CountryCode );

			var Query = Args.ToString();

			if( Query.IsNotNullOrWhiteSpace() )
			{
				var SearchUri = $"{StructuredAddressUri}{Query}";

				var Response = GetResponse( SearchUri );

				if( Response is not null )
				{
					var Result = JsonConvert.DeserializeObject<SearchAddressStructuredResponse>( Response );

					if( Result is not null )
					{
						RetVal.AddRange( from StructuredResult in Result.results
						                 let Pos = StructuredResult.position
						                 let Address = StructuredResult.address
						                 select new Maps.MapsApi.AddressCoordinate
						                        {
							                        Latitude  = Pos.lat,
							                        Longitude = Pos.lon,
							                        Address = new Maps.MapsApi.AddressData
							                                  {
								                                  CompanyName  = address.CompanyName.Trim(),
								                                  Suite        = address.Suite.Trim(),
								                                  StreetNumber = Address.streetNumber.Trim(),
								                                  Street       = Address.streetName.Trim(),
								                                  City         = Address.municipality.Trim(),
								                                  State        = Address.countrySubdivisionName.Trim(),
								                                  StateCode    = Address.countrySubdivision.Trim(),
								                                  PostalCode   = Address.postalCode.Trim(),
								                                  Country      = Address.country.Trim(),
								                                  CountryCode  = Address.countryCode.Trim(),

								                                  Longitude = Pos.lon,
								                                  Latitude  = Pos.lat,
								                                  Point = new Point
								                                          {
									                                          Distance  = StructuredResult.dist,
									                                          Latitude  = Pos.lat,
									                                          Longitude = Pos.lon
								                                          },

								                                  Vicinity         = Address.crossStreet.Trim(),
								                                  FormattedAddress = Address.freeformAddress.Trim(),

								                                  RateLimited = false
							                                  }
						                        } );
					}
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return RetVal;
	}

	private List<Maps.MapsApi.AddressCoordinate> FindLocationByUnStructuredAddress( Maps.MapsApi.AddressData address, out bool rateLimited )
	{
		rateLimited = false;
		var RetVal = new List<Maps.MapsApi.AddressCoordinate>();

		try
		{
			var Args  = new StringBuilder();
			var First = true;

			void AppendArg( string arg )
			{
				arg = arg.Trim();

				if( arg.IsNotNullOrWhiteSpace() )
				{
					if( First )
						First = false;
					else
						Args.Append( ", " );

					Args.Append( arg );
				}
			}

			Args.Append( $"{address.StreetNumber.Trim()} ".TrimStart() );
			AppendArg( address.Street );
			AppendArg( address.City );
			AppendArg( address.StateCode );
			AppendArg( address.State );
			AppendArg( address.PostalCode );
			AppendArg( address.CountryCode );
			AppendArg( address.Country );

			var Query = Uri.EscapeDataString( Args.ToString() );

			if( Query.IsNotNullOrWhiteSpace() )
			{
				var SearchUri = $"{AddressSearchUri}&query={Query}";
				var Response  = GetResponse( SearchUri );

				if( Response is not null )
				{
					var Result = JsonConvert.DeserializeObject<SearchAddressResponse>( Response );

					if( Result is not null )
					{
						RetVal.AddRange( from AddressResult in Result.results
						                 let Pos = AddressResult.position
						                 let Address = AddressResult.address
						                 let City = Address.municipality.Trim()
						                 let LocalName = Address.localName.Trim()
						                 let State = Address.countrySubdivisionName.Trim()
						                 let Country = Address.country.Trim()
						                 where Country.Compare( address.Country, StringComparison.OrdinalIgnoreCase ) == 0
						                       && State.Compare( address.State, StringComparison.OrdinalIgnoreCase ) == 0
						                       && ( City.Compare( address.City, StringComparison.OrdinalIgnoreCase ) == 0
						                            || LocalName.Compare( address.City, StringComparison.OrdinalIgnoreCase ) == 0 )
						                 select new Maps.MapsApi.AddressCoordinate
						                        {
							                        Latitude  = Pos.lat,
							                        Longitude = Pos.lon,
							                        Address = new Maps.MapsApi.AddressData
							                                  {
								                                  CompanyName  = address.CompanyName.Trim(),
								                                  Suite        = address.Suite.Trim(),
								                                  StreetNumber = Address.streetNumber.Trim(),
								                                  Street       = Address.streetName.Trim(),
								                                  City         = City,
								                                  State        = State,
								                                  StateCode    = Address.countrySubdivision.Trim(),
								                                  PostalCode   = Address.postalCode.Trim(),
								                                  Country      = Country,
								                                  CountryCode  = Address.countryCode.Trim(),

								                                  Longitude = Pos.lon,
								                                  Latitude  = Pos.lat,
								                                  Point = new Point
								                                          {
									                                          Latitude  = Pos.lat,
									                                          Longitude = Pos.lon
								                                          },

								                                  Vicinity         = Address.crossStreet.Trim(),
								                                  FormattedAddress = Address.freeformAddress.Trim(),

								                                  RateLimited = false
							                                  }
						                        } );
					}
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return RetVal;
	}

#region Azure Key
	private string? _FormatAndKey;
	private string  FormatAndKey => _FormatAndKey ??= $"json?subscription-key={Key}&api-version=1.0";
#endregion


#region Azure Uri
	private const string AZURE_MAPS_BASE_URI = "https://atlas.microsoft.com";

	private static string? _AddressSearchBaseUri;
	private static string  AddressSearchBaseUri => _AddressSearchBaseUri ??= $"{AZURE_MAPS_BASE_URI}/search/address";


	private static string? _AddressSearchUri;
	private        string  AddressSearchUri => _AddressSearchUri ??= $"{AddressSearchBaseUri}/{FormatAndKey}";

	private static string? _StructuredAddressUri;
	private        string  StructuredAddressUri => _StructuredAddressUri ??= $"{AddressSearchBaseUri}/structured/{FormatAndKey}";

	private static string? _RouteUri;
	private        string  RouteUri => _RouteUri ??= $"{AZURE_MAPS_BASE_URI}/route/directions/{FormatAndKey}";
#endregion
}