﻿using Database.Model.Database;
using Database.Model.Databases.Users;

namespace MapsApi;

public class PointCache
{
	private const decimal LIMIT = 0.0005M; // 55 Metres  

	public List<(PointToPoint CachePoint, Point Point)> Find( UsersEntities db, RouteBase.TRAVEL_MODE mode, Point from, List<Point> to )
	{
		var Result = new List<(PointToPoint CachePoint, Point Point)>();

		var FromLatitude  = from.Latitude;
		var FromLongitude = from.Longitude;

	#if NET7_0_OR_GREATER
		var Points = db.Entity.PointToPoints;
	#else
		var Points = db.PointToPoints;
	#endif

		// Find all entries for this from point (110 metres)
		var FromPoints = ( from D in Points
		                   where ( D.Mode == (byte)mode )
		                         && ( Math.Abs( D.FromLatitude - FromLatitude ) <= LIMIT )
		                         && ( Math.Abs( D.FromLongitude - FromLongitude ) <= LIMIT )
		                   select D ).ToList();

		// Really should only be one
		foreach( var From in FromPoints )
		{
			foreach( var To in to )
			{
				if( ( Math.Abs( From.ToLatitude - To.Latitude ) <= LIMIT )
				    && ( Math.Abs( From.ToLongitude - To.Longitude ) <= LIMIT ) )
					Result.Add( ( From, To ) );
			}
		}

		return ( from R in Result
		         orderby R.CachePoint.DistanceInMeters
		         select R ).ToList();
	}


#region Find Route
	public Maps.MapsApi.Directions? FindRoute( RouteBase.TRAVEL_MODE mode, List<Point> points )
	{
		using var Db = new UsersEntities();

		List<PointToPoint> Legs = new();

		if( points.Count > 1 )
		{
			var StartPoint = points[ 0 ];
			var Count      = points.Count;
			var LastNdx    = Count - 1;
			var EndPoint   = points[ LastNdx ];

			var Circular = Equals( StartPoint, EndPoint );

			List<Point> Out,
			            Back;

			if( Circular && ( Count > 3 ) ) // Not just out and back
			{
				points.RemoveAt( LastNdx );
				points.RemoveAt( 0 );

				var Furthest = StartPoint.Furthest( points );
				points.Remove( Furthest );

				( Out, Back ) = Point.SideOfLineLeft( new Points
				                                      {
					                                      From = StartPoint,
					                                      To   = Furthest
				                                      }, points
				                                    );
				Out.Insert( 0, StartPoint );
				Out.Add( Furthest );
				Back.Reverse();
				Back.Insert( 0, Furthest ); // Starting point for Back
				Back.Add( StartPoint );
			}
			else
			{
				Out  = points;
				Back = new List<Point>();
			}

			const byte OUT  = 0,
			           BACK = 1;

			for( var State = OUT; State <= BACK; ++State )
			{
				var Points = State == OUT ? Out : Back;

				while( Points.Count > 0 )
				{
					var From = Points[ 0 ];
					Points.RemoveAt( 0 );
					var To = Find( Db, mode, From, Points ); // All known to locations (Shortest Distance First)

					if( To.Count > 0 )
					{
						var (CachePoint, Point) = To[ 0 ];
						Legs.Add( CachePoint );

						// Make sure To is the next start point
						var NextPoints = ( from P in Points
						                   where Equals( P, Point )
						                   select P ).Take( 1 ).ToList();

						NextPoints.AddRange( from P in NextPoints
						                     where !Equals( P, Point )
						                     select P );
						Points = NextPoints;
					}
					else
						return null;
				}
			}

			if( Legs.Count > 0 )
			{
				var Result = new Maps.MapsApi.Directions();

				var Route = new Maps.MapsApi.Route();
				Result.Routes.Add( Route );

				foreach( var Leg in Legs )
					Route.Legs.Add( JsonConvert.DeserializeObject<Maps.MapsApi.Leg>( Leg.LegsAsJson )! );

				return Result;
			}
		}
		return null;
	}
#endregion
}