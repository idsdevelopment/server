﻿using System.Diagnostics.CodeAnalysis;

namespace MapsApi.GeoLocation;

public class Point
{
	public enum MEASUREMENT
	{
		KILOMETRES,
		MILES
	}

	public decimal Latitude { get; set; }

	public decimal Longitude { get; set; }

	public decimal Distance { get; set; }

	private int?   HCode;
	public  string CompanyName = "";

	public override string ToString() => Latitude + "," + Longitude;

	public override bool Equals( object? obj )
	{
		if( obj is Point P )
			return ( Latitude == P.Latitude ) && ( Longitude == P.Longitude );
		return false;
	}

	[SuppressMessage( "ReSharper", "NonReadonlyMemberInGetHashCode" )]
	public override int GetHashCode()
	{
		unchecked
		{
			if( HCode is null )
			{
				var Rand = new Random();
				HCode = Rand.Next( int.MinValue, int.MaxValue );
			}
			return HCode.Value;
		}
	}

	public decimal DistanceTo( Point p, MEASUREMENT measurement )
	{
		var Dist = Measurement.SimpleGpsDistance( Latitude, Longitude, p.Latitude, p.Longitude );

		return measurement switch
		       {
			       MEASUREMENT.KILOMETRES => Dist.KiloMetres,
			       _                      => Dist.Miles
		       };
	}

	public decimal DistanceTo( Point p ) => DistanceTo( p, MEASUREMENT.KILOMETRES );

	public Point Closest( IEnumerable<Point> points )
	{
		var Closest = this;
		var Dist    = decimal.MaxValue;

		foreach( var Point in points )
		{
			var Temp = DistanceTo( Point );

			if( Temp < Dist )
			{
				Dist             = Temp;
				Closest.Distance = Temp;
				Closest          = Point;
			}
		}
		return Closest;
	}

	public Point Furthest( IEnumerable<Point> points )
	{
		var Furthest = this;
		var Dist     = decimal.MinValue;

		foreach( var Point in points )
		{
			var Temp = DistanceTo( Point );

			if( Temp > Dist )
			{
				Dist              = Temp;
				Furthest.Distance = Temp;
				Furthest          = Point;
			}
		}
		return Furthest;
	}

	public static (List<Point> Left, List<Point>Right, List<Point> OnLine ) SideOfLine( Points line, IEnumerable<Point> points )
	{
		var FromLat  = line.From.Latitude;
		var FromLong = line.From.Longitude;
		var ToLat    = line.To.Latitude;
		var ToLong   = line.To.Longitude;

		var Left  = new List<Point>();
		var Right = new List<Point>();
		var On    = new List<Point>();

		foreach( var P in points )
		{
			switch( Maths.SideOfLine( FromLat, FromLong, ToLat, ToLong, P.Latitude, P.Longitude ) )
			{
			case < 0:
				Left.Add( P );
				break;

			case > 0:
				Right.Add( P );
				break;

			default:
				On.Add( P );
				break;
			}
		}
		return ( Left, Right, On );
	}

	public static (List<Point> Left, List<Point> Right) SideOfLineLeft( Points line, IEnumerable<Point> points )
	{
		var (Left, Right, OnLine) = SideOfLine( line, points );

		if( OnLine.Count > 0 )
			Left.AddRange( OnLine );
		return ( Left, Right );
	}

	public static (List<Point> Left, List<Point> Right) SideOfLineRight( Points line, IEnumerable<Point> points )
	{
		var (Left, Right, OnLine) = SideOfLine( line, points );

		if( OnLine.Count > 0 )
			Right.AddRange( OnLine );
		return ( Left, Right );
	}
}

public class Points
{
	public Point From = new(),
	             To   = new();


	public decimal DistanceTo( Point.MEASUREMENT measurement = Point.MEASUREMENT.KILOMETRES ) => From.DistanceTo( To, measurement );
}