﻿#nullable enable

namespace Maps.GoogleMapsApi;

public abstract class GoogleMaps : AMapsApi
{
	protected override string Key => Context.Configuration.GoogleMapsApiKey;

	internal abstract string BaseUrl { get; }

	public string Url( string queryType, string queryParameters )
	{
		var Result =
			new StringBuilder( BaseUrl ).Append( queryType ).Append( "/json?" ).Append( queryParameters ).Append( "&key=" )
			                            .Append( Key );

		return Result.ToString();
	}

	public string Url( string queryType, Dictionary<string, string> queryParameters ) => Url( queryType, MapsApi.DictionaryToQueryParameters( queryParameters ) );

	internal GoogleMaps( RequestContext.RequestContext context )
		: base( context )
	{
	}
}