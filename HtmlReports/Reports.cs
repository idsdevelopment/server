﻿using Protocol.Data;

namespace ReportsModule;

public abstract partial class Reports
{
	private const string
		REPORT_DATE      = "REPORT_DATE",
		REPORT_TIME      = "REPORT_TIME",
		PAGE_NUMBER      = "PAGE_NUMBER",
		EOF              = "EOF",
		TOTAL_PAGE_TOKEN = "~!@TOTAL_PAGES@!~";

	protected IRequestContext Context { get; }

	protected Pulsar Pulsar { get; }

	protected abstract string TemplateName { get; }

	protected Reports( IRequestContext context, DataSource dataSource, Report report ) : this( context, dataSource, ToPaperSize( report ), ToOrientation( report ) )
	{
		UpdateReportDateTime( report.ReportDateTime );
	}

	protected Reports( IRequestContext context, DataSource dataSource, PAPER_SIZES paperSize = PAPER_SIZES.A4,
					   ORIENTATION     orientation = ORIENTATION.PORTRAIT )
	{
		Context = context;

		DefaultPaperSize   = paperSize;
		DefaultOrientation = orientation;

		Data = new ReportData
			   {
				   PaperSize   = DefaultPaperSize,
				   Orientation = DefaultOrientation,
				   Source      = dataSource,
				   PageNumber  = 1
			   };

		( Pulsar = new Pulsar() ).RegisterBlockFunction( "REPORT", DoReport )
								 .RegisterBlockFunction( "PAGE", DoPage )
								 .RegisterBlockFunction( "HEADER", DoHeader )
								 .RegisterBlockFunction( "FOOTER", DoFooter )
								 .RegisterBlockFunction( "BAND", DoBand )

								 //
								 .RegisterFunction( "NEXT", DoNext )
								 .RegisterFunction( "PAGE_BREAK", DoPageBreak )
								 .RegisterFunction( PAGE_NUMBER, DoPageNumber )
								 .RegisterFunction( REPORT_DATE, DoReportDate )
								 .RegisterFunction( REPORT_TIME, DoReportTime )
								 .RegisterFunction( "BARCODE", DoBarcode )

								 //
								 .Assign( "USER_ID", Context.UserName )
								 .Assign( "TOTAL_PAGES", TOTAL_PAGE_TOKEN )
								 .Assign( PAGE_NUMBER, 1 );

		UpdateReportDateTime( DateTimeOffset.UtcNow );
	}

	private void UpdateReportDateTime( DateTimeOffset now )
	{
		Pulsar.Assign( REPORT_DATE, $"{now:yyyy/MM/dd}" )
			  .Assign( REPORT_TIME, $"{now:t}" );
	}

	private static PAPER_SIZES ToPaperSize( Report r )
	{
		return r.PaperSize switch
			   {
				   Report.PAPER_SIZE.A4 => PAPER_SIZES.A4,
				   _                    => PAPER_SIZES.LETTER
			   };
	}

	private static ORIENTATION ToOrientation( Report r ) => r.IsLandscape ? ORIENTATION.LANDSCAPE : ORIENTATION.PORTRAIT;
}