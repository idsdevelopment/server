﻿namespace ReportsModule;

public partial class Reports
{
	private const decimal PAGE_MARGIN_CM = 0.9m; // Dead area printer cannot print on in cm Article padding of 0.1


	public enum ORIENTATION
	{
		PORTRAIT,
		LANDSCAPE
	}

	public enum PAPER_SIZES
	{
		A4,
		LETTER
	}

	public struct PaperSize // Must be struct
	{
		public PAPER_SIZES Size;

		public decimal HeightCm,
		               WidthCm;
	}


	// Defined in portrait mode less print margins
	private static readonly Dictionary<PAPER_SIZES, PaperSize> PaperSizes = new()
	                                                                        {
		                                                                        {
			                                                                        PAPER_SIZES.A4, new PaperSize
			                                                                                        {
				                                                                                        Size     = PAPER_SIZES.A4,
				                                                                                        WidthCm  = 21.0m,
				                                                                                        HeightCm = 29.7m
			                                                                                        }
		                                                                        },
		                                                                        {
			                                                                        PAPER_SIZES.LETTER, new PaperSize
			                                                                                            {
				                                                                                            Size     = PAPER_SIZES.LETTER,
				                                                                                            WidthCm  = 21.59m,
				                                                                                            HeightCm = 27.94m
			                                                                                            }
		                                                                        }
	                                                                        };

	public ORIENTATION DefaultOrientation = ORIENTATION.PORTRAIT;


	public PAPER_SIZES DefaultPaperSize = PAPER_SIZES.A4;
}