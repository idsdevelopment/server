﻿#if NET7_0_OR_GREATER
using Environment = Hosting.Environment;

#else
using System.Web.Hosting;

#endif

namespace ReportsModule;

public partial class Reports
{
	public virtual bool NothingFound { get; protected set; } = false;

	public virtual string ToCsv() => "";

	public string Print()
	{
		var Template = TemplateName.Replace( '/', '\\' );

	#if NET7_0_OR_GREATER
		if( Template.StartsWith( "\\" ) )
			Template = Template[ 1.. ];

		var Path = Environment.MapPath( $@"\Reports\{Template}" );
	#else
		if( Template.StartsWith( "\\" ) )
			Template = Template.Substring( 1 );
		var Path = HostingEnvironment.MapPath( $@"\bin\Reports\{Template}" ) ?? "";
	#endif

		if( ( Path != "" ) && Data.Source is { } Source )
		{
			// Total pages is too large by 1.
			// Incremented in footer call, potential cause for
			// total page > last page number displayed? 
			Data.RecordCount = Source.Count;
			return Pulsar.Fetch( Path, true ).Replace( TOTAL_PAGE_TOKEN, ( Data.PageNumber - 1 ).ToString() );
		}
		return "";
	}
}