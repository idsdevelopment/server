﻿namespace ReportsModule;

public partial class Reports
{
	private string DoNext( Pulsar pulsar, Pulsar.FunctionArgs args )
	{
		string Eof;

		var Ndx = ++Data.CurrentIndex;

		if( Data.Source is { } Source && Ndx < Data.RecordCount )
		{
			Data.CurrentRecord = Source[ Ndx ];
			pulsar.Assign( Data.DataSourceName, Data.CurrentRecord );
			Eof = "0";
		}
		else
		{
			Data.LastRecord = true;
			Eof             = "1";
		}

		pulsar.Assign( EOF, Eof );
		return "";
	}
}