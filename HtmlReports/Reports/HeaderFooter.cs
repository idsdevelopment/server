﻿namespace ReportsModule;

public partial class Reports
{
	private const string HEADER = """
										<header style="height:~HEIGHT~;">
											~CONTENT~ 
										</header> 
										""";

	private const string FOOTER = """
										<footer style="height:~HEIGHT~;">
											~CONTENT~
										</footer>
										""";


	private string DoHeaderFooter( string? content, Pulsar.FunctionArgs args, bool isHeader )
	{
		if( content is not null )
		{
			var    Height = GetHeightName( args );
			string Text;

			if( isHeader )
			{
				Data.HeaderSizeCm = Height;
				Text              = HEADER;
			}
			else
			{
				Data.FooterSizeCm = Height;
				Text              = FOOTER;
			}

			Data.PaperHeightLessHeaderAndFooterCm -= Height;
			Data.RemainingHeightCm                =  Data.PaperHeightLessHeaderAndFooterCm;

			return Text.ReplaceHeight( Height )
			           .ReplaceContent( content );
		}
		return "";
	}

	private string DoHeader( Pulsar pulsar, string? content, Pulsar.FunctionArgs args )
	{
		if( content is not null )
			Data.Header = DoHeaderFooter( content, args, true );
		else
			Data.HeaderBreakVariable = PrintOnChangeGetVariable( args );
		return "";
	}

	private string DoFooter( Pulsar pulsar, string? content, Pulsar.FunctionArgs args )
	{
		if( content is not null )
		{
			Data.Footer = DoHeaderFooter( content, args, false );
			pulsar.Assign( PAGE_NUMBER, Data.PageNumber );
		}
		else
			Data.FooterBreakVariable = PrintOnChangeGetVariable( args );
		return "";
	}
}