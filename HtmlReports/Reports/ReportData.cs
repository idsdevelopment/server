﻿namespace ReportsModule;

public partial class Reports
{
	public sealed class ReportData
	{
		public bool NeedHeader => PaperHeightLessHeaderAndFooterCm == RemainingHeightCm;

		public object? CurrentRecord,
		               HeaderBreakObject,
		               FooterBreakObject;

		public string DataSourceName      = "",
		              Header              = "",
		              Footer              = "",
		              HeaderBreakVariable = "",
		              FooterBreakVariable = "";

		public string GlobalsName = "";

		public bool LastRecord,
		            PrintedHeaderOrBand;


		public ORIENTATION Orientation;

		public PaperSize PageSize = new();


		// ReSharper disable once MemberHidesStaticFromOuterClass
		public PAPER_SIZES PaperSize;

		public decimal
			// Real size of paper
			PaperSizeCm,

			// Less Size of Footer & Header
			PaperHeightLessHeaderAndFooterCm,
			RemainingHeightCm,
			HeaderSizeCm,
			FooterSizeCm;

		public int
			RecordCount,
			CurrentIndex,
			PageNumber;

		public DataSource? Source;

		public bool NeedFooter( decimal bandHeight, bool printHeader )
		{
			var Height = RemainingHeightCm;

			if( !printHeader )
				Height += HeaderSizeCm;

			return ( Height < bandHeight ) || LastRecord || ( Height <= 0 );
		}
	}
}