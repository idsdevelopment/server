﻿namespace ReportsModule;

public partial class Reports
{
	protected readonly ReportData Data;

	private string DoReport( Pulsar pulsar, string? content, Pulsar.FunctionArgs args )
	{
		if( content is null && Data.Source is { } Source )
		{
			var Eof = true;

			foreach( var Arg in args )
			{
				switch( Arg.Key.ToUpper() )
				{
				case "DATASOURCE":
					var DataSourceName = Arg.Value.Trim();
					Data.DataSourceName = DataSourceName;

					if( Source.Count > 0 )
					{
						var Record = Source[ 0 ];
						Data.CurrentRecord = Record;
						pulsar.Assign( DataSourceName, Record );
						Eof = false;
					}
					break;

				case "GLOBALS":
					var GlobalsName = Arg.Value.Trim();
					Data.GlobalsName = GlobalsName;
					pulsar.Assign( GlobalsName, Source.Globals );
					break;

				case "ORIENTATION":
					Data.Orientation = Arg.Value.Trim().ToLower() == "landscape" ? ORIENTATION.LANDSCAPE : ORIENTATION.PORTRAIT;
					break;
				}
			}

			Data.LastRecord = Eof;
			pulsar.Assign( EOF, Eof ? "1" : "0" );

			var PaperSize = PaperSizes[ Data.PaperSize ];

			string Size;

			switch( PaperSize )
			{
			case {Size: PAPER_SIZES.A4}:
				Size = "A4";
				break;

			case {Size: PAPER_SIZES.LETTER}:
				Size = "letter";
				break;

			default:
				return "Unknown paper size";
			}

			if( Data.Orientation == ORIENTATION.LANDSCAPE )
				( PaperSize.HeightCm, PaperSize.WidthCm ) = ( PaperSize.WidthCm, PaperSize.HeightCm );

			--PaperSize.HeightCm;
			Data.PageSize = PaperSize; // Prints landscape better

            Data.PaperHeightLessHeaderAndFooterCm = Data.RemainingHeightCm = Data.PaperSizeCm = PaperSize.HeightCm - 2 * PAGE_MARGIN_CM;

			var PageMarginCm = $"{PAGE_MARGIN_CM:N3}cm";
			var WidthCm  = $"{PaperSize.WidthCm:N3}cm";
			var HeightCm  = $"{PaperSize.HeightCm:N3}cm";

            return $$"""
						<style>
							@page{
									size:{{Size}} {{(Data.Orientation==ORIENTATION.LANDSCAPE ? "landscape" : "portrait")}}; 
									margin:0; 
									padding:0; 
								 }
							body,html{
										padding:0;
										margin:0;
										color:black;
										background-color:white;
									  }
							html,body,header,footer{
														page-break-before:avoid;
														page-break-after:avoid;
													}
							header > * {
											margin:0;
											width:100%; 
										}
							footer > * { 
											margin:0;
											width:100%;
										}
							article[data-page]{
								page-break-after:always;
								page-break-before:avoid;
								page-break-inside:avoid;
								margin:0;
								padding:{{PageMarginCm}};
								width:{{WidthCm}};
								height:{{HeightCm}};
								display:flex;
								flex-direction:column;
							}
							section[data-band]{
								width:{{WidthCm}};
								max-width:{{WidthCm}};
								overflow:hidden;
							}
							footer{
									position:relative;
									width:100%;
									overflow:hidden;
									flex-grow: 1;
									display: flex;
									flex-direction: column-reverse;
								}
							aside[data-pageBreak]{
											width:100%;
											flex-grow:1;
								}
						</style> 
						""" ;
		}
		return content + ( Data.PrintedHeaderOrBand ? GetFooter( pulsar ) : "" );
	}
}