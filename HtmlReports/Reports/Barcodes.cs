﻿using System.Drawing;
using TrwBarcode.Qr;
using Utils;

namespace ReportsModule;

public partial class Reports
{
	private static string DoBarcode( Pulsar pulsar, Pulsar.FunctionArgs args )
	{
		const double DPI_300_IN_PIXELS = 118.11;

		var Barcode = new TBarcode.TrwBarcode
					  {
						  ExtendTextArea = true,
						  BarcodeType    = TBarcode.TrwBarcode.BARCODE_TYPE.CODE128_B,
						  Height         = (int)Math.Ceiling( 2 * DPI_300_IN_PIXELS ), // Default 2 x 4 cm
						  Width          = (int)Math.Ceiling( 4 * DPI_300_IN_PIXELS ),
						  Font           = new Font( "Courier New", 14, FontStyle.Bold ),
						  ThickBarWidth  = 12,
						  ThinBarWidth   = 6,

						  QrProperties =
						  {
							  ErrorCorrection = QrProperties.ERROR_CORRECTION.HIGH,
							  MaskPattern     = QrProperties.MASK_PATTERN.AUTO
						  }
					  };

		var Data = "";

		foreach( var Arg in args )
		{
			var Value = Arg.Value.Trim();

			Color ToColor()
			{
				try
				{
					return Color.FromName( Value );
				}
				catch
				{
				}
				return Color.Red;
			}

			int ToInt()
			{
				return !int.TryParse( Value, out var Val ) ? 0 : Val;
			}

			double ToDouble()
			{
				return !double.TryParse( Value, out var Val ) ? 0 : Val;
			}

			int CmToPixels()
			{
				return (int)Math.Ceiling( ToDouble() * DPI_300_IN_PIXELS );
			}

			switch( Arg.Key.TrimToUpper() )
			{
			case "TYPE":
				Barcode.BarcodeType = Value.ToUpper() switch
									  {
										  "CODE128B"  => TBarcode.TrwBarcode.BARCODE_TYPE.CODE128_B,
										  "CODE39"    => TBarcode.TrwBarcode.BARCODE_TYPE.CODE39,
										  "CODE39EXT" => TBarcode.TrwBarcode.BARCODE_TYPE.CODE39_EXT,
										  "EAN13"     => TBarcode.TrwBarcode.BARCODE_TYPE.EAN13,
										  "EAN8"      => TBarcode.TrwBarcode.BARCODE_TYPE.EAN8,
										  "INT2OF5"   => TBarcode.TrwBarcode.BARCODE_TYPE.IINT_2of5,
										  "UPCA"      => TBarcode.TrwBarcode.BARCODE_TYPE.UPCA,
										  "UPCE"      => TBarcode.TrwBarcode.BARCODE_TYPE.UPCE_0,
										  "QR"        => TBarcode.TrwBarcode.BARCODE_TYPE.QR,
										  _           => TBarcode.TrwBarcode.BARCODE_TYPE.CODE128_B
									  };
				break;

			case "BARCOLOUR":
			case "BARCOLOR":
				Barcode.BarColor = ToColor();
				break;

			case "BACKGROUNDCOLOUR":
			case "BACKGROUNDCOLOR":
				Barcode.BackgroundColor = ToColor();
				break;

			case "TEXTCOLOUR":
			case "TEXTCOLOR":
				Barcode.TextColor = ToColor();
				break;

			case "CODE39CHARACTERGAP":
				Barcode.Code39CharacterGap = ToInt();
				break;

			case "HEIGHT":
				Barcode.Height = CmToPixels();
				break;

			case "WIDTH":
				Barcode.Width = CmToPixels();
				break;

			case "QRERRORCORRECTION":
				Barcode.QrProperties.ErrorCorrection = Value.ToUpper() switch
													   {
														   "HIGH"     => QrProperties.ERROR_CORRECTION.HIGH,
														   "MEDIUM"   => QrProperties.ERROR_CORRECTION.MEDIUM,
														   "LOW"      => QrProperties.ERROR_CORRECTION.LOW,
														   "QUARTILE" => QrProperties.ERROR_CORRECTION.QUARTILE,
														   _          => QrProperties.ERROR_CORRECTION.AUTO
													   };
				break;

			case "BARCODE":
			case "VALUE":
				Data = Value;
				break;

			case "THICKBARWIDTH":
				var Thickness = ToInt();
				Barcode.ThickBarWidth = Thickness;
				Barcode.ThinBarWidth  = Thickness / 2;
				break;
			}
		}

		if( Barcode.BarcodeType == TBarcode.TrwBarcode.BARCODE_TYPE.QR )
			Barcode.QrProperties.Data = ( QrProperties.ENCODING.AUTO, Data );
		else
			Barcode.Barcode = Data;

		var Image = Barcode.AsBase64String( TBarcode.TrwBarcode.IMAGE_FORMAT.PNG );

		double PixelsToCm( int pixels )
		{
			return Maths.Round2( pixels / DPI_300_IN_PIXELS );
		}

		var MaxHeight = PixelsToCm( Barcode.Height );
		var MaxWidth  = PixelsToCm( Barcode.Width );

		return $"""<img style="max-width:{MaxWidth}cm;max-height:{MaxHeight}cm;height:auto;width:auto;" src="data:image/png;base64,{Image}" alt="Barcode: {Data}" />""";
	}
}