﻿namespace DataSourceModule;

public class DataSource : List<object>
{
	public object Globals { get; protected set; } = null!;
}