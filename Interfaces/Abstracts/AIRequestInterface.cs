﻿#nullable enable

namespace Interfaces.Abstracts;

// ReSharper disable once InconsistentNaming
public abstract class AIRequestInterface : IRequestContext
{
	public static NewContextDelegate? NewContext;

	[JsonIgnore]
	public int UserId { get; set; }

	[JsonIgnore]
	public string UserName { get; set; } = "";

	[JsonIgnore]
	public string Password { get; set; } = "";

	[JsonIgnore]
	public string CarrierId { get; set; } = "";

	[JsonIgnore]
	public sbyte TimeZoneOffset { get; set; }

	[JsonIgnore]
	public bool Debug { get; set; }

	[JsonIgnore]
	public bool IsIds { get; set; }

	[JsonIgnore]
	public bool IsNewCarrier { get; set; }

	[JsonIgnore]
	public abstract string IpAddress { get; set; }

	[JsonIgnore]
	public ConcurrentDictionary<string, dynamic> Variables { get; protected set; } = new();

	[JsonIgnore]
	public abstract (string Account, string Key) BlobStorageAccount { get; }

	[JsonIgnore]
	public string UserStorageId => ( _UserStorageId!.IsNotNullOrEmpty() ? _UserStorageId : _UserStorageId = ToStorageId( UserName, UserId ) )!;

	[JsonIgnore]
	public abstract string StorageAccount { get; }

	[JsonIgnore]
	public abstract string StorageAccountKey { get; }

	[JsonIgnore]
	public abstract string StorageTableEndpoint { get; }

	[JsonIgnore]
	public abstract string StorageFileEndpoint { get; }

	[JsonIgnore]
	public abstract string StorageBlobEndpoint { get; }

	[JsonIgnore]
	public abstract string ServiceBusPrimaryConnectionString { get; }

	[JsonIgnore]
	public abstract string ServiceBusSecondaryConnectionString { get; }

	[JsonIgnore]
	public abstract string SendGridApiKey { get; }

	[JsonIgnore]
	public abstract bool SendToIds1 { get; }

	[JsonIgnore]
	public abstract bool SendAccountsToIds1 { get; }

	[JsonIgnore]
	public abstract bool IsSpecialLogin { get; }

	[JsonIgnore]
	public abstract DateTime LastMessagingCleanup { get; set; }

	[JsonIgnore]
	public abstract long NextInvoiceNumber { get; set; }

	[JsonIgnore]
	public abstract DateTime LastAddressCleanup { get; set; }

	protected static Func<string, string, IRequestContext>? NewRequestContextImplementation;

	protected static string ToStorageId( string userName, int id ) => $"{userName.ToAlphaNumeric()}{id:D}";

	private string? _UserStorageId;

	public static IRequestContext? NewRequestContext( string carrierId, string userName = "" ) => NewRequestContextImplementation?.Invoke( carrierId, userName );

	public abstract IRequestContext Clone( string carrierId = "" );

	public abstract void SystemLogException( string        message );
	public abstract void SystemLogException( Exception     ex,          string    caller = "", string                    fileName = "", int                     lineNumber = 0 );
	public abstract void SystemLogExceptionProgram( string programName, Exception ex,          [CallerMemberName] string caller   = "", [CallerFilePath] string fileName   = "", [CallerLineNumber] int lineNumber = 0 );

	public abstract void SystemLogException( string message, Exception ex );

	public abstract void LogDifferences( string storageArea, string program, string description, object before, object after );
	public abstract void LogDifferences( string storageArea, string program, string description, object before );

	public abstract void LogDeleted( string storageArea, string program, string description, object value1 );
	public abstract void LogNew( string     storageArea, string program, string description, object value1 );

	public abstract string GetUserStorageId( string userName );

	public string GetStorageId( LOG storageId )
	{
		return storageId switch
			   {
				   LOG.LOGIN => "Login",
				   LOG.STAFF => "Staff",
				   LOG.TRIP  => "Trip",
				   _         => throw new ArgumentException( "Invalid storageId" )
			   };
	}

	public abstract void LogProgramStarted( string program, string description = "" );
	public abstract void LogProgramEnded( string   program, string description = "" );

#region Emails
	public abstract void SendEmail( IRequestContext context, string from, string to, string subject, string body );
#endregion

	public delegate IRequestContext NewContextDelegate( string carrierId, string userName );

#region Preferences
#region System
	public abstract bool DisableReusedTripIds { get; }
#endregion

	public abstract ICarrierServicePreferences CarrierServicePreferences { get; }
#endregion

#region PayPal
	[JsonIgnore]
	public abstract string PayPalClientId { get; }

	[JsonIgnore]
	public abstract string PayPalClientSecret { get; }
#endregion

#region Meggasing
	public abstract void       Broadcast( TripUpdateMessage              msg );
	public abstract bool       Broadcast( IEnumerable<TripUpdateMessage> msg );
	public abstract TripUpdate Broadcast( TripUpdate                     t );
#endregion
}