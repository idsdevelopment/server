﻿#nullable enable

namespace Interfaces.Interfaces;

public interface ICarrierServicePreferences
{
	public bool Import { get; }
	public bool Export { get; }

	string AuthToken { get; }
	string CarrierId { get; }

	public bool UseJbTest { get; }
	public bool IsPml     { get; }
}

public interface IPreferences
{
	ICarrierServicePreferences CarrierServicePreferences { get; }
}