﻿#nullable enable

namespace Interfaces.Interfaces;

public interface IEmails
{
	public void SendEmail( IRequestContext context, string from, string to, string subject, string body );
}