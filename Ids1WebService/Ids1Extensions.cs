﻿namespace Ids1WebService;

internal static class Ids1Extensions
{
	private static readonly string[] Ids1CarrierXlate =
	[
		"app demo",
		"drummed",
		"rumour",
		"aurumvan",
		"aurumvic",
		"Australia",
		"bell",
		"Bowden",
		"buffalo",
		"buffalonew",
		"carrierId",
		"cerum",
		"citycore",
		"ConLog",
		"ConLogDemo",
		"connect",
		"Coral",
		"DAN",
		"degama",
		"demo",
		"dispatch",
		"fandg",
		"fandgtraining",
		"firstcall",
		"FOSS",
		"frontier",
		"gisborne",
		"IDS",
		"IDSACCOUNTS",
		"intexp",
		"kaaos",
		"kiandrait",
		"linfox",
		"lite",
		"metro",
		"mill",
		"nickels",
		"onetime",
		"parcel",
		"pent",
		"phoenix",
		"phoenixbackup",
		"phoenixtest",
		"phoenixtraining",
		"PML",
		"PMLtest",
		"priority",
		"raestransport",
		"rapid",
		"rapidusa",
		"right",
		"simons",
		"speedia",
		"straight",
		"TDR",
		"timax",
		"trisort",
		"tristate",
		"tristateaz",
		"unice",
		"LoopBackTest"
	];


	public static Trip ToTrip( this remoteTripDetailed r )
	{
		var (DAdr1, DAdr2)     = SplitAddress( r.deliveryStreet );
		var (PAdr1, PAdr2)     = SplitAddress( r.pickupStreet );
		var (Status1, Status2) = r.status.FromIds1Status();
		var (PuCo, PuLoc)      = SplitCompany( r.pickupCompany );
		var (DelCo, DelLoc)    = SplitCompany( r.deliveryCompany );
		var SLevel = r.serviceLevel.Trim();

		string  Pop, Pod;
		STATUS2 Status3;

		if( Status1 <= STATUS.PICKED_UP )
		{
			Pop     = r.podName;
			Pod     = "";
			Status3 = STATUS2.DONT_UPDATE_POD;
		}
		else
		{
			Pod     = r.podName;
			Pop     = "";
			Status3 = STATUS2.DONT_UPDATE_POP;
		}

		var DeliveryTime = r.deliveredTime.AsPacificStandardTime();
		var LastUpdated  = r.lastUpdated.UnixTicksToDateTime();

		var Temp = new Trip
		           {
			           Program      = "",
			           TripId       = r.tripId.NullTrim(),
			           LastModified = LastUpdated,

			           AccountId = r.accountId.NullTrim(),

			           CallTime            = r.callTime.AsPacificStandardTime(),
			           IsCallTimeSpecified = r.callTimeSpecified,

			           Board = r.board.ToString(),

			           DeliveryTime          = DeliveryTime,
			           VerifiedTime          = DeliveryTime,
			           DeliveryTimeSpecified = r.deliveredTimeSpecified,

			           DueTime          = r.deadlineTime.AsPacificStandardTime(),
			           DueTimeSpecified = r.deadlineTimeSpecified,

			           PickupTime          = r.pickupTime.AsPacificStandardTime(),
			           PickupTimeSpecified = r.pickupTimeSpecified,

			           ReadyTime          = r.readyTime.AsPacificStandardTime(),
			           ReadyTimeSpecified = r.readyTimeSpecified,

			           Driver         = r.driver.NullTrim(),
			           CallerEmail    = r.callerEmail.NullTrim(),
			           CallerName     = r.caller.NullTrim(),
			           CallerPhone    = r.callerPhone.Max50(),
			           CurrentZone    = r.currentZone.IsNotNullOrEmpty() ? r.currentZone : r.pickupZone,
			           DangerousGoods = r.dangerousGoods,

			           BillingNotes        = r.billingNotes.NullTrim(),
			           BillingAddressPhone = "",
			           BillingContact      = "",

			           PickupNotes   = r.pickupNotes.NullTrim(),
			           DeliveryNotes = r.deliveryNotes.NullTrim(),

			           DeliveryCompanyName         = DelCo.NullTrim(),
			           DeliveryAddressBarcode      = DelLoc.NullTrim(),
			           DeliveryAccountId           = r.deliveryAccountId,
			           DeliveryAddressAddressLine1 = DAdr1.NullTrim(),
			           DeliveryAddressAddressLine2 = DAdr2.NullTrim(),
			           DeliveryAddressCity         = r.deliveryCity.NullTrim(),
			           DeliveryAddressCountry      = r.deliveryCountry.NullTrim(),
			           DeliveryAddressEmailAddress = r.deliveryEmail.NullTrim(),
			           DeliveryAddressPhone        = r.deliveryPhone.Max20(),
			           DeliveryAddressPostalCode   = r.deliveryPc.NullTrim(),
			           DeliveryAddressRegion       = r.deliveryProvince.NullTrim(),
			           DeliveryAddressSuite        = r.deliverySuite.NullTrim(),
			           DeliveryContact             = r.deliveryContact.NullTrim(),
			           DeliveryZone                = r.deliveryZone.NullTrim(),

			           PickupCompanyName         = PuCo.NullTrim(),
			           PickupAddressBarcode      = PuLoc.NullTrim(),
			           PickupAccountId           = r.pickupAccountId.NullTrim(),
			           PickupAddressAddressLine1 = PAdr1.NullTrim(),
			           PickupAddressAddressLine2 = PAdr2.NullTrim(),
			           PickupAddressCity         = r.pickupCity.NullTrim(),
			           PickupAddressCountry      = r.pickupCountry.NullTrim(),
			           PickupAddressEmailAddress = r.pickupEmail.NullTrim(),
			           PickupAddressPhone        = r.pickupPhone.Max20(),
			           PickupAddressPostalCode   = r.pickupPc.NullTrim(),
			           PickupAddressRegion       = r.pickupProvince.NullTrim(),
			           PickupAddressSuite        = r.pickupSuite.NullTrim(),
			           PickupContact             = r.pickupContact.NullTrim(),
			           PickupZone                = r.pickupZone.NullTrim(),

			           Status1 = Status1,
			           Status2 = Status2,
			           Status3 = Status3,

			           Reference    = r.clientReference.NullTrim(),
			           ServiceLevel = SLevel,
			           PackageType  = r.packageType.NullTrim(),
			           Pieces       = r.pieces,
			           Weight       = (decimal)r.weight,

			           Packages = [],

			           PickupAddressNotes   = "",
			           DeliveryAddressNotes = "",
			           BillingAddressNotes  = "",

			           POP = Pop,
			           POD = Pod,

			           ReceivedByDevice = r.carCharge,
			           ReadByDriver     = r.redirect,

			           CallTakerId = r.callTakerId,
			           Ok          = true
		           };

		return Temp;
	}

	public static string ToIds1CarrierId( this string carrierId )
	{
		var CarrierId = carrierId.Trim();

		return ( from C in Ids1CarrierXlate
		         where C.Compare( CarrierId, StringComparison.OrdinalIgnoreCase ) == 0
		         select C ).FirstOrDefault() ?? CarrierId;
	}


	public static updateTripStatusOnly ToUpdateTripStatusOnly( this TripStatusTransferRecord s ) => new()
	                                                                                                {
		                                                                                                accountId = s.AccountId,
		                                                                                                status    = (int)s.Status,
		                                                                                                tripId    = s.TripId
	                                                                                                };


	// Don't use if it has Signatures
	public static remoteTripDetailed ToRemoteTripDetailed( this TripTransferRecord t )
	{
		var Now = DateTimeOffset.Now.LocalTimeToServerTime();

		var DeliveredTime = t.VerifiedTime.ServerTime();

		var PickupTime          = t.PickupTime.ServerTime();
		var PickupTimeSpecified = t.PickupTime.Specified();

		var ResellerId = t.ResellerId.ToIds1CarrierId();

		if( !int.TryParse( t.Board.Trim(), out var Board ) )
			Board = 0;

		return new remoteTripDetailed
		       {
			       resellerId = ResellerId,
			       accountId  = t.AccountId,

			       status      = (int)t.Status1.ToIds1Status( t.Status2 ),
			       tripId      = t.TripId,
			       lastUpdated = DateTimeExtensions.UnixTicksMilliSeconds(),

			       pieces = (int)t.Pieces,
			       weight = (float)t.Weight,
			       height = 0,
			       length = 0,
			       width  = 0,

			       driver = t.Driver,
			       POD    = true,

			       billingNotes = t.BillingNotes,
			       board        = Board,

			       callTakerId       = t.CalltakerId,
			       callTime          = t.CallTime.ServerTime(),
			       callTimeSpecified = t.CallTime.Specified(),
			       caller            = t.CallerName,
			       callerEmail       = t.CallerEmail,
			       callerPhone       = t.CallerPhone,

			       carChargeAmount       = 0,
			       clientReference       = t.Reference,
			       currentZone           = t.CurrentZone,
			       dangerousGoods        = t.DangerousGoods,
			       declaredValueAmount   = 0,
			       deadlineTime          = t.DueTime.ServerTime(),
			       deadlineTimeSpecified = t.DueTime.Specified(),

			       deliveryResellerId = ResellerId,
			       deliveryAccountId  = t.AccountId,
			       deliveryAddressId  = t.DeliveryAddressId,
			       deliveryCompany    = t.DeliveryCompanyName,
			       deliveryStreet     = t.DeliveryAddressAddressLine1,
			       deliverySuite      = t.DeliveryAddressSuite,
			       deliveryCity       = t.DeliveryAddressCity,
			       deliveryProvince   = t.DeliveryAddressRegion,
			       deliveryPc         = t.DeliveryAddressPostalCode,
			       deliveryCountry    = t.DeliveryAddressCountry,

			       deliveredTime          = DeliveredTime,
			       deliveredTimeSpecified = true,

			       deliveryArriveTime          = DeliveredTime,
			       deliveryArriveTimeSpecified = true,

			       deliveryAmount      = 0,
			       deliveryContact     = t.DeliveryContact,
			       deliveryDescription = "",
			       deliveryEmail       = t.DeliveryAddressEmailAddress,
			       deliveryNotes       = t.DeliveryNotes,
			       deliveryPhone       = t.DeliveryAddressPhone,
			       deliveryZone        = t.DeliveryZone,

			       disbursementAmount                = 0,
			       discountAmount                    = 0,
			       disputed                          = false,
			       dontFinalizeDate                  = Epoch,
			       dontFinalizeDateSpecified         = false,
			       dontFinalizeFlag                  = false,
			       dontFinalizeFlagSpecified         = false,
			       driverAssignTime                  = Epoch,
			       driverAssignTimeSpecified         = false,
			       driverPaidCommissionDate          = Epoch,
			       driverPaidCommissionDateSpecified = false,
			       insuranceAmount                   = 0,
			       internalId                        = null,
			       invoiceId                         = 0,
			       invoiced                          = false,
			       isCashTrip                        = false,
			       isCashTripReconciled              = false,
			       isDriverPaid                      = false,
			       mailDrop                          = false,
			       miscAmount                        = 0,
			       modified                          = true,
			       modifiedTripDate                  = Now,
			       modifiedTripDateSpecified         = true,
			       modifiedTripFlag                  = true,
			       modifiedTripFlagSpecified         = true,
			       noGoodsAmount                     = 0,
			       packageType                       = t.PackageType,
			       pallets                           = "",

			       pickupResellerId = ResellerId,
			       pickupAccountId  = t.PickupAccountId,
			       pickupAddressId  = t.PickupAddressId,
			       pickupCompany    = t.PickupCompanyName,
			       pickupSuite      = t.PickupAddressSuite,
			       pickupStreet     = t.PickupAddressAddressLine1,
			       pickupCity       = t.PickupAddressCity,
			       pickupProvince   = t.PickupAddressRegion,
			       pickupPc         = t.PickupAddressPostalCode,
			       pickupCountry    = t.PickupAddressCountry,

			       pickupTime                = PickupTime,
			       pickupArriveTime          = PickupTime,
			       pickupTimeSpecified       = PickupTimeSpecified,
			       pickupArriveTimeSpecified = PickupTimeSpecified,

			       pickupContact                    = t.PickupContact,
			       pickupDescription                = "",
			       pickupEmail                      = t.PickupAddressEmailAddress,
			       pickupNotes                      = t.PickupNotes,
			       pickupPhone                      = t.PickupAddressPhone,
			       pickupZone                       = t.PickupZone,
			       podName                          = t.Status1 <= STATUS.PICKED_UP ? t.POP : t.POD,
			       priorityInvoiceId                = 0,
			       priorityInvoiceIdSpecified       = false,
			       priorityStatus                   = 0,
			       priorityStatusSpecified          = false,
			       processedByIdsRouteDate          = Epoch,
			       processedByIdsRouteDateSpecified = false,
			       readyTime                        = t.ReadyTime.ServerTime(),
			       readyTimeSpecified               = t.ReadyTime.Specified(),
			       readyTimeString                  = "",
			       readyToInvoiceFlag               = false,
			       readyToInvoiceFlagSpecified      = false,
			       receivedByIdsRouteDate           = Epoch,
			       receivedByIdsRouteDateSpecified  = false,
			       redirectAmount                   = 0,
			       returnTrip                       = false,
			       serviceLevel                     = t.ServiceLevel,
			       sigFilename                      = "",
			       skipUpdateCommonAddress          = true,
			       sortOrder                        = "",
			       stopGroupNumber                  = "",
			       totalAmount                      = 0,
			       totalFixedAmount                 = 0,
			       totalPayrollAmount               = 0,
			       totalTaxAmount                   = 0,
			       waitTimeAmount                   = 0,
			       waitTimeMins                     = 0,
			       weightAmount                     = 0,
			       weightOrig                       = 0,

			       carCharge = t.ReceivedByDevice,
			       redirect  = t.ReadByDriver
		       };
	}

	public static remoteTrip ToRemoteTrip( this TripTransferRecord t )
	{
		var Now = DateTimeOffset.Now.LocalTimeToServerTime();

		var (SigPoints, SigHeight, SigWidth) = GetSignature( t.Signatures );

		if( !int.TryParse( t.Board.Trim(), out var Board ) )
			Board = 0;

		return new remoteTrip
		       {
			       sigFilename = SigPoints,
			       sigHeight   = SigHeight,
			       sigWidth    = SigWidth,

			       resellerId = t.ResellerId.ToIds1CarrierId(),
			       accountId  = t.AccountId,

			       status      = (int)t.Status1.ToIds1Status( t.Status2 ),
			       tripId      = t.TripId,
			       lastUpdated = DateTimeExtensions.UnixTicksMilliSeconds(),

			       pieces = (int)t.Pieces,
			       weight = (float)t.Weight,
			       height = 0,
			       length = 0,
			       width  = 0,

			       driver       = t.Driver,
			       POD          = true,
			       billingNotes = t.BillingNotes,
			       board        = Board,

			       callTakerId       = t.CalltakerId,
			       callTime          = t.CallTime.ServerTime(),
			       callTimeSpecified = t.CallTime.Specified(),
			       caller            = t.CallerName,
			       callerEmail       = t.CallerEmail,
			       callerPhone       = t.CallerPhone,

			       carChargeAmount       = 0,
			       clientReference       = t.Reference,
			       currentZone           = t.CurrentZone,
			       dangerousGoods        = t.DangerousGoods,
			       declaredValueAmount   = 0,
			       deadlineTime          = t.DueTime.ServerTime(),
			       deadlineTimeSpecified = t.DueTime.Specified(),

			       deliveryAddressId = t.DeliveryAddressId,
			       deliveryCompany   = t.DeliveryCompanyName,

			       deliveredTime               = t.DeliveryTime.ServerTime(),
			       deliveredTimeSpecified      = t.DeliveryTime.Specified(),
			       deliveryAmount              = 0,
			       deliveryArriveTime          = Epoch,
			       deliveryArriveTimeSpecified = false,
			       deliveryContact             = t.DeliveryContact,
			       deliveryEmail               = t.DeliveryAddressEmailAddress,
			       deliveryNotes               = t.DeliveryNotes,
			       deliveryPhone               = t.DeliveryAddressPhone,
			       deliveryZone                = t.DeliveryZone,

			       disbursementAmount                = 0,
			       discountAmount                    = 0,
			       disputed                          = false,
			       dontFinalizeDate                  = Epoch,
			       dontFinalizeDateSpecified         = false,
			       dontFinalizeFlag                  = false,
			       dontFinalizeFlagSpecified         = false,
			       driverAssignTime                  = Epoch,
			       driverAssignTimeSpecified         = false,
			       driverPaidCommissionDate          = Epoch,
			       driverPaidCommissionDateSpecified = false,
			       insuranceAmount                   = 0,
			       internalId                        = null,
			       invoiceId                         = 0,
			       invoiced                          = false,
			       isCashTrip                        = false,
			       isCashTripReconciled              = false,
			       isDriverPaid                      = false,
			       mailDrop                          = false,
			       miscAmount                        = 0,
			       modified                          = true,
			       modifiedTripDate                  = Now,
			       modifiedTripDateSpecified         = true,
			       modifiedTripFlag                  = true,
			       modifiedTripFlagSpecified         = true,
			       noGoodsAmount                     = 0,
			       packageType                       = t.PackageType,
			       pallets                           = "",

			       pickupAddressId = t.PickupAddressId,
			       pickupCompany   = t.PickupCompanyName,

			       pickupArriveTime                 = Epoch,
			       pickupArriveTimeSpecified        = false,
			       pickupContact                    = t.PickupContact,
			       pickupEmail                      = t.PickupAddressEmailAddress,
			       pickupNotes                      = t.PickupNotes,
			       pickupPhone                      = t.PickupAddressPhone,
			       pickupTime                       = t.PickupTime.ServerTime(),
			       pickupTimeSpecified              = t.PickupTime.Specified(),
			       pickupZone                       = t.PickupZone,
			       podName                          = t.Status1 <= STATUS.PICKED_UP ? t.POP : t.POD,
			       priorityInvoiceId                = 0,
			       priorityInvoiceIdSpecified       = false,
			       priorityStatus                   = 0,
			       priorityStatusSpecified          = false,
			       processedByIdsRouteDate          = Epoch,
			       processedByIdsRouteDateSpecified = false,
			       readyTime                        = t.ReadyTime.ServerTime(),
			       readyTimeSpecified               = t.ReadyTime.Specified(),
			       readyTimeString                  = "",
			       readyToInvoiceFlag               = false,
			       readyToInvoiceFlagSpecified      = false,
			       receivedByIdsRouteDate           = Epoch,
			       receivedByIdsRouteDateSpecified  = false,
			       redirectAmount                   = 0,
			       returnTrip                       = false,
			       serviceLevel                     = t.ServiceLevel,
			       skipUpdateCommonAddress          = true,
			       sortOrder                        = "",
			       stopGroupNumber                  = "",
			       totalAmount                      = 0,
			       totalFixedAmount                 = 0,
			       totalPayrollAmount               = 0,
			       totalTaxAmount                   = 0,
			       waitTimeAmount                   = 0,
			       waitTimeMins                     = 0,
			       weightAmount                     = 0,
			       weightOrig                       = 0,
			       billingCompany                   = t.BillingCompanyName,

			       carCharge = t.ReceivedByDevice,
			       redirect  = t.ReadByDriver
		       };
	}


	public static updateTripDetailedQuick ToUpdateTripDetailedQuick( this TripTransferRecord t ) => new()
	                                                                                                {
		                                                                                                _tripVal               = t.ToRemoteTripDetailed(),
		                                                                                                broadcastUpdateMessage = true
	                                                                                                };

	public static updateTripDetailedQuickWithAddressNotes ToUpdateTripDetailedQuickWithAddressNotes( this TripTransferRecord t )
	{
		var RTrip = t.ToRemoteTripDetailed();
		var (SigPoints, SigHeight, SigWidth) = GetSignature( t.Signatures );

		RTrip.sigFilename = SigPoints;

		return new updateTripDetailedQuickWithAddressNotes
		       {
			       _tripVal             = RTrip,
			       billingAddressNotes  = t.BillingAddressNotes.MaxLength( 255 ),
			       pickupAddressNotes   = t.PickupAddressNotes.MaxLength( 255 ),
			       deliveryAddressNotes = t.DeliveryAddressNotes.MaxLength( 255 ),

			       sigHeight = SigHeight,
			       sigWidth  = SigWidth,

			       broadcastUpdateMessage = true
		       };
	}


	public static updateTripQuickBroadcast ToUpdateTripQuickBroadcast( this TripTransferRecord t ) => new()
	                                                                                                  {
		                                                                                                  _tripVal         = t.ToRemoteTrip(),
		                                                                                                  updateDriverZone = false
	                                                                                                  };

	private static (string Adr1, string Adr2) SplitAddress( string adr )
	{
		var Lines = adr.Replace( '\r', '\n' ).Replace( "\n\n", "\n" ).Split( new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries );

		return Lines.Length switch
		       {
			       0 => ( "", "" ),
			       1 => ( Lines[ 0 ], "" ),
			       _ => ( Lines[ 0 ], Lines[ 1 ] )
		       };
	}

	private static (string CoName, string Location) SplitCompany( string coName )
	{
		coName = coName.Trim();

		var Result = ( CoName: coName, Location: "" );

		var P2 = coName.Length;

		if( ( P2-- > 3 ) && ( coName[ P2 ] == ')' ) ) //  (?)
		{
			var P = coName.LastIndexOf( '(' );

			if( ( P >= 0 ) && ( P2 > P ) )
			{
				Result.Location = coName.SubStr( P + 1, P2 - P - 1 );
				//	Result.CoName   = coName.SubStr( 0, P );
			}
		}
		return Result;
	}

	private static (string SigPoints, int SigHeight, int SigWidth) GetSignature( IList<Signature> sigs )
	{
		string SigPoints;
		int    SigHeight, SigWidth;

		if( sigs.Count > 0 )
		{
			var Sig = sigs[ 0 ];
			SigPoints = Sig.Points;
			SigHeight = Sig.Height;
			SigWidth  = Sig.Width;
		}
		else
		{
			SigPoints = "";
			SigHeight = SigWidth = 0;
		}

		return ( SigPoints, SigHeight, SigWidth );
	}

#region Date / Time
	internal static readonly DateTime Epoch = DateTimeExtensions.Epoch;
	internal static bool Specified( this DateTimeOffset? time ) => time >= Epoch;
	internal static DateTime ServerTime( this DateTimeOffset? time ) => time?.AsPacificStandardTime().DateTime ?? DateTimeExtensions.Epoch;
	internal static DateTime ServerTime( this DateTimeOffset time ) => time.AsPacificStandardTime().DateTime;
#endregion
}