﻿namespace Ids1WebService;

// ReSharper disable once InconsistentNaming
public partial class IDSClient
{
	public partial class Ids1
	{
		public static void PmlAddDriver( IRequestContext context, string driverCode, string program )
		{
			RunVoid( context, program, sendContext =>
			                           {
				                           Ids1Service.Client( sendContext ).PmlAddDriver( sendContext, driverCode );
			                           } );
		}

		public static void AddSatchel( IRequestContext context, TripTransferRecord trip, string program )
		{
			RunVoid( context, program, sendContext =>
			                           {
				                           Ids1Service.Client( sendContext ).AddSatchel( sendContext, trip, program );
			                           } );
		}

		public static void UpliftComplete( IRequestContext context, TripTransferRecord trip, string program )
		{
			RunVoid( context, program, sendContext =>
			                           {
				                           Ids1Service.Client( sendContext ).UpliftComplete( sendContext, trip, program );
			                           } );
		}

		public static void Futile( IRequestContext context, string program, string tripId, DateTimeOffset? dateTime, string pickupNote )
		{
			RunVoid( context, program, sendContext =>
			                           {
				                           Ids1Service.Client( sendContext ).Futile( sendContext, tripId, dateTime, pickupNote );
			                           } );
		}
	}

	internal class Pml
	{
		internal const string COMPANY_NAME = "Melbourne Warehouse( MelWare)",
		                      SUITE        = "",
		                      STREET       = "1 Innovation Court",
		                      CITY         = "Derrimut",
		                      REGION       = "Victoria",
		                      POSTAL_CODE  = "3030",
		                      COUNTRY      = "Australia";

		internal class Test : IReseller
		{
			internal const string RESELLER_ID = "PMLtest";
			public         string ResellerId => RESELLER_ID;
			public         string AccountId  => "PMLAust";
			public         string AddressId  => "0be33ab064c7f6ff0164c80d35db0007";
		}

		internal class Oz : IReseller
		{
			internal const string RESELLER_ID = "PML";
			public         string ResellerId => RESELLER_ID;
			public         string AccountId  => "PMLOz";
			public         string AddressId  => "0be33ab06950c5f9016f48a9c4d27b5b";
		}


		public interface IReseller
		{
			string ResellerId { get; }
			string AccountId  { get; }
			string AddressId  { get; }
		}


		internal static bool IsPmlTest( IRequestContext ctx ) => string.Compare( ctx.CarrierId, Test.RESELLER_ID, StringComparison.InvariantCultureIgnoreCase ) == 0;

		internal static IReseller Reseller( IRequestContext ctx ) => IsPmlTest( ctx ) ? new Test() : new Oz();
	}

	public void UpliftComplete( IRequestContext ctx, TripTransferRecord t, string program )
	{
		try
		{
			var Update = ToUpdateTripDetailedQuick( ctx, t );
			var Trip   = Update._tripVal;
			Trip.serviceLevel = "upliftcomplete";
			Trip.pallets      = t.BillingNotes;
			Trip.billingNotes = t.BillingNotes;
			AddTrip( ctx, Trip );
		}
		catch( Exception Exception )
		{
			ctx.SystemLogExceptionProgram( program, Exception );
		}
	}


	public void AddSatchel( IRequestContext ctx, TripTransferRecord t, string program )
	{
		try
		{
			var Update = ToUpdateTripDetailedQuick( ctx, t );
			var Trip   = Update._tripVal;
			AddTrip( ctx, Trip );
		}
		catch( Exception Exception )
		{
			ctx.SystemLogExceptionProgram( program, Exception );
		}
	}

	public void PmlAddDriver( IRequestContext context, string driverCode )
	{
		driverCode = driverCode.Trim().ToUpper();

		var ReSeller = Pml.Reseller( context );

		var Packet = new pmlAddDriver
		             {
			             authToken = AuthToken,
			             accountId = ReSeller.AccountId,
			             driverId  = driverCode
		             };

		LogAndRetryError( context, nameof( PmlAddDriver ), Packet, () =>
		                                                           {
			                                                           pmlAddDriver( Packet );
		                                                           } );
	}

	public void Futile( IRequestContext context, string tripId, DateTimeOffset? dateTime, string pickupNote )
	{
		UpdateTripStatusAndTime( context, tripId, STATUS.DELETED, dateTime, pickupNote, "", "" );
	}


	private static updateTripDetailedQuick ToUpdateTripDetailedQuick( IRequestContext ctx, TripTransferRecord t )
	{
		var Reseller = Pml.Reseller( ctx );

		return ( t with
		         {
			         ResellerId = Reseller.ResellerId,

			         DeliveryAccountId = Reseller.AccountId,
			         PickupAccountId = Reseller.AccountId,

			         DeliveryAddressId = Reseller.AddressId,
			         PickupAddressId = Reseller.AddressId
		         } ).ToUpdateTripDetailedQuick();
	}
}