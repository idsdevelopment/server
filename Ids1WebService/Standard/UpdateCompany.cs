﻿#nullable enable

namespace Ids1WebService;

public static class Extensions
{
	public static remoteUserValue ToRemoteUserValue( this PrimaryCompanyTransferRecord c )
	{
		var C = c.Company;
		var A = C.Address;

		var Now = DateTime.UtcNow.AsPacificStandardTime().DateTime;

		return new remoteUserValue
		       {
			       accountId                                 = c.CompanyCode,
			       addr                                      = A.AddressLine1,
			       addr1                                     = A.AddressLine2,
			       assignedRouteId                           = "",
			       cell                                      = A.Mobile,
			       city                                      = A.City,
			       country                                   = A.Country,
			       createDate                                = Now,
			       createDateSpecified                       = true,
			       currentRole                               = "",
			       currentZone                               = "",
			       currentZoneIDS2                           = "",
			       dayPattern                                = "",
			       deviceId                                  = "",
			       drlat                                     = "",
			       drlong                                    = "",
			       email                                     = A.EmailAddress,
			       emergencyContactName                      = "",
			       emergencyContactNotes                     = "",
			       emergencyContactPhone                     = "",
			       ext                                       = "",
			       fax                                       = A.Fax,
			       firstName                                 = c.UserName,
			       internalId                                = null,
			       isEnabled                                 = true,
			       lastGpsUpdate                             = 0,
			       lastKnownAddress                          = "",
			       lastName                                  = c.UserName,
			       lastTripId                                = "",
			       lastUpdated                               = Now,
			       lastUpdatedSpecified                      = true,
			       locale                                    = "",
			       pager                                     = "",
			       pass                                      = c.Password,
			       payRate                                   = 0,
			       pcode                                     = A.PostalCode,
			       personalInformationNotes                  = "",
			       personalInformationStartDate              = Now,
			       personalInformationStartDateSpecified     = true,
			       personalInformationTerminateDate          = DateTimeExtensions.Epoch,
			       personalInformationTerminateDateSpecified = false,
			       phone                                     = A.Phone,
			       prov                                      = A.Region,
			       resellerId                                = c.CarrierId,
			       roles                                     = Array.Empty<remoteUserRole>(),
			       routeValue                                = 0,
			       routeValueSpecified                       = false,
			       spottedAddressDescription                 = "",
			       spottedDateAssignedSpecified              = false,
			       spottedDateAssigned                       = DateTimeExtensions.Epoch,
			       spottedDaysEstimate                       = 0,
			       trailerCapacity                           = 0,
			       trailerDestinationZone                    = "",
			       trailerLoad                               = 0,
			       trailerLocationZone                       = "",
			       trailerNotes                              = "",
			       trailerPriority                           = 0,
			       trailerStatus                             = 0,
			       trailerTitle                              = "",
			       trailerType                               = 0,
			       userId                                    = c.UserName,
			       weighting                                 = 0
		       };
	}

	public static addUser ToAddUser( this PrimaryCompanyTransferRecord c ) => new()
	                                                                          {
		                                                                          userVal = c.ToRemoteUserValue()
	                                                                          };

	public static addAccount ToAddAccount( this PrimaryCompanyTransferRecord c )
	{
		var A  = c.Company.Address;
		var Bc = c.BillingCompany;
		var B  = Bc.Address;

		var Now   = DateTimeOffset.UtcNow.AsPacificStandardTime();
		var Ticks = Now.Ticks;

		return new addAccount
		       {
			       authToken = "",
			       remoteAccountValue = new remoteAccountValue
			                            {
				                            resellerId = c.CarrierId,

				                            isEnabled = true,

				                            companyFax   = A.Fax,
				                            companyName  = c.Company.CompanyName,
				                            companyPhone = A.Phone,

				                            addressDescription = "",
				                            addressId          = null,
				                            addressLastUpdated = Ticks,
				                            accountId          = c.CompanyCode,
				                            addressSuite       = A.Suite,
				                            addressStreet      = A.AddressLine1,
				                            addressCity        = A.City,
				                            addressProvince    = A.Region,
				                            addressCountry     = A.Country,
				                            addressPc          = A.PostalCode,
				                            adminUserId        = c.UserName,
				                            addressNotes       = A.Notes,

				                            bn                        = "",
				                            billingAddressDescription = "",
				                            billingAddressId          = null,
				                            billingAddressLastUpdated = Ticks,
				                            billingAddressNotes       = B.Notes,
				                            billingAddressSuite       = B.Suite,
				                            billingAddressStreet      = B.AddressLine1,
				                            billingAddressCity        = B.City,
				                            billingAddressProvince    = B.Region,
				                            billingAddressCountry     = B.Country,
				                            billingAddressPc          = B.PostalCode,

				                            invoiceMethod = Bc.EmailInvoice ? 0 : 1,
				                            invoiceType   = Bc.BillingPeriod,

				                            drlat  = "",
				                            drlong = "",

				                            created          = Now.Date,
				                            createdSpecified = true,

				                            discountBeforeTaxes             = false,
				                            discountType                    = 0,
				                            discountValue                   = 0,
				                            displayPrefChargeTotals         = false,
				                            displayPrefInvoiceAddressDetail = false,
				                            displayPrefPodDefaultOn         = false,
				                            displayPrefShowPieces           = false,
				                            displayPrefSortOnClientRef      = false,
				                            displayPrefTrailerScreen        = false,
				                            displayPrefWaybillTotal         = false,
				                            internalId                      = null,
				                            invoiceCreditLimit              = 0,

				                            isArchived                                  = false,
				                            isAutoDispatch                              = false,
				                            isFieldServicesAccount                      = false,
				                            isLiteAccount                               = false,
				                            isNonGpsAccount                             = false,
				                            isPeekAccount                               = false,
				                            isReseller                                  = false,
				                            isTrial                                     = false,
				                            isTripEntryReferenceFieldMandatory          = false,
				                            isTripEntryReferenceFieldMandatorySpecified = false,
				                            isWaitMeasuredAtPickup                      = false,
				                            lastInvoiceBalance                          = 0,
				                            lastInvoiceDate                             = DateTimeExtensions.Epoch,
				                            lastInvoiceDateSpecified                    = false,
				                            lastInvoiceId                               = 0,
				                            lastPaymentDate                             = DateTimeExtensions.Epoch,
				                            lastPaymentDateSpecified                    = false,
				                            lastUpdated                                 = Ticks,
				                            notes                                       = A.Notes,
				                            notificationType                            = 0,
				                            outstanding                                 = 0,
				                            paymentType                                 = 0,
				                            tz                                          = "",
				                            zoom                                        = ""
			                            }
		       };
	}
}

// ReSharper disable once InconsistentNaming
public partial class IDSClient
{
	public partial class Ids1
	{
		public static void AddPrimaryCompany( IRequestContext context, string program, PrimaryCompanyTransferRecord primaryCompany )
		{
			RunVoid( context, program, sendContext =>
			                           {
				                           var Client = Ids1Service.Client( sendContext );
				                           Client.UpdatePrimaryCompanyUser( primaryCompany.ToAddUser() );
				                           Client.UpdatePrimaryCompany( primaryCompany.ToAddAccount() );
			                           } );
		}
	}

	public void UpdatePrimaryCompanyUser( addUser user )
	{
		user.authToken = AuthToken;
		addUser( user );
	}

	public void UpdatePrimaryCompany( addAccount account )
	{
		account.authToken = AuthToken;
		addAccount( account );
	}
}