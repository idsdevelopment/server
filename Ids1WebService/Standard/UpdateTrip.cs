﻿namespace Ids1WebService;

// ReSharper disable once InconsistentNaming
public partial class IDSClient
{
	public partial class Ids1
	{
		public static void AddTrip( IRequestContext context, string program, remoteTripDetailed trip )
		{
			RunVoid( context, program, sendContext =>
			                           {
				                           Ids1Service.Client( sendContext ).AddTrip( sendContext, trip );
			                           } );
		}

		public static void UpdateTrip( IRequestContext context, string program, updateTripDetailedQuickWithAddressNotes trip, updateTripQuickBroadcast? sigTrip )
		{
			RunVoid( context, program, sendContext =>
			                           {
				                           Ids1Service.Client( sendContext ).UpdateTrip( sendContext, trip, sigTrip );
			                           } );
		}


		public static void UpdateTrip( IRequestContext context, bool isNew, TripTransferRecord trip )
		{
			var Program = trip.Program;

			if( isNew )
				AddTrip( context, Program, trip.ToRemoteTripDetailed() );
			else
			{
				UpdateTrip( context, Program,
				            trip.ToUpdateTripDetailedQuickWithAddressNotes(),
				            ( trip.Signatures.Count > 0 ) && ( trip.Signatures[ 0 ].Points != "" ) ? trip.ToUpdateTripQuickBroadcast() : null );
			}
		}

		public static void UpdateTripStatus( IRequestContext context, TripStatusTransferRecord status )
		{
			RunVoid( context, status.Program, sendContext =>
			                                  {
				                                  Ids1Service.Client( sendContext ).UpdateTripStatus( sendContext, status.ToUpdateTripStatusOnly() );
			                                  } );
		}
	}

	public void UpdateTripStatus( IRequestContext context, updateTripStatusOnly status )
	{
		status.authToken = AuthToken;

		LogAndRetryError( context, nameof( UpdateTripStatus ), status, () =>
		                                                               {
			                                                               updateTripStatusOnly( status );
		                                                               },
		                  status.tripId );
	}

	public void AddTrip( IRequestContext context, remoteTripDetailed trip )
	{
		var Trip = new addTrip
		           {
			           authToken            = AuthToken,
			           selectedChargeValues = [],
			           selectedCharges      = [],
			           trip                 = trip
		           };

		LogAndRetryError( context, nameof( AddTrip ), Trip, () =>
		                                                    {
			                                                    addTrip( Trip );
		                                                    },
		                  trip.tripId );
	}

	public void UpdateTrip( IRequestContext context, updateTripDetailedQuickWithAddressNotes t, updateTripQuickBroadcast? sigTrip )
	{
		t.authToken = AuthToken;

		LogAndRetryError( context, nameof( UpdateTrip ), t, () =>
		                                                    {
			                                                    updateTripDetailedQuickWithAddressNotes( t );
		                                                    },
		                  t._tripVal.tripId );
	}
}