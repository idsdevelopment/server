﻿// ReSharper disable InconsistentNaming

#nullable enable

namespace Ids1WebService;

public partial class IDSClient
{
	public partial class Ids1
	{
		public static string GetTripReference( IRequestContext context, string tripId, bool stripTest = false )
		{
			try
			{
				return Ids1Service.Client( context, stripTest ).GetTripReference( context, tripId );
			}
			catch( Exception Exception )
			{
				context.SystemLogException( Exception );
			}
			return "";
		}

		public static remoteTrip? GetTrip( IRequestContext context, string tripId, bool stripTest = false )
		{
			try
			{
				return Ids1Service.Client( context, stripTest ).GetTrip( context, tripId );
			}
			catch( Exception Exception )
			{
				context.SystemLogException( Exception );
			}
			return null;
		}
	}


	public remoteTrip? GetTrip( IRequestContext context, string tripId )
	{
		return LogAndRetryError( context, nameof( GetTrip ), tripId, () =>
		                                                             {
			                                                             var Trip = findTrip( new findTrip
			                                                                                  {
				                                                                                  authToken = AuthToken,
				                                                                                  tripId    = tripId
			                                                                                  } );
			                                                             return Trip?.@return;
		                                                             }, tripId );
	}

	public string GetTripReference( IRequestContext context, string tripId ) => GetTrip( context, tripId )?.clientReference ?? "";
}