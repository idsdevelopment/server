﻿#nullable enable

// ReSharper disable MemberHidesStaticFromOuterClass

using System.Linq;
using System.Runtime.Caching;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using Ids1WebService.Logs;
using Timer = System.Timers.Timer;

// ReSharper disable ExplicitCallerInfoArgument

namespace Ids1WebService;

public class EndPoints
{
	public const string JBTEST2 = "http://jbtest2.internetdispatcher.org:8080/ids-beans/IDSWS",
	                    OZ      = "http://oz.internetdispatcher.org:8080/ids-beans/IDSWS",
	                    REPORTS = "http://reports.internetdispatcher.org:8080/ids-beans/IDSWS",
	                    CUSTOM  = "http://custom.internetdispatcher.org:8080/ids-beans/IDSWS";
}

// ReSharper disable once InconsistentNaming
public partial class IDSClient : DSWSClient
{
	public class Ids1Service
	{
		internal class CacheRecord
		{
			public IDSClient Client
			{
				get
				{
					lock( LockObject )
					{
						while( Locked )
							Thread.Sleep( 1 );

						Locked = true;
						Delay.Start();
						return _Client;
					}
				}
			}

			public string EndPoint
			{
				get
				{
					lock( LockObject )
						return _Endpoint;
				}
			}

			private static readonly object LockObject = new();

			private readonly string _Endpoint;
			private          bool   Locked;

			private readonly IDSClient _Client;

			private readonly Timer Delay = new( 10 )
			                               {
				                               AutoReset = false
			                               };

			~CacheRecord()
			{
				lock( LockObject )
					Delay.Dispose();
			}

			internal CacheRecord( IDSClient client, string endPoint )
			{
				_Client   = client;
				_Endpoint = endPoint;

				Delay.Elapsed += ( _, _ ) =>
				                 {
					                 Locked = false;
				                 };
			}
		}

		private static readonly MemoryCache ClientCache = MemoryCache.Default;

		public static IDSClient Client( IRequestContext context, bool stripTest = false )
		{
			var Prefs = context.Ids1ServicePreferences;
			var Cid   = context.CarrierId.Trim();

			if( stripTest )
				Cid = Cid.ReplaceLastOccurrence( "test", "", StringComparison.OrdinalIgnoreCase );

			//Ids circuit breaker login
			var AToken = $"{Cid}-{SOAP_LOGIN.UserName}-{SOAP_LOGIN.Password}";

			var Key          = CacheKey( AToken );
			var CachedClient = ClientCache.Get( Key );

			string EndPoint;

			if( Prefs.UseJbTest )
				EndPoint = EndPoints.JBTEST2;
			else if( Prefs.IsPml )
				EndPoint = EndPoints.OZ;
			else
				EndPoint = EndPoints.CUSTOM;

			if( CachedClient is CacheRecord Cr )
			{
				if( Cr.EndPoint == EndPoint ) // Probably Jbtest Switch Changed
					return Cr.Client;

				ClientCache.Remove( Key );
			}

			ClientCache.Add( Key, Cr = new CacheRecord( new IDSClient( AToken, EndPoint ), EndPoint ), new CacheItemPolicy
			                                                                                           {
				                                                                                           SlidingExpiration = new TimeSpan( 0, 10, 0 )
			                                                                                           } );
			return Cr.Client;
		}

		private static string CacheKey( string authToken ) => $"Login-{authToken}";
	}

	public partial class Ids1
	{
		public static void DeleteTrip( IRequestContext context, string tripId, string program )
		{
			RunVoid( context, program, () =>
			                           {
				                           Ids1Service.Client( context ).DeleteTrip( context, tripId );
			                           } );
		}

		public static void PickupTrip( IRequestContext context, string tripId, string driver, DateTimeOffset? pickupTime, string pop, decimal pieceCount )
		{
			Ids1Service.Client( context ).PickupTrip( context, tripId, driver, pickupTime, pop, pieceCount );
		}

		public static void PickupTrip( IRequestContext context, string tripId, string program, DateTimeOffset? dateTime )
		{
			RunVoid( context, program, () =>
			                           {
				                           Ids1Service.Client( context ).PickupTrip( context, tripId, dateTime );
			                           } );
		}

		public static void DeliverTrip( IRequestContext context, string tripId, string program, DateTimeOffset? dateTime )
		{
			RunVoid( context, program, () =>
			                           {
				                           Ids1Service.Client( context ).DeliverTrip( context, tripId, dateTime );
			                           } );
		}

		public static void VerifyTrip( IRequestContext context, string tripId, string program, DateTimeOffset? dateTime )
		{
			RunVoid( context, program, () =>
			                           {
				                           Ids1Service.Client( context ).VerifyTrip( context, tripId, dateTime );
			                           } );
		}


		private static void RunVoid( IRequestContext context, string program, Action action, [CallerMemberName] string caller = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNumber = 0 )
		{
			Tasks.RunVoid( () =>
			               {
				               try
				               {
					               action();
				               }
				               catch( Exception Exception )
				               {
					               context.SystemLogException( Exception, $"Context: {context.CarrierId} - {context.UserName}\r\n{program},\r\nCaller: {caller},\r\nFile Name: {fileName},\r\n{lineNumber}" );
				               }
			               } );
		}
	}

	internal static readonly (string UserName, string Password) SOAP_LOGIN = ( "AzureWebService", "8y7b25b798538793b98" );

	public void DeleteTrip( IRequestContext context, string tripId )
	{
		var Packet = new updateTripStatusOnly
		             {
			             authToken = AuthToken,
			             accountId = "",
			             tripId    = tripId,
			             status    = (int)IDS1_STATUS.DELETED
		             };

		LogAndRetryError( context, nameof( DeleteTrip ), Packet, () =>
		                                                         {
			                                                         updateTripStatusOnly( Packet );
		                                                         },
		                  tripId );
	}

	public void UpdateTripStatusAndTime( IRequestContext context, string tripId, STATUS status, STATUS1 status1, DateTimeOffset? dateTime, string pickupNote, string deliveryNote, string billingNote )
	{
		var Packet = new pmlUpdateTripStatusAndTime
		             {
			             authToken     = AuthToken,
			             accountId     = "",
			             tripId        = tripId,
			             status        = (int)status.ToIds1Status( status1 ),
			             date          = dateTime?.DateTime ?? DateTime.MinValue,
			             dateSpecified = dateTime is { },
			             pickupNote    = pickupNote,
			             deliveryNote  = deliveryNote,
			             billingNote   = billingNote
		             };

		LogAndRetryError( context, nameof( UpdateTripStatusAndTime ), Packet, () =>
		                                                                      {
			                                                                      pmlUpdateTripStatusAndTime( Packet );
		                                                                      },
		                  tripId );
	}

	public void UpdateTripStatusAndTime( IRequestContext context, string tripId, STATUS status, DateTimeOffset? dateTime, string pickupNote, string deliveryNote, string billingNote )
	{
		UpdateTripStatusAndTime( context, tripId, status, STATUS1.UNSET, dateTime, pickupNote, deliveryNote, billingNote );
	}


	public void PickupTrip( IRequestContext context, string tripId, string driver, DateTimeOffset? pickupTime, string pop, decimal pieceCount )
	{
		var Packet = new pickupTripWithPieces
		             {
			             authToken           = AuthToken,
			             tripId              = tripId,
			             driver              = driver,
			             pickupTime          = ServerTime( pickupTime ),
			             pickupTimeSpecified = true,
			             pop                 = pop,
			             pieces              = (int)pieceCount
		             };

		LogAndRetryError( context, nameof( PickupTrip ), Packet, () =>
		                                                         {
			                                                         pickupTripWithPieces( Packet );
		                                                         }, tripId );
	}

	public void PickupTrip( IRequestContext context, string tripId, DateTimeOffset? dateTime )
	{
		UpdateTripStatusAndTime( context, tripId, STATUS.PICKED_UP, dateTime, "", "", "" );
	}

	public void DeliverTrip( IRequestContext context, string tripId, DateTimeOffset? dateTime )
	{
		UpdateTripStatusAndTime( context, tripId, STATUS.DELIVERED, dateTime, "", "", "" );
	}

	public void VerifyTrip( IRequestContext context, string tripId, DateTimeOffset? dateTime )
	{
		UpdateTripStatusAndTime( context, tripId, STATUS.VERIFIED, dateTime, "", "", "" );
	}

	internal static DateTime ServerTime( DateTimeOffset? time ) => time?.AsPacificStandardTime().DateTime ?? DateTimeExtensions.Epoch;

	public IDSClient( string authToken, string endPoint ) : base( new BasicHttpBinding( BasicHttpSecurityMode.None ), new EndpointAddress( endPoint ) )
	{
		AuthToken = authToken;
	}

#region Error Retry
	public class MyLockClass
	{
	}

	// ReSharper disable once InconsistentNaming
	private static readonly string[] KEY_PHRASES =
	{
		"Java",
		"meta data"
	};

	protected static void LogAndRetryError( IRequestContext context, string functionName, object ids1Object, Action action, string tripId = "", [CallerMemberName] string caller = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNumber = 0 )
	{
		ToIds1Log.Log( context, functionName, ids1Object );
		RetryError( context, action, tripId, caller, fileName, lineNumber );
	}

	protected static T? LogAndRetryError<T>( IRequestContext context, string functionName, object ids1Object, Func<T> func, string tripId = "", [CallerMemberName] string caller = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNumber = 0 )
	{
		ToIds1Log.Log( context, functionName, ids1Object );
		var Temp = RetryError( context, func, tripId, caller, fileName, lineNumber );
		ToIds1Log.Log( context, $"{functionName} - Returned Value", Temp );
		return Temp;
	}

	private static T? InternalRetryError<T>( IRequestContext context, Func<T>? func = null, Action? action = null, string tripId = "", [CallerMemberName] string caller = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNumber = 0 )
	{
		T? Result = default;

		var RequestTime = DateTime.UtcNow;

		var LockObject = SemaphoreQueue.SemaphoreQueueByTypeAndId<MyLockClass>( context.CarrierId.TrimToLower() );

		try
		{
			while( true )
			{
				try
				{
					if( action is not null )
						action();
					else if( func is not null )
						Result = func();
					break;
				}
				catch( Exception Exception )
				{
					void LogException()
					{
						var E = tripId.IsNotNullOrWhiteSpace() ? new Exception( $"Trip Id: {tripId}", Exception ) : Exception;
						context.SystemLogException( E, caller, fileName, lineNumber );
					}

					var Message = Exception.Message;

					if( KEY_PHRASES.Any( word => Message.Contains( word, StringComparison.OrdinalIgnoreCase ) ) )
					{
						LogException();
						break;
					}

					var Now = DateTime.UtcNow;

					if( ( Now - RequestTime ).Hours >= 2 )
					{
						LogException();
						context.SystemLogException( new Exception( "Retry - Tried for 2 hours. Give up." ) );
						break;
					}
					Thread.Sleep( 60_000 );
				}
			}
		}
		finally
		{
			// Min 500ms between ids1 requests
			Tasks.RunVoid( 500, () =>
			                    {
				                    LockObject.Release();
			                    } );
		}
		return Result;
	}

	protected static void RetryError( IRequestContext context, Action action, string tripId = "", [CallerMemberName] string caller = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNumber = 0 )
	{
		InternalRetryError<object>( context, null, action, tripId, caller, fileName, lineNumber );
	}

	protected static T? RetryError<T>( IRequestContext context, Func<T>? func = null, string tripId = "", [CallerMemberName] string caller = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNumber = 0 ) => InternalRetryError( context, func, null, tripId, caller, fileName, lineNumber );
#endregion

#region AuthToken
	private string _AuthToken = "";


	public string AuthToken
	{
		get => _AuthToken;
		set
		{
			var Parts = value.Split( new[] {'-'}, StringSplitOptions.RemoveEmptyEntries );

			if( Parts.Length == 3 )
				value = $"{Parts[ 0 ].ToIds1CarrierId()}-{Parts[ 1 ].Trim()}-{Parts[ 2 ]}";

			_AuthToken = value;
		}
	}
#endregion
}