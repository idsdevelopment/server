﻿global using System.Net;
global using System.Net.Mail;
global using System.Net.Mime;
global using Database.Model.Databases.Carrier;
global using Interfaces.Interfaces;
global using System.Diagnostics;
global using System.Text;
global using Interfaces.Abstracts;
global using Utils;
global using Protocol.Data;

#if !NET7_0_OR_GREATER
global using System;
global using System.Collections.Generic;
global using System.IO;
global using System.Linq;
#endif