﻿namespace Emails;

public static class Extensions
{
	public static MailMessage AddHtml( this MailMessage message, string html )
	{
		message.Body       = html;
		message.IsBodyHtml = true;
		return message;
	}

	public static MailMessage AddPdf( this MailMessage message, string pdfFileName, byte[] pdf ) => AddAttachment( message, pdfFileName, ".pdf", pdf, MediaTypeNames.Application.Pdf );

	public static MailMessage AddText( this MailMessage message, string fileName, byte[] text ) => AddAttachment( message, fileName, ".txt", text, MediaTypeNames.Text.Plain );

	public static MailMessage AddText( this MailMessage message, string fileName, string text ) => AddText( message, fileName, Encoding.UTF8.GetBytes( text ) );


	public static MailMessage AddCsv( this MailMessage message, string csvFileName, byte[] csv ) => AddAttachment( message, csvFileName, ".csv", csv, "text/csv" );

	public static MailMessage AddCsv( this MailMessage message, string csvFileName, string csv ) => AddCsv( message, csvFileName, Encoding.UTF8.GetBytes( csv ) );

	public static bool IsValidEmailAddress( this string? emailAddress )
	{
		if( emailAddress is not null )
		{
			try
			{
				new MailAddress( emailAddress );
				return true;
			}
			catch( FormatException )
			{
			}
			catch( ArgumentException )
			{
			}
		}
		return false;
	}


	private static MailMessage AddAttachment( this MailMessage message, string fileName, string ext, byte[] bytes, string mediaType )
	{
		if( !fileName.EndsWith( ext, StringComparison.OrdinalIgnoreCase ) )
			fileName += ext;

		// Don't dispose the stream, the attachment will dispose it.
		var Stream     = new MemoryStream( bytes );
		var Attachment = new Attachment( Stream, fileName, mediaType );
		message.Attachments.Add( Attachment );
		return message;
	}
}

public class Client : SmtpClient
{
	private static bool InProcessingEmails;

	private readonly IRequestContext           Context;
	private readonly CarrierDb.EmailPreference Preferences;

	public Client( IRequestContext context ) : this( context, GetPreferences( context ) )
	{
	}

	private Client( IRequestContext context, CarrierDb.EmailPreference preferences ) : base( preferences.SmtpServer )
	{
		Preferences = preferences;
		Context     = context;
		Port        = preferences.SmtpPort;
		EnableSsl   = preferences.SmtpUseSsl;

		UseDefaultCredentials = false;
		Credentials           = new NetworkCredential( preferences.SmtpUserName, preferences.SmtpPassword );
	}

	public void SendCsv( string to, string subject, string csv, string fileName, string? plainText = null )
	{
		try
		{
			plainText ??= $"{fileName} Csv Attached";
			using var Message = BaseMessage( Context, to, subject, plainText );
			Message.Body = plainText;
			Message.AddCsv( fileName, csv );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public void SendPdf( string to, string subject, string fileName, byte[] pdf, string plainText = "" )
	{
		try
		{
			if( pdf.Length > 0 )
			{
				if( fileName.IsNullOrWhiteSpace() )
					fileName = "Attachment.pdf";

				if( plainText.IsNullOrWhiteSpace() )
					plainText = $"{fileName} Attached";

				using var Message = BaseMessage( Context, to, subject, plainText );
				Message.AddPdf( fileName, pdf );

				Database.Model.Databases.Users.Emails.Send( Context, Message );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public void SendHtmlAndPdf( string to, string subject, string html, string fileName, byte[] pdf, string plainText = "" )
	{
		try
		{
			using var Message = BaseMessage( Context, to, subject, plainText );
			Message.AddHtml( html );

			if( pdf.Length > 0 )
			{
				if( fileName.IsNullOrWhiteSpace() )
					fileName = "Attachment.pdf";

				Message.AddPdf( fileName, pdf );
			}

			Database.Model.Databases.Users.Emails.Send( Context, Message );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public static void Send( IRequestContext context, string to, string subject, string body )
	{
		try
		{
			using var Message = BaseMessage( context, to, subject, body );
			Database.Model.Databases.Users.Emails.Send( context, Message );
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
	}

	public void SendHtml( string to, string subject, string html, string plainText = "" )
	{
		try
		{
			using var Message = BaseMessage( Context, to, subject, plainText );
			Message.AddHtml( html );

			Database.Model.Databases.Users.Emails.Send( Context, Message );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public void SendHtmlAndPdf( IRequestContext context,
	                            string to, string subject, string fileName,
	                            string html, string plainText = "",
	                            Report.PAPER_SIZE paperSize = Report.PAPER_SIZE.A4,
	                            bool landscape = false )
	{
		SendHtmlAndPdf( to, subject, html, fileName, Pdf.Pdf.HtmlToPdf( context, html, paperSize, landscape ), plainText );
	}

	public static void ProcessEmails()
	{
		if( !InProcessingEmails )
		{
			InProcessingEmails = true;

			try
			{
				while( true )
				{
					var Email = Database.Model.Databases.Users.Emails.GetNexMessage();

					if( Email.Message is { } Msg )
					{
						var Context = AIRequestInterface.NewRequestContext( Email.CarrierId );

						if( Context is not null )
						{
							using var Client = new Client( Context );

							while( true )
							{
								try
								{
									if( Email.Expired && Client.Preferences.SmtpErrorAddress.IsValidEmailAddress() )
									{
										var ToAddress = Msg.To;
										var Addr      = ToAddress.Count > 0 ? ToAddress[ 0 ].Address : "Unknown";

										if( Msg.From is { } From )
										{
											var Msg1 = new MailMessage( From.Address, Client.Preferences.SmtpErrorAddress, $"Cannot Send Email To {Addr}: {Msg.Subject}", Msg.Body );

											foreach( var Attachment in Msg.Attachments )
												Msg1.Attachments.Add( Attachment );

											Msg.Dispose();
											Msg = Msg1;
										}
									}

									var EmailFrom = Msg.From is { } F
										                ? F.Address
										                : "Missing 'From' Address";

									var EmailTo = Msg.To is { Count: > 0 } T
										              ? string.Join( ';', T )
										              : "Missing 'To' Address";

									var EmailSubject = Msg.Subject.IsNotNullOrWhiteSpace()
										                   ? Msg.Subject
										                   : "Missing 'Subject'";

									void Log( bool failed )
									{
										Database.Model.Databases.Users.Emails.SentEmails.Log( Context, Email.MessageId, EmailFrom, EmailTo, EmailSubject, failed );
									}

									try
									{
										using( Msg )
											Client.Send( Msg );

										Log( false );
									}
									catch( Exception Exception )
									{
										Debug.WriteLine( Exception );

										Log( true );

										if( !Email.Expired )
											throw;
									}

									Database.Model.Databases.Users.Emails.DeleteEmail( Email.MessageId );
								}
								catch
								{
									if( Email.Expired )
										Database.Model.Databases.Users.Emails.DeleteEmail( Email.MessageId );
								}

								Email = Database.Model.Databases.Users.Emails.GetNexMessage( Email.CarrierId );

								if( Email.Message is null )
									break;
							}
						}
					}
					else
						break;
				}
			}
			finally
			{
				InProcessingEmails = false;
			}
		}
	}

	public void Send( Email email )
	{
		try
		{
			void AddAddresses( MailAddressCollection addresses, IEnumerable<string> addresses1 )
			{
				foreach( var Address in addresses1 )
				{
					foreach( var Address1 in ToMultipleAddresses( Address ) )
						addresses.Add( Address1 );
				}
			}

			using var Message = BaseMessage( email.To, email.From, email.Subject, email.Body );
			AddAddresses( Message.CC, email.Cc );
			AddAddresses( Message.Bcc, email.Bcc );

			foreach( var A in email.Attachments )
			{
				switch( A.Type )
				{
				case Email.Attachment.TYPE.PDF:
					Message.AddPdf( A.Name, A.Bytes );
					break;

				case Email.Attachment.TYPE.CSV:
					Message.AddCsv( A.Name, A.Bytes );
					break;

				default:
					Message.AddText( A.Name, A.Bytes );
					break;
				}
			}

			Database.Model.Databases.Users.Emails.Send( Context, Message );
		}
		catch( Exception Exception )

		{
			Context.SystemLogException( Exception );
		}
	}

	private static CarrierDb.EmailPreference GetPreferences( IRequestContext context )
	{
		using var Db = new CarrierDb( context );
		return Db.GetEmailPreferences();
	}

	private static IEnumerable<string> ToMultipleAddresses( string addresses )
	{
		return addresses.Split( new[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries )
		                .Select( a => a.Trim() )
		                .Where( a => a.IsValidEmailAddress() )
		                .ToArray();
	}

	private static MailMessage BaseMessage( string to, string from, string subject, string body )
	{
		var From = new MailAddress( from );

		var Message = new MailMessage
		              {
			              Subject = subject,
			              Body    = body,
			              From    = From
		              };

		foreach( var Address in ToMultipleAddresses( to ) )
			Message.To.Add( Address );

		return Message;
	}

	private static MailMessage BaseMessage( IRequestContext context, string to, string subject, string body )
	{
		var FromAddress = GetPreferences( context ).SmtpFromAddress;
		return BaseMessage( to, FromAddress, subject, body );
	}
}