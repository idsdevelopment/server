﻿using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows;
using Formulas;

namespace FormulaTester
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click( object sender, RoutedEventArgs e )
        {
            var Value = new Formula().Evaluate( Formula.Text,
                                                new { TestRate = 12.29, TestPieces = 4 },
                                                new Dictionary<string, string>
                                                {
                                                    { "RATE", "TestRate" },
                                                    { "PCS", "TestPieces" }
                                                } );
            Errors.Text = "";
            Result.Content = (string)Value.Value;

            if( Value.ErrorList.Count > 0 )
            {
                var Txt = new StringBuilder();

                foreach( var Error in Value.ErrorList )
                    Txt.Append( Error ).Append( "\r\n" );

                Errors.Text = Txt.ToString();
            }
        }
    }
}