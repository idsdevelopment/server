﻿#nullable enable

using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitFormula( FormulasParser.FormulaContext context ) => VisitResult( context.result() );
	}
}