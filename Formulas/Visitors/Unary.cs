﻿#nullable enable

using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitChangeSign( FormulasParser.ChangeSignContext context ) => -VisitUnaryMinus( context.unaryMinus() );
	}
}