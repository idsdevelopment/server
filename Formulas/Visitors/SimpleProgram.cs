﻿#nullable enable

using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitSimpleProgram( FormulasParser.SimpleProgramContext context )
		{
			var Let = context.let();

			if( Let is { } )
			{
				var Value = VisitLet( Let );

				if( HaveResult )
					return Value;
			}

			var If = context.@if();

			if( If is { } )
			{
				var Value = VisitIf( If );

				if( HaveResult )
					return Value;
			}

			var Sp = context.simpleProgram();

			if( Sp is { } )
			{
				// ReSharper disable once LoopCanBeConvertedToQuery
				foreach( var SimpleProgramContext in Sp )
				{
					var Value = VisitSimpleProgram( SimpleProgramContext );

					if( HaveResult )
						return Value;
				}
			}

			return 0;
		}
	}
}