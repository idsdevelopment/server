﻿#nullable enable

using Antlr4.Runtime;
using Formulas.Grammar;

namespace Formulas.Visitors;

internal partial class FormulaVisitors
{
	public override Result VisitVariable( FormulasParser.VariableContext context )
	{
		var Id = context.IDENT().GetText().Trim();

		if( string.Compare( Id, "PI", StringComparison.OrdinalIgnoreCase ) == 0 )
			return (decimal)Math.PI;

		if( PropertyMap.TryGetValue( Id, out var PropertyName ) )
		{
			var PropertyInfo = Record.GetType().GetProperty( PropertyName );

			if( PropertyInfo is not null )
			{
				var PropertyValue = PropertyInfo.GetValue( Record, null );

				return PropertyValue switch
				       {
					       bool BoolValue       => BoolValue ? 1 : 0,
					       byte ByteValue       => ByteValue,
					       char CharValue       => CharValue,
					       decimal DecimalValue => DecimalValue,
					       double DoubleValue   => (decimal)DoubleValue,
					       float FloatValue     => (decimal)FloatValue,
					       int IntValue         => IntValue,
					       long LongValue       => LongValue,
					       sbyte SbyteValue     => SbyteValue,
					       short ShortValue     => ShortValue,
					       uint UIntValue       => UIntValue,
					       ulong ULongValue     => ULongValue,
					       ushort UShortValue   => UShortValue,
					       string StringValue   => StringValue,
					       _                    => 0
				       };
			}
		}

		if( !Variables.TryGetValue( Id, out var Value ) )
			throw new RecognitionException( $"Undefined Variable: {Id}", null, null, context );

		return Value;
	}
}