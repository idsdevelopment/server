﻿#nullable enable

using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitLogicalAndOr( FormulasParser.LogicalAndOrContext context )
		{
			var AndOr = context.logicalAndOr();

			if( AndOr is { } )
			{
				if( VisitLogicalAndOr( AndOr ) != 0 )
					return 1;
			}

			return VisitLogicalAnd( context.logicalAnd() );
		}

		public override Result VisitLogicalAnd( FormulasParser.LogicalAndContext context )
		{
			var And = context.logicalAnd();

			if( And is { } )
			{
				if( VisitLogicalAnd( context.logicalAnd() ) == 0 )
					return 0;
			}

			return VisitConditionalExpression( context.conditionalExpression() );
		}
	}
}