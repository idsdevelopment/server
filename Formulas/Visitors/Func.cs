﻿#nullable enable

using System;
using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitRounding( FormulasParser.RoundingContext context )
		{
			var Value  = VisitLogicalAndOr( context.logicalAndOr() );
			var Places = 2;
			var Digit  = context.DIGIT();

			if( Digit is { } )
				int.TryParse( Digit.GetText().Trim(), out Places );

			return Math.Round( Value, Places, MidpointRounding.AwayFromZero );
		}

		public override Result VisitMinMax( FormulasParser.MinMaxContext context )
		{
			var Logical = context.logicalAndOr();
			var LVal    = VisitLogicalAndOr( Logical[ 0 ] );
			var RVal    = VisitLogicalAndOr( Logical[ 1 ] );

			return context.MAX() is { } ? Math.Max( LVal, RVal ) : Math.Min( LVal, RVal );
		}

		public override Result VisitContains( FormulasParser.ContainsContext context )
		{
			var Logical = context.logicalAndOr();
			var LVal    = VisitLogicalAndOr( Logical[ 0 ] );
			var RVal    = VisitLogicalAndOr( Logical[ 1 ] );

			return ( (string)LVal ).Contains( RVal ) ? 1 : 0;
		}


		public override Result VisitFunc( FormulasParser.FuncContext context )
		{
			var Rounding = context.rounding();

			if( Rounding is { } )
				return VisitRounding( Rounding );

			var MinMax = context.minMax();

			if( MinMax is { } )
				return VisitMinMax( MinMax );

			var Contains = context.contains();

			if( Contains is { } )
				return VisitContains( Contains );

			var Value = VisitLogicalAndOr( context.logicalAndOr() );

			var Func = context.funcname().GetText().Trim().ToLower();

			try
			{
				return Func switch
				       {
					       "sin"     => (decimal)Math.Sin( (double)Value ),
					       "asin"    => (decimal)Math.Asin( (double)Value ),
					       "sqrt"    => (decimal)Math.Sqrt( (double)Value ),
					       "acos"    => (decimal)Math.Acos( (double)Value ),
					       "cos"     => (decimal)Math.Cos( (double)Value ),
					       "tan"     => (decimal)Math.Tan( (double)Value ),
					       "atan"    => (decimal)Math.Atan( (double)Value ),
					       "log"     => (decimal)Math.Log( (double)Value ),
					       "log10"   => (decimal)Math.Log10( (double)Value ),
					       "floor"   => (decimal)Math.Floor( (double)Value ),
					       "ceiling" => (decimal)Math.Ceiling( (double)Value ),

					       // String support
					       "upper"     => ( (string)Value ).ToUpper(),
					       "toupper"   => ( (string)Value ).ToUpper(),
					       "lower"     => ( (string)Value ).ToLower(),
					       "tolower"   => ( (string)Value ).ToLower(),
					       "trim"      => ( (string)Value ).Trim(),
					       "trimright" => ( (string)Value ).TrimEnd(),
					       "trimend"   => ( (string)Value ).TrimEnd(),
					       "trimleft"  => ( (string)Value ).TrimStart(),
					       "trimstart" => ( (string)Value ).TrimStart(),
					       _           => 0
				       };
			}
			catch
			{
			}

			return 0;
		}
	}
}