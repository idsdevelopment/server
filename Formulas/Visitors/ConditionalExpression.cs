﻿#nullable enable

using System;
using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitConditionalExpression( FormulasParser.ConditionalExpressionContext context )
		{
			while( true )
			{
				var Expression = context.plusOrMinus();

				if( Expression is { } )
					return VisitPlusOrMinus( Expression );

				var Conditionals = context.conditionalExpression();

				if( Conditionals is { } )
				{
					var Value = Relop( VisitConditionalExpression( Conditionals[ 0 ] ),
					                   VisitConditionalExpression( Conditionals[ 1 ] ), context.relop() );

					// Ternary
					if( context.QMARK() is { } )
					{
						if( Conditionals.Length >= 4 )
						{
							context = Value != 0 ? Conditionals[ 2 ] : Conditionals[ 3 ];
							continue;
						}
						return 0;
					}
					return Value;
				}
				return 0;
			}
		}

		private static Result Relop( Result lval, Result rval, FormulasParser.RelopContext ctx )
		{
			var AsString = lval.IsString || rval.IsString;

			if( ctx.EQ() is { } )
				return AsString              ? lval == (string)rval ? 1 : 0 :
				       lval == (decimal)rval ? 1 : 0;

			if( ctx.NOT_EQ() is { } )
				return AsString              ? string.CompareOrdinal( lval, rval ) != 0 ? 1 : 0 :
				       lval != (decimal)rval ? 1 : 0;

			if( ctx.GT() is { } )
				return AsString    ? string.CompareOrdinal( lval, rval ) > 0 ? 1 : 0 :
				       lval > rval ? 1 : 0;

			if( ctx.GT_EQ() is { } )
				return AsString     ? string.CompareOrdinal( lval, rval ) >= 0 ? 1 : 0 :
				       lval >= rval ? 1 : 0;

			if( ctx.LT() is { } )
				return AsString    ? string.CompareOrdinal( lval, rval ) < 0 ? 1 : 0 :
				       lval < rval ? 1 : 0;

			if( ctx.LT_EQ() is { } )
				return AsString     ? string.CompareOrdinal( lval, rval ) <= 0 ? 1 : 0 :
				       lval <= rval ? 1 : 0;

			if( ctx.IGNORE_CASE_EQ() is { } )
				return string.Compare( ( (string)lval ).Trim(), ( (string)rval ).Trim(), StringComparison.OrdinalIgnoreCase ) == 0 ? 1 : 0;

			return 0;
		}
	}
}