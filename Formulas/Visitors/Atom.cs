﻿#nullable enable

using System;
using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitConstantPI( FormulasParser.ConstantPIContext context ) => (decimal)Math.PI;

		public override Result VisitConstantE( FormulasParser.ConstantEContext context ) => (decimal)Math.E;

		public override Result VisitToFunction( FormulasParser.ToFunctionContext context ) => VisitFunc( context.func() );

		public override Result VisitToBrackets( FormulasParser.ToBracketsContext context ) => VisitLogicalAndOr( context.logicalAndOr() );

		public override Result VisitToString( FormulasParser.ToStringContext context ) => context.STRING().GetText().Trim( '"' );
	}
}