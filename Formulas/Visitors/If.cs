﻿#nullable enable

using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitIfBody( FormulasParser.IfBodyContext context )
		{
			var Value = VisitLogicalAndOr( context.logicalAndOr() );

			if( Value != 0 )
			{
				foreach( var LetContext in context.let() )
					VisitLet( LetContext );
				return 0;
			}

			var Else = context.@else();

			return Else is { } ? VisitElse( Else ) : 0;
		}

		public override Result VisitElse( FormulasParser.ElseContext context )
		{
			var If = context.ifBody();

			if( If is { } )
				return VisitIfBody( If );

			var Let = context.let();

			if( Let is { } )
			{
				foreach( var LetContext in Let )
					VisitLet( LetContext );
			}
			return 0;
		}

		public override Result VisitIf( FormulasParser.IfContext context ) => VisitIfBody( context.ifBody() );
	}
}