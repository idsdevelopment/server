﻿#nullable enable

using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitPlusOrMinus( FormulasParser.PlusOrMinusContext context )
		{
			var Rval      = VisitMultOrDiv( context.multOrDiv() );
			var PlusMinus = context.plusOrMinus();

			if( PlusMinus is { } )
			{
				var LVal = VisitPlusOrMinus( PlusMinus );

				if( context.PLUS() is { } )
				{
					if( LVal.IsString || Rval.IsString )
						return LVal + (string)Rval;

					return LVal + (decimal)Rval;
				}

				return LVal - Rval;
			}

			return Rval;
		}
	}
}