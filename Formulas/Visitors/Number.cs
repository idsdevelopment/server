﻿#nullable enable

using System.Text;
using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitNumber( FormulasParser.NumberContext context )
		{
			var Digits = context.DIGIT();
			var Num    = new StringBuilder();

			if( Digits is { } )
			{
				foreach( var Digit in Digits )
					Num.Append( Digit.GetText().Trim() );
			}
			else
				Num.Append( '0' ); // Implied 0

			if( decimal.TryParse( Num.ToString(), out var Value ) )
			{
				var Point = context.point();

				if( Point is { } )
					Value += VisitPoint( Point );

				var Minus = context.MINUS();

				if( Minus is { } )
					Value = -Value;

				return Value;
			}

			return 0;
		}

		public override Result VisitPoint( FormulasParser.PointContext context )
		{
			var Digits = context.DIGIT();

			if( Digits is { } )
			{
				var Num = new StringBuilder( "0." );

				foreach( var Digit in Digits )
					Num.Append( Digit.GetText().Trim() );

				if( decimal.TryParse( Num.ToString(), out var Value ) )
					return Value;
			}

			return 0;
		}
	}
}