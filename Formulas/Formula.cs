﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using Antlr4.Runtime;
using Formulas.Grammar;
using Formulas.Visitors;

namespace Formulas
{
	public class Formula
	{
		private class ErrorListener : BaseErrorListener
		{
			private readonly FormulasParser Parser;

			public override void SyntaxError( IRecognizer recognizer, IToken offendingSymbol, int line, int charPositionInLine, string msg, RecognitionException e )
			{
				Parser.HasErrors = true;
				Parser.SyntaxErrors.Add( $"Line: {line},  Position: {charPositionInLine},  Symbol: '{offendingSymbol.Text.Trim()}'" );
			}

			public ErrorListener( FormulasParser parser )
			{
				Parser = parser;
			}
		}

		private FormulasParser Parser = null!;


		public Dictionary<string, string> GetPropertyMap( object record )
		{
			return record.GetType().GetProperties().ToDictionary( propertyInfo => propertyInfo.Name, propertyInfo => propertyInfo.Name );
		}

		public (List<string> ErrorList, Result Value) Evaluate( string formula, object record ) => Evaluate( formula, record, GetPropertyMap( record ) );

		private ( List<string> ErrorList, Result Value ) Evaluate( string formula, object record, Dictionary<string, string> propertyMap )
		{
			Result Value = 0;

			try
			{
				var InStream = new AntlrInputStream( formula.Trim() );
				Parser = new FormulasParser( new CommonTokenStream( new FormulasLexer( InStream ) ) );

				Parser.RemoveErrorListeners();
				Parser.AddErrorListener( new ErrorListener( Parser ) );

				var Tree = Parser.formula();

				if( !Parser.HasErrors )
				{
					try
					{
						Value = new FormulaVisitors( record, propertyMap ).Visit( Tree );
					}
					catch( Exception E )
					{
						Parser.SyntaxErrors.Add( E.Message );
					}
				}
			}
			catch
			{
				Parser.SyntaxErrors.Add( "Error parsing formula." );
			}

			return ( ErrorList: Parser.SyntaxErrors, Value );
		}
	}
}