 lexer grammar FormulasLexer;

//!!!! Be careful of lexer precidence order !!!!

ELSE: WS+ [Ee][Ll][Ss][Ee] WS+
	;

ENDIF: WS+ [Ee][Nn][Dd][Ii][Ff]
	;

RESULT: [Rr][Ee][Ss][Uu][Ll][Tt]
	;

PI: [Pp]'i'
	;

COS: [Cc]'os'
   ;

SIN: [Ss]'in'
   ;

TAN: [Tt]'an'
   ;

ACOS: [Aa]'cos'
   ;

ASIN: [Aa]'sin'
   ;

ATAN: [Aa]'tan'
   ;

LOG10: [Ll]'og10'
   ;

LOG: [Ll]'og'
   ;

SQRT: [Ss]'qrt'
	;

ROUND: [Rr]'ound'
	;

MIN: [Mm]'in'
	;

MAX: [Mm]'ax'
	;

FLOOR: [Ff]'loor'
	;

CEILING: [Cc]'eiling'
	;

TO_UPPER: ( [Tt]'o' )? [Uu]'pper'
	;

TO_LOWER: ( [Tt]'o' )? [Ll]'ower'
	;

TRIM_RIGHT: FTRIM [Rr]'ight'
	;

TRIM_LEFT: FTRIM [Ll]'eft'
	;

TRIM_END: FTRIM [Ee]'nd'
	;

TRIM_START: FTRIM [Ss]'tart'
	;

TRIM: FTRIM
	;

CONTAINS:	[Cc]'ontains'
	;

IDENT: FLETTER ( FLETTER | FDIGIT | '_' )*
	;

LET: [Ll][Ee][Tt] WS+
	;

IF: [Ii][Ff] WS+
	;

THEN: WS+ [Tt][Hh][Ee][Nn]
	;

ASSIGN: '='
	;

LPAREN: '('
   ;

RPAREN: ')'
   ;

PLUS: '+'
   ;

MINUS: '-'
   ;

MULT: '*'
   ;

DIV: '/'
   ;

MODULO: '%'
   ;

COMMA: ','
	;

AND: ( WS+ [Aa][Nn][Dd] WS+ ) | '&&'
	;

OR: ( WS+ [Oo][Rr] WS+ ) | '||'
	;

GT: '>'
   ;

GT_EQ: '>='
   ;

LT: '<'
   ;

LT_EQ: '<='
   ;
  
EQ: '=='
   ;

NOT_EQ: '!='
   ;

IGNORE_CASE_EQ: '?='
	;

POINT: '.'
   ;

E: 'E' WS+
   ;

POW: '^'
   ;

QMARK: '?'
	;

COLON: ':'
	;

DIGIT: FDIGIT
   ;

EXPONENT : [Ee] ('+'|'-')? FDIGIT+ 
	;

STRING :   '"' ( FESCAPED_QUOTE | ~([\u0000-\u001f]) )*? '"'				// Skip control characters
	;

WS: [ \r\n\t] + -> channel (HIDDEN)
   ;

fragment FTRIM: [Tt]'rim'
	;

fragment FESCAPED_QUOTE : '\\"'
	;

fragment FDIGIT: ('0' .. '9')
	;

fragment FLETTER: ( 'a' .. 'z' ) | ( 'A' .. 'Z' )
   ;
