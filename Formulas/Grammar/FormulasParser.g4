
parser grammar FormulasParser;

options
	{ 
		tokenVocab=FormulasLexer; 		// Use tokens from lexer
	}	
	 
formula: result EOF
   ;

result: simpleProgram
	| logicalAndOr
	;

simpleProgram: ( if | let ) simpleProgram*
	;

let: LET? ( IDENT | RESULT ) ASSIGN conditionalExpression
	;

if: ifBody ENDIF
	;

ifBody: IF logicalAndOr THEN let+ else?
	;

else: ELSE ( ifBody | let+ )
	;

logicalAndOr: logicalAndOr OR logicalAnd
	| logicalAnd
	;

logicalAnd: logicalAnd AND conditionalExpression
	| conditionalExpression
	;

conditionalExpression: conditionalExpression relop conditionalExpression
	| conditionalExpression relop conditionalExpression QMARK conditionalExpression COLON conditionalExpression
	| plusOrMinus
	;


plusOrMinus: plusOrMinus PLUS multOrDiv		
	| plusOrMinus MINUS multOrDiv			
	| multOrDiv								
   ;

multOrDiv: multOrDiv MULT pow				
	| multOrDiv DIV pow						
	| multOrDiv MODULO pow						
	| pow									
	;

pow: unaryMinus ( POW pow )?
   ;

unaryMinus: atom					#ToAtom
	|  MINUS unaryMinus				#ChangeSign
	;

atom: PI									#ConstantPI
	| E										#ConstantE
	| scientific							#ToScientific
	| variable								#ToVariable
	| LPAREN logicalAndOr RPAREN			#ToBrackets
	| func									#ToFunction
	| STRING								#ToString
   ;

scientific: ( number EXPONENT? ) MODULO?
   ;

func: funcname LPAREN logicalAndOr RPAREN
	| rounding
	| minMax
	| contains
   ;

rounding: ROUND LPAREN logicalAndOr ( COMMA DIGIT )? RPAREN
	;

minMax: ( MIN | MAX ) LPAREN logicalAndOr COMMA logicalAndOr RPAREN
	;

contains: CONTAINS LPAREN logicalAndOr COMMA logicalAndOr RPAREN
	;

funcname: COS
   | TAN
   | SIN
   | ACOS
   | ATAN
   | ASIN
   | LOG
   | LOG10
   | SQRT
   | FLOOR
   | CEILING
   | TRIM
   | TRIM_RIGHT
   | TRIM_LEFT
   | TO_UPPER
   | TO_LOWER
   ;

relop: EQ
	| NOT_EQ
	| GT
	| GT_EQ
	| LT
	| LT_EQ
	| IGNORE_CASE_EQ
   ;

number: MINUS? ( DIGIT+ )? point?
   ;

point: ( POINT DIGIT+ )
	;

variable: IDENT
   ;

