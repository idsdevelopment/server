﻿namespace MessagingStorage.Cleanups;

public static class Cleanup<T>
{
	public const string DATE_FORMAT = "yyyy/MM/dd HH:mm:ss";
#if DEBUG
	private const int INTERVAL_IN_MINUTES = 10;
#else
	    private const int INTERVAL_IN_MINUTES = 60;
#endif

	// ReSharper disable once StaticMemberInGenericType
	private static readonly Dictionary<string, DateTime> LastCleanup = new();

	public static void DoCleanup( IRequestContext context )
	{
		try
		{
			var Cid = context.CarrierId.TrimToLower();
			var Now = DateTime.UtcNow;

			lock( LastCleanup )
			{
				if( !LastCleanup.TryGetValue( Cid, out var LastDateTime ) )
					LastCleanup.Add( Cid, LastDateTime = DateTime.MinValue );

				if( ( Now - LastDateTime ).TotalMinutes < INTERVAL_IN_MINUTES )
					return;

				if( ( Now - context.LastMessagingCleanup ).TotalMinutes < INTERVAL_IN_MINUTES ) // Could have been cleaned up by another processor instance
					return;

				context.LastMessagingCleanup = Now;
				LastCleanup[ Cid ]           = Now;
			}

			var OkQueues = new List<string>();

			var MaxHoursToKeep = 1;

			bool DoDelete( QueueStorageBase queue )
			{
				var Metadata = queue.MetaData;

				bool SetMetaData()
				{
					Metadata[ MessageQueueStorage<T>.LAST_MODIFIED ] = DateTime.UtcNow.ToString( DATE_FORMAT, CultureInfo.InvariantCulture );
					queue.MetaData                                   = Metadata;
					return false;
				}

				DateTime LastModified;
				var      HoursToKeep = MessageQueueStorage<T>.DEFAULT_TIME_TO_LIVE_HOURS;

				try
				{
					if( !Metadata.TryGetValue( MessageQueueStorage<T>.LAST_MODIFIED, out var MDate ) )
						return SetMetaData();

					if( !DateTime.TryParseExact( MDate, DATE_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out LastModified ) )
						return SetMetaData();

					if( Metadata.TryGetValue( MessageQueueStorage<T>.TIME_TO_LIVE_HOURS, out var Ttl ) )
						int.TryParse( Ttl, out HoursToKeep );
				}
				catch
				{
					LastModified = DateTime.MinValue;
				}

				MaxHoursToKeep = Math.Max( MaxHoursToKeep, HoursToKeep );

				if( ( Now - LastModified ).TotalHours >= HoursToKeep )
				{
					queue.Delete(); // Delete the unused queue
					return true;
				}
				return false;
			}

			try
			{
				IList<MessageTopicTableStorage<T>.Topic> Topics;
				var                                      TopicTable = new MessageTopicTableStorage<T>( context );

				try
				{
					// Expired Topics (Queues may still be active)
					Topics = TopicTable.GetTopics();
				}
				catch( Exception Exception )
				{
					context.SystemLogException( Exception );
					return;
				}

				foreach( var Topic in Topics )
				{
					try
					{
						var Q = MessageQueueStorage<T>.GetQueueStorage( context, Topic.PartitionKey, Topic.Queue, Topic.Board );

						if( DoDelete( Q ) )
						{
							TopicTable.DeleteEntity( Topic ); // Delete the topic
							continue;
						}
						OkQueues.Add( Q.QueueName );
					}
					catch( Exception Exception )
					{
						context.SystemLogException( $"P={Topic.PartitionKey}, Q={Topic.QueueName}, B={Topic.Board}", Exception );
					}
				}
			}
			catch( Exception Exception )
			{
				context.SystemLogException( Exception );
				return;
			}

			while( true )
			{
				// Cleanup lost acks
				var AckTable = new MessageAckTableStorage<T>( context );

				try
				{
					AckTable.DeleteAcks( ( from A in AckTable.GetAcks()
					                       where ( Now - A.Timestamp! ).Value.TotalHours >= MaxHoursToKeep
					                       select new MessageAckTableStorage<T>.AckById( context, A ) ).ToList() );
					break;
				}
				catch( Exception E )
				{
					context.SystemLogException( E );
					return;
				}
			}

			try
			{
				// Cleanup orphan queues
				var Orphans = from Q in new MessageQueueStorage<T>( context ).QueueList()
				              where !OkQueues.Contains( Q )
				              select Q;

				foreach( var QueueName in Orphans )
					DoDelete( new MessageQueueStorage<T>( context, QueueName ) );
			}
			catch( Exception Exception )
			{
				context.SystemLogException( Exception );
			}
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
	}
}