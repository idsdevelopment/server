﻿// ReSharper disable GrammarMistakeInComment


// ReSharper disable StringCompareToIsCultureSpecific

namespace MessagingStorage;

public static class MessageAckTableStorageExtensions
{
	public static string AsKey( this long key ) => $"{key:00000000000000000000}";

	public static string AsKey( this DateTime key ) => $"{key:yyyyMMddHHmmss}";
}

public class MessageAckTableStorage<T> : TableStorage
{
	public class Ack : TableEntry
	{
		public string? DateAdded { get; set; }

		public string? DataAsJson { get; set; }

		public long Id { get; set; }

		public Ack() : base( null )
		{
		}


		public Ack( IRequestContext context, long id, string dataAsJson ) : base( context )
		{
			DataAsJson = dataAsJson;
			DateAdded  = DateTime.UtcNow.AsKey();
			Id         = id;
		}
	}


	public class AckByQueue : Ack
	{
		public AckByQueue()
		{
		}

		public AckByQueue( IRequestContext context, string queueName, long id, string dataAsJson ) : base( context, id, dataAsJson )
		{
			PartitionKey = queueName;
			RowKey       = id.AsKey();
		}
	}


	public class AckById : Ack
	{
		public AckById()
		{
		}

		// Can't be base class Ack
		// Need to guarantee key order

		// ReSharper disable once SuggestBaseTypeForParameterInConstructor
		public AckById( IRequestContext context, AckByQueue q ) : base( context, q.Id, q.DataAsJson ?? "" )
		{
			PartitionKey = q.RowKey;
			RowKey       = q.PartitionKey;
		}
	}


	public class AckByDate : Ack
	{
		// Can't be base class Ack
		// Need to guarantee key order

		// ReSharper disable once SuggestBaseTypeForParameterInConstructor
		public AckByDate( IRequestContext context, AckByQueue q ) : base( context, q.Id, q.DataAsJson ?? "" )
		{
			PartitionKey = q.DateAdded ?? "";
			RowKey       = q.PartitionKey;
		}
	}


	public IList<AckByQueue> GetAcks( string queueName ) => GetPartitionEntities<AckByQueue>( queueName );

	public IList<AckByQueue> GetAcks() => GetEntities<AckByQueue>();

	public List<AckById> GetAcks( List<long> ids )
	{
		try
		{
			var Count = ids.Count;

			if( Count > 0 )
			{
				var Ids = ( from I in ids
				            orderby I
				            select I ).ToList();

				var Min = Ids[ 0 ].AsKey();
				var Max = Ids[ Count - 1 ].AsKey();

				// Contains not supported
				var Temp = Query<AckById>( a => ( a.PartitionKey.CompareTo( Min ) >= 0 )
				                                && ( a.PartitionKey.CompareTo( Max ) <= 0 )
				                         ).ToList();

				return ( from A in Temp
				         where Ids.Contains( A.Id )
				         select A ).ToList();
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return new List<AckById>();
	}


	public void DeleteAcks( List<AckById> acks )
	{
		try
		{
			// Delete By Id
			foreach( var Ack in acks )
				DeleteEntity( Ack );

			foreach( var Aq in from A in acks
			                   select new AckByQueue( Context, A.RowKey, A.Id, A.DataAsJson ?? "" ) )
				DeleteEntity( Aq );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}


	public void DeleteAcks( List<long> acks )
	{
		try
		{
			if( acks.Count > 0 )
			{
				var Acks = GetAcks( acks );

				if( Acks.Count > 0 )
					DeleteAcks( Acks );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public void AddAcks( IList<AckByQueue> acks )
	{
		if( acks.Count > 0 )
		{
			try
			{
				// By Queue Name
				foreach( var Ack in acks )
					AddEntity( Ack );

				// By Id
				foreach( var Ack in ById( acks ) )
					AddEntity( Ack );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
	}


	public MessageAckTableStorage( IRequestContext context ) : base( context, $"MessagingAck{typeof( T ).Name.Trim()}" )
	{
	}

	private IEnumerable<Ack> ById( IEnumerable<AckByQueue> acks ) => from A in acks
	                                                                 select new AckById( Context, A );
}