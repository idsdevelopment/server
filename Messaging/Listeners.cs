﻿#if !DISABLE_MESSAGING
using System.Diagnostics;
using MessagingStorage.Cleanups;
#endif

// ReSharper disable StaticMemberInGenericType

namespace MessagingStorage.Listeners;

public static class Listener<T>
{
	private const int CONNECTION_TIME_LIMIT_IN_MINUTES = 5,
	                  POLLING_DELAY_IN_MS              = 2_000;

	public class DataAndId
	{
		public T    Data;
		public long Id;

		public DataAndId( long id, T data )
		{
			Id   = id;
			Data = data;
		}
	}

	// ReSharper disable once StaticMemberInGenericType
	private static object Rand = (long)0; //Boxed

	private static long NextTick
	{
		get
		{
			lock( Rand )
			{
				var R = (long)Rand;

				if( R == 0 ) // Try To Ensure unique sequence per server
					R = new Random().Next( 1_000_000_000 );

				unchecked
				{
					Rand = ++R;
					return R;
				}
			}
		}
	}

	public static bool ListenAck( IRequestContext context, List<long> packetIds )
	{
	#if !DISABLE_MESSAGING
		var StorageContext = context.Clone();

		Task.Run( () =>
		          {
			          try
			          {
				          var AckTable = new MessageAckTableStorage<T>( StorageContext );
				          AckTable.DeleteAcks( packetIds );
			          }
			          catch( Exception Exception )
			          {
				          StorageContext.SystemLogException( Exception );
			          }
		          } );
	#endif
		return true;
	}

	public static (bool Ok, List<DataAndId> Data ) Listen( IRequestContext context, string topic, string queue, string timeToLiveHours, string board = "" )
	{
		var Result = ( Ok: false, Data: new List<DataAndId>() );
	#if !DISABLE_MESSAGING
		topic = topic.FixQueueName();
		queue = queue.FixQueueName();
		board = board.FixQueueName();
		Debug.WriteLine( $"\r\n---- Attach: {topic}-{queue}" );

		try
		{
			void AddObject( long id, string json )
			{
				var Json = JsonConvert.DeserializeObject<T>( json );

				if( Json is not null )
					Result.Data.Add( new DataAndId( id, Json ) );
			}

			var Queue     = MessageQueueStorage<T>.GetQueueStorage( context, topic, queue, board );
			var QueueName = Queue.QueueName;

			// Mark queue as still active
			var MetaData = Queue.MetaData;
			MetaData[ MessageQueueStorage<T>.LAST_MODIFIED ] = DateTime.UtcNow.ToString( Cleanup<T>.DATE_FORMAT, CultureInfo.InvariantCulture );

			if( !int.TryParse( timeToLiveHours, out var Ttl ) )
				Ttl = MessageQueueStorage<T>.DEFAULT_TIME_TO_LIVE_HOURS;

			MetaData[ MessageQueueStorage<T>.TIME_TO_LIVE_HOURS ] = Ttl.ToString();
			Queue.MetaData                                        = MetaData;

			var TopicTable = new MessageTopicTableStorage<T>( context );
			Debug.WriteLine( $"Add Topic: {topic}" );
			TopicTable.AddTopic( topic, queue, board, QueueName );
			var ConnectionCountDownInMs = CONNECTION_TIME_LIMIT_IN_MINUTES * 60 * 1000;
			var AckTable                = new MessageAckTableStorage<T>( context );

			do
			{
				var Acks = AckTable.GetAcks( QueueName );

				// Lost Ack, Resend
				if( Acks.Count > 0 )
				{
					foreach( var Ack in from A in Acks // Maintain processing order
					                    orderby A.Id
					                    select A )
					{
						if( Ack.DataAsJson is not null )
							AddObject( Ack.Id, Ack.DataAsJson );
					}
					Debug.WriteLine( $"** Returned Acks: {QueueName} # {Result.Data.Count}" );
					Result.Ok = true;
					return Result;
				}

				var Messages = Queue.ReceiveMessages(); // Will Be Deleted From Queue
				Debug.WriteLine( $"Listening: {QueueName} # {Messages.Count}" );

				if( Messages.Count > 0 )
				{
					foreach( var Message in Messages )
					{
						var Tick = NextTick;
						AddObject( Tick, Message );
						Acks.Add( new MessageAckTableStorage<T>.AckByQueue( context, QueueName, Tick, Message ) );
					}

					Tasks.RunVoid( () =>
					               {
						               AckTable.AddAcks( Acks );
					               } );
					Debug.WriteLine( $"** Returned Messages: {QueueName} # {Result.Data.Count}" );
					Result.Ok = true;
					break;
				}
				Thread.Sleep( POLLING_DELAY_IN_MS );
				ConnectionCountDownInMs -= POLLING_DELAY_IN_MS;
			}
			while( ConnectionCountDownInMs > 0 );
		}
		catch( ThreadAbortException )
		{
		}
		catch( Exception E )
		{
			context.SystemLogException( new Exception( $"Topic: \"{topic}\", Queue: \"{queue}\", Board: \"{board}\"", E ) );
		}
	#endif
		return Result;
	}
}