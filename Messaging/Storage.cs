﻿namespace MessagingStorage;

public static class Storage
{
	public static string MakeStorageName( string prefix, Type type, string topic, string queue, string board = "" )
	{
		var Type = type.Name.Trim();

		board = board.Trim();

		if( board.IsNotNullOrWhiteSpace() )
			board = $"-{board}";

		var Temp = $"{prefix.Trim()}-{Type}-{topic.Trim()}-{queue.Trim()}{board}".ToLower();

		var RetVal = new StringBuilder();

		foreach( var C in Temp )
		{
			RetVal.Append( C switch
			               {
				               >= 'A' and <= 'Z'                      => (char)( (int)C - (int)'A' + (int)'a' ),
				               >= 'a' and <= 'z' or >= '0' and <= '9' => C,
				               _                                      => '-'
			               } );
		}

		return RetVal.ToString().MaxLength( 63 ).Trim( '-' );
	}

	public static string MakeMessageStorageName( Type type, string topic, string queue, string board = "" ) => MakeStorageName( "msg", type, topic, queue, board );
}