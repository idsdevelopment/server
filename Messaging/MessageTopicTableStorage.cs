﻿

// ReSharper disable StringCompareToIsCultureSpecific

namespace MessagingStorage;

public class MessageTopicTableStorage<T> : TableStorage
{
	public class Topic : TableEntry
	{
		public string TopicName => PartitionKey;

		public string Queue { get; set; } = "";

		public string QueueName => RowKey;

		public string Board { get; set; } = "";

		public Topic() : base( null )
		{
		}

		public Topic( IRequestContext? context, string topic, string queue, string board, string queueName ) : base( context, topic.TrimToLower(), queueName.TrimToLower() )
		{
			Queue = queue;
			Board = board;
		}

		public Topic( IRequestContext? context ) : base( context )
		{
		}
	}

	public void AddTopic( string topic, string queue, string board, string queueName )
	{
		topic     = topic.TrimToLower();
		queueName = queueName.TrimToLower();

		if( !Exists<Topic>( topic, queueName ) )
			AddEntity( new Topic( Context, topic, queue, board, queueName ) );
	}


	public IList<Topic> GetQueues( string topic ) => GetPartitionEntities<Topic>( topic.TrimToLower() );

	public IList<Topic> GetTopics() => GetEntities<Topic>();


	public MessageTopicTableStorage( IRequestContext context ) : base( context, $"MessagingTopic{typeof( T ).Name.Trim()}" )
	{
	}
}