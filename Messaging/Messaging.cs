﻿using MessagingStorage;
using MessagingStorage.Listeners;
using MessagingStorage.Senders;

namespace Messaging;

public static class Messaging<T>
{
	public static void Send( IRequestContext context, string topic, string queue, List<T> packets, string board = "" ) => Sender<T>.Send( context, topic, queue, packets, board );

	public static bool ListenAck( IRequestContext context, List<long> packetIds ) => Listener<T>.ListenAck( context, packetIds );

	public static (bool Ok, List<Listener<T>.DataAndId> Data) Listen( IRequestContext context, string topic, string queue, string timeToLiveHours, string board = "" ) => Listener<T>.Listen( context, topic, queue, timeToLiveHours, board );

	public static void Clear( IRequestContext context, string topic, string queue, string board = "" ) => MessageQueueStorage<T>.Clear( context, topic, queue, board );
}