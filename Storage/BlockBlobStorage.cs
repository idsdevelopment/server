﻿#nullable enable

namespace Storage;

public class BlockBlobStorage : BlobStorage<CloudBlockBlob>
{
	protected const int MIN_BUFFER_SIZE = 16384;

	protected static int AdjustBufferSize( int size ) => Math.Max( MIN_BUFFER_SIZE, size );

	protected override CloudBlockBlob CreateBlobReference( string blobName ) => Container!.GetBlockBlobReference( blobName );

	public BlockBlobStorage( IRequestContext context, string containerName, string path, string blobName ) : base( context, containerName, path, blobName )
	{
	}
}