﻿#nullable enable

namespace Storage;

public class PageBlobStorage : BlobStorage<CloudPageBlob>
{
	protected override CloudPageBlob CreateBlobReference( string blobName ) => Container!.GetPageBlobReference( blobName );

	public PageBlobStorage( IRequestContext context, string containerName, string path, string blobName, long size ) : base( context, containerName, path, blobName )
	{
		if( Blob is not null && !Blob.Exists() )
			Blob.Create( size );
	}
}