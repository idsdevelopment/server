﻿#nullable enable

namespace Storage;

public class AppendBlobStorage : BlobStorage<CloudAppendBlob>
{
	public void Append( string message )
	{
		if( Blob is not null )
		{
			// Don't use Blob.AppendText
			using var Stream = new MemoryStream( Encoding.UTF8.GetBytes( message ) ) { Position = 0 };
			Blob.AppendBlock( Stream );
		}
	}

	public string ReadText() => Blob is not null ? Blob.DownloadText( Encoding.UTF8 ) : "";

	public static string ToDateTime( DateTimeOffset t, int offset ) => $"{t:yyyy-MM-dd_hh-mm-ss}_Tz_{offset:D2}";
	public static string ToDateTime( DateTimeOffset t ) => ToDateTime( t, t.Offset.Hours );

	public static string ToDateTimeHours( DateTimeOffset t, int offset ) => $"{t:yyyy-MM-dd}_Tz_{offset:D2}_{t:HH}";
	public static string ToDateTimeHours( DateTimeOffset t ) => ToDateTimeHours( t, t.Offset.Hours );

	public static string ToDate( DateTimeOffset t, int offset ) => $"{t:yyyy-MM-dd}_Tz_{offset:D2}";
	public static string ToDate( DateTimeOffset t ) => ToDate( t, t.Offset.Hours );

	protected override CloudAppendBlob CreateBlobReference( string blobName ) => Container!.GetAppendBlobReference( blobName );

	protected override void CreateBlob()
	{
		if( Blob is not null && !Blob.Exists() )
			Blob.CreateOrReplace();
	}

	public AppendBlobStorage( IRequestContext context, string containerName, string path, string blobName, bool toIdsStorage = false ) : base( context, containerName, path, blobName, toIdsStorage )
	{
	}
}