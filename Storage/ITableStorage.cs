﻿using Microsoft.WindowsAzure.Storage.Table;

namespace Storage.Interfaces;

public interface ITableStorage
{
	void Add( TableEntity entity );
	void Add( List<TableEntity> entities );
}