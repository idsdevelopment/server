﻿#nullable enable

using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;

// ReSharper disable StringCompareToIsCultureSpecific
// ReSharper disable InconsistentNaming

namespace Storage;

public class TableStorage : StorageAccount, ITableStorage
{
#if DEBUG
	private static readonly TimeSpan SERVER_TIMEOUT = new( 0, 5, 0 );
#else
		private static readonly TimeSpan SERVER_TIMEOUT = new TimeSpan( 0, 5, 0 );
#endif

	private static readonly TableRequestOptions RequestOptions = new()
	                                                             {
		                                                             RetryPolicy          = ExponentialRetryPolicy,
		                                                             ServerTimeout        = SERVER_TIMEOUT,
		                                                             MaximumExecutionTime = SERVER_TIMEOUT
	                                                             };

	public CloudTable? Table { get; private set; }

	public readonly string TableName;

	private bool Do404( Exception E, ref bool doInit )
	{
		if( doInit && E.Message.Contains( "(404)" ) ) // Happens if I Manually delete SystemLog while running
		{
			doInit = false;

			Init();

			return true;
		}

		return false;
	}


	private Exception Do400or409( ITableEntity entity, Exception e )
	{
		string Message;

		if( e.Message.Contains( "(409)" ) )
			Message = $"Duplicate key: T={Table?.Name ?? "Table NULL"}, P={entity.PartitionKey}, R={entity.RowKey}";
		else
		{
			var Is400 = e.Message.Contains( "(400)" );

			if( Is400 || e.Message.Contains( "(413)" ) )
				Message = $"({( Is400 ? "400" : "413" )}) Error: T={Table?.Name ?? "Table NULL"}, P={entity.PartitionKey}, R={entity.RowKey}\r\nEntry={JsonConvert.SerializeObject( entity, Formatting.Indented )}";
			else
				return e;
		}
		return new Exception( Message.MaxLength( 60_000 ), e );
	}

	private void Init()
	{
		try
		{
			var TableClient = Account.CreateCloudTableClient();
			TableClient.DefaultRequestOptions = RequestOptions;
			Table                             = TableClient.GetTableReference( TableName );

			var Creating = false;

			// Maybe in the process of deletion / Creation
			for( var I = 300 * 2; I-- >= 0; ) // 5 minutes
			{
				if( !Table.Exists() )
				{
					if( !Creating )
					{
						Creating = true;
						Table.CreateIfNotExists();
					}
					else
						Thread.Sleep( 500 );
				}
				else
				{
					Creating = false;
					break;
				}
			}

			if( Creating )
				throw new Exception( "Create table timeout" );
		}
		catch( Exception E )
		{
			Context.SystemLogException( $"Cannot create table: {TableName}", E );
		}
	}

	public void AddOrReplace( TableEntity entity )
	{
		var DoInit = true;

		while( true )
		{
			try
			{
				// Create the TableOperation that inserts the entity.
				var InsertOperation = TableOperation.InsertOrReplace( entity );

				// Execute the insert operation.
				Table?.Execute( InsertOperation, RequestOptions );
			}
			catch( Exception E )
			{
				if( Do404( E, ref DoInit ) )
					continue;

				try
				{
					Context.SystemLogException( Do400or409( entity, E ) );
				}
				catch
				{
				}
			}

			break;
		}
	}


	public void Add( TableEntity entity, bool logError )
	{
		var DoInit = true;

		while( true )
		{
			try
			{
				// Create the TableOperation that inserts the entity.
				var InsertOperation = TableOperation.Insert( entity );

				// Execute the insert operation.
				Table?.Execute( InsertOperation, RequestOptions );
			}
			catch( Exception E )
			{
				if( Do404( E, ref DoInit ) )
					continue;

				try
				{
					if( logError )
						Context.SystemLogException( Do400or409( entity, E ) );
				}
				catch
				{
				}
			}

			break;
		}
	}


	public void Delete( TableEntity entity, bool logError = true )
	{
		var DoInit = true;

		while( true )
		{
			try
			{
				entity.ETag = "*";

				// Create the TableOperation that inserts the entity.
				var DeleteOperation = TableOperation.Delete( entity );

				// Execute the delete operation.
				Table?.Execute( DeleteOperation, RequestOptions );
			}
			catch( Exception E )
			{
				if( Do404( E, ref DoInit ) )
					continue;

				try
				{
					if( logError )
						Context.SystemLogException( Do400or409( entity, E ) );
				}
				catch
				{
				}
			}

			break;
		}
	}

	public TableStorage( IRequestContext context, string tableName, bool toIdsStorage = false )
		: base( context, toIdsStorage )
	{
		TableName = tableName;
		Init();
	}


	public void Add( TableEntity entity )
	{
		Add( entity, true );
	}


	public void Add( List<TableEntity> entities )
	{
		if( entities.Count > 0 )
		{
			var E    = entities[ 0 ];
			var PKey = E.PartitionKey; // Batch operations must have the same partition key

			try
			{
				// Create the batch operation.
				var BatchOperation = new TableBatchOperation();
				var Ndx            = 0;

				foreach( var Entity in entities )
				{
					Entity.PartitionKey = PKey;
					Entity.RowKey       = $"{Entity.RowKey}-{Ndx++}";
					BatchOperation.Insert( Entity );
				}

				Table?.ExecuteBatch( BatchOperation, RequestOptions );
			}
			catch( Exception Ex )
			{
				try
				{
					Context.SystemLogException( Ex );
				}
				catch
				{
				}
			}
		}
	}
}