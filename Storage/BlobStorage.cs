﻿#nullable enable

using Interfaces.Abstracts;

namespace Storage;

public abstract class BlobStorage<T> : StorageAccount where T : CloudBlob
{
	public (List<string> Directories, List<string> Blobs) BlobList
	{
		get
		{
			var Blobs       = new List<string>();
			var Directories = new List<string>();

			if( Container is { } )
			{
				foreach( var BlobItem in from Item in Container.ListBlobs()
				                         select Item )
				{
					switch( BlobItem )
					{
					case CloudBlobDirectory D:
						Directories.Add( D.Prefix ); // Leave '/' symbol.  Indicates has sub directories
						break;

					case CloudBlob B:
						Blobs.Add( B.Name );
						break;
					}
				}
			}
			return ( Directories, Blobs );
		}
	}


	public string BlobName { get; } = "";

	public CloudBlobContainer? Container { get; }

	public T? Blob { get; }

	private BlobStorage( IRequestContext context, string containerName, string blobName, bool toIdsStorage = false )
		: base( context, toIdsStorage )
	{
		try
		{
			var BlobClient = Account.CreateCloudBlobClient();

			BlobClient.DefaultRequestOptions = new BlobRequestOptions
			                                   {
				                                   RetryPolicy = ExponentialRetryPolicy
			                                   };

			// Create the blob if it doesn't exist.
			containerName = MakeContainerName( containerName );
			Container     = BlobClient.GetContainerReference( containerName );

			var Creating = false;

			// Maybe in the process of deletion / Creation
			for( var I = 300 * 2; I-- >= 0; ) // 5 minutes
			{
				if( !Container.Exists() )
				{
					if( !Creating )
					{
						Creating = true;

						Container.CreateIfNotExists();
					}
					else
						Thread.Sleep( 500 );
				}
				else
				{
					Creating = false;
					break;
				}
			}

			if( Creating )
				throw new Exception( "Create blob container timeout" );

			if( blobName.IsNotNullOrEmpty() )
			{
				Blob = CreateBlobReference( BlobName = blobName );
				CreateBlob();
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( $"Cannot create blob container: {containerName}", Exception );
		}
	}


	protected abstract T CreateBlobReference( string blobName );

	public List<string> FlatBlobList( string prefix ) => ( from B in FlatBlobListDetailed( prefix )
	                                                       select B.Name ).ToList();

	public List<CloudBlob> FlatBlobListDetailed( string prefix )
	{
		var Result = new List<CloudBlob>();

		if( Container is { } )
		{
			foreach( var BlobItem in from Item in Container.ListBlobs( prefix, true )
			                         select Item )
			{
				if( BlobItem is CloudBlob B )
					Result.Add( B );
			}
		}
		return Result;
	}


	public static IRequestContext? NewContext() => AIRequestInterface.NewContext?.Invoke( "idsroute", "" );

	protected virtual void CreateBlob()
	{
	}

	public void Delete()
	{
		Blob?.Delete();
	}

	protected BlobStorage( IRequestContext context, string containerName, string path, string blobName, bool toIdsStorage = false )
		: this( context, containerName, CombinePathAndName( path, blobName ), toIdsStorage )
	{
	}
}