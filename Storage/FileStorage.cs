﻿using Microsoft.WindowsAzure.Storage.File;

namespace Storage;

public class FileStorage : StorageAccount
{
	private readonly CloudFileDirectory Dir;

	public CloudFile File( string fileName ) => Dir.GetFileReference( fileName );

	public FileStorage( IRequestContext context, string shareName, string directory ) : base( context )
	{
		var FileClient = Account.CreateCloudFileClient();

		//Get a reference to the file share we created previously.
		var Share = FileClient.GetShareReference( shareName.Trim().ToLower() );

		if( !Share.Exists() )
			Share.Create();

		//Get a reference to the root directory for the share.
		var RootDir = Share.GetRootDirectoryReference();

		//Get a reference to the directory we created previously.
		Dir = RootDir.GetDirectoryReference( directory );

		if( !Dir.Exists() )
			Dir.Create();
	}
}