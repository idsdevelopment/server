﻿#nullable enable

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;

namespace Storage;

public class QueueStorage : StorageAccount
{
	public IDictionary<string, string> Metadata => Queue?.Metadata ?? new Dictionary<string, string>();

	private readonly CloudQueue? Queue;

	private readonly CloudQueueClient QueueClient;

	public readonly string QueueName = "";

	public void SetMetadata()
	{
		if( Queue is { } )
		{
			try
			{
				Queue.SetMetadata();
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
	}

	public void Clear()
	{
		if( Queue is { } )
		{
			try
			{
				Queue.Clear();
			}
			catch
			{
			}
		}
	}

	// Can't get message Id version to work
	public void Add( string id, string message, TimeSpan? ttl = null )
	{
		if( Queue is { } )
		{
			try
			{
				var M = new CloudQueueMessage( id, "" );
				M.SetMessageContent( message );
				Queue.AddMessage( M, ttl );
			}
			catch( Exception E )
			{
				Context.SystemLogException( E );
			}
		}
	}

	public void AddAsync( string id, string message, TimeSpan? ttl = null )
	{
		if( Queue is { } )
		{
			try
			{
				var M = new CloudQueueMessage( id, "" );
				M.SetMessageContent( message );

				Queue.AddMessageAsync( M, ttl, null, new QueueRequestOptions(), new OperationContext() );
			}
			catch( Exception E )
			{
				Context.SystemLogException( E );
			}
		}
	}

	public void Add( string message, TimeSpan? ttl = null )
	{
		Queue?.AddMessage( new CloudQueueMessage( message ), ttl );
	}

	public void AddAsync( string message, TimeSpan? ttl = null )
	{
		if( Queue is { } )
		{
			var M = new CloudQueueMessage( message );
			M.SetMessageContent( message );

			Queue.AddMessageAsync( new CloudQueueMessage( message ), ttl, null, new QueueRequestOptions(),
			                       new OperationContext() );
		}
	}

	public CloudQueueMessage? Peek()
	{
		if( Queue is { } )
		{
			try
			{
				return Queue.PeekMessage();
			}
			catch( Exception E )
			{
				Context.SystemLogException( E );
			}
		}

		return default;
	}

	public List<CloudQueueMessage> GetMessages( int messageCount = 32, bool delete = true )
	{
		if( Queue is { } )
		{
			try
			{
				var Retval = Queue.GetMessages( messageCount );

				if( Retval is null )
					return new List<CloudQueueMessage>();

				var CloudQueueMessages = Retval.ToList();

				if( delete && ( CloudQueueMessages.Count > 0 ) )
				{
					try
					{
						foreach( var Message in CloudQueueMessages )
							Queue.DeleteMessage( Message );
					}
					catch( Exception E )
					{
						Context.SystemLogException( E );
					}
				}
				return CloudQueueMessages;
			}
			catch( ThreadAbortException )
			{
			}
			catch( Exception E )
			{
				Context.SystemLogException( E );
			}
		}

		return new List<CloudQueueMessage>();
	}

	public CloudQueueMessage? GetMessage( bool delete = true )
	{
		if( Queue is { } )
		{
			try
			{
				var Retval = Queue.GetMessage();

				if( delete )
				{
					try
					{
						Queue.DeleteMessageAsync( Retval );
					}
					catch( Exception E )
					{
						Context.SystemLogException( E );
					}
				}
				return Retval;
			}
			catch( ThreadAbortException )
			{
			}
			catch( Exception E )
			{
				Context.SystemLogException( E );
			}
		}
		return default;
	}

	public void DeleteMessage( CloudQueueMessage message )
	{
		if( Queue is { } )
		{
			try
			{
				Queue.DeleteMessage( message );
			}
			catch( Exception E )
			{
				Context.SystemLogException( E );
			}
		}
	}

	public void DeleteMessageAsync( CloudQueueMessage message )
	{
		if( Queue is { } )
		{
			try
			{
				Queue.DeleteMessageAsync( message );
			}
			catch( Exception E )
			{
				Context.SystemLogException( E );
			}
		}
	}

	public void Delete()
	{
		if( Queue is { } )
		{
			try
			{
				Queue.Delete();
			}
			catch( Exception E )
			{
				Context.SystemLogException( E );
			}
		}
	}


	public List<string> QueueList( string? prefix = null ) => ( from Q in QueueClient.ListQueues( prefix )
	                                                            select Q.Name ).ToList();

	public void FetchAttributes()
	{
		Queue?.FetchAttributes();
	}

	public QueueStorage( IRequestContext context ) : base( context )
	{
		// Create the queue client
		QueueClient = Account.CreateCloudQueueClient();

		QueueClient.DefaultRequestOptions = new QueueRequestOptions
		                                    {
			                                    RetryPolicy = ExponentialRetryPolicy
		                                    };
	}

	public QueueStorage( IRequestContext context, string queueName ) : this( context )
	{
		try
		{
			QueueName = queueName;

			// Create queue if it doesn't exist. 
			Queue = QueueClient.GetQueueReference( MakeContainerName( queueName ) );

			var Creating = false;

			// Maybe in the process of deletion / Creation
			for( var I = 300 * 2; I-- >= 0; ) // 5 minutes
			{
				try
				{
					if( !Queue.Exists() )
					{
						if( !Creating )
						{
							Creating = true;

							Queue.CreateIfNotExists();
						}
						else
							Thread.Sleep( 500 );
					}
					else
					{
						Creating = false;
						break;
					}
				}
				catch( StorageException E ) when( E.RequestInformation.HttpStatusCode == 409 ) // Exists() failed ???????
				{
					return;
				}
			}

			if( Creating )
				throw new Exception( "Create queue timeout" );
		}
		catch( Exception E )
		{
			Context.SystemLogException( $"Cannot create Queue: {queueName}", E );
			throw;
		}
	}
}