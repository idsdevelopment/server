﻿global using System;
global using System.Diagnostics;
global using System.Text;
global using Interfaces.Interfaces;
global using Protocol.Data;
#if !NET5_0_OR_GREATER
	global using System.IO;
	global using System.Threading;
	global using System.Threading.Tasks;
#endif