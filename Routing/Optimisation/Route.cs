﻿#nullable enable

using System.Diagnostics;
using MapsApi.GeoLocation;
using Point = MapsApi.GeoLocation.Point;
using Route = Protocol.Data.Maps.Route.Route;

// ReSharper disable CompareOfFloatsByEqualityOperator

namespace RoutingModule.Optimisation;

public class Routing
{
	private const int ADDRESS_EXPIRES_IN_DAYS = 30;

	public List<OptimisedEntry> Route
	{
		get => _Route.Route;
		set => _Route.Route = value;
	}

	private readonly OptimisedList _Route;

	// ReSharper disable once NotAccessedField.Local
	private readonly RequestContext.RequestContext Context;

	public string FileName = "";

	public List<string> OriginalJson = new();

	public Routing( RequestContext.RequestContext context )
	{
		Context = context;

		_Route = new OptimisedList( context )
		         {
			         Route = new List<OptimisedEntry>()
		         };
	}

	public Routing( RequestContext.RequestContext context, List<OptimisedEntry> oList ) : this( context )
	{
		Context      = context;
		_Route.Route = oList;
	}

	public class RouteSegment
	{
		public string TripId { get; set; } = "";

		public string AccountId { get; set; } = "";

		public Maps.MapsApi.AddressData? Account { get; set; }

		public Maps.MapsApi.AddressData? From { get; set; }

		public Maps.MapsApi.AddressData? To { get; set; }

		public string Reference { get; set; } = "";

		public decimal Pieces { get; set; }

		public decimal Weight { get; set; }

		public string Notes { get; set; } = "";
	}

	public class OptimisedEntry
	{
		public double Distance { get; set; }

		public Point Point { get; internal set; } = new();

		public string CompanyName = ""; // Mainly for debugging

		public bool IsPickupCompany;

		public int Order;

		public Point PickupPoint = new();

		public RouteSegment? Segment;

		public double TotalDistance,
		              MapDistance,
		              MapTotalDistance,
		              MapDuration,
		              MapTotalDuration;

		public Maps.MapsApi.AddressData? WorkingAddress;
	}

	public class OptimisedList
	{
		public int Count => Route.Count;

		public List<OptimisedEntry> Route { get; protected internal set; }

		private readonly RequestContext.RequestContext Context;

		public OptimisedList( RequestContext.RequestContext context )
		{
			Context = context;
			Route   = new List<OptimisedEntry>();
		}

		public OptimisedList( RequestContext.RequestContext context, List<OptimisedEntry> oList )
		{
			Context = context;
			Route   = oList;
		}

		public void Add( RouteSegment segment )
		{
			var F = segment.From;
			var T = segment.To;

			if( F is not null && T is not null )
			{
				Route.Add( new OptimisedEntry
				           {
					           Segment         = segment,
					           WorkingAddress  = F,
					           Point           = F.Point,
					           IsPickupCompany = F.IsPickupCompany,
					           PickupPoint     = F.PickupPoint,
					           CompanyName     = F.CompanyName
				           } );

				Route.Add( new OptimisedEntry
				           {
					           Segment         = segment,
					           WorkingAddress  = T,
					           Point           = T.Point,
					           IsPickupCompany = T.IsPickupCompany,
					           PickupPoint     = T.PickupPoint,
					           CompanyName     = T.CompanyName
				           } );
			}
		}

		public void AddSingle( RouteSegment segment )
		{
			Route.Add( new OptimisedEntry
			           {
				           Segment = segment
			           } );
		}

		public Maps.MapsApi.Directions? Optimise( Maps.MapsApi.OPTIMISATION_METHOD method, RouteBase.TRAVEL_MODE travelMode )
		{
			return method switch
			       {
				       Maps.MapsApi.OPTIMISATION_METHOD.FURTHEST => OFurthest( travelMode ),
				       _                                         => null
			       };
		}

		private class RouteOptimiseFurthest : Maps.MapsApi.OptimiseFurthest
		{
			public RouteOptimiseFurthest( RequestContext.RequestContext context, IEnumerable<OptimisedEntry> points )
				: base( context, ToPoints( points ) )
			{
			}

			public override decimal CalculateDistance( Point from, Point to ) => new Points { From = from, To = to }.DistanceTo();

			private static List<Maps.MapsApi.OptimiseEntry> ToPoints( IEnumerable<OptimisedEntry> points )
			{
				return points.Select( p => new Maps.MapsApi.OptimiseEntry
				                           {
					                           Object          = p,
					                           Point           = p.Point,
					                           IsPickupCompany = p.IsPickupCompany,
					                           FromPoint       = p.PickupPoint,
					                           CompanyName     = p.CompanyName
				                           } ).ToList();
			}
		}

		public OptimisedEntry this[ int ndx ] => Route[ ndx ];

		private Maps.MapsApi.Directions OFurthest( RouteBase.TRAVEL_MODE travelMode )
		{
			foreach( var Entry in Route )
			{
				var P = Entry.Point;
				P.Latitude  = Maths.Round6( P.Latitude );
				P.Longitude = Maths.Round6( P.Longitude );
			}
			var NewRoute = new RouteOptimiseFurthest( Context, Route ).Optimise( travelMode );

			var Optimised = new List<OptimisedEntry>();

			double TotalDistance = 0;

			foreach( var Rte in NewRoute.Routes )
			{
				Maps.MapsApi.Leg? PreviousLeg = null;

				foreach( var Leg in Rte.Legs )
				{
					TotalDistance += Leg.Distance!.Value;

					OptimisedEntry? FindOriginal( Point? p )
					{
						if( p is not null )
						{
							return ( from R in Route
							         where ( R.Point.Latitude == p.Latitude )
							               && ( R.Point.Longitude == p.Longitude )
							         select R ).FirstOrDefault();
						}
						return null;
					}

					var OStart = FindOriginal( Leg.OriginalStartLocation );

					if( OStart is not null )
					{
						var OEnd = FindOriginal( Leg.OriginalEndLocation );

						if( OEnd is not null )
						{
							if( PreviousLeg is null )
							{
								OStart.Distance = 0;
								Optimised.Add( OStart );
							}
							OEnd.Distance = Leg.Distance.Value;
							Optimised.Add( OEnd );
						}
					}
					PreviousLeg = Leg;
				}
			}

			foreach( var Entry in Optimised )
				Entry.TotalDistance = TotalDistance;

			Route = Optimised;

			return NewRoute;
		}
	}

	public class CompanyMapAddress
	{
		public string CompanyName { get; set; }
		public string Suite       { get; set; }

		public string  AddressLine1 { get; set; }
		public string  AddressLine2 { get; set; }
		public string  City         { get; set; }
		public string  Region       { get; set; }
		public string  Country      { get; set; }
		public string  PostalCode   { get; set; }
		public decimal Latitude     { get; set; }
		public decimal Longitude    { get; set; }

		public bool IsPickupCompany { get; set; }

		public CompanyMapAddress( CompanyBase company, Maps.MapsApi.AddressCoordinate a )
		{
			Longitude    = a.Longitude;
			Latitude     = a.Latitude;
			PostalCode   = ( a.Address is not null ? a.Address.PostalCode : company.PostalCode ).TrimToUpper();
			CompanyName  = company.CompanyName.TrimToUpper();
			Suite        = company.Suite.TrimToUpper();
			AddressLine1 = company.AddressLine1.TrimToUpper();
			AddressLine2 = company.AddressLine2.TrimToUpper();
			City         = company.City.TrimToUpper();
			Region       = company.Region.TrimToUpper();
			Country      = company.Country.TrimToUpper();
		}

		public CompanyMapAddress( string companyName, string suite, MapAddress m )
		{
			CompanyName = companyName.TrimToUpper();
			Suite       = suite.TrimToUpper();

			Longitude    = m.Longitude;
			Latitude     = m.Latitude;
			PostalCode   = m.PostalCode;
			AddressLine1 = m.AddressLine1;
			AddressLine2 = m.AddressLine2;
			City         = m.City;
			Region       = m.Region;
			Country      = m.Country;
		}
	}

	internal static Maps.MapsApi.AddressData ToAddressData( Company company )
	{
		var Addr  = company.AddressLine1.TrimToUpper();
		var Addr2 = company.AddressLine2.TrimToUpper();

		if( Addr2.Contains( Addr ) || Addr.Contains( Addr2 ) )
			Addr2 = "";

		return new Maps.MapsApi.AddressData
		       {
			       City            = company.City,
			       CompanyName     = company.CompanyName,
			       Country         = company.Country,
			       CountryCode     = company.CountryCode,
			       Longitude       = company.Longitude,
			       Latitude        = company.Latitude,
			       PostalCode      = company.PostalCode,
			       State           = company.Region,
			       StateCode       = "",
			       StreetNumber    = GetStreetNumber( ref Addr ), // Must be before street
			       Street          = $"{Addr} {Addr2}",
			       Suite           = company.Suite,
			       Vicinity        = "",
			       IsPickupCompany = company.IsPickupCompany
		       };
	}

	internal static Maps.MapsApi.AddressData ToAddressData( CompanyMapAddress a, Point pickupPoint )
	{
		var Addr  = a.AddressLine1.TrimToUpper();
		var Addr2 = a.AddressLine2.TrimToUpper();

		if( Addr2.Contains( Addr ) || Addr.Contains( Addr2 ) )
			Addr2 = "";

		return new Maps.MapsApi.AddressData
		       {
			       CompanyName     = a.CompanyName,
			       Suite           = a.Suite,
			       Longitude       = a.Longitude,
			       Latitude        = a.Latitude,
			       PostalCode      = a.PostalCode,
			       City            = a.City,
			       Country         = a.Country,
			       CountryCode     = "",
			       State           = a.Region,
			       StateCode       = "",
			       StreetNumber    = GetStreetNumber( ref Addr ), // Must be before street
			       Street          = $"{Addr} {Addr2}",
			       Vicinity        = "",
			       IsPickupCompany = a.IsPickupCompany,
			       PickupPoint     = pickupPoint
		       };
	}

	internal static CompanyMapAddress ToMapAddress( Company company, Maps.MapsApi.AddressCoordinate a ) => new( company, a ) { IsPickupCompany = company.IsPickupCompany };

	public Maps.MapsApi.Directions? Optimise( Maps.MapsApi.OPTIMISATION_METHOD method, RouteBase.TRAVEL_MODE travelMode ) => _Route.Optimise( method, travelMode );

	public void Add( RouteSegment segment )
	{
		_Route.Add( segment );
	}

	public static List<CompanyMapAddress> GetAddresses( RequestContext.RequestContext context, Companies companies )
	{
		var Result  = new List<CompanyMapAddress>();
		var Expires = DateTime.UtcNow.AddDays( ADDRESS_EXPIRES_IN_DAYS );
		var Updates = new List<long>();

		try
		{
			var       Maps         = new Maps.MapsApi( context );
			using var Db           = new UsersEntities();
			var       MapAddresses = Db.MapAddresses;

			foreach( var CompanyAddress in companies )
			{
				// Try and find by company address first
				CompanyAddress.AddressLine1 = CompanyAddress.AddressLine1.TrimToUpper();
				CompanyAddress.City         = CompanyAddress.City.TrimToUpper();
				CompanyAddress.Region       = CompanyAddress.Region.TrimToUpper();
				CompanyAddress.Country      = CompanyAddress.Country.TrimToUpper();

				var Rec = ( from A in MapAddresses
				            where ( A.AddressLine1 == CompanyAddress.AddressLine1 )
				                  && ( A.City == CompanyAddress.City )
				                  && ( A.Region == CompanyAddress.Region )
				                  && ( A.Country == CompanyAddress.Country )
				            select A ).FirstOrDefault();

				if( Rec is not null )
				{
					if( ( Rec.Latitude != 0 ) && ( Rec.Longitude != 0 ) )
					{
						Updates.Add( Rec.Id );

						Result.Add( new CompanyMapAddress( CompanyAddress.CompanyName, CompanyAddress.Suite, Rec )
						            {
							            IsPickupCompany = CompanyAddress.IsPickupCompany
						            } );
						continue;
					}
					MapAddresses.Remove( Rec );
					Db.SaveChanges();
				}

				List<Maps.MapsApi.AddressCoordinate> MapAddress;

				while( true )
				{
					MapAddress = Maps.FindLocationByAddress( ToAddressData( CompanyAddress ), out var RateLimited );

					if( !RateLimited )
						break;

					Thread.Sleep( 100 );
				}

				if( MapAddress.Count > 0 )
				{
					var NewRec = ToMapAddress( CompanyAddress, MapAddress[ 0 ] ); // Highest Confidence record
					NewRec.IsPickupCompany = CompanyAddress.IsPickupCompany;

					MapAddresses.Add( new MapAddress
					                  {
						                  Longitude = NewRec.Longitude,
						                  Latitude  = NewRec.Latitude,

						                  AddressLine1 = NewRec.AddressLine1.TrimToUpper(),
						                  AddressLine2 = NewRec.AddressLine2.TrimToUpper(),
						                  PostalCode   = NewRec.PostalCode.TrimToUpper(),
						                  Country      = NewRec.Country.TrimToUpper(),
						                  Region       = NewRec.Region.TrimToUpper(),
						                  City         = NewRec.City.TrimToUpper(),
						                  Expires      = Expires
					                  } );
					Db.SaveChanges();

					Result.Add( NewRec );
				}
			}

			if( Updates.Count > 0 )
			{
				Tasks.RunVoid( () =>
				               {
					               try
					               {
						               using var Db1 = new UsersEntities();

						               var Ids = string.Join( " OR Id=", Updates );

						               var Sql = $"UPDATE MapAddresses SET Expires='{Expires:d}' WHERE Id={Ids}";

						               Db1.Database.ExecuteSqlCommand( Sql );
					               }
					               catch
					               {
					               }
				               } );
			}
		}
		catch( Exception Exception )
		{
			context.SystemLogException( nameof( GetAddresses ), Exception );
		}
		return Result;
	}

	public AddressValidList ValidateAddresses( RequestContext.RequestContext context, AddressValidationList addresses )
	{
		var Distinct = from A in addresses
		               let CoName = A.CompanyName.TrimToUpper()
		               let A1 = A.AddressLine1.TrimToUpper()
		               let City = A.City.TrimToUpper()
		               let Region = A.Region.TrimToUpper()
		               let Country = A.Country.TrimToUpper()
		               orderby Country, Region, City, A1, CoName
		               group A by new { CoName, A1, City, Region, Country }
		               into G
		               select G.First();

		var Result = new AddressValidList( Distinct );

		try
		{
			var Addresses = new Companies();

			Addresses.AddRange(
			                   from A in addresses
			                   select new Company
			                          {
				                          CompanyName  = A.CompanyName,
				                          Suite        = A.Suite,
				                          AddressLine1 = A.AddressLine1,
				                          AddressLine2 = A.AddressLine2,
				                          City         = A.City,
				                          Region       = A.Region,
				                          Country      = A.Country,
				                          CountryCode  = A.CountryCode,
				                          PostalCode   = A.PostalCode,
				                          Latitude     = A.Latitude,
				                          Longitude    = A.Longitude
			                          }
			                  );

			var Recs = GetAddresses( context, Addresses );

			foreach( var Addr in Result )
			{
				Addr.CompanyName  = Addr.CompanyName.TrimToUpper();
				Addr.Suite        = Addr.Suite.TrimToUpper();
				Addr.AddressLine1 = Addr.AddressLine1.TrimToUpper();
				Addr.AddressLine2 = Addr.AddressLine2.TrimToUpper();
				Addr.City         = Addr.City.TrimToUpper();
				Addr.Region       = Addr.Region.TrimToUpper();
				Addr.Country      = Addr.Country.TrimToUpper();
				Addr.CountryCode  = Addr.CountryCode.TrimToUpper();
				Addr.PostalCode   = Addr.PostalCode.TrimToUpper();

				foreach( var MapAddress in Recs )
				{
					if( Addr.AddressLine1.CompareEqualsIgnoreCase( MapAddress.AddressLine1 )
					    && Addr.City.CompareEqualsIgnoreCase( MapAddress.City )
					    && Addr.Region.CompareEqualsIgnoreCase( MapAddress.Region )
					    && Addr.Country.CompareEqualsIgnoreCase( MapAddress.Country ) )
					{
						Addr.IsValid   = true;
						Addr.Latitude  = MapAddress.Latitude;
						Addr.Longitude = MapAddress.Longitude;

						if( Addr.PostalCode == "" )
							Addr.PostalCode = MapAddress.PostalCode;

						break;
					}
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}

	public Distances GetDistances( RequestContext.RequestContext context, RouteOptimisationAddresses companyAddresses )
	{
		var Result = new Distances();
		var Recs   = GetAddresses( context, companyAddresses.Companies );

		var L = Recs.Count;

		if( L >= 2 ) // Need at least 2 addresses
		{
			L     -= 1;
			Route =  [];

			for( var I = 0; I < L; I++ )
			{
				var A1 = Recs[ I ];
				var A2 = Recs[ I + 1 ];

				var FromPoint = new Point
				                {
					                Latitude    = A1.Latitude,
					                Longitude   = A1.Longitude,
					                CompanyName = A1.CompanyName
				                };

				Add( new RouteSegment { From = ToAddressData( A1, FromPoint ), To = ToAddressData( A2, FromPoint ) } );
			}
			Optimise( Maps.MapsApi.OPTIMISATION_METHOD.FURTHEST, companyAddresses.TravelMode );

			var First = true;

			foreach( var R in Route )
			{
				if( First )
					First = false;
				else
					Result.Add( R.Distance );
			}
		}

		return Result;
	}

	public static Protocol.Data.Maps.Route.Point ConvertPoint( Point p ) => new()
	                                                                        {
		                                                                        Latitude  = p.Latitude,
		                                                                        Longitude = p.Longitude
	                                                                        };

	public static List<Step> ConvertSteps( List<Maps.MapsApi.Step> steps ) => ( from S in steps
	                                                                            select new Step
	                                                                                   {
		                                                                                   Distance         = (decimal)S.Distance!.Value,
		                                                                                   Duration         = (decimal)S.Duration!.Value,
		                                                                                   StartLocation    = ConvertPoint( S.StartLocation! ),
		                                                                                   EndLocation      = ConvertPoint( S.EndLocation! ),
		                                                                                   HtmlInstructions = S.Instructions
	                                                                                   } ).ToList();

	public static List<Leg> ConvertLegs( List<Maps.MapsApi.Leg> legs )
	{
		var Result = new List<Leg>();

		foreach( var Leg in legs )
		{
			Result.Add( new Leg
			            {
				            Distance                  = (decimal)Leg.Distance!.Value,
				            Duration                  = (decimal)Leg.Duration!.Value,
				            StartLocation             = ConvertPoint( Leg.StartLocation! ),
				            EndLocation               = ConvertPoint( Leg.EndLocation! ),
				            HtmlInstructions          = "",
				            Order                     = Leg.Order,
				            Steps                     = ConvertSteps( Leg.Steps ),
			            } );
		}
		return Result;
	}

	public static Route ConvertToRoute( List<Maps.MapsApi.Leg> legs )
	{
		var Result = new Route();
		Result.AddRange( ConvertLegs( legs ) );
		return Result;
	}

	public DistancesAndAddresses DistancesWithAddresses( RequestContext.RequestContext context, RouteOptimisationAddresses companyAddresses )
	{
		var Result = new DistancesAndAddresses();
		var Recs   = GetAddresses( context, companyAddresses.Companies );

	#if DEBUG
		Debug.WriteLine( "------------ Received -------------------" );

		foreach( var Rec in Recs )
			Debug.WriteLine( $"{Rec.CompanyName} -- {Rec.IsPickupCompany}" );
		Debug.WriteLine( "-----------------------------------------" );
	#endif
		var L = Recs.Count;

		if( L >= 2 ) // Need at least 2 addresses
		{
			L -= 1;

			Route = [];

			for( var I = 0; I < L; I += 2 )
			{
				var From = Recs[ I ];
				var To   = Recs[ I + 1 ];

				var FromPoint = new Point
				                {
					                Latitude    = From.Latitude,
					                Longitude   = From.Longitude,
					                CompanyName = From.CompanyName
				                };
				Add( new RouteSegment { From = ToAddressData( From, FromPoint ), To = ToAddressData( To, FromPoint ) } );
			}

			var Directions = Optimise( Maps.MapsApi.OPTIMISATION_METHOD.FURTHEST, companyAddresses.TravelMode );

			Maps.MapsApi.AddressData? Previous = null;

			foreach( var R in Route )
			{
				var W = R.WorkingAddress;

				if( Previous is not null )
				{
					Result.Add( new DistanceAndAddress
					            {
						            DistanceInKilometres = (decimal)R.Distance,
						            FromCompany          = Previous.AsCompany,
						            ToCompany            = W!.AsCompany
					            } );
				}
				Previous = W;
			}

			if( Directions is not null )
			{
				// Flatten the legs
				var Legs = new List<Maps.MapsApi.Leg>();

				foreach( var R in Directions.Routes )
				{
					foreach( var Leg in R.Legs )
						Legs.Add( Leg );
				}

				foreach( var D in Result )
				{
					D.Route = ConvertToRoute( ( from Lg in Legs
					                            where ( D.FromCompany.Latitude == Lg.OriginalStartLocation!.Latitude )
					                                  && ( D.FromCompany.Longitude == Lg.OriginalStartLocation.Longitude )
					                            select Lg ).Take( 1 ).ToList() );
				}
			}
		}
		return Result;
	}

	private static string GetStreetNumber( ref string addr )
	{
		var Num = new StringBuilder();
		addr = addr.Trim();
		var L = addr.Length;

		if( ( L > 0 ) && addr[ 0 ] is >= '0' and <= '9' )
		{
			for( var I = 0; I < L; I++ )
			{
				var C = addr[ I ];

				if( C is ' ' )
					break;

				Num.Append( C );
			}
		}

		var Number = Num.ToString();
		addr = addr.Substring( Number.Length ).Trim();

		return Number;
	}
}