﻿using Database.Model.Databases.Carrier;

namespace Reports.Internal.AccountExport;

internal sealed class AccountExportDataSource : DataSource
{
	//public sealed record Global();

	public sealed record GlobalAddressExportCompanyItem(
				string CompanyId,
				string SecondaryId,
				string CompanyName,
				string CompanyNumber,
				string Barcode,

				string ContactName,
				string EmailAddress,
				string EmailAddress1,
				string EmailAddress2,
				string Phone,
				string Phone1,
				string Mobile,
				string Mobile1,
				string Fax,

				string CreditLimit,
				string EmailInvoice,
				string Enabled,
				string BillingPeriod,
				string BillToCompany,
				string InvoiceEmailAddresses,
				string InvoiceOverride,
				string MultipleTripsOnInvoice,

				string IsDeliveryCompany,
				string IsPickupCompany,
				string Suite,
				string AddressLine1,
				string AddressLine2,
				string City,
				string Region,
				string RegionCode,
				string Country,
				string CountryCode,
				string PostalCode,
				string PostalBarcode,
				string LocationBarcode,
				string Latitude,
				string Longitude,
				string Vicinity,
				string ZoneName,

				string Notes,
				string UserName,
				string Password
		);

	public sealed record GlobalAddressExportAddressItem(
				string CompanyId,
				string SecondaryId,
				string CompanyName,
				string CompanyNumber,
				string Barcode,

				string ContactName,
				string EmailAddress,
				string EmailAddress1,
				string EmailAddress2,
				string Phone,
				string Phone1,
				string Mobile,
				string Mobile1,
				string Fax,

				string CreditLimit,
				string EmailInvoice,
				string Enabled,
				string BillingPeriod,
				string BillToCompany,
				string InvoiceEmailAddresses,
				string InvoiceOverride,
				string MultipleTripsOnInvoice,

				string IsDeliveryCompany,
				string IsPickupCompany,
				string Suite,
				string AddressLine1,
				string AddressLine2,
				string City,
				string Region,
				string RegionCode,
				string Country,
				string CountryCode,
				string PostalCode,
				string PostalBarcode,
				string LocationBarcode,
				string Latitude,
				string Longitude,
				string Vicinity,
				string ZoneName,

				string Notes,
				string UserName,
				string Password
		);

	public sealed record GlobalAddressExportItem(GlobalAddressExportCompanyItem GlobalAddressExportCompanyItem,
											 GlobalAddressExportAddressItem GlobalAddressExportAddressItem);
	public AccountExportDataSource(IRequestContext context, Report options)
	{
		try
		{
			var GlobalAddressBookId = "CORE_GLOBAL_ADDRESS_BOOK";

			using var Db = new CarrierDb(context);
			var GlobalCompanies = Db.GetCustomerCompaniesDetailed(GlobalAddressBookId);


			foreach (var C in GlobalCompanies)
			{

				var CompanyExport = new GlobalAddressExportCompanyItem(
					C.Company.CompanyId.ToString(),
				C.Company.SecondaryId.ToString(),
				C.Company.CompanyName.ToString(),
				C.Company.CompanyNumber.ToString(),
				C.Company.Barcode.ToString(),

				C.Company.ContactName.ToString(),
				C.Company.EmailAddress.ToString(),
				C.Company.EmailAddress1.ToString(),
				C.Company.EmailAddress2.ToString(),
				C.Company.Phone.ToString(),
				C.Company.Phone1.ToString(),
				C.Company.Mobile.ToString(),
				C.Company.Mobile1.ToString(),
				C.Company.Fax.ToString(),

				C.Company.CreditLimit.ToString(),
				C.Company.EmailInvoice.ToString(),
				C.Company.Enabled.ToString(),
				C.Company.BillingPeriod.ToString(),
				C.Company.BillToCompany.ToString(),
				C.Company.InvoiceEmailAddresses.ToString(),
				C.Company.InvoiceOverride.ToString(),
				C.Company.MultipleTripsOnInvoice.ToString(),

				C.Company.IsDeliveryCompany.ToString(),
				C.Company.IsPickupCompany.ToString(),
				C.Company.Suite.ToString(),
				C.Company.AddressLine1.ToString(),
				C.Company.AddressLine2.ToString(),
				C.Company.City.ToString(),
				C.Company.Region.ToString(),
				C.Company.RegionCode.ToString(),
				C.Company.Country.ToString(),
				C.Company.CountryCode.ToString(),
				C.Company.PostalCode.ToString(),
				C.Company.PostalBarcode.ToString(),
				C.Company.LocationBarcode.ToString(),
				C.Company.Latitude.ToString(),
				C.Company.Longitude.ToString(),
				C.Company.Vicinity.ToString(),
				C.Company.ZoneName.ToString(),

				C.Company.Notes.ToString(),
				C.Company.UserName.ToString(),
				C.Company.Password

				);

				var AddressExport = new GlobalAddressExportAddressItem(
					C.Address.CompanyId.ToString(),
				C.Address.SecondaryId.ToString(),
				C.Address.CompanyName.ToString(),
				C.Address.CompanyNumber.ToString(),
				C.Address.Barcode.ToString(),

				C.Address.ContactName.ToString(),
				C.Address.EmailAddress.ToString(),
				C.Address.EmailAddress1.ToString(),
				C.Address.EmailAddress2.ToString(),
				C.Address.Phone.ToString(),
				C.Address.Phone1.ToString(),
				C.Address.Mobile.ToString(),
				C.Address.Mobile1.ToString(),
				C.Address.Fax.ToString(),

				C.Address.CreditLimit.ToString(),
				C.Address.EmailInvoice.ToString(),
				C.Address.Enabled.ToString(),
				C.Address.BillingPeriod.ToString(),
				C.Address.BillToCompany.ToString(),
				C.Address.InvoiceEmailAddresses.ToString(),
				C.Address.InvoiceOverride.ToString(),
				C.Address.MultipleTripsOnInvoice.ToString(),

				C.Address.IsDeliveryCompany.ToString(),
				C.Address.IsPickupCompany.ToString(),
				C.Address.Suite.ToString(),
				C.Address.AddressLine1.ToString(),
				C.Address.AddressLine2.ToString(),
				C.Address.City.ToString(),
				C.Address.Region.ToString(),
				C.Address.RegionCode.ToString(),
				C.Address.Country.ToString(),
				C.Address.CountryCode.ToString(),
				C.Address.PostalCode.ToString(),
				C.Address.PostalBarcode.ToString(),
				C.Address.LocationBarcode.ToString(),
				C.Address.Latitude.ToString(),
				C.Address.Longitude.ToString(),
				C.Address.Vicinity.ToString(),
				C.Address.ZoneName.ToString(),

				C.Address.Notes.ToString(),
				"",
				""
					);

				Add(new GlobalAddressExportItem(CompanyExport, AddressExport));
			}
			
			//Globals = new Global();

		}
		catch (Exception Exception)
		{
			context.SystemLogException(Exception);
		}
	}
}