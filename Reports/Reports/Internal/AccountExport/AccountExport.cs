﻿using Reports.Internal.AccountExport;
using Utils.Csv;


namespace Reports;

internal sealed class AccountExport : ReportsModule.Reports
{
	protected override string TemplateName => @"Internal\AccountExport\AccountExport.html";

	public AccountExport( IRequestContext context, Report options ) : base( context, new AccountExportDataSource( context, options ), options )
	{
	}

	public override string ToCsv()
	{
		var Csv = new Csv();
		if (Data.Source is { } Source)
		{
			//var Globals = (GlobalAddressExportDataSource.Global)Source.Globals;

			Csv[0][0].AsString = "Global Address Book Export";
			Csv[1][0].AsString = "";

			Csv[3][0].AsString = "Printed On:";
			Csv[3][1].AsString = $"{DateTime.Now:D}";

			//AsDecimal

			Csv[4][0].AsString = "";
			var RowHeaders = Csv[5];
			var col = 0;
			RowHeaders[col].AsString = "CompanyId"; col++;
			RowHeaders[col].AsString = "SecondaryId"; col++;
			RowHeaders[col].AsString = "CompanyName"; col++;
			RowHeaders[col].AsString = "CompanyNumber"; col++;
			RowHeaders[col].AsString = "Barcode"; col++;

			RowHeaders[col].AsString = "ContactName"; col++;
			RowHeaders[col].AsString = "EmailAddress"; col++;
			RowHeaders[col].AsString = "EmailAddress1"; col++;
			RowHeaders[col].AsString = "EmailAddress2"; col++;
			RowHeaders[col].AsString = "Phone"; col++;
			RowHeaders[col].AsString = "Phone1"; col++;
			RowHeaders[col].AsString = "Mobile"; col++;
			RowHeaders[col].AsString = "Mobile1"; col++;
			RowHeaders[col].AsString = "Fax"; col++;

			RowHeaders[col].AsString = "CreditLimit"; col++;
			RowHeaders[col].AsString = "EmailInvoice"; col++;
			RowHeaders[col].AsString = "Enabled"; col++;
			RowHeaders[col].AsString = "BillingPeriod"; col++;
			RowHeaders[col].AsString = "BillToCompany"; col++;
			RowHeaders[col].AsString = "InvoiceEmailAddresses"; col++;
			RowHeaders[col].AsString = "InvoiceOverride"; col++;
			RowHeaders[col].AsString = "MultipleTripsOnInvoice"; col++;

			RowHeaders[col].AsString = "IsDeliveryCompany"; col++;
			RowHeaders[col].AsString = "IsPickupCompany"; col++;
			RowHeaders[col].AsString = "Suite"; col++;
			RowHeaders[col].AsString = "AddressLine1"; col++;
			RowHeaders[col].AsString = "AddressLine2"; col++;
			RowHeaders[col].AsString = "City"; col++;
			RowHeaders[col].AsString = "Region"; col++;
			RowHeaders[col].AsString = "RegionCode"; col++;
			RowHeaders[col].AsString = "Country"; col++;
			RowHeaders[col].AsString = "CountryCode"; col++;
			RowHeaders[col].AsString = "PostalCode"; col++;
			RowHeaders[col].AsString = "PostalBarcode"; col++;
			RowHeaders[col].AsString = "LocationBarcode"; col++;
			RowHeaders[col].AsString = "Latitude"; col++;
			RowHeaders[col].AsString = "Longitude"; col++;
			RowHeaders[col].AsString = "Vicinity"; col++;
			RowHeaders[col].AsString = "ZoneName"; col++;

			RowHeaders[col].AsString = "Notes"; col++;
			RowHeaders[col].AsString = "UserName"; col++;
			RowHeaders[col].AsString = "Password"; col++;

			var i = 6;
			foreach (var source in Source)
			{
				var row = Csv[i];
				col = 0;
				var s = (AccountExportDataSource.GlobalAddressExportItem)source;
				var c = s.GlobalAddressExportCompanyItem;
				row[col].AsString = c.CompanyId; col++;
				row[col].AsString = c.SecondaryId; col++;
				row[col].AsString = c.CompanyName; col++;
				row[col].AsString = c.CompanyNumber; col++;
				row[col].AsString = c.Barcode; col++;

				row[col].AsString = c.ContactName; col++;
				row[col].AsString = c.EmailAddress; col++;
				row[col].AsString = c.EmailAddress1; col++;
				row[col].AsString = c.EmailAddress2; col++;
				row[col].AsString = c.Phone; col++;
				row[col].AsString = c.Phone1; col++;
				row[col].AsString = c.Mobile; col++;
				row[col].AsString = c.Mobile1; col++;
				row[col].AsString = c.Fax; col++;

				row[col].AsString = c.CreditLimit; col++;
				row[col].AsString = c.EmailInvoice; col++;
				row[col].AsString = c.Enabled; col++;
				row[col].AsString = c.BillingPeriod; col++;
				row[col].AsString = c.BillToCompany; col++;
				row[col].AsString = c.InvoiceEmailAddresses; col++;
				row[col].AsString = c.InvoiceOverride; col++;
				row[col].AsString = c.MultipleTripsOnInvoice; col++;

				row[col].AsString = c.IsDeliveryCompany; col++;
				row[col].AsString = c.IsPickupCompany; col++;
				row[col].AsString = c.Suite; col++;
				row[col].AsString = c.AddressLine1; col++;
				row[col].AsString = c.AddressLine2; col++;
				row[col].AsString = c.City; col++;
				row[col].AsString = c.Region; col++;
				row[col].AsString = c.RegionCode; col++;
				row[col].AsString = c.Country; col++;
				row[col].AsString = c.CountryCode; col++;
				row[col].AsString = c.PostalCode; col++;
				row[col].AsString = c.PostalBarcode; col++;
				row[col].AsString = c.LocationBarcode; col++;
				row[col].AsString = c.Latitude; col++;
				row[col].AsString = c.Longitude; col++;
				row[col].AsString = c.Vicinity; col++;
				row[col].AsString = c.ZoneName; col++;

				row[col].AsString = c.Notes; col++;
				row[col].AsString = c.UserName; col++;
				row[col].AsString = c.Password; col++;

				i++;

				//They appear to be the same information
				// I can uncomment in the future if we need to double check that

				//row = Csv[i];
				//col = 0;
				//var a = s.GlobalAddressExportAddressItem;
				//row[col].AsString = a.CompanyId; col++;
				//row[col].AsString = a.SecondaryId; col++;
				//row[col].AsString = a.CompanyName; col++;
				//row[col].AsString = a.CompanyNumber; col++;
				//row[col].AsString = a.Barcode; col++;

				//row[col].AsString = a.ContactName; col++;
				//row[col].AsString = a.EmailAddress; col++;
				//row[col].AsString = a.EmailAddress1; col++;
				//row[col].AsString = a.EmailAddress2; col++;
				//row[col].AsString = a.Phone; col++;
				//row[col].AsString = a.Phone1; col++;
				//row[col].AsString = a.Mobile; col++;
				//row[col].AsString = a.Mobile1; col++;
				//row[col].AsString = a.Fax; col++;

				//row[col].AsString = a.CreditLimit; col++;
				//row[col].AsString = a.EmailInvoice; col++;
				//row[col].AsString = a.Enabled; col++;
				//row[col].AsString = a.BillingPeriod; col++;
				//row[col].AsString = a.BillToCompany; col++;
				//row[col].AsString = a.InvoiceEmailAddresses; col++;
				//row[col].AsString = a.InvoiceOverride; col++;
				//row[col].AsString = a.MultipleTripsOnInvoice; col++;

				//row[col].AsString = a.IsDeliveryCompany; col++;
				//row[col].AsString = a.IsPickupCompany; col++;
				//row[col].AsString = a.Suite; col++;
				//row[col].AsString = a.AddressLine1; col++;
				//row[col].AsString = a.AddressLine2; col++;
				//row[col].AsString = a.City; col++;
				//row[col].AsString = a.Region; col++;
				//row[col].AsString = a.RegionCode; col++;
				//row[col].AsString = a.Country; col++;
				//row[col].AsString = a.CountryCode; col++;
				//row[col].AsString = a.PostalCode; col++;
				//row[col].AsString = a.PostalBarcode; col++;
				//row[col].AsString = a.LocationBarcode; col++;
				//row[col].AsString = a.Latitude; col++;
				//row[col].AsString = a.Longitude; col++;
				//row[col].AsString = a.Vicinity; col++;
				//row[col].AsString = a.ZoneName; col++;

				//row[col].AsString = a.Notes; col++;
				//row[col].AsString = a.UserName; col++;
				//row[col].AsString = a.Password; col++;

				//i++;
			}

		}
		else { Csv[0][0] = "Source Failed"; }
		return Csv.ToString();
	}
}