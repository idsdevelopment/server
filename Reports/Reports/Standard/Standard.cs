﻿using Emails;
using NUglify;

namespace Reports;

public sealed class Standard
{
	public static ReportReply Execute( IRequestContext context, Report r )
	{
		var Result = new ReportReply
		             {
			             ReportFlags = r.ReportFlags
		             };

		try
		{
			ReportsModule.Reports? Report = r.Name.NullTrim().ToLower() switch
			                                {
				                                "calltakerstatistics"    => new CallTakerStatistics( context, r ),
				                                "calltakerdaily"         => new CallTakerDaily( context, r ),
				                                "calltakerrevenue"       => new CallTakerRevenue( context, r ),
				                                "waybillreport"          => new WaybillReport( context, r ),
				                                "drivermanifest"         => new DriverManifest( context, r ),
				                                "driverarrivetimereport" => new DriverArriveTimeReport( context, r ),
				                                "generaltripreport"      => new GeneralTripReport( context, r ),
				                                "glpostingtotals"        => new GLPostingTotals( context, r ),
				                                "invoicereport"          => new InvoiceReport( context, r ),
				                                "bulkinvoicereport"      => new BulkInvoiceReport( context, r ),
				                                "fuelsurchargedaily"     => new FuelSurchargeDaily( context, r ),
				                                "fuelsurchargeperiod"    => new FuelSurchargePeriod( context, r ),
				                                "barcodetest"            => new BarcodeTest( context, r ),
				                                "portalmanifest"         => new PortalManifest( context, r ),
				                                "historyreport"          => new HistoryReport( context, r ),
				                                "globaladdressexport"    => new GlobalAddressExport( context, r ),
				                                "accountexport"          => new AccountExport( context, r ),
				                                "taxreconciliation"      => new TaxReconciliation( context, r ),

				                                _ => null
			                                };

			if( Report is not null && !Report.NothingFound )
			{
				var Flags = r.ReportFlags;

				var IsCsv = ( Flags & Protocol.Data.Report.REPORT_FLAGS.CSV ) != 0;
				var Csv   = ( IsCsv ? Report.ToCsv() : "" ).NullTrim();
				var Html  = Uglify.Html( Report.Print() ).Code;

				if( ( Flags & Protocol.Data.Report.REPORT_FLAGS.STORAGE ) != 0 )
					StorageV2.Reports.Report.Save( context, r.StorageCategory, r.StorageSubCategory, r.StorageSubCategory1, r.Name, IsCsv ? Csv : Html );

				var PdfBytes = ( Flags & Protocol.Data.Report.REPORT_FLAGS.PDF ) != 0
					               ? Pdf.Pdf.HtmlToPdf( context, Html, r.PaperSize, r.IsLandscape )
					               : [];

				if( ( ( Flags & Protocol.Data.Report.REPORT_FLAGS.EMAIL ) != 0 ) && ( r.EmailAddresses.Count > 0 ) )
				{
					if( ( Flags & ( Protocol.Data.Report.REPORT_FLAGS.PDF | Protocol.Data.Report.REPORT_FLAGS.CSV ) ) == 0 ) // No PDF and no CSV
						Flags = Protocol.Data.Report.REPORT_FLAGS.HTML_IN_EMAIL;

					foreach( var EmailAddress in r.EmailAddresses )
					{
						var Address = EmailAddress.NullTrim();

						if( Address.IsValidEmailAddress() )
						{
							using var Client = new Client( context );

							if( IsCsv )
								Client.SendCsv( Address, r.EmailSubject, Csv, r.Name, r.PlainText );

							else if( ( Flags & Protocol.Data.Report.REPORT_FLAGS.PDF_HTML ) == Protocol.Data.Report.REPORT_FLAGS.PDF_HTML )
								Client.SendHtmlAndPdf( Address, r.EmailSubject, Html, r.Name, PdfBytes );

							else if( ( Flags & Protocol.Data.Report.REPORT_FLAGS.PDF ) != 0 )
								Client.SendPdf( Address, r.EmailSubject, r.Name, PdfBytes, r.PlainText );

							else if( ( Flags & Protocol.Data.Report.REPORT_FLAGS.HTML_IN_EMAIL ) != 0 )
								Client.SendHtml( Address, r.EmailSubject, Html );
						}
					}
				}

				Result.HtmlReport = ( r.ReportFlags & Protocol.Data.Report.REPORT_FLAGS.PRINT ) != 0 ? Html : "";
				Result.Csv        = Csv;
				Result.Pdf        = PdfBytes;
			}
			else
				Result.ReportFlags = Protocol.Data.Report.REPORT_FLAGS.NOTHING_FOUND;
		}
		catch( Exception E )
		{
			context.SystemLogExceptionProgram( r.Name, E );
		}

		return Result;
	}
}