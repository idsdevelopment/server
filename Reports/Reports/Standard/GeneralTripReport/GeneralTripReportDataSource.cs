﻿// ReSharper disable InconsistentNaming

using Database.Model.Databases.Carrier;

//using StorageV2.Carrier;


namespace Reports.Reports.Standard.GeneralTripReport;  

internal sealed class GeneralTripReportDataSource : DataSource    
{
    public sealed record Global(string FromDate, string ToDate, string TotalShipments, string TotalCharge, string Account, string LastId);

    public sealed record GeneralTripReportItem(string ShipmentId, string Date, string Driver, string ST,
                                                string Service, string Package, string Shipper,
                                                string PUCompany, string PUAddress, string DELCompany, string DELAddress,
                                                string Reference, string Pieces, string Weight, string Charge);

    public GeneralTripReportDataSource(IRequestContext context, Report options)
    {
        var Track = "";
        try
        {
            var FromDate = options.DateTimeArg1.StartOfDay();
            var ToDate = options.DateTimeArg2.EndOfDay();

            var FromStatus = (STATUS)options.DecimalArg1;
            var ToStatus = (STATUS)options.DecimalArg2;

            var CompanyCode = options.StringArg1;
            var CompanyName = "";

            using var Db = new CarrierDb(context);

            if (CompanyCode == "All")
            {
                CompanyName = "All Accounts";
            }
            else
            {
                var Company = Db.GetCompanyByCompanyNumber(CompanyCode);
                CompanyName = Company.Company.CompanyName;
            }

            // Search by CALL, VER or PUVER?
            var Recs =
                (from T in Db.SearchTripsBy("CALL", FromDate, ToDate, FromStatus, ToStatus)
                 where (CompanyCode.Equals("All", StringComparison.CurrentCultureIgnoreCase) || CompanyCode.Equals(T.AccountId, StringComparison.CurrentCultureIgnoreCase)) // && T.AccountId.IsNotNull())
                 orderby T.CallTime
                 select T).ToList();

            Track = $"{CompanyCode} && {CompanyCode.Equals("All", StringComparison.CurrentCultureIgnoreCase)} From: {FromStatus.AsString()}/{FromDate.Date:D}, To {ToStatus.AsString()}/{ToDate.Date:D} ";//Recs.Count.ToString();//CompanyCode;//

            var TotalShipments = Recs.Count;
            var TotalCharge    = 0.00m;

            //static decimal BuildCharges(List<TripCharge> charges)
            //{
            //    var Result = 0.00m;

            //    foreach (var C in charges)
            //        Result += C.Value;

            //    return Result != 0.00m ? Result : 0.00m;
            //}

            foreach (var T in Recs)
            {
                //var charge = BuildCharges(T.TripCharges);
                var charge = T.TotalFixedAmount;// IDS 1 report appears to contain total before taxes
                TotalCharge += charge;

                Add(new GeneralTripReportItem(
                                               T.TripId,
                                               //$"{T.VerifiedTime:yyyy/MM/dd hh:mm tt}",
                                               $"{T.CallTime:yyyy/MM/dd hh:mm tt}",
                                               T.Driver,
                                               T.Status1.ToString(),
                                               T.ServiceLevel,
                                               T.PackageType,
                                               T.AccountId,
                                               T.PickupCompanyName,
                                               T.PickupAddressSuite.IsNotNullOrEmpty() ? $"{T.PickupAddressSuite}-{T.PickupAddressAddressLine1}" : T.PickupAddressAddressLine1, // whole address? Too long?
                                               T.DeliveryCompanyName,
                                               T.DeliveryAddressSuite.IsNotNullOrEmpty() ? $"{T.DeliveryAddressSuite}-{T.DeliveryAddressAddressLine1}" : T.DeliveryAddressAddressLine1,
                                               T.Reference,
                                               T.Pieces.ToString("0"),
                                               T.Weight.ToString("0.0"), //$"{T.Weight:N0}"
                                               charge.ToString("C")// IDS 1 report appears to contain total before taxes
                                              ));
            }

            Globals = new Global($"{FromDate.Date:D}", $"{ToDate.Date:D}", $"{TotalShipments}", TotalCharge.ToString("C"), CompanyName, Recs[Recs.Count - 1].TripId); //  $"{TotalCharge}"
        }
        catch (Exception Exception)
        {
            context.SystemLogException(Exception);
            //Add(new GeneralTripReportItem(
            //    $"Exception{Exception.Message}", $"{Track}", "", "", "", "", "", "", "", "", "", "", "", "", ""
            //    ));
        }
    }
}