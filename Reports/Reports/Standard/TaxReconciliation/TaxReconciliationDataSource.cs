﻿// ReSharper disable InconsistentNaming

using Database.Model.Databases.Carrier;

namespace Reports.Reports.Standard.TaxReconciliation;

internal sealed class TaxReconciliationDataSource : DataSource
{
	public TaxReconciliationDataSource( IRequestContext context, Report options )
	{
		try
		{
			var FromDate = options.DateTimeArg1.StartOfDay();
			var ToDate   = options.DateTimeArg2.EndOfDay();

			using var Db = new CarrierDb( context );

			var Pref = Db.GetPreferences();

			string GetPreference( string description )
			{
				return ( from P in Pref
				         where P.Description.Contains( description )
				         select P.StringValue ).FirstOrDefault() ?? "";
			}

			var CarrierName = GetPreference( "Company Name" );

			var getinvoice = new GetInvoices { FromDateTime = FromDate, ToDateTime = ToDate };

			//	var invoicesandtrips = Db.GetInvoicesAndTrips( getinvoice ); // I don't think I need trips // I do if I don't want to have to look up account id
			//	var invoices         = Db.GetInvoices( getinvoice );
			//var InvoicesByAccount = (from I in Db.GetInvoices(getinvoice)

			var InvoicesByAccount = ( from I in Db.GetInvoicesAndTrips( getinvoice )
			                          let UAccount = I.BillingCompanyName
			                          //where StaffIds.Contains(UDriver) // I don't think I have to do a lookup, might need to - validity check later then 
			                          orderby UAccount, I.InvoiceDateTime, I.InvoiceTo
			                          group I by UAccount
			                          into Ct
			                          let Account = Ct.Key
			                          select new
			                                 {
				                                 Account,
				                                 Invoices = Ct.ToList()
			                                 } ).ToList();

			var TotalAmountIncludingTax = 0.00m;
			var TotalTax                = 0.00m;
			var LastLine                = "";

			foreach( var A in InvoicesByAccount )
			{
				var AccountTotalAmountIncludingTax = 0.00m;
				var AccountTotalTax                = 0.00m;

				var AccountInvoices = new List<TaxReconItem>();

				foreach( var I in A.Invoices )
				{
					AccountTotalAmountIncludingTax += I.TotalValue;
					AccountTotalTax                += I.TotalTaxA;

					AccountInvoices.Add( new TaxReconItem(
					                                      I.InvoiceNumber.ToString(),
					                                      $"{I.InvoiceTo:yyyy/MM/dd}",       // date MM/DD/YYYY ?
					                                      $"{I.InvoiceDateTime:yyyy/MM/dd}", // date MM/DD/YYYY ?
					                                      I.TotalValue.ToString( "C" ),
					                                      I.TotalTaxA.ToString( "C" )
					                                     ) );
					LastLine = I.InvoiceNumber.ToString();
				}
				TotalAmountIncludingTax += AccountTotalAmountIncludingTax;
				TotalTax                += AccountTotalTax;

				var AccountSummary = new TaxReconAccount( A.Account, A.Invoices[ 0 ].Trips[ 0 ].AccountId, AccountTotalAmountIncludingTax.ToString( "C" ), AccountTotalTax.ToString( "C" ) );
				Add( new TaxReconPerAccountItem( AccountInvoices, AccountSummary ) );
			}

			Globals = new Global( $"{FromDate.Date:D}", $"{ToDate.Date:D}", CarrierName, TotalAmountIncludingTax.ToString( "C" ), TotalTax.ToString( "C" ), LastLine );
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
			//Add(new GLItem(Exception.Message, "", "", "")); //test line
			Globals = new Global( "Error: ", $"{Exception.Message}", "", "", "", "" );
		}
	}

	public sealed record Global( string FromDate, string ToDate, string CarrierName, string TotalIncTax, string TotalTax, string LastLine );

	public sealed record TaxReconAccount( string AccountName, string AccountId, string ActTotAmtIncTax, string ActTotTax );

	public sealed record TaxReconItem( string InvoiceId, string PeriodEnd, string Created, string AmtIncTax, string Tax );

	public sealed record TaxReconPerAccountItem(
		List<TaxReconItem> Items,
		TaxReconAccount TaxReconAccount );
}