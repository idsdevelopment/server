﻿using Reports.Reports.Standard.CallTakerRevenue;
using Utils.Csv;

namespace Reports;

internal sealed class CallTakerRevenue : ReportsModule.Reports
{
	protected override string TemplateName => @"Standard\CallTakerRevenue\CallTakerRevenue.html";
	public CallTakerRevenue( IRequestContext context, Report options ) : base( context, new CallTakerRevenueDataSource( context, options ), options )
	{
	}

    public override string ToCsv()
    {
        var Csv = new Csv();
        if (Data.Source is { } Source)
        {
            //var Row = Csv[0];
            var Globals = (CallTakerRevenueDataSource.Global)Source.Globals;
            Csv[0][0].AsString = "Call Taker Revenue Report";
            Csv[1][0].AsString = "";

            Csv[2][0].AsString = "Dates Selected:";
            Csv[2][1].AsString = $"{Globals.FromDate} - {Globals.ToDate}";
            Csv[3][0].AsString = "Printed On:";
            Csv[3][1].AsString = $"{DateTime.Now:D}";
            Csv[4][0].AsString = "Branch:";
            Csv[4][1].AsString = Globals.Branch;

            Csv[1][4].AsString = "Jobs";
            Csv[1][5].AsString = "Revenue";
            Csv[2][3].AsString = "CSR:";
            Csv[2][4].AsString = Globals.CSRTotalJobs;
            Csv[2][5].AsString = (Globals.CSRRevenue);
            Csv[3][3].AsString = "Internet:";
            Csv[3][4].AsString = (Globals.WebTotalJobs);
            Csv[3][5].AsString = (Globals.WebRevenue);
            Csv[4][3].AsString = "Total:";
            Csv[4][4].AsString = (Globals.TotalJobs);
            Csv[4][5].AsString = (Globals.TotalRevenue);

            //AsDecimal

            Csv[5][0].AsString = "";
            var RowHeaders = Csv[6];
            RowHeaders[0].AsString = "Operator";
            RowHeaders[1].AsString = "Total Jobs"; //AsDecimal 

            var i = 7;
            foreach (var source in Source)
            {
                var row = Csv[i];
                var s = (CallTakerRevenueDataSource.CallTakerRevenueItem)source;
                row[0].AsString = s.Operator;
                row[1].AsString = s.TotalJobs;

                i++;
            }

        }

        return Csv.ToString();
    }
}