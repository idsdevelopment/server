﻿using Reports.Reports.Standard.InvoiceReport;

namespace Reports;

internal sealed class InvoiceReport : ReportsModule.Reports
{
	protected override string TemplateName => @"Standard\InvoiceReport\InvoiceReport.html";

	public InvoiceReport( IRequestContext context, Report options ) : base( context, new InvoiceReportDataSource( context, options ), options )
	{
	}
}