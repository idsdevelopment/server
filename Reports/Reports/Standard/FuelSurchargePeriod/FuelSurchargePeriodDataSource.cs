﻿using Database.Model.Databases.Carrier;

// ReSharper disable InconsistentNaming

namespace Reports.Reports.Standard.FuelSurchargePeriod;

internal sealed class FuelSurchargePeriodDataSource : DataSource 
{
	public sealed record Global( string FromDate,                            string ToDate,                             string CarrierName,                     string SelectedTruck,
								 string PeriodTotalJobs,                     string PeriodTotalServiceCommisionable,    string PeriodTotalServiceComPercentage, string PeriodTotalServiceComAmount,
								 string PeriodTotalFuelChargeCommissionable, string PeriodTotalFuelChargeComPercentage, string PeriodTotalFuelChargeComAmount,
								 string PeriodTotalCom,                      string PeriodTotalCommissionDue,           string LastLine );

	public sealed record DriverLineItem( string DriverNumber,                       string DriverFirstName,                   string DriverLastName,
										 string TruckTotalJobs,                     string TruckTotalServiceCommisionable,    string TruckTotalServiceComPercentage, string TruckTotalServiceComAmount,
										 string TruckTotalFuelChargeCommissionable, string TruckTotalFuelChargeComPercentage, string TruckTotalFuelChargeComAmount,
										 string TruckTotalCom,                      string TruckTotalCommissionDue
	);

	public FuelSurchargePeriodDataSource( IRequestContext context, Report options )
	{
		try
		{
			var FromDate = options.DateTimeArg1.StartOfDay();
			var ToDate   = options.DateTimeArg2.EndOfDay(); //AddDays(1);// EndOfDay(); 
			//SelectedReportSettings.SelectedTo = SelectedTo.AddDays( 1 ); // Set it to the beginning of the next day (12:00 am) // This can/is also done in reports themselves by using EndOfDay()
			var SelectedTruck = options.StringArg1; // probably going to be all 

			int     PeriodTotalJobs                 = 0;
			decimal PeriodTotalServiceCommisionable = 0m;
			//	decimal PeriodTotalServiceComPercentage = 0m;
			decimal PeriodTotalServiceComAmount = 0m;

			decimal PeriodTotalFuelChargeCommissionable = 0m;
			//		decimal PeriodTotalFuelChargeComPercentage = 0m;
			decimal PeriodTotalFuelChargeComAmount = 0m;

			decimal PeriodTotalCom           = 0m;
			decimal PeriodTotalCommissionDue = 0m;

			var LastDriver = "";

			using var Db            = new CarrierDb( context );
			var       E             = Db.Entity;
			var       ChargeDetails = E.ChargeDetails.ToDictionary( x => x.ChargeId, x => x );

			var Pref = Db.GetPreferences();

			var CarrierName = ( from P in Pref
								where P.Description.Contains( "Company Name" )
								select P ).FirstOrDefault()
										  ?.StringValue ?? "";

			var Staff = Db.GetStaff();

			var StaffIds = ( from S in Staff
							 select S.StaffId.ToUpper() ).ToList();

			var Recs = ( from T in Db.SearchTripsByDelByStatus( FromDate, ToDate, STATUS.POSTED, STATUS.FINALISED )
			             let UDriver = T.Driver.ToUpper()
			             where StaffIds.Contains( UDriver )
						 orderby UDriver, T.VerifiedTime
						 group T by UDriver
						 into Ct
						 let Driver = Ct.Key
						 select new
								{
									Driver,
									StaffRec = Staff.FirstOrDefault( s => s.StaffId.Compare( Driver, StringComparison.OrdinalIgnoreCase ) == 0 ),
									Trips    = Ct.ToList()
								} ).ToList();

			foreach( var Rec in Recs )
			{
				if( SelectedTruck.Equals( "All", StringComparison.InvariantCultureIgnoreCase ) || Rec.Driver.Equals( SelectedTruck, StringComparison.InvariantCultureIgnoreCase ) )
				{
					int TruckTotalJobs = Rec.Trips.Count;

					decimal TruckTotalServiceCommisionable = 0m;
					decimal TruckTotalServiceComPercentage = Rec.StaffRec.MainCommission1; // or main commission 1? //Rec.StaffRec.TotalCommission;
					decimal TruckTotalServiceComAmount     = 0m;

					decimal TruckTotalFuelChargeCommissionable = 0m;
					decimal TruckTotalFuelChargeComPercentage  = Rec.StaffRec.MainCommission1; // is this a different amount? //TotalCommission
					decimal TruckTotalFuelChargeComAmount      = 0m;

					foreach( var T in Rec.Trips )
					{
						TruckTotalServiceCommisionable += T.TotalPayrollAmount;
						foreach( var C in T.TripCharges )
						{
							if( ChargeDetails[ C.ChargeId ].IsFuelSurcharge )
							{
								TruckTotalFuelChargeCommissionable += C.Value;
							}
						}
					}
					TruckTotalServiceComAmount = (TruckTotalServiceCommisionable * TruckTotalServiceComPercentage);
					TruckTotalFuelChargeComAmount = (TruckTotalFuelChargeComPercentage * TruckTotalFuelChargeCommissionable);

					decimal TruckTotalCom           = TruckTotalServiceComAmount + TruckTotalFuelChargeComAmount; //TruckTotalServiceCommisionable + TruckTotalFuelChargeCommissionable;
					decimal TruckTotalCommissionDue = TruckTotalCom;

					Add( new DriverLineItem(
											Rec.Driver,
											Rec.StaffRec.FirstName,
											Rec.StaffRec.LastName,
											TruckTotalJobs.ToString(),
											TruckTotalServiceCommisionable.ToString( "C" ),
											TruckTotalServiceComPercentage.ToString( "P0" ),
											TruckTotalServiceComAmount.ToString( "C" ),
											TruckTotalFuelChargeCommissionable.ToString( "C" ),
											TruckTotalFuelChargeComPercentage.ToString( "P0" ),
											TruckTotalFuelChargeComAmount.ToString( "C" ),
											TruckTotalCom.ToString( "C" ),
											TruckTotalCommissionDue.ToString( "C" )
										   ) );

					PeriodTotalJobs += TruckTotalJobs;

					PeriodTotalServiceCommisionable += TruckTotalServiceCommisionable;
					//PeriodTotalServiceComPercentage = 0m; // leave blank
					PeriodTotalServiceComAmount += TruckTotalServiceComAmount;

					PeriodTotalFuelChargeCommissionable += TruckTotalFuelChargeCommissionable;
					//PeriodTotalFuelChargeComPercentage = 0m; //""
					PeriodTotalFuelChargeComAmount += TruckTotalFuelChargeComAmount;

					PeriodTotalCom           += TruckTotalCom;
					PeriodTotalCommissionDue += TruckTotalCommissionDue;

					LastDriver = Rec.Driver;
				}
			}
			//Add(new DriverLineItem(
			//			SelectedTruck,
			//			"", "", "", "", "", "", "", "", "", "", ""
			//			));

			// Globals
			Globals = new Global( $"{FromDate.Date:D}", $"{ToDate.Date:D}", CarrierName, SelectedTruck, PeriodTotalJobs.ToString(),
								  //Globals = new Global($"{FromDate.Date:D}", $"{ToDate:MM/dd/yyyy HH:mm}", CarrierName, SelectedTruck, PeriodTotalJobs.ToString(),
								  PeriodTotalServiceCommisionable.ToString( "C" ), "", PeriodTotalServiceComAmount.ToString( "C" ),
								  PeriodTotalFuelChargeCommissionable.ToString( "C" ), "", PeriodTotalFuelChargeComAmount.ToString( "C" ),
								  PeriodTotalCom.ToString( "C" ), PeriodTotalCommissionDue.ToString( "C" ), LastDriver
								);
		}

		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
	}
}