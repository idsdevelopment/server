﻿using Reports.Reports.Standard.FuelSurchargeDaily;
using Utils.Csv;

namespace Reports;

internal sealed class FuelSurchargeDaily : ReportsModule.Reports
{
	protected override string TemplateName => @"Standard\FuelSurchargeDaily\FuelSurchargeDaily.html"; 

	public FuelSurchargeDaily(IRequestContext context, Report options) : base(context, new FuelSurchargeDailyDataSource(context, options), options)
	{
	}

    public override string ToCsv()
    {
        var Csv = new Csv();
        if (Data.Source is { } Source)
        {
            var Globals = (FuelSurchargeDailyDataSource.Global)Source.Globals;

            Csv[0][0].AsString = "Fuel Surcharge Daily Report";
            Csv[1][0].AsString = "";

            Csv[2][0].AsString = "Dates Selected:";
            Csv[2][1].AsString = $"{Globals.FromDate} - {Globals.ToDate}";
            Csv[3][0].AsString = "Printed On:";
            Csv[3][1].AsString = $"{DateTime.Now:D}";

            var i = 4;
            foreach (var source in Source)
            {
                Csv[i][0].AsString = "";
                i++;

                var s = (FuelSurchargeDailyDataSource.FuelSurchargeDailyItem)source;

                // Driver Info
                Csv[i][0].AsString = s.Driver.Truck;
                Csv[i][1].AsString = $"{s.Driver.DriverFirstName} {s.Driver.DriverLastName}";
                i++;

                // Headers
                var RowHeaders = Csv[i];
                RowHeaders[0].AsString = "Date";
                RowHeaders[1].AsString = "Total Shipments";
                RowHeaders[2].AsString = "Total Service Com'able";
                RowHeaders[3].AsString = "Total Service Com";
                RowHeaders[4].AsString = "Fuel Chrg Com'able";
                RowHeaders[5].AsString = "Fuel Chrg Com";
                RowHeaders[6].AsString = "Total Com";
                RowHeaders[6].AsString = "Cash Collected";
                RowHeaders[6].AsString = "Fixed Chrg Deductions";
                RowHeaders[6].AsString = "Comm Due";

                i++;

                foreach (var da in s.Items)
                {
                    var row = Csv[i];
                    row[0].AsString = da.Date;
                    row[1].AsString = da.DailyTotalShipments;
                    row[2].AsString = da.DailyTotalServiceCommisionable;
                    row[3].AsString = da.DailyTotalServiceCommision;
                    row[4].AsString = da.DailyTotalFuelCommisionable;
                    row[5].AsString = da.DailyTotalFuelCommision;
                    row[6].AsString = da.DailyTotalCom;
                    row[7].AsString = da.DailyCashCollected;
                    row[8].AsString = da.DailyFixedChargeDeductions;
                    row[9].AsString = da.DailyCommDue;

                    i++;
                }

                var TotalsRow = Csv[i];
                TotalsRow[0].AsString = "Totals";
                TotalsRow[1].AsString = s.Driver.TruckTotalShipments;
                TotalsRow[2].AsString = s.Driver.TruckTotalServiceCommisionable;
                TotalsRow[3].AsString = s.Driver.TruckTotalServiceCommision;
                TotalsRow[4].AsString = s.Driver.TruckTotalFuelCommisionable;
                TotalsRow[5].AsString = s.Driver.TruckTotalFuelCommision;
                TotalsRow[6].AsString = s.Driver.TruckTotalCom;
                TotalsRow[7].AsString = s.Driver.TotalCashCollected;
                TotalsRow[8].AsString = s.Driver.TotalFixedChargeDeductibles;
                TotalsRow[9].AsString = s.Driver.TotalCommDue;

                i++;
            }

        }

        return Csv.ToString();
    }
}