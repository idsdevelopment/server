﻿using Database.Model.Databases.Carrier;

// ReSharper disable InconsistentNaming

namespace Reports.Reports.Standard.FuelSurchargeDaily;

internal sealed class FuelSurchargeDailyDataSource : DataSource  
{
	public sealed record Global(string FromDate, string ToDate);

	public sealed record Driver( string DriverFirstName,                string DriverLastName, string Truck, string TruckTotalShipments,
								 string TruckTotalServiceCommisionable, string TruckTotalServiceCommision,
								 string TruckTotalFuelCommisionable,    string TruckTotalFuelCommision,
								 string TruckTotalCom,
								 string TotalCashCollected, string TotalFixedChargeDeductibles, // these appear blank on examples
								 string TotalCommDue );

	public sealed record DriverLineItem( string Date,                           string DailyTotalShipments,
										 string DailyTotalServiceCommisionable, string DailyTotalServiceCommision,
										 string DailyTotalFuelCommisionable,    string DailyTotalFuelCommision,
										 string DailyTotalCom,
										 string DailyCashCollected, string DailyFixedChargeDeductions,
										 string DailyCommDue );

	public sealed record FuelSurchargeDailyItem( List<DriverLineItem> Items,
												 Driver               Driver );

	public FuelSurchargeDailyDataSource( IRequestContext context, Report options )
	{
		try
		{
			var FromDate      = options.DateTimeArg1.StartOfDay();
			var ToDate        = options.DateTimeArg2.EndOfDay();
			var SelectedTruck = options.StringArg1;

			Globals = new Global($"{FromDate.Date:D}", $"{ToDate.Date:D}");

			using var Db            = new CarrierDb( context );
			var       E             = Db.Entity;
			var       ChargeDetails = E.ChargeDetails.ToDictionary( x => x.ChargeId, x => x );

			var Staff = Db.GetStaff();

			//var howtogetcommissionamounts = Db.PriorityGetUnCalculatedDriverCommissionForDriver
			var StaffIds = ( from S in Staff
							 select S.StaffId.ToUpper() ).ToList();

			var DriverShipments = ( from T in Db.SearchTripsByDelByStatus( FromDate, ToDate, STATUS.POSTED, STATUS.FINALISED )
									let UDriver = T.Driver.ToUpper()
									where StaffIds.Contains( UDriver )
									orderby UDriver, T.VerifiedTime
									group T by UDriver
									into Ct
									let Driver = Ct.Key
									select new
										   {
											   Driver,
											   StaffRec = Staff.FirstOrDefault( s => s.StaffId.Compare( Driver, StringComparison.OrdinalIgnoreCase ) == 0 ),
											   Trips    = Ct.ToList()
										   } ).ToList();

			var dateCutoff = new DateTimeOffset( 2000, 1, 1, 1, 1, 1, new TimeSpan( 1, 0, 0 ) );

			decimal GetFuelSurcharge( List<TripCharge> charges )
			{
				var fuelcharges = 0m;

				foreach( var C in charges )
				{
					if( ChargeDetails[ C.ChargeId ].IsFuelSurcharge )
					{
						fuelcharges += C.Value;
					}
				}
				return fuelcharges;
			}

			foreach( var DS in DriverShipments )
			{
				int     TruckTotalShipments             = 0;
				decimal TruckTotalServiceCommisionable  = 0m;
				decimal TruckTotalServiceCom            = 0m;
				decimal TruckTotalFuelCommisionable     = 0m;
				decimal TruckTotalFuelCommision         = 0m;
				decimal TruckTotalComm                  = 0m;
				decimal TruckTotalCashCollected         = 0m;
				decimal TruckTotalFixedChargeDeductions = 0m;
				decimal TruckTotalCommDue               = 0m;

				if( SelectedTruck == "All" || DS.Driver == SelectedTruck )
				{
					TruckTotalShipments += DS.Trips.Count;
					decimal TruckServiceComPercentage = DS.StaffRec.MainCommission1;
					decimal TruckFuelComPercentage    = DS.StaffRec.MainCommission1; // does this come from a different value?

					var Daily = ( from T in DS.Trips
								  let UDate = T.VerifiedTime
								  orderby T.VerifiedTime
								  group T by UDate.Date
								  //group T by T.AccountId
								  into Ct
								  let Date = Ct.Key
								  select new
										 {
											 Date,
											 Trips = Ct.ToList()
										 } ).ToList();

					var DailyShipments = new List<DriverLineItem>();

					foreach( var D in Daily )
						//foreach (var D in DS.Trips)
					{
						//...
						var Date                           = $"{D.Date:dd MMMM yyyy}";
						var DailyTotalShipments            = D.Trips.Count;
						var DailyTotalServiceCommisionable = 0.00m;
						var DailyTotalServiceCommision     = 0.00m;
						var DailyFuelChargeCommissionable  = 0.00m;
						var DailyFuelChargeCommission      = 0.00m;
						var DailyTotalCom                  = 0.00m;
						var DailyCashCollected             = 0.00m;
						var DailyFixedChargeDeductions     = 0.00m;
						var DailyCommDue                   = 0.00m;

						foreach( var T in D.Trips )
						{
							DailyTotalServiceCommisionable += T.TotalPayrollAmount;
							DailyFuelChargeCommissionable  += GetFuelSurcharge( T.TripCharges );
						}
						DailyTotalServiceCommision = DailyTotalServiceCommisionable * TruckServiceComPercentage;
						DailyFuelChargeCommission  = DailyFuelChargeCommissionable * TruckFuelComPercentage;
						DailyTotalCom              = DailyTotalServiceCommision + DailyFuelChargeCommission;
						DailyCashCollected         = 0.00m; // where to get this? 
						DailyFixedChargeDeductions = 0.00m; // where to get this? 
						DailyCommDue               = DailyTotalCom;

						DailyShipments.Add( new DriverLineItem(
															   Date,
															   DailyTotalShipments.ToString(),
															   DailyTotalServiceCommisionable.ToString( "C" ),
															   DailyTotalServiceCommision.ToString( "C" ),
															   DailyFuelChargeCommissionable.ToString( "C" ),
															   DailyFuelChargeCommission.ToString( "C" ),
															   DailyTotalCom.ToString( "C" ),
															   //DailyCashCollected.ToString("C"),
															   //DailyFixedChargeDeductions.ToString("C"),
															   "", "",
															   DailyCommDue.ToString( "C" )
															  ) );

						//TruckTotalShipments += DailyTotalShipments;
						TruckTotalServiceCommisionable  += DailyTotalServiceCommisionable;
						TruckTotalServiceCom            += DailyTotalServiceCommision;
						TruckTotalFuelCommisionable     += DailyFuelChargeCommissionable;
						TruckTotalFuelCommision         += DailyFuelChargeCommission;
						TruckTotalComm                  += DailyTotalCom;
						TruckTotalCashCollected         += DailyCashCollected;
						TruckTotalFixedChargeDeductions += DailyFixedChargeDeductions;
						TruckTotalCommDue               += DailyCommDue;
					}

					Add( new FuelSurchargeDailyItem(
													DailyShipments,
													new Driver( DS.StaffRec.FirstName, DS.StaffRec.LastName, DS.Driver, //DS.StaffRec.StaffId,//
																//new Driver(TruckServiceComPercentage.ToString("P0"), TruckFuelComPercentage.ToString("P0"), DS.Driver,
																TruckTotalShipments.ToString(),
																TruckTotalServiceCommisionable.ToString( "C" ),
																TruckTotalServiceCom.ToString( "C" ),
																TruckTotalFuelCommisionable.ToString( "C" ),
																TruckTotalFuelCommision.ToString( "C" ),
																TruckTotalComm.ToString( "C" ),
																//TruckTotalCashCollected.ToString("C"),
																//TruckTotalFixedChargeDeductions.ToString("C"),
																"", "", // more obvious that they aren't coming from anywhere
																TruckTotalCommDue.ToString( "C" ) )
												   ) );
				}
			}
		}

		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
	}
}