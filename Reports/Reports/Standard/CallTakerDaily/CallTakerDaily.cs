﻿using Reports.Reports.Standard.CallTakerDaily;
using Utils.Csv;

namespace Reports;

internal sealed class CallTakerDaily : ReportsModule.Reports
{
	protected override string TemplateName => @"Standard\CallTakerDaily\CallTakerDaily.html";
	public CallTakerDaily( IRequestContext context, Report options ) : base( context, new CallTakerDailyDataSource( context, options ), options )
	{
    
	}

    public override string ToCsv() 
    {
        var Csv = new Csv();
        if (Data.Source is { } Source) 
        {
            var Globals = (CallTakerDailyDataSource.Global)Source.Globals;
            Csv[0][0].AsString = "Call Taker Daily Report";
            Csv[1][0].AsString = "";

            Csv[2][0].AsString = "Dates Selected:";
            Csv[2][1].AsString = $"{Globals.FromDate} - {Globals.ToDate}";
            Csv[3][0].AsString = "Printed On:";
            Csv[3][1].AsString = $"{DateTime.Now:D}";
            Csv[4][0].AsString = "Branch:";
            Csv[4][1].AsString = Globals.Branch;

            Csv[1][4].AsString = "Jobs";
            Csv[2][3].AsString = "CSR:";
            Csv[2][4].AsString = Globals.CSRTotalJobs;
            Csv[3][3].AsString = "Internet:";
            Csv[3][4].AsString = Globals.WebTotalJobs;
            Csv[4][3].AsString = "Total:";
            Csv[4][4].AsString = Globals.TotalJobs;

            //AsDecimal

            Csv[5][0].AsString = "";
            var RowHeaders = Csv[6];
            RowHeaders[0].AsString = "Operator";
            RowHeaders[1].AsString = "Total Jobs"; //AsDecimal 
            
            var i = 7;
            foreach (var source in Source) 
            {
                var row = Csv[i];
                var s = (CallTakerDailyDataSource.CallTakerDailyItem)source;
                row[0].AsString = s.Operator;
                row[1].AsString = s.TotalJobs;

                i++;
            }













            //Row[3].AsString = Data.DataSourceName;
            //Row[4].AsString = Data.Header;
            //Row[5].AsString = Data.CurrentRecord.ToString();
            //CallTakerDailyDataSource isthisdata = (CallTakerDailyDataSource)Data.Source[0];
            //var hi = (CallTakerDailyDataSource.Global)isthisdata.Globals;
            //Row[6].AsString = hi.WebTotalJobs;

            //var hii = (CallTakerDailyDataSource.CallTakerDailyItem)isthisdata[0];
            //Row[7].AsString = hii.Operator;

            //Row[3].AsString = Source[0].ToString();
            //Row[4].AsString = Source.Globals.ToString();
            //Row[5].AsString = Data.CurrentRecord.ToString();

            //var firstrecord = Source[0];
            //DoNext();



        }

        return Csv.ToString();
    }
}