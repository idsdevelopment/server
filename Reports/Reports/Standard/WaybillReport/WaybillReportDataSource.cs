﻿using System.Net;
using Database.Model.Databases.Carrier;
using Signatures = StorageV2.Carrier.Signatures;

//using System.Drawing.Imaging;  


namespace Reports.Reports.Standard.WaybillReport;

internal class WaybillReportDataSource : DataSource
{
	public sealed record Global( string Carrier, string FromDate, string ToDate, string HowMany, string LogoString,
	                             string CarrierName, string Suite, string Street, string City, string Postal, string Region,
	                             string Tel, string Fax, string Email, string ShipmentId );

	public sealed record WaybillReportItem( string ShipmentId, string PackageType, string ServiceLevel, string Reference, string Pieces,
	                                        string ReadyTime, string Billing, string Driver, string Pod, string DeliveryDate, string Weight, string Volume,
	                                        string PickupCompanyName, string PickupAddressAddressLine1,
	                                        string PickupAddressCity, string PickupAddressRegion, string PickupAddressCountry, string PickupAddressPostalCode,
	                                        string PickupContactName, string PickupContactPhone, string PickupNotes,
	                                        string DeliveryCompanyName, string DeliveryAddressAddressLine1,
	                                        string DeliveryAddressCity, string DeliveryAddressRegion, string DeliveryAddressCountry, string DeliveryAddressPostalCode,
	                                        string DeliveryContactName, string DeliveryContactPhone, string DeliveryNotes,
	                                        string PickupSignature, string DeliverySignature )
	{
		public WaybillReportItem( string shipmentId, string packageType, string serviceLevel, string reference, decimal pieces,
		                          string readyTime, string billing, string driver, string pod, string deliveryDate, string weight, string volume,
		                          string pickupCompanyName, string pickupAddressAddressLine1,
		                          string pickupAddressCity, string pickupAddressRegion, string pickupAddressCountry, string pickupAddressPostalCode,
		                          string pickupContactName, string pickupContactPhone, string pickupNotes,
		                          string deliveryCompanyName, string deliveryAddressAddressLine1,
		                          string deliveryAddressCity, string deliveryAddressRegion, string deliveryAddressCountry, string deliveryAddressPostalCode,
		                          string deliveryContactName, string deliveryContactPhone, string deliveryNotes,
		                          string pickupSignature, string deliverySignature )
			: this( shipmentId, packageType, serviceLevel, reference, pieces.ToString( "0" ),
			        readyTime, billing, driver, pod, deliveryDate, weight, volume,
			        pickupCompanyName, pickupAddressAddressLine1,
			        pickupAddressCity, pickupAddressRegion, pickupAddressCountry, pickupAddressPostalCode,
			        pickupContactName, pickupContactPhone, pickupNotes,
			        deliveryCompanyName, deliveryAddressAddressLine1,
			        deliveryAddressCity, deliveryAddressRegion, deliveryAddressCountry, deliveryAddressPostalCode,
			        deliveryContactName, deliveryContactPhone, deliveryNotes,
			        pickupSignature, deliverySignature )
		{
		}
	}

	public WaybillReportDataSource( IRequestContext context, Report options )
	{
		try
		{
			var FromDate   = options.DateTimeArg1.StartOfDay();
			var ToDate     = options.DateTimeArg2.EndOfDay();
			var Carrier    = context.CarrierId;
			var ShipmentId = options.StringArg1;

		#region Logo
			string       LogoString;
			const string URL = "http://www.internetdispatcher.com/banners/phoenix.jpg";

 #pragma warning disable SYSLIB0014
			using( var Client = new WebClient() )
 #pragma warning restore SYSLIB0014
			{
				var DataBytes = Client.DownloadData( new Uri( URL ) );
				LogoString = Convert.ToBase64String( DataBytes );
			}
		#endregion

			using var Db = new CarrierDb( context );
			var       E  = Db.Entity;

			var Recs = ( from T in E.Trips
			             where T.TripId == ShipmentId
			             orderby T.CallTime
			             select T ).ToList();

			var HowMany = Recs.Count;

		#region Preferences
			var Pref = Db.GetPreferences();

			string GetPreference( string description )
			{
				return ( from P in Pref
				         where P.Description.Contains( description )
				         select P.StringValue ).FirstOrDefault() ?? "";
			}

			var CarrierName = GetPreference( "Company Name" );
			var Suite       = GetPreference( "Company Suite" );

			if( Suite.IsNotNullOrEmpty() )
				Suite = Suite + " - ";

			var Street = GetPreference( "Company Street" );
			var City   = GetPreference( "Company City" );
			var Postal = GetPreference( "Company Prov" );
			var Region = GetPreference( "Company Postal" );
			var Tel    = GetPreference( "Company Phone" );
			var Fax    = GetPreference( "Company Fax" );
			var Email  = GetPreference( "Admin Email" );
		#endregion

			//Globals = new Global(Carrier, $"{FromDate.Date:D}", $"{ToDate.Date:D}", HowMany.ToString(), logostring);//);//test, logostring );
			Globals = new Global( Carrier, $"{FromDate.Date:D}", $"{ToDate.Date:D}", HowMany.ToString(), LogoString,
			                      CarrierName, Suite, Street, City, Postal, Region, Tel, Fax, Email, ShipmentId );

		#region Signatures
			var Signatures = new Signatures( context );

			if( Recs.Count > 0 )
			{
				foreach( var Shipment in Recs )
				{
					var TripId = Shipment.TripId;

					var Sigs = Signatures.GetSignatures( TripId );

					Signature? PickupSignature   = null,
					           DeliverySignature = null;

					foreach( var Signature in Sigs )
					{
						switch( Signature.Status )
						{
						case < STATUS.PICKED_UP when PickupSignature is null:
							PickupSignature = Signature;
							break;

						case STATUS.PICKED_UP:
							PickupSignature = Signature;
							break;

						default:
							DeliverySignature ??= Signature;
							break;
						}
					}

					string ConvertSig( Signature? sig )
					{
						return sig is not null ? new List<Signature> { sig }.ToPngBase64() : "";
					}
				#endregion

					Add( new WaybillReportItem(
					                           TripId,
					                           Shipment.PackageType,
					                           Shipment.ServiceLevel,
					                           Shipment.Reference,
					                           Shipment.Pieces,
					                           Shipment.ReadyTime.ToString( "yyyy/MM/dd hh:mm tt" ),
					                           Shipment.AccountId, //shouldn't BillingAccountId be assigned? Came up blank 
					                           Shipment.DriverCode,
					                           Shipment.POD.IsNotNullOrEmpty() ? "Yes" : "No", // Is this value stored outside of the shipment?
					                           Shipment.DeliveredTime.ToString( "yyyy/MM/dd hh:mm tt" ),
					                           Shipment.Weight.ToString( "0" ),
					                           Shipment.Volume.ToString( "0" ),
					                           Shipment.PickupCompanyName,
					                           Shipment.PickupAddressAddressLine1,
					                           Shipment.PickupAddressCity,
					                           Shipment.PickupAddressRegion,
					                           Shipment.PickupAddressCountry,
					                           Shipment.PickupAddressPostalCode,
					                           Shipment.PickupContact,
					                           Shipment.PickupAddressPhone, // There are many phone numbers. List them all?
					                           Shipment.PickupNotes,
					                           Shipment.DeliveryCompanyName,
					                           Shipment.DeliveryAddressAddressLine1,
					                           Shipment.DeliveryAddressCity,
					                           Shipment.DeliveryAddressRegion,
					                           Shipment.DeliveryAddressCountry,
					                           Shipment.DeliveryAddressPostalCode,
					                           Shipment.DeliveryContact,
					                           Shipment.DeliveryAddressPhone, // There are many phone numbers. List them all?
					                           Shipment.DeliveryNotes,
					                           ConvertSig( PickupSignature ),
					                           ConvertSig( DeliverySignature )
					                          ) );
				}
			}
			else
			{
				Add( new WaybillReportItem( "", "", "", "", 0, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
				                            "", "", "", "", "", "", "", "", "", "" ) );
			}
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
	}
}