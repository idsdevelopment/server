﻿using Reports.Reports.Standard.WaybillReport;

namespace Reports;

internal sealed class WaybillReport : ReportsModule.Reports
{
	protected override string TemplateName => @"Standard\WaybillReport\WaybillReport.html"; 

	public WaybillReport(IRequestContext context, Report options) : base(context, new WaybillReportDataSource(context, options), options)
	{
	}
}