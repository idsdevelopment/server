﻿// ReSharper disable InconsistentNaming
using System.Globalization;
using Database.Model.Databases.Carrier;
using Database.Model.Databases.MasterTemplate;

namespace Reports.Reports.Standard.GLPostingTotals;

internal sealed class GLPostingTotalsDataSource : DataSource
{
	public class GLData
	{
		public string       ChargeId               { get; set; } = "";
		public ChargeDetail Details                { get; set; } = new();
		public decimal      TotalPerInvoicedPeriod { get; set; }
	}

	public sealed record Global( string FromDate, string ToDate, string AccountsReceivableControlTotal, string Adjustments, string Payments, string TotalInvoices, string LastLine );

	public sealed record GLItem( string GLCode, string GLDescription, string Debit, string Credit );

	public GLPostingTotalsDataSource( IRequestContext context, Report options )
	{
		try
		{
			var FromDate = options.DateTimeArg1.StartOfDay();
			var ToDate   = options.DateTimeArg2.EndOfDay();

			using var Db            = new CarrierDb( context );
			var       E             = Db.Entity;
			var       ChargeDetails = E.ChargeDetails;

			var AccountString = "account_pref_charge";

			var _IncludeCharges = ( from AC in ChargeDetails
									let _Charge = AC.ChargeId
									where AC.DisplayOnInvoice && !AC.IsText && !AC.ChargeId.Contains(AccountString) // remove account specific charges
									orderby AC.SortIndex
									select new GLData
										   {
											   ChargeId               = AC.ChargeId,
											   Details                = AC,
											   TotalPerInvoicedPeriod = 0.00m
										   } ).ToList();

			const string BaseChargeId    = "base";
			const string BaseChargeLabel = "Base Delivery Charge";

			var BaseCharge = new GLData
							 {
								 ChargeId = BaseChargeId,
								 Details = new ChargeDetail
										   {
											   ChargeId = BaseChargeId,
											   Label    = BaseChargeLabel,
											   Value    = 0.00m
										   },
								 TotalPerInvoicedPeriod = 0.00m
							 };
			_IncludeCharges.Insert( 0, BaseCharge );

			var getinvoice       = new GetInvoices {FromDateTime = FromDate, ToDateTime = ToDate};
			var invoicesandtrips = Db.GetInvoicesAndTrips( getinvoice );

			var           TotalInvoices = invoicesandtrips.Count;
			var           LastLine      = "";
			const decimal Payments      = 0.00m;
			const decimal Adjustments   = 0.00m;
			var           AcntRecConTot = 0.00m;

			foreach( var IT in invoicesandtrips )
			{
				foreach( var T in IT.Trips )
				{
					foreach( var C in T.TripCharges )
					{
						var ind = _IncludeCharges.FindIndex(item => C.ChargeId.Contains(item.ChargeId));


						if ( ind != -1 )
							_IncludeCharges[ ind ].TotalPerInvoicedPeriod += C.Value;
					}
					var indbase = _IncludeCharges.FindIndex( item => item.ChargeId == BaseChargeId );
					_IncludeCharges[ indbase ].TotalPerInvoicedPeriod += T.DeliveryAmount;
				}
			}

			foreach (var c in _IncludeCharges)
			{
				// Credits are blank ""
				Add( new GLItem( c.ChargeId, c.Details.Label, "", c.TotalPerInvoicedPeriod.ToString( "C" ) ) ); 
				AcntRecConTot += c.TotalPerInvoicedPeriod;
				// values will be replaced with totals
				LastLine = c.ChargeId;
			}

			//Add(new GLItem("", "Accounts Receivable Control Total", AcntRecConTot.ToString("C"), 0.00m.ToString("C")));
			//Add(new GLItem("", "", AcntRecConTot.ToString("C"), AcntRecConTot.ToString("C")));
			Globals = new Global( $"{FromDate.Date:D}", $"{ToDate.Date:D}", AcntRecConTot.ToString( "C" ), Adjustments.ToString( CultureInfo.InvariantCulture ), Payments.ToString( CultureInfo.InvariantCulture ), TotalInvoices.ToString(), LastLine );
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
			//Add(new GLItem(Exception.Message, "", "", "")); //test line
		}
	}
}