﻿using Reports.Reports.Standard.CallTakerStatistics;
using Utils.Csv;

namespace Reports;

internal sealed class CallTakerStatistics : ReportsModule.Reports 
{
	protected override string TemplateName => @"Standard\CallTakerStatistics\CallTakerStatistics.html";
	public CallTakerStatistics( IRequestContext context, Report options ) : base( context, new CallTakerStatisticsDataSource( context, options ), options )
	{
	}

    public override string ToCsv()
    {
        var Csv = new Csv();
        if (Data.Source is { } Source)
        {
            var Globals = (CallTakerStatisticsDataSource.Global)Source.Globals;

            Csv[0][0].AsString = "Call Taker Statistics Report";
            Csv[1][0].AsString = "";

            Csv[2][0].AsString = "Dates Selected:";
            Csv[2][1].AsString = $"{Globals.FromDate} - {Globals.ToDate}";
            Csv[3][0].AsString = "Printed On:";
            Csv[3][1].AsString = $"{DateTime.Now:D}";

            //AsDecimal

            Csv[4][0].AsString = "";
            var RowHeaders = Csv[5];
            RowHeaders[0].AsString = "Operator";
            RowHeaders[1].AsString = "Total Jobs";
            RowHeaders[2].AsString = "Daily Avg";
            RowHeaders[3].AsString = "Before 9:00";
            RowHeaders[4].AsString = "9:00 - 10:00";
            RowHeaders[5].AsString = "10:00 - 11:00";
            RowHeaders[6].AsString = "11:00 - 12:00";
            RowHeaders[7].AsString = "12:00 - 1:00";
            RowHeaders[8].AsString = "1:00 - 2:00";
            RowHeaders[9].AsString = "2:00 - 3:00";
            RowHeaders[10].AsString = "3:00 - 4:00";
            RowHeaders[11].AsString = "After 4:00";


            var i = 6;
            foreach (var source in Source)
            {
                var row = Csv[i];
                var s = (CallTakerStatisticsDataSource.CallTakerStatisticsItem)source;
                row[0].AsString = s.Operator;
                row[1].AsString = s.TotalJobs;
                row[2].AsString = s.DailyAverage;
                row[3].AsString = s.Before_9;
                row[4].AsString = s._9_10;
                row[5].AsString = s._10_11;
                row[6].AsString = s._11_12;
                row[7].AsString = s._12_1;
                row[8].AsString = s._1_2;
                row[9].AsString = s._2_3;
                row[10].AsString = s._3_4;
                row[11].AsString = s.After_4;

                i++;
            }

        }

        return Csv.ToString();
    }
}