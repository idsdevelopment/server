﻿using Reports.Reports.Standard.DriverManifest;
using Utils.Csv;

namespace Reports;

internal sealed class DriverManifest : ReportsModule.Reports
{
	protected override string TemplateName => @"Standard\DriverManifest\DriverManifest.html"; 

	public DriverManifest(IRequestContext context, Report options) : base(context, new DriverManifestDataSource(context, options), options)
	{
	}

    public override string ToCsv()
    {
        var Csv = new Csv();
        if (Data.Source is { } Source)
        {
            var Globals = (DriverManifestDataSource.Global)Source.Globals;

            Csv[0][0].AsString = "Driver Manifest Report";
            Csv[1][0].AsString = "";

            Csv[2][0].AsString = "Dates Selected:";
            Csv[2][1].AsString = $"{Globals.FromDate} - {Globals.ToDate}";
            Csv[3][0].AsString = "Printed On:";
            Csv[3][1].AsString = $"{DateTime.Now:D}";

            var i = 4;
            foreach (var source in Source)
            {
                Csv[i][0].AsString = "";
                i++;

                var s = (DriverManifestDataSource.DriverManifestItem)source;

                // Driver Info
                Csv[i][0].AsString = s.Driver.Truck;
                Csv[i][1].AsString = $"{s.Driver.DriverFirstName} {s.Driver.DriverLastName}";
                i++;

                // Headers
                var RowHeaders = Csv[i];
                RowHeaders[0].AsString = "Shipment Id";
                RowHeaders[1].AsString = "DEL Time";
                RowHeaders[2].AsString = "POD Name";
                RowHeaders[3].AsString = "Serv";
                RowHeaders[4].AsString = "Wt";
                RowHeaders[5].AsString = "Shipper";
                RowHeaders[6].AsString = "Consignee";
                RowHeaders[7].AsString = "Com'able Amount";
                
                i++;

                // Data
                foreach (var da in s.Items)
                {
                    var row = Csv[i];
                    row[0].AsString = da.ShipmentId;
                    row[1].AsString = da.DELTime;
                    row[2].AsString = da.PODName;
                    row[3].AsString = $"{da.ServiceLevel}";
                    row[4].AsString = $"{da.Weight}";
                    row[5].AsString = $"{da.FromCompany}\n{da.FromAddress}, {da.FromCity}";
                    row[6].AsString = $"{da.ToCompany}\n{da.ToAddress}, {da.ToCity}";
                    row[7].AsString = da.Charges;

                    i++;
                }

                Csv[i][4].AsString = $"Totals for Driver {s.Driver.Truck}";
                Csv[i][5].AsString = "Total Commisionable Amt";
                Csv[i][6].AsString = s.Driver.TruckTotalCommisionable;
                i++;
                Csv[i][4].AsString = $"Total n. Shipments: {s.Driver.TotalShipments}";
                Csv[i][5].AsString = "Calculated Commission";
                Csv[i][6].AsString = s.Driver.TruckTotalCalculated;
                i++;
                Csv[i][4].AsString = "";
                Csv[i][5].AsString = "Commission Due";
                Csv[i][6].AsString = s.Driver.TruckTotalCommissionDue;
                i++;
            }

        }

        return Csv.ToString();
    }
}