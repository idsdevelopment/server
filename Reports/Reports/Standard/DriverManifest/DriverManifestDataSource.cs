﻿using Database.Model.Databases.Carrier;

// ReSharper disable InconsistentNaming

namespace Reports.Reports.Standard.DriverManifest; 

internal sealed class DriverManifestDataSource : DataSource
{
	public sealed record Global(string FromDate, string ToDate);

	public sealed record Driver( string DriverFirstName, string DriverLastName, string Truck, 
								string TruckTotalCommisionable, string TruckTotalCalculated, string TruckTotalCommissionDue, 
								string TotalShipments);   

	public sealed record DriverLineItem(string ShipmentId,
										 string DELTime = "", string PODName = "", string ServiceLevel = "", string Weight = "", // /Hr?
										 string FromCompany = "", string FromAddress = "", string FromCity = "",
										 string ToCompany = "", string ToAddress = "", string ToCity = "",
										 string Charges = ""); 

	public sealed record DriverManifestItem(List<DriverLineItem> Items, 
											Driver Driver);

	public DriverManifestDataSource( IRequestContext context, Report options )
	{
		try
		{
			var FromDate = options.DateTimeArg1.StartOfDay();
			var ToDate   = options.DateTimeArg2.EndOfDay();
			var SelectedTruck = options.StringArg1;

			Globals = new Global($"{FromDate.Date:D}", $"{ToDate.Date:D}");
			decimal TruckTotalCommisionable = 0m;
			decimal TruckTotalCalculated = 0m;
			decimal TruckTotalCommissionDue = 0m;

			using var Db = new CarrierDb( context );
			
			var Staff = Db.GetStaff();
			var StaffIds = ( from S in Staff
			                 select S.StaffId.ToUpper() ).ToList();

			var Recs = ( from T in Db.SearchTripsByDelByStatus( FromDate, ToDate, STATUS.VERIFIED, STATUS.FINALISED )
			             let UDriver = T.Driver.ToUpper()
			             where StaffIds.Contains( UDriver )
						 orderby UDriver, T.VerifiedTime
						 group T by UDriver
			             into Ct
			             let Driver = Ct.Key
			             select new
			                    {
				                    Driver,
				                    StaffRec = Staff.FirstOrDefault( s => s.StaffId.Compare( Driver, StringComparison.OrdinalIgnoreCase ) == 0 ),
				                    Trips    = Ct.ToList()
			                    } ).ToList();

			var dateCutoff = new DateTimeOffset( 2000, 1, 1, 1, 1, 1, new TimeSpan( 1, 0, 0 ) );

			//// it was a static, change to tally totals
			//string BuildCharges( List<TripCharge> charges )
			//{
			//	var Result = new StringBuilder("");
			//	//var Result = 0.00m;

			//	foreach( var C in charges )
			//		// This was just to see what is currently being stored in charges. 
			//		// The answer is not much. 
			//		Result.Append( $"{C.ChargeId} {C.Value} {C.Text}/ " );
			//	//Result += C.Value;

			//	// Not sure what the difference between Total Commisionable Amount, Calculated Commision and Commission Due is 
			//	//TruckTotalCommisionable = TruckTotalCalculated = TruckTotalCommissionDue += Result;
			//	TruckTotalCommisionable = TruckTotalCalculated = TruckTotalCommissionDue += 0;

			//	//return Result != 0.00m ? Result.ToString("0.00") : "";
			//	return Result.ToString();
			//}

			decimal SumCommCharges(decimal payroll)
			{
				TruckTotalCommisionable += payroll;
				return TruckTotalCommisionable;
			}
			decimal CalcTruckTotalCom(decimal TruckTotalServiceComPercentage)
			{
				TruckTotalCalculated = TruckTotalCommisionable * TruckTotalServiceComPercentage;
				TruckTotalCommissionDue = TruckTotalCalculated;
				return TruckTotalCalculated;
			}

			static string ToTimeString( DateTimeOffset? dateTime )
			{
				return dateTime is not null ? $"{dateTime:yyyy/MM/dd hh:mm tt}" : "N/A";
			}

            foreach (var Rec in Recs)
            {
                var TotalShipments = 0;
				TruckTotalCommisionable = 0m;
				TruckTotalCalculated = 0m;
				TruckTotalCommissionDue = 0m;

				if (SelectedTruck == "All" || Rec.Driver == SelectedTruck)
                {
					TotalShipments += Rec.Trips.Count;
					decimal TruckTotalServiceComPercentage = Rec.StaffRec.MainCommission1;

					Add(new DriverManifestItem((from T in Rec.Trips
                                                let Weight = $"{T.Weight:N0}"
                                                //let Charges = BuildCharges( T.TripCharges )
                                                let CommisionableRunningTotal = SumCommCharges(T.TotalPayrollAmount)
                                                select new DriverLineItem(
                                                                                           T.TripId,
                                                                                           ToTimeString(T.VerifiedTime),
                                                                                           T.POD,
                                                                                           T.ServiceLevel,
                                                                                           Weight,
                                                                                           T.PickupCompanyName,
                                                                                           T.PickupAddressAddressLine1,
                                                                                           T.PickupAddressCity,
                                                                                           T.DeliveryCompanyName,
                                                                                           T.DeliveryAddressAddressLine1,
                                                                                           T.DeliveryAddressCity,
                                                                                           T.TotalPayrollAmount.ToString("C")
                                                                                          //Charges
                                                                                          )
                                                                         ).ToList(),
                                                new Driver(Rec.StaffRec.FirstName, Rec.StaffRec.LastName, Rec.Driver,
                                                            TruckTotalCommisionable.ToString("C"),
															CalcTruckTotalCom(TruckTotalServiceComPercentage).ToString("C"),
															TruckTotalCommissionDue.ToString("C"),
                                                            TotalShipments.ToString())
                                           ));
                }
            }
        }


        catch ( Exception Exception )
		{
			context.SystemLogException( Exception );    
		}
	}
}