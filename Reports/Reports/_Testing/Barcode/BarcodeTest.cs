﻿using Reports._Testing.Barcode;

namespace Reports;

internal sealed class BarcodeTest : ReportsModule.Reports
{
	protected override string TemplateName => @"_Testing\Barcode\BarcodeTest.html";

	public BarcodeTest( IRequestContext context, Report options ) : base( context, new BarcodeTestDataSource( context, options ), options )
	{
	}
}