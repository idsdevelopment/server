﻿namespace Reports._Testing.Barcode;

internal sealed class BarcodeTestDataSource : DataSource
{
	public BarcodeTestDataSource( IRequestContext context, Report options )
	{
		Add("123456");
		Add("78901234567890");
	}
}