﻿using Database.Model.Databases.Carrier;
#if !NET7_0_OR_GREATER
using System.Data.Entity.SqlServer;
#endif


// ReSharper disable InconsistentNaming

namespace Reports.Portal.ManifestReport;

internal sealed class PortalManifestDataSource : DataSource
{
	public PortalManifestDataSource( IRequestContext context, Report options )
	{
		try
		{
			var FromDate = options.DateTimeArg1.StartOfDay();
			var ToDate   = options.DateTimeArg2.EndOfDay();
			//var SelectedTruck = options.StringArg1;

			//string AcountId = "GhostBustersInc";
			var AcountId = options.StringArg1;

			// Fetch Account Information
			using var Db        = new CarrierDb( context );
			var       Account   = Db.GetPrimaryCompanyByCustomerCode( AcountId );
			var       otherRecs = Db.GetTripsForAccount( AcountId );

			var AccountName          = Account.Company.CompanyName;
			var AccountAddress       = Account.Company.AddressLine1;
			var AccountCity          = Account.Company.City;
			var AccountStateProvince = Account.Company.Region;
			var AccountCountry       = Account.Company.Country;
			var AccountPostalZip     = Account.Company.PostalCode;
			var AccountPhone         = Account.Company.Phone;

			Globals = new Global( $"{FromDate.Date:D}", $"{ToDate.Date:D}", AcountId, AccountName,
			                      AccountAddress, AccountCity, AccountStateProvince, AccountCountry, AccountPostalZip, AccountPhone );

			var Recs = ( from T in Db.SearchTripsByDelByStatus( FromDate, ToDate, STATUS.VERIFIED, STATUS.FINALISED )
			             let Ref = T.Reference.ToUpper()
			             where T.AccountId.ToUpper() == AcountId.ToUpper()
			             orderby Ref, T.VerifiedTime
			             group T by Ref
			             into Ct
			             let Reference = Ct.Key
			             select new
			                    {
				                    Reference,
				                    Trips = Ct.ToList()
			                    } ).ToList();

			var dateCutoff = new DateTimeOffset( 2000, 1, 1, 1, 1, 1, new TimeSpan( 1, 0, 0 ) );

			var TotalWeight = 0.00m;
			var TotalPieces = 0.00m;

			decimal SumWeightandPieces( decimal w, decimal p )
			{
				TotalWeight += w;
				TotalPieces += p;
				return 0.00m;
			}

/*
			static string ToTimeString(DateTimeOffset? dateTime)
			{
				return dateTime is not null ? $"{dateTime:yyyy/MM/dd hh:mm tt}" : "N/A";
			}
*/
			foreach( var Rec in Recs )
			{
				var RefId = Rec.Reference;
				TotalWeight = 0.00m;
				TotalPieces = 0.00m;

				Add( new ReferenceItem( ( from T in Rec.Trips
				                          let tot = SumWeightandPieces( T.Weight, T.Pieces )
				                          select new ReferenceLineItem(
				                                                       T.TripId,
				                                                       T.DeliveryCompanyName,
				                                                       T.DeliveryAddressAddressLine1,
				                                                       T.DeliveryAddressCity,
				                                                       T.DeliveryAddressRegion,
				                                                       T.DeliveryAddressCountry,
				                                                       T.DeliveryAddressPostalCode,
				                                                       T.DeliveryNotes,
				                                                       T.ServiceLevel,
				                                                       $"{T.Pieces:N0}",
				                                                       $"{T.Weight:N0}",
				                                                       ""
				                                                      ) ).ToList(),
				                        new Reference( RefId,
				                                       $"{TotalPieces:N0}",
				                                       $"{TotalWeight:N0}" )
				                      ) );
			}
		}

		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
	}

	// Account Information
	public sealed record Global(
		string FromDate,
		string ToDate,
		string AccountId,
		string AccountName,
		string AccountAddress,
		string AccountCity,
		string AccountStateProvince,
		string AccountCountry,
		string AccountPostalZip,
		string AccountPhone );

	// Reference #, Total Pcs, Total Wgt
	public sealed record Reference( string ReferenceNumber, string TotalPieces, string TotalWeight );

	// Waybill #, Receiver Name, Receiver Address, SVC, Pcs, Wgt, Extra SVC
	public sealed record ReferenceLineItem(
		string ShipmentId,
		string ReceiverName,
		string ReceiverAddress,
		string ReceiverCity,
		string ReceiverStateProv,
		string ReceiverCountry,
		string ReceiverPostalZip,
		string ReceiverDeliveryNotes,
		string SVC,
		string Pieces,
		string Weight,
		string ExtraSVC );

	public sealed record ReferenceItem(
		List<ReferenceLineItem> Items,
		Reference Reference );
}