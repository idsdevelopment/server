﻿using Reports.Portal.ManifestReport;

namespace Reports;

internal sealed class PortalManifest : ReportsModule.Reports
{
	protected override string TemplateName => @"Portal\ManifestReport\PortalManifest.html"; 

	public PortalManifest(IRequestContext context, Report options) : base(context, new PortalManifestDataSource(context, options), options)
	{
	}

    
}